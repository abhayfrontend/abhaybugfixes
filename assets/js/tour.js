// Define the tour!

        var tour = {
            id: "demo-tour",
            showPrevButton: true,
            steps: [

                
                {
                    title: "Full Screen",
                    content: "View this page in full screen mode.",
                    target: "navbar-fullscreen",
                    placement: "left"
                },
                {
                    title: "dnky",
                    content: "Check this link to know more about dnky.",
                    target: "dnkyLink",
                    placement: "top"
                },
                {
                    title: "Customizer",
                    content: "This is the customizer for the theme where you can customize menu options.",
                    target: "customizer-toggle-icon",
                    placement: "left"
                }
            ]
        };

       

// Start the tour!
$('#btnStartTour').on('click', function (e) {
    hopscotch.startTour(tour);
});
