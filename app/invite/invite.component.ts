import { Component, OnInit } from '@angular/core';
import { PreviewService } from '../shared/auth/preview.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import * as AWS from '../shared/configs/aws.config';
import 'rxjs/add/operator/catch';
import { base } from '../shared/configs/util';
@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.scss']
})
export class InviteComponent implements OnInit {
  templates = [];
  id = 0;
  phone = 0;
  name = '';
  org = '';
  message;
  contacts = [];
  sent = 0;
  invitedBy;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };

  changeUrl() {
    this.route.params.subscribe(params => {
      // console.log(params);
      this.id = params.id;
      this.phone = params.phone;
      this.name = params.name;
      this.org = params.org;
    });
    // this.externalRoute.navigateByUrl('https://play.google.com/store/apps/details?id=com.sthaapak.yuva&referrer=' + this.id);
    // window.location.href = 'https://play.google.com/store/apps/details?id=' + this.id + '&referrer=' + this.referrer;
    window.close();
  }
  constructor(private route: ActivatedRoute, public previewService: PreviewService, private router: Router, private http: HttpClient,
    private roleGuardService: RoleGuardService) {

    this.route.params.subscribe(params => {
      // console.log(params);
      this.id = params.id;
      this.phone = params.phone;
      this.name = params.name;
      this.org = params.org;
      this.invitedBy = params.invt;
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + params.t
        })
      };
    });
  }

  ngOnInit() {
    this.templates = [{
      name: 'Registration - English', message: `Hello ` + this.name + `. You have been inivited to join Productivise by ` + this.invitedBy + `
   Link: http://productivise.io:3000/create/com.sthaapak.productivise/` + this.org + `/` + this.id
    }];
    this.message = `Hello ` + this.name + `. You have been inivited to Productivise by ` + this.invitedBy + `
   Link: http://productivise.io:3000/create/com.sthaapak.productivise/` + this.org + `/` + this.id;

    // console.log('Message Sent!!', this.message);
    const sns = AWS.sns;
    // if (this.org) {
    sns.publish({
      Message: this.message, PhoneNumber: '+91' + this.phone, MessageAttributes: {
        'AWS.SNS.SMS.SenderID': {
          DataType: 'String',
          StringValue: 'PRDCTVSE'
        },
        'AWS.SNS.SMS.MaxPrice': {
          DataType: 'Number',
          StringValue: '00.01'
        },
        'AWS.SNS.SMS.SMSType': {
          DataType: 'String',
          StringValue: 'Transactional'
        }
      }
    }).promise().then(res => {
      // console.log(res);
      this.sent = this.sent + 1;
      this.register();
    });
    // }

  }

  register() {
    let newUser = <any>{
      'login': this.phone,
      'firstName': this.name.split(' ')[0],
      'lastName': this.name.split(' ')[1] || null,
      'email': this.name.split(' ')[0] + this.phone + '@gmail.com',
      'imageUrl': null,
      'activated': false,
      'langKey': null,
      'createdBy': this.invitedBy,
      'createdDate': new Date().toISOString(),
      'lastModifiedBy': this.invitedBy,
      'lastModifiedDate': new Date().toISOString(),
      'authorities': [
        'ROLE_USER'
      ],
      'phone': this.phone,
      'userId': null,
      'userType': 0,
      'orgId': this.org,
      'deviceID': null,
      'geoHash': null,
      'location': null,
      'aadharNumber': null,
      'userStatus': null,
      'userPassword': 'qwerty'
    };
    // console.log('newUser:', newUser);
    this.http.post(base + '/api/users/saas/register', newUser, this.httpOptions).subscribe(resp => {
      // console.log('user:', resp);

      let result = <any>resp;
      if (result.status !== 200) {
        // console.log('responStatus:', result.status);
      }
      // console.log('memberData:', result);

      let memberData = {
        'name': result['firstName'] + (result['lastName'] || ''),
        'userId': result['id'],
        'learnerId': result['learner']['id'],
        'category': this.org,
        'subCategory': null,
        'locationName': result['location'],
        'gehash1': result['geoHash'],
        'memberType': 'APPLICANT',
        'aadhaar': result['phone'] || null,
        'mobileNo': result['phone'] || this.phone,
        'cardId': null
      };
      // console.log('memberData:', memberData);
      this.http.post(base + '/api/members', memberData, this.httpOptions).subscribe(response => {
        // console.log(response);
        let memberResponse = <any>response;
        // console.log('member:', memberResponse);
        newUser.id = result.id;
        newUser.learnerId = result['learner']['id'];
        newUser.langKey = memberResponse.id;
        newUser.userPassword = 'qwerty';

        this.http.put(base + '/api/users', newUser, this.httpOptions).subscribe(respo => {
          // console.log('user:', respo);
          this.changeUrl();
        });
      });
    }, error => {
      window.open('', '_self').close();
      if (error.status === 400) {
        // console.log('Wrong phone number', error);
      }
      else {
        // console.log('user exists', error);
      }

    }
    )


  }

}



