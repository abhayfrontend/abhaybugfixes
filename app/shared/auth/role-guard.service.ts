import { Injectable, OnInit } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { AuthenticationService } from './authentication-service.service';
import * as JWT from 'jwt-decode';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import { PreviewService } from './preview.service';
import 'rxjs/add/operator/map';
import { base } from '../../shared/configs/util';
import swal from 'sweetalert2';

@Injectable()
export class RoleGuardService implements CanActivate {
  organizationID;
  userType;
  token = localStorage.getItem('token') || null;
  tokenPayload: any;
  userID;
  httpOptions: any;
  authenticated;
  constructor(public auth: AuthenticationService, public router: Router, private http: HttpClient, private previewService: PreviewService) {
    this.token = localStorage.getItem('token') || null;
    // console.log('TOKEN:', this.token)
    if (!this.token) {
      this.router.navigateByUrl('/login');
    }
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
    this.userID = this.previewService.userIDCast.subscribe(userId => this.userID = userId);
    if (localStorage.getItem('token')) {

      this.authenticated = this.auth.isAuthenticated();

    }

  }


  setUser(e) {
  }

  validUserRole(roles, expectedRoles): boolean {

    for (let i = 0; i < roles.length; i++) {
      if (expectedRoles.indexOf(roles[i])) {
        return true;
      }
    }
    return false;
  }


  canActivate(route: ActivatedRouteSnapshot) {
    this.token = localStorage.getItem('token') || null;
    // console.log('TOKEN:', this.token)
    if (!this.token) {
      this.router.navigateByUrl('/login');
    }
    // this will be passed from the route config
    // on the data property
    if (!this.authenticated) {
      this.token = localStorage.getItem('token') || null;
      this.authenticated = this.auth.isAuthenticated();
      if (!this.authenticated) {
        this.router.navigate(['/login']);
      }
    }
    const expectedRole = route.data.expectedRole;
    const expectedUserType = route.data.expectedUserType;
    const token = localStorage.getItem('token');
    this.token = localStorage.getItem('token');
    // decode the token to get its payload
    const tokenPayload: any = JWT(token);
    this.userID = this.previewService.userIDCast.subscribe(userId => this.userID = userId);
    this.previewService.editUsername(tokenPayload.sub);
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    if (this.authenticated) {
      return this.http.get(base + '/api/users/' + tokenPayload.sub,
        this.httpOptions).map((res) => {
          let activate = false;
          this.userType = JSON.parse(JSON.stringify(res)).userType;
          this.organizationID = JSON.parse(JSON.stringify(res)).orgId;
          this.previewService.editUsername(JSON.parse(JSON.stringify(res)).firstName + ' ' + JSON.parse(JSON.stringify(res)).lastName || '');

          if (
            !this.authenticated
          ) {

            swal('Incorrect Credentials', '', 'error');
            //this.router.navigateByUrl('/login');

            activate = false;
          }
          else {
            if (Number(this.userType) !== 6 && Number(this.userType) !== 6) {

            }

            this.previewService.editUserID(this.userType);
            this.previewService.editOrganizationId(this.organizationID);
            if (expectedUserType && expectedUserType.indexOf(this.userType) < 0) {


              activate = false;
            }

            activate = true;
            if (Number(this.userType) !== 6 && Number(this.userType) !== 7) {
              // console.log('Goto: Login');
              swal('Incorrect Credentials', '', 'error');
              this.router.navigate(['/login']);
              activate = false;
            }

          }
          return activate;
        });
    }
    else {
      return false;
    }



    /*
       .subscribe(res => {
         let activate = false;
         this.userType = JSON.parse(JSON.stringify(res)).userType;
         this.organizationID = JSON.parse(JSON.stringify(res)).orgId;
         // console.log('userType in auth:', this.userType);
         if (
           !this.authenticated
         ) {
           // console.log('authenticated:', this.auth.isAuthenticated());
           // console.log('validuser: ', this.validUserRole(Array(tokenPayload.auth), Array(expectedRole)));
           // console.log('Goto: Login');
           this.router.navigateByUrl('/login');
           // console.log('went');
           activate = false;
         }
         else {
           // console.log('usertype:', expectedUserType);
           // console.log('thisusertype:', this.userType);
           this.previewService.editUserID(this.userType);
           this.previewService.editOrganizationId(this.organizationID);
           if (expectedUserType && expectedUserType.indexOf(this.userType) < 0) {
             // console.log('Goto: Login');
             this.router.navigate(['organization']);
             // console.log('went');
             activate = false;
           }
           // console.log('didn');
           activate = true;
         }
         return activate;
       });
 */

  }

}
