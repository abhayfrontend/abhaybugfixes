import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../../../models/user';
import { PreviewService } from './preview.service';
import { base } from '../../shared/configs/util';

import {
	Router,
	CanActivate,
	ActivatedRouteSnapshot,
	RouterStateSnapshot
} from '@angular/router';



@Injectable()
export class UserGuardService implements CanActivate {


	private BASE_URL: string = base + '/api';
	private headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
	constructor(private http: HttpClient, private previewService: PreviewService, private router: Router) { }

	canActivate(route: ActivatedRouteSnapshot) {
		for (let index = 0; index < route.data.expectedUserTypes.length; index++) {
			const element = route.data.expectedUserTypes[index];
			if (element === this.previewService.userID.getValue()) {
				return true;
			}
		}
		this.router.navigate(['/dashboard']);
	}
}
