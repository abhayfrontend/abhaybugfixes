import { Injectable, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class PreviewService {



  private contentButtonText = new BehaviorSubject<string>('');
  contentButtonTextCast = this.contentButtonText.asObservable();


  public titles = new BehaviorSubject<Array<string>>([]);
  titlesCast = this.titles.asObservable();

  public thumbnailImage = new BehaviorSubject<Array<string>>([]);
  thumbnailImageCast = this.thumbnailImage.asObservable();

  public questionImage = new BehaviorSubject<Array<string>>([]);
  questionImageCast = this.questionImage.asObservable();

  private title = new BehaviorSubject<string>('');
  titleCast = this.title.asObservable();

  private source = new BehaviorSubject<string>('./splash-screen.png');
  sourceCast = this.source.asObservable();

  private description = new BehaviorSubject<string>('');
  descriptionCast = this.description.asObservable();

  private buttonText = new BehaviorSubject<string>('');
  buttonTextCast = this.buttonText.asObservable();

  private contentSource = new BehaviorSubject<Array<string>>([]);
  contentSourceCast = this.contentSource.asObservable();

  private contentTitle = new BehaviorSubject<string>('Start');
  contentTitleCast = this.contentTitle.asObservable();

  private contentDescription = new BehaviorSubject<Array<string>>([]);
  contentDescriptionCast = this.contentDescription.asObservable();

  private questions = new BehaviorSubject<Array<object>>([]);
  questionsCast = this.questions.asObservable();

  private options = new BehaviorSubject<Array<string>>([]);
  optionsCast = this.options.asObservable();

  private microContentType = new BehaviorSubject<Array<string>>([]);
  microContentTypeCast = this.microContentType.asObservable();

  public userID = new BehaviorSubject<string>('');
  userIDCast = this.userID.asObservable();

  public organizationId = new BehaviorSubject<string>('');
  organizationIdCast = this.organizationId.asObservable();

  private learnerList = new BehaviorSubject<Array<string>>(['']);
  learnerListCast = this.learnerList.asObservable();

  public contentToSchedule = new BehaviorSubject<Object>({});
  public contentToEdit = new BehaviorSubject<Object>({});
  contentToEditCast = this.contentToEdit.asObservable();
  public Username = new BehaviorSubject<String>('');
  public Organization = new BehaviorSubject<Object>({});
  public Config = new BehaviorSubject<Object>({});
  public SmsList = new BehaviorSubject<Array<Array<string>>>([['']]);
  public groupToEdit = new BehaviorSubject<Object>({});
  public campaignGroups = new BehaviorSubject<Array<Object>>([]);
  public supportGroups = new BehaviorSubject<Array<Object>>([]);
  public statsOf = new BehaviorSubject<Object>({});
  public usertoEdit = new BehaviorSubject<Object>({});
  public newsTimeSpent = new BehaviorSubject<Array<String>>([]);
  public newsTimeSpentBy = new BehaviorSubject<Array<String>>([]);

  editNewsTimeSpentBy(obj) {
    this.newsTimeSpentBy = obj;
  }

  editNewsTimeSpent(obj) {
    this.newsTimeSpent = obj;
  }

  editUser(row) {
    this.usertoEdit.next(row);
  }

  editCampaignGroup(obj) {

    this.campaignGroups.next(obj);

  }

  editSupportGroup(obj) {

    this.supportGroups.next(obj);

  }

  editGroup(obj) {

    this.groupToEdit.next(obj);
  }
  editSmsList(obj) {
    this.SmsList.next(obj);

  }

  editConfig(obj) {
    this.Config.next(obj);

  }

  editOrganization(obj) {
    this.Organization.next(obj);

  }

  editStatsOf(e) {
    this.statsOf.next(e);

  }

  editUsername(obj) {
    let learnerName = obj.split(' ')[0];
    let learnerLastName = obj.split(' ')[1] === 'null' ? '' : obj.split(' ')[1];
    this.Username.next(learnerName + ' ' + learnerLastName);
    // console.log('name:', this.Username.getValue())
  }

  editContentToEdit(obj) {
    this.contentToEdit.next(obj);

  }




  editContentToSchedule(obj) {
    this.contentToSchedule.next(obj);

  }

  editlearnerList(learnerList) {
    this.learnerList.next(learnerList);

  }

  editOrganizationId(organizationID) {
    this.organizationId.next(organizationID);

  }


  editUserID(userId) {
    this.userID.next(userId);

  }

  editTitle(newTitle) {
    this.title.next(newTitle);
  }
  editSource(source) {
    if (source === 1) {
      this.source.next('splash-screen.png');
    }
    if (source === 2) {
      this.source.next('theme-1.png');
    }
    if (source === 3) {
      this.source.next('theme-2.png');
    }
    if (source === 4) {
      this.source.next('theme-3.png');
    }
  }

  editDescription(newTitle) {
    this.description.next(newTitle);
  }

  editButtonText(newTitle) {
    this.buttonText.next(newTitle);
  }

  editContentSourceText(newSource) {
    let array = this.contentSource.getValue();
    array.push(newSource);
    this.contentSource.next(array);
  }

  editMicroContentType(newContent) {
    let array = this.microContentType.getValue();
    array.push(newContent);
    this.microContentType.next(array);
  }

  editThumbnailImage(newContent) {
    let array = this.thumbnailImage.getValue();
    array.push(newContent);
    this.thumbnailImage.next(array);
  }

  editQuestionImage(newContent) {
    // console.log(this.questionImage.getValue());
    let array = this.questionImage.getValue();
    array.push(newContent);
    this.questionImage.next(array);
    // console.log(this.questionImage.getValue());
  }


  editContentTitleText(newTitle) {
    this.contentTitle.next(newTitle);
  }

  editContentDescriptionText(newDescription) {
    let array = this.contentDescription.getValue();
    array.push(newDescription);
    this.contentDescription.next(array);
    // console.log(this.contentDescription.getValue());
  }

  editContentButtonText(newButtonText) {
    this.contentButtonText.next(newButtonText);
  }

  editTitles(newTitle) {
    let array = this.titles.getValue();
    array.push(newTitle);
    this.titles.next(array);
    // console.log(this.titles.getValue());
  }

  editQuestions(newQuestion) {
    let array = this.questions.getValue();
    array.push(newQuestion);
    this.questions.next(array);
    // console.log(this.questions.getValue());
  }

  editOptions(newOption) {
    let array = this.options.getValue();
    array.push(...newOption);
    this.options.next(array);
    // console.log('Options in service:', this.options.getValue());
  }



  resetAll() {
    this.contentButtonText = new BehaviorSubject<string>('');
    this.contentButtonTextCast = this.contentButtonText.asObservable();

    this.titles = new BehaviorSubject<Array<string>>([]);
    this.titlesCast = this.titles.asObservable();

    this.title = new BehaviorSubject<string>('');
    this.titleCast = this.title.asObservable();

    this.description = new BehaviorSubject<string>('');
    this.descriptionCast = this.description.asObservable();

    this.buttonText = new BehaviorSubject<string>('');
    this.buttonTextCast = this.buttonText.asObservable();

    this.contentSource = new BehaviorSubject<Array<string>>([]);
    this.contentSourceCast = this.contentSource.asObservable();

    this.contentTitle = new BehaviorSubject<string>('Start');
    this.contentTitleCast = this.contentTitle.asObservable();

    this.contentDescription = new BehaviorSubject<Array<string>>([]);
    this.contentDescriptionCast = this.contentDescription.asObservable();

    this.microContentType = new BehaviorSubject<Array<string>>([]);
    this.microContentTypeCast = this.microContentType.asObservable();

    this.options = new BehaviorSubject<Array<string>>([]);
    this.optionsCast = this.options.asObservable();

    this.questions = new BehaviorSubject<Array<object>>([]);
    this.questionsCast = this.questions.asObservable();

    this.contentDescription = new BehaviorSubject<Array<string>>([]);
    this.contentDescriptionCast = this.contentDescription.asObservable();

    this.thumbnailImage = new BehaviorSubject<Array<string>>([]);
    this.thumbnailImageCast = this.thumbnailImage.asObservable();

  }
  deleteTitle(e: number) {
    this.titles.getValue().splice(e - 1, 1);
    // console.log('deleted:', this.titles.getValue());
  }
  constructor() {


  }

}
