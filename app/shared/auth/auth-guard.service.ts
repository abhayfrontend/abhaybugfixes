// src/app/auth/auth-guard.service.ts
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthenticationService } from './authentication-service.service';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { base } from '../configs/util';
@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthenticationService, public router: Router, private http: HttpClient) { }
  userID;
  token = localStorage.getItem('token') || null;
  userType;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.token
    })
  };
  canActivate() {
    return Promise.resolve(this.http.get(base + '/api/users/' + this.userID, this.httpOptions).subscribe(res => {
      this.userType = JSON.parse(JSON.stringify(res)).userType;

    })).then(res => true);
  }

}
