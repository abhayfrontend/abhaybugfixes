import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from './role-guard.service';
import { Observable } from 'rxjs';
@Injectable()
export class AuthenticationService {
  constructor(public jwtHelper: JwtHelperService, private http: HttpClient) {

  }
  userType;
  userID;
  authenticated = false;
  // ...
  public isAuthenticated() {

    const token = localStorage.getItem('token');
    let authenticated;
    // Check whether the token is expired and return
    // true or false
    if (!this.jwtHelper.isTokenExpired(token)) {
      this.authenticated = true;
    }
    authenticated = !this.jwtHelper.isTokenExpired(token);
    return authenticated;
  }
}
