import { Routes, RouterModule } from '@angular/router';
import { UserGuardService } from '../auth/userguard.service';

//Route for content layout with sidebar, navbar and footer.


export const Full_ROUTES: Routes = [
  /**
   * {
    path: 'charts',
    loadChildren: './charts/charts.module#ChartsNg2Module'
  },
   {
    path: 'forms',
    loadChildren: './forms/forms.module#FormModule'
  },
  {
    path: 'maps',
    loadChildren: './maps/maps.module#MapsModule'
  },
  {
    path: 'tables',
    loadChildren: './tables/tables.module#TablesModule'
  },
  {
    path: 'datatables',
    loadChildren: './data-tables/data-tables.module#DataTablesModule'
  },
  {
    path: 'uikit',
    loadChildren: './ui-kit/ui-kit.module#UIKitModule'
  },
  {
    path: 'components',
    loadChildren: './components/ui-components.module#UIComponentsModule'
  },
  {
    path: 'pages',
    loadChildren: './pages/full-pages/full-pages.module#FullPagesModule'
  },
  {
    path: 'cards',
    loadChildren: './cards/cards.module#CardsModule'
  },
  {
    path: 'chat',
    loadChildren: './chat/chat.module#ChatModule'
  },
  {
    path: 'chat-ngrx',
    loadChildren: './chat-ngrx/chat-ngrx.module#ChatNGRXModule'
  },
  {
    path: 'inbox',
    loadChildren: './inbox/inbox.module#InboxModule'
  },
  {
    path: 'taskboard',
    loadChildren: './taskboard/taskboard.module#TaskboardModule'
  },
  {
    path: 'taskboard-ngrx',
    loadChildren: './taskboard-ngrx/taskboard-ngrx.module#TaskboardNGRXModule'
  },
  {
    path: 'player',
    loadChildren: './player/player.module#PlayerModule'
  },
  {
    path: 'calendar',
    loadChildren: './calendar/calendar.module#CalendarsModule'
  },
   */
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },


  {
    path: 'colorpalettes',
    loadChildren: './color-palette/color-palette.module#ColorPaletteModule'
  },

  {
    path: 'messaging',
    loadChildren: './messaging/messaging.module#MessagingModule'
  }



  // Code  Added by Dnky on Thu Aug 23 05:11:46 IST 2018
  , {
    path: 'user',
    loadChildren: './user/user.module#UserModule'
  }



  // Code  Added by Dnky on Wed Sep 05 11:55:56 IST 2018
  , {
    path: 'messaging',
    loadChildren: './messaging/messaging.module#MessagingModule',

  }


  // Code  Added by Dnky on Wed Sep 05 12:49:15 IST 2018
  /*,

  {
    path: 'user',
    loadChildren: './user/user.module#UserModule'
  }
*/
  // Code  Added by Dnky on Wed Sep 05 12:49:29 IST 2018
  , {
    path: 'messaging',
    loadChildren: './messaging/messaging.module#MessagingModule'
  }

  // Code  Added by Dnky on Wed Sep 05 12:51:32 IST 2018
  , {
    path: 'poweruser',
    loadChildren: './poweruser/poweruser.module#PoweruserModule',
    data: {
      expectedUserTypes: [1, 6]
    },
    canActivate: [UserGuardService]
  }

  // Code  Added by Dnky on Wed Sep 05 12:54:38 IST 2018
  , {
    path: 'organization',
    loadChildren: './organization/organization.module#OrganizationModule'
  }

  // Code  Added by Dnky on Wed Sep 05 14:18:19 IST 2018
  , {
    path: 'config',
    loadChildren: './config/config.module#ConfigModule'
  }

  // code for loading content module
  , {
    path: 'content',
    loadChildren: './content/content.module#ContentModule'
  }

  // code for loading grouping module
  , {
    path: 'grouping',
    loadChildren: './grouping/grouping.module#GroupingModule'
  }

  // code for loading scheduler module
  , {
    path: 'scheduler',
    loadChildren: './scheduler/scheduler.module#SchedulerModule'
  },
  {
    path: 'notification',
    loadChildren: './notification/notification.module#NotificationModule'
  },
  {
    path: 'industry',
    loadChildren: './industry/industry.module#IndustryModule'
  },
  {
    path: 'department',
    loadChildren: './jobFamily/jobFamily.module#JobFamilyModule'
  },
  {
    path: 'jobrole',
    loadChildren: './jobRole/jobRole.module#JobRoleModule'
  }, {
    path: 'analytics',
    loadChildren: './analytics/analytics.module#AnalyticsModule'
  }
  //dnkyReplace
];


