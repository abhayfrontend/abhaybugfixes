import { RouteInfo } from './sidebar.metadata';

//Sidebar menu Routes and data
export const ROUTES: RouteInfo[] = [

    {
        path: '/dashboard/dashboard1', title: 'Dashboard', icon: 'ft-home', class: '', badge: '', badgeClass: '',
        isExternalLink: false, expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: [

            //  { path: '/dashboard/dashboard2', title: 'Dashboard2', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
        ]
    },
    {
        path: '', title: 'App Config', icon: 'ft-settings', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false, expectedUserTypes: [1, 2, 3, 4, 5, 6, 7],
        submenu: [
            {
                path: '/config/add', title: 'Add App Config', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/config/list', title: 'List App Configs ', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            }
        ]
    }
    , {
        path: '', title: 'Power User  ', icon: 'ft-user-check', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false, expectedUserTypes: [1, 2, 3, 4, 5, 6, 7],
        submenu: [
            {
                path: '/poweruser/add', title: 'Add Power User  ', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/poweruser/list', title: 'List Power Users ', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            }
        ]
    }
    , {
        path: '', title: 'User', icon: 'ft-user', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false, expectedUserTypes: [1, 2, 3, 4, 5, 6, 7],
        submenu: [
            {
                path: '/user/add', title: 'Add User', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/user/list', title: 'List Users ', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/user/bulk', title: 'Bulk Register Users ', icon: 'ft-users', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            }
        ]
    },
    // path for grouping module
    {
        path: '', title: 'Grouping', icon: 'ft-users', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false, expectedUserTypes: [1, 2, 3, 4, 5, 6, 7],
        submenu: [
            {
                path: '/grouping/groups', title: 'Groups', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/grouping/createGroup', title: 'Create group', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/grouping/filteredGroups', title: 'Filtered groups', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            }
        ]
    },

    {
        path: '', title: 'Notification', icon: 'ft-bell', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false, expectedUserTypes: [1, 2, 3, 4, 5, 6, 7],
        submenu: [
            {
                path: '/notification/add', title: 'Create Notification', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/notification/list', title: 'List Notifications ', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            }
        ]

    },


    // path for content module
    {
        path: '', title: 'Content', icon: 'ft-book', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false, expectedUserTypes: [1, 2, 3, 4, 5, 6, 7],
        submenu: [
            {
                path: '/content/create', title: 'Create Content', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/content/list', title: 'Content List ', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/content/microList', title: 'Micro Content List', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
           /* {
                path: '/content/sopList', title: 'SOP Content List', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
          */  {
                path: '/content/infoCardList', title: 'Info Cards List', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/content/feedbackformList', title: 'Feedback Form List', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            }
        ]
    },


    // path for scheduler module
    /*{
        path: '', title: 'Scheduler', icon: 'ft-clock', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false, expectedUserTypes: [1, 2, 3, 4, 5, 6, 7],
        submenu: [
            {
                path: '/scheduler/scheduler', title: 'Scheduler', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
        ]
    }
    // Code  Added by Dnky on Wed Sep 05 12:54:38 IST 2018
    , */
    {
        path: '', title: 'Organization', icon: 'ft-globe', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false, expectedUserTypes: [7],
        submenu: [
            {
                path: '/organization/add', title: 'Add Organization', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [7], submenu: []
            },
            {
                path: '/organization/list', title: 'List Organizations ', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [7], submenu: []
            }
        ]
    },

    // Code  Added by Dnky on Wed Sep 05 12:49:15 IST 2018

    // Code  Added by Dnky on Wed Sep 05 12:49:29 IST 2018
    , {
        path: '', title: 'Internal Messaging ', icon: 'ft-message-circle', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false, expectedUserTypes: [7],
        submenu: [
            {
                path: '/messaging/add', title: 'Add Internal Messaging ', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/messaging/list', title: 'List Internal Messaging s ', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            }
        ]
    },

    {
        path: '', title: 'Industries', icon: 'ft-message-circle', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false, expectedUserTypes: [1, 2, 3, 4, 5, 6],
        submenu: [
            {
                path: '/industry/add', title: 'Add Industry', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/industry/list', title: 'List of Industries', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/industry/bulk', title: 'Bulk Upload Industies', icon: 'ft-users', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            }
        ]
    },
    {
        path: '', title: 'Departments', icon: 'ft-message-circle', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false, expectedUserTypes: [1, 2, 3, 4, 5, 6],
        submenu: [
            {
                path: '/department/add', title: 'Add Department', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/department/list', title: 'List of Departments', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/department/bulk', title: 'Bulk Upload Departments', icon: 'ft-users', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            }
        ]
    },
    {
        path: '', title: 'Job Roles', icon: 'ft-message-circle', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false, expectedUserTypes: [1, 2, 3, 4, 5, 6, 7],
        submenu: [
            {
                path: '/jobrole/add', title: 'Add Job Role', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/jobrole/list', title: 'List of Job Roles', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/jobrole/bulk', title: 'Bulk Upload Job Roles', icon: 'ft-users', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            }
        ]
    },
    {
        path: '', title: 'Analytics', icon: 'ft-message-circle', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false, expectedUserTypes: [1, 2, 3, 4, 5, 6, 7],
        submenu: [
            {
                path: '/analytics/time', title: 'Time Usage', icon: 'ft-plus', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            },
            {
                path: '/analytics/events', title: 'Events Usage', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false,
                expectedUserTypes: [1, 2, 3, 4, 5, 6, 7], submenu: []
            }
        ]
    }

    // Code  Added by Dnky on Wed Sep 05 12:51:32 IST 2018


    // Code  Added by Dnky on Wed Sep 05 14:18:19 IST 2018


    //dnkyReplace

    /*
    */
];
