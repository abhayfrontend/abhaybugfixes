import { Component, OnInit } from '@angular/core';
import { ROUTES } from './sidebar-routes.config';
import { RouteInfo } from "./sidebar.metadata";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { PreviewService } from '../auth/preview.service';

declare var $: any;

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];

    constructor(private router: Router, private previewService: PreviewService,
        private route: ActivatedRoute, public translate: TranslateService) {

    }

    ngOnInit() {

        $.getScript('./assets/js/app-sidebar.js');
        this.menuItems = ROUTES.filter(menuItem => {
            for (let index = 0; index < menuItem.expectedUserTypes.length; index++) {
                const element = menuItem.expectedUserTypes[index];
                if (Number(this.previewService.userID.getValue()) === element) {
                    return menuItem;
                }

            }
        });

    }
    //NGX Wizard - skip url change
    ngxWizardFunction(path: string) {
        if (path.indexOf('forms/ngx') !== -1)
            this.router.navigate(['forms/ngx/wizard'], { skipLocationChange: false });
    }
}
