import { Component, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable/release';
import { PublishedContentResourceService, MicroContentResourceService } from '../../sthaapak';
import { PreviewService } from '../../shared/auth/preview.service';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import swal from 'sweetalert2';
import { base } from '../../shared/configs/util';
import { SurveyResourceService } from '../../sthaapak/sdk/surveyResource.service';
import { SurveyQuestionResourceService } from '../../sthaapak/sdk/surveyQuestionResource.service';


@Component({
    selector: 'app-dt-filter',
    templateUrl: './feedback-content-list.component.html',
    styleUrls: ['./feedback-content-list.component.scss']
})

export class FeedbackContentListComponent implements OnInit {
    rows = [];
    temp = [];
    temp2 = [];
    status = ['DRAFT', 'PUBLISHED', 'UNDER REVIEW', 'EXPIRED', 'APPROVED', 'REJECTED'];
    types = ['BLOG', 'VIDEO'];
    coursePlan = '266702';
    filterOption;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.roleGuardService.token
        })
    };
    organizationId;
    // Table Column Titles
    columns = [
        { prop: 'name' },
        { prop: 'category' },
        { prop: 'region' },
        { prop: 'description' },
        { prop: 'emailSubmit' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private contentService: PublishedContentResourceService, private previewService: PreviewService, private http: HttpClient, private router: Router,
        private surveyGroupResource: SurveyResourceService, private surveyQuestion: SurveyQuestionResourceService, private roleGuardService: RoleGuardService) {
        if (!localStorage.getItem('token')) {
            this.router.navigateByUrl('/login');
        }
    }
    ngOnInit() {

        this.organizationId = this.previewService.organizationId.getValue();
        this.getContentList();
    }

    getContentList() {
        this.temp = [];
        this.rows = [];
        this.temp2 = [];
        let cards = [];

        this.http.get(base + '/api/surveys', this.httpOptions).subscribe(res => {

            let result = <any>res;
            // console.log('resil:', result);
            // console.log('cards', cards);
            result = result.filter(element => {
                if (element.organizationId === this.organizationId) {
                    element.emailSubmit = element.emailSubmit ? element.emailSubmit.replace(/,/g, '\n') : '';
                    return true;
                }
                return false;
            });
            this.temp = [...result];
            this.rows = this.temp;
            //this.temp = [...this.temp, ...this.temp2];
            // console.log('temp:', this.temp);
        });
    }

    changeFilter(e) {
        // console.log('filter:', this.temp);
        this.filterOption = e.target.value;
        this.rows = this.temp;
    }

    reset() {
        this.filterOption = '';
        this.rows = this.temp;
    }

    filterListByType(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            if (val === 'VIDEO' && d.videoLink) {
                return true;

            }
            if (val === 'BLOG' && d.previewImage) {
                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByAuthor(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            return d.author === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByStatus(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log(d.state);
            // console.log(val);
            return d.state === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPostDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log('date:', d.postDate)
            if (new Date(d.postDate).getDate() === new Date(val).getDate() && new Date(d.postDate).getMonth() === new Date(val).getMonth() && new Date(d.postDate).getFullYear() === new Date(val).getFullYear()) {


                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPublDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {

            if (new Date(d.publishedDate).getDate() === new Date(val).getDate() && new Date(d.publishedDate).getMonth() === new Date(val).getMonth() && new Date(d.publishedDate).getFullYear() === new Date(val).getFullYear()) {

                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }


    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    scheduleContent(obj) {

        swal({
            title: 'Are you sure you want to schedul this content?',
            text: "This will take you to the Content Scheduler!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, schedule it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.previewService.editContentToSchedule(obj);
                this.router.navigate(['/scheduler/scheduler']);
            }
            else {
                swal(
                    'Cancelled',
                    'Your content could not be scheduled.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your content could not be scheduled.',
                    'error'
                )
            }
        })

    }

    deleteContent(row) {

        swal({
            title: 'Are you sure you want to delete this Feedback Module?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.surveyGroupResource.deleteSurveyUsingDELETE(row.id).subscribe(res => {
                    // console.log('res:', res);
                    swal(
                        'SuccessFully Deleted Feedback Module',
                        'Your content was deleted.',
                        'success'
                    ).then(a =>
                        // console.log(a)
                        console.log("")
                    );
                    this.getContentList();
                });

            }
            else {
                swal(
                    'Cancelled',
                    'Your Feedback Module was not deleted.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your Feedback Module was not deleted.',
                    'error'
                )
            }
        })


    }

    editContent(obj) {
        swal({
            title: 'Are you sure you want to edit this Feedback Module?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {

            // console.log('con:', confirm)
            if (confirm.value === true) {
                obj.contentType = 'Survey';
                obj.questionSlides = [];
                this.surveyQuestion.getAllSurveyQuestionsUsingGET().subscribe(optionResponse => {
                    let options = <any>optionResponse;
                    // console.log('opy:', options);
                    options.forEach((e, i) => {
                        let incorrect;
                        if (Number(e.surveyId) === Number(obj.id)) {
                            let correct = e.defaultData ? e.defaultData.split(',')[0] : '';
                            if (e.defaultData) {
                                let a = e.defaultData.split(',');
                                incorrect = a.splice(1, a.length - 1);
                            }

                            if (incorrect) {
                                incorrect = incorrect.map(el => { return { 'value': el } });
                            }
                            obj.questionSlides.push({
                                title: e.name,
                                randomize: e.randomize,
                                mandatory: e.mandatory,
                                anonymous: e.anonymous,
                                correctAnswer: correct,
                                incorrectAnswers: incorrect,
                                imageUrl: e.imageUrl,
                                timer: {
                                    timeLimit: e.timeLimit,
                                    outOfTimePrompt: e.outOfTimeMessage
                                },
                                stars: e.stars,
                                id: e.id,
                                displayOrder: e.displayOrder,
                                surveyId: e.surveyId,
                                qType: e.type
                            });
                            // console.log('oobj', obj);

                        }
                    });
                    // console.log('Final object:', obj);
                    this.previewService.editContentToEdit(obj);
                    this.router.navigate(['/content/feedbackformEdit']);

                }, err => {
                    swal(
                        'Cancelled',
                        'Your Feedback Module could not be edited.',
                        'error'
                    )
                });
            }
            else {
                swal(
                    'Cancelled',
                    'Your Feedback Module could not be edited.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your Feedback Module could not be edited.',
                    'error'
                )
            }
        })

    }
    getCellClass({ row, column, value }): any {
        return {
            'is-unReview': value === 'Under Review',
            'is-published': value === 'Published',
            'is-approved': value === 'Approved',
            'is-expired': value === 'Expiry',
            'is-rejected': value === 'Rejected',
        };
    }

    disableContent(row) {
        row.enabled = false;
        this.surveyGroupResource.updateSurveyUsingPUT(row).toPromise().then(res => {
            // console.log(res);
            this.getContentList();
        })
    }
    enableContent(row) {
        row.enabled = true;
        this.surveyGroupResource.updateSurveyUsingPUT(row).toPromise().then(res => {
            // console.log(res);
            this.getContentList();
        })
    }
}
