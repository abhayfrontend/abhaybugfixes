import { Component, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable/release';
import { PublishedContentResourceService, MicroContentResourceService, IndustryResourceService } from '../../sthaapak';
import { PreviewService } from '../../shared/auth/preview.service';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import swal from 'sweetalert2';
import { base, proxyApi } from '../../shared/configs/util';


@Component({
    selector: 'app-dt-filter',
    templateUrl: './industry-content-list.component.html',
    styleUrls: ['./industry-content-list.component.scss']
})

export class IndustryContentListComponent implements OnInit {
    rows = [];
    temp = [];
    temp2 = [];
    status = ['DRAFT', 'PUBLISHED', 'UNDER REVIEW', 'EXPIRED', 'APPROVED', 'REJECTED'];
    types = ['BLOG', 'VIDEO'];
    coursePlan = '266702';
    filterOption;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.roleGuardService.token
        })
    };
    organizationId;
    // Table Column Titles
    columns = [
        { prop: 'name' },
        { prop: 'content' },
        { prop: 'imageLink' },
        { prop: 'tags' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private contentService: PublishedContentResourceService, private previewService: PreviewService, private router: Router,
        private industry: IndustryResourceService, private http: HttpClient, private roleGuardService: RoleGuardService) {
        if (!localStorage.getItem('token')) {
            this.router.navigateByUrl('/login');
        }
    }
    ngOnInit() {
        this.organizationId = this.previewService.organizationId.getValue();
        this.getContentList();

    }

    getContentList() {
        this.temp = [];
        this.rows = [];
        this.temp2 = [];
        let cards = [];

        this.industry.getAllIndustriesUsingGET().subscribe(res => {

            let result = <any>res;
            // console.log('resil:', result);
            // console.log('cards', cards);
            result = result.filter(element => {

                if (element.organizations.length > 0 && element.organizations[0].id === this.organizationId) {
                    return true;
                }


                return false;
            });
            this.temp = [...result];
            this.rows = this.temp;
            //this.temp = [...this.temp, ...this.temp2];


            // console.log('temp:', this.temp);
        });
    }

    changeFilter(e) {
        // console.log('filter:', this.temp);
        this.filterOption = e.target.value;
        this.rows = this.temp;
    }

    filterListByType(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            if (val === 'VIDEO' && d.videoLink) {
                return true;

            }
            if (val === 'BLOG' && d.previewImage) {
                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByAuthor(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            return d.author === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByStatus(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log(d.state);
            // console.log(val);
            return d.state === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPostDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log('date:', d.postDate)
            if (new Date(d.postDate).getDate() === new Date(val).getDate() && new Date(d.postDate).getMonth() === new Date(val).getMonth() && new Date(d.postDate).getFullYear() === new Date(val).getFullYear()) {


                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPublDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {

            if (new Date(d.publishedDate).getDate() === new Date(val).getDate() && new Date(d.publishedDate).getMonth() === new Date(val).getMonth() && new Date(d.publishedDate).getFullYear() === new Date(val).getFullYear()) {

                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }


    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    reset() {
        this.filterOption = '';
        this.rows = this.temp;
    }
    scheduleContent(obj) {

        swal({
            title: 'Are you sure you want to schedule this content?',
            text: "This will take you to the Content Scheduler!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, schedule it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.previewService.editContentToSchedule(obj);
                this.router.navigate(['/scheduler/scheduler']);
            }
            else {
                swal(
                    'Cancelled',
                    'Your content was not scheduled.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your content was not scheduled.',
                    'error'
                )
            }
        })

    }

    sendUpdate(category, subcategory, screen) {

        let message = {
            topic: this.previewService.organizationId.getValue(),
            data: {
                silent: true,
                save: false,
                title: category,
                body: subcategory,
                screenType: screen,
                image: 'jpg',
                specialParameters: 'll',
                tags: 'Silent',
                orgId: this.previewService.organizationId.getValue(),
                name: screen
            }
        }
        // console.log('message:', message);
        this.http.post(proxyApi + '/contentUpdate', message, this.httpOptions).subscribe((response) => {
            // Response is a message ID string.
            // console.log('Successfully sent message:', response);

        }, (error) => {
            // console.log('Error sending message:', error);
        });
    }

    deleteContent(row) {
        if (Number(this.previewService.organizationId.getValue()) === 261351) {
            this.coursePlan = '211544';
        }
        swal({
            title: 'Are you sure you want to delete this Info Card?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.industry.deleteIndustryUsingDELETE(row.id).subscribe(res => {
                    this.sendUpdate(row.value, row.path, 'Industry');
                    swal(
                        'Deleted',
                        'Your Info Card was deleted successfully.',
                        'success'
                    ).then(res => {
                        this.getContentList();
                    })
                })
            }
            else {
                swal(
                    'Cancelled',
                    'Your Info Card was not deleted.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your Info Card was not deleted.',
                    'error'
                )
            }
        })
    }

    editContent(obj) {
        if (Number(this.previewService.organizationId.getValue()) === 261351) {
            this.coursePlan = '211544';
        }
        swal({
            title: 'Are you sure you want to edit this Info Card?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.previewService.editContentToEdit(obj);
                this.router.navigate(['/content/InfoCardEdit'])
            }
            else {
                swal(
                    'Cancelled',
                    'Your Info Card was not edited.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your Info Card was not edited.',
                    'error'
                )
            }
        })

    }
    getCellClass({ row, column, value }): any {
        return {
            'is-unReview': value === 'Under Review',
            'is-published': value === 'Published',
            'is-approved': value === 'Approved',
            'is-expired': value === 'Expiry',
            'is-rejected': value === 'Rejected',
        };
    }

    disableContent(row) {
        row.enabled = false;
        this.http.put(base + '/api/industry', row, this.httpOptions).toPromise().then(res => {
            // console.log(res);
            this.getContentList();
        })
    }
    enableContent(row) {
        row.enabled = true;
        this.http.put(base + '/api/industry', row, this.httpOptions).toPromise().then(res => {
            // console.log(res);
            this.getContentList();
        })
    }
}
