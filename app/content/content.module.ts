import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CustomFormsModule } from 'ng2-validation';
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { CreateContentComponent } from './create-content/create-content.component';

import { FileSelectDirective } from 'ng2-file-upload';

import { ContentRoutingModule } from './content-routing.module';
import { ContentListComponent } from './content-dashboard/content-list.component';
import { ContentEditComponent } from './edit-content/content-edit.component';
import { NgxEditorModule } from 'ngx-editor';
import { MicroContentListComponent } from './micro-content-dashboard/micro-content-list.component';
import { SOPListComponent } from './sop-content/sop-list.component';
import { IndustryContentListComponent } from './industry-cards-list/industry-content-list.component';
import { FeedbackContentListComponent } from './feedback-content-dashboard/feedback-content-list.component';
import { TinymceModule } from 'angular2-tinymce';
import 'tinymce/plugins/emoticons/plugin.js';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        ContentRoutingModule,
        CustomFormsModule,
        MatchHeightModule,
        NgbModule,
        NgxDatatableModule,
        NgxEditorModule,
        TinymceModule.withConfig({

        })
    ],
    declarations: [
        CreateContentComponent,
        ContentListComponent,
        ContentEditComponent,
        MicroContentListComponent,
        SOPListComponent,
        IndustryContentListComponent,
        FeedbackContentListComponent,
        FileSelectDirective
    ]


})
export class ContentModule { }
