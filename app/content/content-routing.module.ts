import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateContentComponent } from './create-content/create-content.component';
import { ContentListComponent } from './content-dashboard/content-list.component';
import { ContentEditComponent } from './edit-content/content-edit.component';
import { MicroContentListComponent } from './micro-content-dashboard/micro-content-list.component';
import { SOPListComponent } from './sop-content/sop-list.component';
import { IndustryContentListComponent } from './industry-cards-list/industry-content-list.component';
import { FeedbackContentListComponent } from './feedback-content-dashboard/feedback-content-list.component';


const routes: Routes = [
  {
    path: '',
    children: [

      {
        path: 'create',
        component: CreateContentComponent,
        data: {
          title: 'Create a new Content '
        }
      },
      {
        path: 'list',
        component: ContentListComponent,
        data: {
          title: 'List of all Content in your Library'
        }
      },
      {
        path: 'edit',
        component: ContentEditComponent,
        data: {
          title: 'List of all Content in your Library'
        }
      },
      {
        path: 'microList',
        component: MicroContentListComponent,
        data: {
          title: 'List of all micro-contents in your Library'
        }
      },
      {
        path: 'microEdit',
        component: CreateContentComponent,
        data: {
          title: 'Edit a micro-content'
        }
      },
      {
        path: 'sopList',
        component: SOPListComponent,
        data: {
          title: 'List of all SOPs in your Library'
        }
      }, {
        path: 'sopEdit',
        component: CreateContentComponent,
        data: {
          title: 'Edit a SOP from your Library'
        }
      },
      {
        path: 'infoCardList',
        component: IndustryContentListComponent,
        data: {
          title: 'List of all Info Cards in your Library'
        }
      },
      {
        path: 'InfoCardEdit',
        component: CreateContentComponent,
        data: {
          title: 'Edit a Info Card'
        }
      },
      {
        path: 'feedbackformList',
        component: FeedbackContentListComponent,
        data: {
          title: 'List of all forms'
        }
      },
      {
        path: 'feedbackformEdit',
        component: CreateContentComponent,
        data: {
          title: 'Edit a Feedback Form'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContentRoutingModule { }
