import { Component, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable/release';
import { PublishedContentResourceService, MicroContentResourceService } from '../../sthaapak';
import { PreviewService } from '../../shared/auth/preview.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { base } from '../../shared/configs/util';

@Component({
    selector: 'app-dt-content',
    templateUrl: './content-list.component.html',
    styleUrls: ['./content-list.component.scss']
})

export class ContentListComponent implements OnInit {
    rows = [];
    temp = [];
    temp2 = [];
    filterOption;
    authors = [];
    organizationId;
    status = ['DRAFT', 'PUBLISHED', 'UNDER REVIEW', 'EXPIRED', 'APPROVED', 'REJECTED'];
    types = ['BLOG', 'VIDEO'];
    // Table Column Titles
    columns = [
        { prop: 'name' },
        { prop: 'schemeUrl' },
        { prop: 'state' },
        { prop: 'postDate' },
        { prop: 'publishedDate' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private contentService: PublishedContentResourceService, private previewService: PreviewService, private router: Router,
        private microResource: MicroContentResourceService) {
        if (!localStorage.getItem('token')) {
            this.router.navigateByUrl('/login');
        }
    }
    ngOnInit() {

        this.organizationId = this.previewService.organizationId.getValue();
        this.getContentList();
    }

    getCellClass({ row, column, value }): any {
        return {
            'is-unReview': value === 'Under Review',
            'is-published': value === 'Published',
            'is-approved': value === 'Approved',
            'is-expired': value === 'Expiry',
            'is-rejected': value === 'Rejected',
        };
    }
    changeFilter(e) {
        // console.log('filter:', this.temp);
        this.filterOption = e.target.value;
        this.rows = this.temp;
    }
    reset() {
        this.filterOption = '';
        this.rows = this.temp;
    }

    getContentList() {
        this.temp = [];
        this.rows = [];
        this.temp2 = [];
        let cards = [];
        this.contentService.getAllPublishedContentsUsingGET().subscribe(res => {
            let result = <any>res;
            // console.log('resil:', result);
            // console.log('cards', cards);
            result.sort((a, b) => { return new Date(b.postDate).getTime() - new Date(a.postDate).getTime() });
            result.forEach(element => {
                if ((element.organizationId && element.organizationId === Number(this.previewService.organizationId.getValue())) &&
                    (cards.indexOf(element.id) < 0) && (element.postDate)) {
                    // console.log('org2: ', element.organizationId)

                    element.publishedDate = new Date(element.publishedDate).toDateString();
                    element.postDate = new Date(element.postDate).toDateString();
                    this.temp2.push(element);
                    if (this.authors.indexOf(element.schemeUrl) < 0) {
                        this.authors.push(element.schemeUrl);
                    }
                    // console.log('temp')
                }
            });


            //this.temp = [...this.temp, ...this.temp2];
            this.temp = [...this.temp, ...this.temp2];
            this.rows = this.temp;


            // console.log('temp:', this.temp);
        });
    }

    filterListByType(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            if (val === 'VIDEO' && d.videoLink) {
                return true;

            }
            if (val === 'BLOG' && d.previewImage) {
                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByAuthor(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            return d.schemeUrl === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByStatus(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log(d.state);
            // console.log(val);
            return d.state === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPostDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log('date:', d.postDate)
            if (new Date(d.postDate).getDate() === new Date(val).getDate() && new Date(d.postDate).getMonth() === new Date(val).getMonth() && new Date(d.postDate).getFullYear() === new Date(val).getFullYear()) {


                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPublDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {

            if (new Date(d.publishedDate).getDate() === new Date(val).getDate() && new Date(d.publishedDate).getMonth() === new Date(val).getMonth() && new Date(d.publishedDate).getFullYear() === new Date(val).getFullYear()) {

                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    updateFilter(event) {

        const val = event.target.value.toLowerCase();

        // filter our data
        const temp3 = this.temp.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    scheduleContent(obj) {

        swal({
            title: 'Are you sure you want to schedule this content?',
            text: "This will take you to the Content Scheduler!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, schedule it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.previewService.editContentToSchedule(obj);
                this.router.navigate(['/scheduler/scheduler']);
            }
            else {
                swal(
                    'Cancelled',
                    'Your content was not scheduled.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your content was not scheduled.',
                    'error'
                )
            }
        })

    }

    deleteContent(row) {


        swal({
            title: 'Are you sure you want to delete this content?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.contentService.deletePublishedContentUsingDELETE(row.id).subscribe(delRes => {
                    // console.log('res:', delRes);
                    this.getContentList();

                })
            }
            else {
                swal(
                    'Cancelled',
                    'Your content was not deleted.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your content was not deleted.',
                    'error'
                )
            }
        })




    }

    editContent(obj) {

        swal({
            title: 'Are you sure you want to edit this content?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.previewService.editContentToEdit(obj);
                this.router.navigate(['/content/edit']);
            }
            else {
                swal(
                    'Cancelled',
                    'Your content was not edited.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your content was not edited.',
                    'error'
                )
            }
        })
    }
}
