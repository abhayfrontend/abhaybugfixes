import { MicroContent } from './../../sthaapak/model/microContent';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ContentService } from './content.service';
import * as AWS from '../../shared/configs/aws.config';
import { AppConfigResourceService } from '../../sthaapak/sdk/appConfigResource.service';
import { PreviewService } from '../../shared/auth/preview.service';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import { CampaignGroupResourceService, DeviceResourceService, UserResourceService, IndustryResourceService } from '../../sthaapak';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { base, proxyApi } from '../../shared/configs/util';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { mergeMap } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';

@Component({
  selector: 'app-create-content',
  templateUrl: './create-content.component.html',
  styleUrls: ['./create-content.component.scss']
})

export class CreateContentComponent implements OnInit, OnDestroy {
  public uploader: FileUploader = new FileUploader({ url: proxyApi + '/upload', itemAlias: 'zip' });

  notificationTitle;
  contentTypeInput;
  tabs: string;
  construct;
  questionId;
  responseCards;
  formDataObj: any = {
    content: {
      contentType: 'MicroLearning',
      targetCategory: '',
      targetSubCategory: '',
      colorTheme: '',
      contentName: '',
      contentDescription: '',
      imageDetails: {
        url: '',
        name: ''
      }
    },
    contentSlides: [],
    contentQuestions: []
  }
  expanded = true;
  message;
  contentArr = [];
  coursePlan = '266702';
  coursePlans = [];
  contentEditId;
  organizationId;
  learners = [];
  htmlContent;
  el;
  constructor(public _contentService: ContentService, public appConfigService: AppConfigResourceService, public sanitizer: DomSanitizer,
    public groupService: CampaignGroupResourceService, public deviceService: DeviceResourceService, private router: Router, private industry: IndustryResourceService,
    private http: HttpClient, private roleGuardService: RoleGuardService, public userResource: UserResourceService, public previewService: PreviewService) {
    this.contentArr = this._contentService.contentArr;
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }

  }
  slideUrl;
  learnerIds = [];
  tokens = [];
  editMicroContent = false;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };
  categories = [];
  subCategories = [];
  contentTypes = ['BLOG', 'VIDEO'];
  operationTypes = ['IMAGE', 'VIDEO', 'AUDIO'];
  FileInputTypes = ['Upload a Local File', 'Paste a Url'];
  contentToEdit;
  contentImageUrl;
  contentVideoUrl;
  contentZipUrl;
  contentAudioUrl;
  contentDocUrl;
  audioUrl;
  videoUrls;
  docUrl;
  imageUrl;
  org;
  ngOnInit() {

    this.industry.getAllIndustriesUsingGET().subscribe(e => {
      let result = <any>e;
      result.forEach(element => {
        element.organizations.forEach(el => {
          // console.log('org2: ', el);
          if (Number(el.id) === Number(this.previewService.organizationId.getValue()) && element.tags === 'newinfo') {
            this.industries.push(element);

          }
        });
        // console.log('jobFamilies:', element.jobFamilies);
      });
      this.industries.unshift({ id: null, name: 'All Industries' });
    })

    $('.clickable').on('click', (e) => {
      e.stopPropagation();
    });
    /* $(document).on('click', (e) => {
       this.categorySelection.subcategoryPopupVisible = false;
       this.categorySelection.categoryPopupVisible = false;
     });
 */

    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    //overide the onCompleteItem property of the uploader so we are 
    //able to deal with the server response.
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      // console.log("ImageUpload:uploaded:", item, status, response);
      this.contentZipUrl = response;
    };
    this.http.get(base + '/api/organizations/' + this.previewService.organizationId.getValue(), this.httpOptions).subscribe(res => {
      this.org = <any>res;
    });
    this.http.get(base + '/api/learners', this.httpOptions).subscribe(res => {
      let result = <any>res;
      // console.log('userResou:', result);
      result = result.filter(e => {
        if ((Number(e.orgId) === Number(this.previewService.organizationId.getValue())) || (Number(e.organizationId) === Number(this.previewService.organizationId.getValue()))) {
          this.learners.push(e);
          return true;
        }
      });
      // console.log('leanrer on init:', this.learners);
      this.getTokens(this.learners);
    });
    this.previewService.organizationIdCast.subscribe(org => {
      this.organizationId = org;
    })
    if (this.previewService.contentToEdit.getValue()) {
      this.contentTypeSelection.showContentSelection = false;
      this.editMicroContent = true;
      let obj = <any>this.previewService.contentToEdit.getValue();
      this.contentEditId = obj.id;
      this.url = obj.thumbnailUrl;
      this.imageName = 'image';
      this.mandatory = false;
      this.contentName = obj.name;
      this.contentDescription = obj.description;
      this.contentTypeSelection.selectedType = obj.contentType;
      this.categorySelection = {
        expanded: false,
        categoryPopupVisible: false,
        questionPopupVisible: false,
        subcategoryPopupVisible: false,
        selectedCategory: obj.category,
        selectedSubCategory: obj.subcategory,
        selectedOperationType: null,
        selectedQuestionType: null,
        selectedFileInputType: '',
        FileInputTypePopupVisible: false,
      };

    }
    this.previewService.contentToEditCast.subscribe(s => {

      // console.log('s1:', s);
      this.contentToEdit = s
      let contentSlides = [];
      let questionSlides = [];

      // console.log('hehe1:', this.contentToEdit);
      if (this.contentToEdit.id && this.contentToEdit.cards && this.contentToEdit.cards[0]['schemeUrl'] !== 'webview') {
        // console.log('hehe', this.contentToEdit);
        this.categorySelection.selectedCategory = this.contentToEdit.category;
        this.categorySelection.selectedSubCategory = this.contentToEdit.subcategory;
        this.contentTypeSelection.selectedType = 'MicroLearning';
        this.contentArr = [];
        let obj = <any>this.contentToEdit;
        this._contentService.formDataObj.content = {
          contentType: obj.contentType,
          targetCategory: obj.category,
          targetSubCategory: obj.subcategory,
          colorTheme: obj.colorTheme,
          contentName: obj.name,
          contentDescription: obj.description,
          imageDetails: {
            url: obj.thumbnailUrl,
            name: 'image'
          },
          id: obj.id
        }

        // console.log('cardsincontent:', obj.cards);

        obj.cards.forEach((element, i) => {
          if (element.cards) {
            // console.log('URL LINK:', element.cards[0].videoLink);
            contentSlides.push({
              slideContentDescription: element.cards[0].content,
              contentType: element.cards[0].contentType,
              slideTitleName: element.cards[0].name,
              slideImageDetails: {
                url: element.cards[0].previewImage || element.cards[0].videoLink,
                name: 'image'
              },
              id: i
            });
            this.contentArr.push({
              slideContentDescription: element.cards[0].content,
              contentType: element.cards[0].contentType,
              slideTitleName: element.cards[0].name,
              slideImageDetails: {
                url: element.cards[0].previewImage || element.cards[0].videoLink,
                name: 'image'
              },
              type: 'contentSlide',
              id: i
            })
          }
          if (this.contentToEdit['id'] && !this.contentToEdit['cards']) {
            this._contentService.formDataObj.contentSlides.push({
              slideContentDescription: element.slideContentDescription,
              contentType: element.contentType,
              slideTitleName: element.slideTitleName,
              slideImageDetails: {
                url: element.slideImageDetails.url,
                name: element.slideImageDetails.name
              },
              id: i
            });
            contentSlides.push({
              slideContentDescription: element.slideContentDescription,
              contentType: element.contentType,
              slideTitleName: element.slideTitleName,
              slideImageDetails: {
                url: element.slideImageDetails.url,
                name: element.slideImageDetails.name
              },
              id: i
            });
            this.contentArr.push({
              slideContentDescription: element.slideContentDescription,
              contentType: element.contentType,
              slideTitleName: element.slideTitleName,
              slideImageDetails: {
                url: element.slideImageDetails.url,
                name: element.slideImageDetails.name
              },
              type: 'contentSlide',
              id: i
            })
          }

        });
        let quests = this.contentToEdit.questionSlides;
        // console.log('qsts:', quests);
        obj.questionSlides.forEach((e, j) => {
          // console.log('ques:', e);
          this.contentArr.push({
            slideTitleName: e.title,
            slideContentDescription: e.subTitle,
            randomize: e.randomize,
            mandatory: e.mandatory,
            anonymous: e.anonymous,
            negative: e.negative,
            correctAnswer: e.correctAnswer,
            imageUrl: e.imageUrl,
            incorrectAnswers: e.incorrectAnswers,
            timer: {
              timeLimit: e.timeLimit,
              outOfTimePrompt: e.outOfTimeMessage
            },
            stars: e.stars,
            id: e.id,
            type: 'questionSlide'
          })
        })
        // console.log('conArr:', this.contentArr);
        this._contentService.formDataObj.contentQuestions = obj.questionSlides;
        this._contentService.formDataObj.contentSlides = contentSlides;
        this._contentService.contentArr = this.contentArr;
        // console.log('oninit:', this._contentService.formDataObj);

      }
      if (this.contentToEdit['id'] && !this.contentToEdit['cards'] && this.contentToEdit['contentType'] !== 'Survey' && this.contentTypeSelection.selectedType === 'Info Cards') {

        // console.log('hehe11221', this.contentToEdit);
        this.categorySelection.selectedCategory = this.contentToEdit.tags;
        this.contentTypeSelection.selectedType = 'Info Cards';
        this.contentArr = [];
        let obj = <any>this.contentToEdit;
        this._contentService.formDataObj.content = {
          contentType: 'Info Cards',
          targetCategory: obj.category,
          targetSubCategory: obj.subcategory,
          imageDetails: {
            url: obj.thumbnailUrl,
            name: 'image'
          },
          id: obj.id
        }
        this._contentService.formDataObj.contentSlides.push({
          slideTitleName: obj.name,
          slideContentDescription: obj.content,
          slideImageDetails: {
            url: obj.imageLink,
            name: obj.imageLink ? 'file' : ''
          },
          slideVideoDetails: {
            url: obj.videoLink,
            name: obj.videoLink ? 'file' : ''
          },
          slideAudioDetails: {
            url: obj.audioLink,
            name: obj.audioLink ? 'file' : ''
          },
          slideDocDetails: {
            url: obj.pdfLink,
            name: obj.pdfLink ? 'file' : ''
          },
          id: obj.id,
          contentType: this.categorySelection.selectedContentType || this.categorySelection.selectedOperationType,
          mandatory: this.mandatory
        });

        this.contentArr.push({
          slideContentDescription: obj.content,
          slideTitleName: obj.name,
          slideImageDetails: {
            url: obj.imageLink,
            name: obj.imageLink ? 'file' : ''
          },
          slideVideoDetails: {
            url: obj.videoLink,
            name: obj.videoLink ? 'file' : ''
          },
          slideAudioDetails: {
            url: obj.audioLink,
            name: obj.audioLink ? 'file' : ''
          },
          slideDocDetails: {
            url: obj.pdfLink,
            name: obj.pdfLink ? 'file' : ''
          },
          type: 'contentSlide',
          id: obj.id
        })

      }
      if (this.contentToEdit['id'] && this.contentToEdit['contentType'] === 'Survey') {

        // console.log('hehe11', this.contentToEdit);
        this.categorySelection.selectedCategory = this.contentToEdit.category;
        this.contentTypeSelection.selectedType = 'Survey';
        this.contentArr = [];
        let obj = <any>this.contentToEdit;
        // console.log('wwhsakgh:', obj.id);
        this._contentService.formDataObj.content = {
          contentType: 'Survey',
          targetCategory: obj.category,
          targetSubCategory: String(obj.id),
          contentName: obj.name,
          imageDetails: {
            url: obj.id,
            name: 'image'
          }

        }
        for (let i = 0; i < obj.questionSlides.length; i++) {

          this._contentService.formDataObj.contentQuestions.push({
            title: obj.questionSlides[i].title,
            timer: {
              timeLimit: this.timeLimit,
              outOfTimePrompt: this.outOfTimeMessage
            },
            stars: this.stars,
            id: obj.questionSlides[i].id,
            displayOrder: obj.questionSlides[i].displayOrder,
            surveyId: obj.questionSlides[i].surveyId,
            qType: obj.questionSlides[i].qType,
            correctAnswer: obj.questionSlides[i].correctAnswer,
            incorrectAnswers: obj.questionSlides[i].incorrectAnswers
          });

          this.contentArr.push({
            slideContentDescription: obj.content,
            slideTitleName: obj.questionSlides[i].title,
            id: obj.questionSlides[i].id,
            qType: obj.questionSlides[i].qType,
            displayOrder: obj.questionSlides[i].displayOrder,
            surveyId: obj.questionSlides[i].surveyId,
            correctAnswer: obj.questionSlides[i].correctAnswer,
            incorrectAnswers: obj.questionSlides[i].incorrectAnswers,

            type: 'questionSlide',

          });

        }
        this._contentService.contentArr = this.contentArr;
        // console.log('obj.id:', this._contentService.formDataObj);

      }

      if (this.contentToEdit['id'] && this.contentToEdit['cards'] && this.contentToEdit['cards'][0]['schemeUrl'] === 'webview') {

        // console.log('hehe HTML5', this.contentToEdit);
        this.categorySelection.selectedCategory = this.contentToEdit.category;
        this.categorySelection.selectedSubCategory = this.contentToEdit.subcategory;
        this.contentTypeSelection.selectedType = 'HTML5 Course';
        this.contentArr = [];
        let obj = <any>this.contentToEdit;
        this._contentService.formDataObj.content = {
          contentType: obj.contentType || 'HTML5 Course',
          targetCategory: obj.category,
          targetSubCategory: obj.subcategory,
          colorTheme: obj.colorTheme,
          contentName: obj.name,
          contentDescription: obj.description,
          imageDetails: {
            url: obj.thumbnailUrl,
            name: 'image'
          },
          id: obj.id
        }

        // console.log('cardsincontent:', obj.cards);

        obj.cards.forEach((element, i) => {
          if (element.cards) {
            // console.log('URL LINK:');
            contentSlides.push({
              slideContentDescription: "",
              contentType: 'HTML5 Course',
              slideTitleName: element.name,
              slideZipUrl: element.description,
              slideImageDetails: {
                url: '',
                name: 'image'
              },
              id: i
            });
            this.contentArr.push({
              slideContentDescription: "",
              contentType: 'HTML5 Course',
              slideTitleName: element.name,
              slideZipUrl: element.description,
              slideImageDetails: {
                url: '',
                name: 'image'
              },
              type: 'contentSlide',
              id: i
            })
          }


        });
        // console.log('conArrHTML5:', this.contentArr);
        this._contentService.formDataObj.contentSlides = contentSlides;
        this._contentService.contentArr = this.contentArr;
        // console.log('oninit:', this._contentService.formDataObj);



      }
      /*  if (this.contentToEdit['id'] && !this.contentToEdit['cards'] && !this.contentToEdit['cards'] && (this.contentToEdit['contentType'] === 'BLOG' ||
         this.contentToEdit['contentType'] === 'News' || this.contentToEdit['contentType'] === 'VIDEO' || this.contentToEdit['contentType'] === 'VIDEOS') ) {
       
          // console.log('hehe', this.contentToEdit);
          this.categorySelection.selectedCategory = this.contentToEdit.category;
          this.categorySelection.selectedSubCategory = this.contentToEdit.subcategory;
          this.contentTypeSelection.selectedType = 'News';
          this.contentArr = [];
          let obj = <any>this.contentToEdit;
          this._contentService.formDataObj.content = {
            contentType: obj.contentType,
            targetCategory: obj.category,
            targetSubCategory: obj.subcategory,
            colorTheme: obj.colorTheme,
            contentName: obj.name,
            contentDescription: obj.description,
            imageDetails: {
              url: obj.thumbnailUrl,
              name: 'image'
            },
            id: obj.id
          }
  
          this._contentService.formDataObj.contentSlides.push({
            slideContentDescription: obj.slideContentDescription,
            contentType: obj.contentType,
            slideTitleName: obj.slideTitleName,
            slideImageDetails: {
              url: obj.slideImageDetails.url,
              name: obj.slideImageDetails.name
            },
            id: obj.content
          });
          contentSlides.push({
            slideContentDescription: obj.slideContentDescription,
            contentType: obj.contentType,
            slideTitleName: obj.slideTitleName,
            slideImageDetails: {
              url: obj.slideImageDetails.url,
              name: obj.slideImageDetails.name
            },
            id: i
          });
          this.contentArr.push({
            slideContentDescription: obj.slideContentDescription,
            contentType: obj.contentType,
            slideTitleName: obj.slideTitleName,
            slideImageDetails: {
              url: obj.slideImageDetails.url,
              name: obj.slideImageDetails.name
            },
            type: 'contentSlide',
            id: i
          })
  
          // console.log('cardsincontent:', obj);
  
  
  
         } */
    });

  }

  selectFileInputType(type) {
    this.contentSlideUrl = '';
    this.categorySelection.selectedFileInputType = type;
  }
  ngOnDestroy() {
    this.previewService.editContentToEdit({});
    this._contentService.formDataObj = {
      content: {
        contentType: 'MicroLearning',
        targetCategory: '',
        targetSubCategory: '',
        colorTheme: '',
        contentName: '',
        contentDescription: '',
        imageDetails: {
          url: '',
          name: ''
        }
      },
      contentSlides: [],
      contentQuestions: []
    }
    this.contentToEdit = null;
  }
  pagesObj = {
    currentPage: 'newContent',
    pagesArr: ['newContent', 'contentSlide', 'questionSlide'],
    status: {
      newContent: true,
      contentSlide: false,
      questionSlide: false
    }
  }

  // ----------------------code for phone starts----------------------------------------------------
  phoneWidth = 320;
  phoneHeight = 640;
  phonesObj = {
    selected: '',
    phoneList: [
      {
        id: 'Nexus5',
        name: 'Nexus 5',
        width: 360,
        height: 640
      },
      {
        id: 'GalaxyY',
        name: 'Galaxy Y',
        width: 240,
        height: 320
      },
      {
        id: 'SamsungS4',
        name: 'Samsung S4',
        width: 360,
        height: 640
      },
      {
        id: 'SamsungGalaxyS3Mini',
        name: 'Samsung Galaxy S3 Mini',
        width: 360,
        height: 600
      }]
  }
  phoneSelectionPopupOpen = false;

  togglePhoneSelectorPopup() {
    this.phoneSelectionPopupOpen = !this.phoneSelectionPopupOpen;
  }

  selectPhoneModel(phoneModel) {
    this.phonesObj.selected = phoneModel.name;
    this.phoneWidth = phoneModel.width;
    this.phoneHeight = phoneModel.height;
    this.phoneSelectionPopupOpen = false;
  }




  // ----------------------code for phone ends----------------------------------------------------


  // ----------------------code for create content starts----------------------------------------------------
  @ViewChild('imageFileInput') imageInput: ElementRef;
  url = '';
  imageName = '';
  sCurExtension;
  mandatory = false;
  contentName = '';
  contentDescription = '';
  validFileExtensions = ['.jpg', '.jpeg', '.bmp', '.gif', '.png'];
  validFileExtensionsVideo = ['.mp4', '.avi', '.mov', '.mwv', '.flv'];
  validFileExtensionsZip = ['.zip'];
  validFileExtensionsDoc = ['.doc', '.docx', '.xlsx', '.csv', '.ppt', '.pptx', '.txt', '.pdf'];
  validFileExtensionsAudio = ['.aac', '.mp3', '.m4a', '.wav', '.wma', '.vox', '.ogg', '.m4p'];
  categorySelection: any = {
    expanded: false,
    categoryPopupVisible: false,
    subcategoryPopupVisible: false,
    selectedCategory: '',
    selectedSubCategory: '',
    selectedOperationType: '',
    selectedQuestionType: '',
    questionTypePopupVisible: false
  };

  jobRoleSelection: any = {
    expanded: false,
    jobRolePopupVisible: false,
    selectedJobRole: '',
    categoryPopupVisible: false,
    subcategoryPopupVisible: false,
    selectedCategory: '',
    selectedSubCategory: '',
  }

  themeSelection: any = {
    expanded: false,
    selectedTheme: {
      theme: 'lightBlue',
      lightBlue: true,
      blue: false,
      green: false,
      purple: false
    }
  };

  contentTypesList = [
    {
      title: 'Info Cards',
      description: 'Create highly engaging and quality multi-media information brochures.',
      icon: 'newspaper-o'
    },
    {
      title: 'News',
      description: 'Create highly engaging and quality news reads.',
      icon: 'newspaper-o'
    },
    {
      title: 'Survey',
      description: 'Create effective surveys to get direct feedback and seniments.',
      icon: 'question'
    },
    {
      title: 'Quiz',
      description: 'Create interactive quiz with multiple choice questions and automatic scoring.',
      icon: 'check'
    },
    {
      title: 'Video',
      description: 'Deliver direct messages in a video format with instant sharing option.',
      icon: 'play'
    },
    {
      title: 'Poll',
      description: 'Create rational and non-biased polls to measure public views and opinion.',
      icon: 'columns'
    },
    {
      title: 'MicroLearning',
      description: 'Create efficient microlearning modules regarding a particular topic or series of topics.',
      icon: 'file-text-o'
    },
    {
      title: 'HTML5 Course',
      description: 'Create efficient HTML5 courses regarding a particular topic or series of topics.',
      icon: 'file-text-o'

    },
    {
      title: 'Pitch Comprehension',
      description: 'Record Voice over PPT.',
      icon: 'play'

    }
    /*,
    {
      title: 'Standard Operating Procedure Checklist',
      description: 'Create daily, weekly, monthly checklist for a particular task.',
      icon: 'files-o'
    }
    */
  ];
  screenType;
  mediaSelection: any = {
    image: true,
    video: true,
    doc: true,
    audio: true,
    imageClickedUrl: function () {
      // console.log('image:', this.image);
      this.image = false;
      this.contentImageUrl = '';
      // console.log('image:', this.image);
    },
    imageClickedUpload: function () {
      // console.log('image:', this.image);
      this.image = true;
      this.contentImageUrl = '';
      // console.log('image:', this.image);
    },
    videoClickedUrl: function () {
      this.video = false;
      this.contentVideoUrl = '';
    },
    videoClickedUpload: function () {
      this.video = true;
      this.contentVideoUrl = '';
    },
    docClickedUrl: function () {
      this.doc = false;
      this.contentDocUrl = '';
    },
    docClickedUpload: function () {
      this.doc = true;
      this.contentDocUrl = '';
    },
    audioClickedUrl: function () {
      this.audio = false;
      this.contentAudioUrl = '';
    },
    audioClickedUpload: function () {
      this.audio = true;
      this.contentAudioUrl = '';
    },
    ZipClickedUpload: function () {
      this.zip = true;
      this.contentZipUrl = '';
    }
  }
  industries = [];
  departments = [];
  jobRoles = [];

  contentTypeSelection = {
    showContentSelection: true,
    selectedType: ''
  }
  showContentTypeList() {
    if (this.editMicroContent === true) {
    }
    else {
      this.contentTypeSelection.showContentSelection = !this.contentTypeSelection.showContentSelection
    }
  }

  selectContentTypeFromList(type) {
    let contentTypeInput;

    contentTypeInput = document.getElementById('contentTypeInput');
    if (contentTypeInput.classList.contains('error')) {
      contentTypeInput.classList.remove('error');
    }
    this.contentTypeSelection.showContentSelection = false;
    this.contentTypeSelection.selectedType = type;
    this.categorySelection.selectedCategory = '';
    this.categorySelection.selectedSubCategory = '';
    this.appConfigService.getAllAppConfigsByOrgUsingGET(Number(this.organizationId)).subscribe(res => {
      let result = <any>res;
      this.categories = result.filter(e => {
        if (this.contentTypeSelection.selectedType === 'News' || this.contentTypeSelection.selectedType === 'Video') {
          if (e.category === 'DASHBOARD' && e.name === 'Blogs') {
            return true;
          }
          return false;
        }

        if (this.contentTypeSelection.selectedType === 'MicroLearning') {
          if (e.category === 'DASHBOARD' && e.name === 'MicroLearning') {
            return true;
          }
          return false;
        }
        if (this.contentTypeSelection.selectedType === 'Info Cards') {
          if (e.category === 'DASHBOARD' && e.name === 'Industry') {
            return true;
          }
          return false;
        }

        if (this.contentTypeSelection.selectedType === 'Survey') {
          if (e.category === 'DASHBOARD' && (e.name === 'FEEDBACK' || e.name === 'Feedback')) {
            return true;
          }
          return false;
        }

        if (this.contentTypeSelection.selectedType === 'Standard Operating Procedure Checklist') {
          if (e.category === 'DASHBOARD' && (e.name === 'Operations')) {
            return true;
          }
          return false;
        }

        if (this.contentTypeSelection.selectedType === 'HTML5 Course') {
          if (e.category === 'DASHBOARD' && (e.name === 'MicroLearning')) {
            return true;
          }
          return false;
        }

        if (this.contentTypeSelection.selectedType === 'Pitch Comprehension') {
          if (e.category === 'DASHBOARD' && (e.name === 'MicroLearning')) {
            return true;
          }
          return false;
        }
      });


      if (this.contentTypeSelection.selectedType === 'News' || this.contentTypeSelection.selectedType === 'Video') {
        this.screenType = 'Blogs';
      }

      if (this.contentTypeSelection.selectedType === 'MicroLearning' || this.contentTypeSelection.selectedType === 'HTML5 Course' ||
        this.contentTypeSelection.selectedType === 'Pitch Comprehension') {
        this.screenType = 'MicroLearning';
      }
      if (this.contentTypeSelection.selectedType === 'Info Cards') {
        this.screenType = 'Industry';
      }

      if (this.contentTypeSelection.selectedType === 'Survey') {
        this.screenType = 'Feedback';
      }

      if (this.contentTypeSelection.selectedType === 'Standard Operating Procedure Checklist') {
        this.screenType = 'Operations';
      }

    })
  }

  expandCategory() {
    this.categorySelection.expanded = true;
  }

  expandJobRole() {
    this.jobRoleSelection.expanded = true;
  }

  collapseCategory() {
    this.categorySelection.expanded = false;
  }
  collapseJobRole() {
    this.jobRoleSelection.expanded = false;
  }

  selectCategory(category) {
    let contentTypeInput;

    contentTypeInput = document.getElementById("categoryInput");
    if (contentTypeInput.classList.contains('error')) {
      contentTypeInput.classList.remove('error');
    }
    this.categorySelection.selectedCategory = category;
    this.categorySelection.selectedSubCategory = '';
    this.categorySelection.categoryPopupVisible = false;
    this.categories.forEach(element => {
      if (category === element.value) {
        this.subCategories = element.subCategory ? element.subCategory.split(',') : [];
      }
    });
  }

  selectIndustry(category) {
    let contentTypeInput;

    contentTypeInput = document.getElementById('jobRoleInput');
    if (contentTypeInput.classList.contains('error')) {
      contentTypeInput.classList.remove('error');
    }
    this.jobRoleSelection.selectedCategory = category;
    this.jobRoleSelection.selectedSubCategory = '';
    this.jobRoleSelection.categoryPopupVisible = false;
    if (category.id !== null) {
      this.industries.forEach(element => {
        if (category.id === element.id) {
          this.departments = element.jobFamilies;
        }
      });
      if (this.departments[0].id) {
        this.departments.unshift({ id: null, name: 'All Departments' });

      }
    }
    else {
      this.industries.forEach(industry => {
        if (industry.id) {
          // console.log("INDUSTRIES:", industry);
          industry.jobFamilies.forEach(element => {
            if (element.id) {
              element.jobRoles.forEach(jobRole => {
                if (jobRole.url && jobRole.id) {
                  this.coursePlans.push(jobRole.url)
                }
              });
            }
          });
        }
      })
    }
    // console.log('CoursePlans Ind:', this.coursePlans);

  }



  selectDepartment(category) {
    let contentTypeInput;

    contentTypeInput = document.getElementById('jobRoleInput');
    if (contentTypeInput.classList.contains('error')) {
      contentTypeInput.classList.remove('error');
    }
    this.jobRoleSelection.selectedSubCategory = category;
    this.jobRoleSelection.selectedJobRole = '';
    this.jobRoleSelection.subcategoryPopupVisible = false;
    if (category.id !== null) {
      this.departments.forEach(element => {
        if (category.id === element.id) {
          this.jobRoles = element.jobRoles;
        }
      });
      if (this.jobRoles[0].id) {
        this.jobRoles.unshift({ id: null, name: 'All Roles' });
      }
    }
    else {
      this.departments.forEach(element => {
        if (element.id) {
          element.jobRoles.forEach(jobRole => {
            if (jobRole.url && jobRole.id) {
              this.coursePlans.push(jobRole.url)
            }
          });
        }
      })
    }
    // console.log('CoursePlans Dep:', this.coursePlans);

  }

  selectJobRole(category) {

    let contentTypeInput;

    contentTypeInput = document.getElementById('jobRoleInput');
    if (contentTypeInput.classList.contains('error')) {
      contentTypeInput.classList.remove('error');
    }
    this.jobRoleSelection.selectedJobRole = category;
    this.jobRoleSelection.jobRolePopupVisible = false;
    if (category.id !== null) {
      this.jobRoles.forEach(element => {
        if (category.id === element.id) {
          this.coursePlans.push(element.url);
        }
      });
    }
    else {
      this.jobRoles.forEach(element => {
        if (element.id && element.url) {
          this.coursePlans.push(element.url)
        }

      })
    }
    // console.log('CoursePlans Dep:', this.coursePlans);

    // console.log('sub2:', this.jobRoleSelection.selectedJobRole);
    // console.log('CoursePlans:', this.coursePlans);

  }



  selectSubCategory(subCategory) {
    let contentTypeInput;

    contentTypeInput = document.getElementById('categoryInput');
    if (contentTypeInput.classList.contains('error')) {
      contentTypeInput.classList.remove('error');
    }
    // console.log('sub1:', subCategory);

    this.categorySelection.selectedSubCategory = subCategory;
    this.categorySelection.subcategoryPopupVisible = false;
    // console.log('sub2:', this.categorySelection.selectedSubCategory);
  }


  selectTheme(theme) {
    this.deselectAllThemes();
    this.themeSelection.selectedTheme[theme] = true;
    this.themeSelection.selectedTheme.theme = theme;
  }

  deselectAllThemes() {
    this.themeSelection.selectedTheme = {
      theme: 'lightBlue',
      lightBlue: false,
      blue: false,
      green: false,
      purple: false
    }
  }

  browseImageClicked() {
    let el: any = this.imageInput.nativeElement;
    el.click();
  }
  validationOnName() {
    let contentTypeInput;
    contentTypeInput = document.getElementById('contentName');
    if (contentTypeInput.classList.contains('error')) {
      contentTypeInput.classList.remove('error');
    }
  }
  ellipsisClick() {
    this.expanded = !this.expanded
  }

  isExpanded() {
    if (this.contentDescription) {
      return (this.contentDescription.length > 10 && this.expanded);
    }
    else return false;
  }

  validationOndescriptionContent() {
    let contentTypeInput;
    contentTypeInput = document.getElementById('contentDescriptionInput');
    if (contentTypeInput.classList.contains('error')) {
      contentTypeInput.classList.remove('error');
    }
  }

  validationOnSlideTitle() {
    this.notificationTitle = this.slideTitleName;
    let contentTypeInput;
    contentTypeInput = document.getElementById('slideTitle');
    if (contentTypeInput.classList.contains('error')) {
      contentTypeInput.classList.remove('error');
    }
  }

  validationOndescriptionSlide() {
    // console.log('Eorking');
    let elm = <HTMLInputElement>document.getElementById('descriptionSlide');
    if (elm.value.length < 1 || elm.value.length > 5000) {
      swal('', 'Please enter atleast 1 character and atmost 5000 characters.');
      // now focus it
    }
  }

  validationOnQuestionTitle() {
    let elm = <HTMLInputElement>document.getElementById('page3TitleName');
    if (elm.value.length < 1 || elm.value.length > 1000) {
      swal('', 'Please enter atleast 1 character and atmost 1000 characters.');
      // now focus it
    }
  }

  validationOnQuestionSubtitle() {
    // console.log('Eorking');
    let elm = <HTMLInputElement>document.getElementById('page2TitleName');
    if (elm.value.length < 1 || elm.value.length > 2500) {
      swal('', 'Please enter atleast 1 character and atmost 2500 characters.');
      // now focus it
    }
  }

  validationOnCorrectAnswer() {
    let elm = <HTMLInputElement>document.getElementById('correctAnswer');
    if (elm.value === '' || elm.value.length > 500) {
      swal('', 'Answer can not be empty and more than 500 characters.');
      // now focus it
    }
  }


  validationOnInCorrectAnswer(e) {
    if (e.target.value === '' || e.target.value.length > 500) {
      swal('', 'Answer can not be empty and more than 500 characters.');
      // now focus it);
    }
  }


  newContentImageChanged(event) {

    let contentTypeInput;
    contentTypeInput = document.getElementById('filePhoto');
    if (contentTypeInput.classList.contains('error')) {
      contentTypeInput.classList.remove('error');
    }
    let timerInterval1;
    let _URL = window.URL;
    let error = false;
    if (event.target.files && event.target.files[0]) {
      this.imageName = event.target.files[0].name;
      let sFileName = event.target.files[0].name;
      // console.log('sfilename: ', sFileName);
      let blnValid = false;
      for (let j = 0; j < this.validFileExtensions.length; j++) {
        this.sCurExtension = this.validFileExtensions[j];
        if (sFileName.slice(sFileName.length - this.sCurExtension.length, sFileName.length).toLowerCase() === this.sCurExtension.toLowerCase()) {
          blnValid = true;
          break;
        }
      }
      if (!blnValid) {
        swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensions.join(', '));
        return false;
      }
      // console.log('size:', event.target.files[0].size);
      if ((event.target.files[0].size < 1000) || (event.target.files[0].size > 8000000)) {
        swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 2 Mb');
        return false;
      }
      const file = event.target.files[0];
      let reader = new FileReader();
      //Initiate the FileReader object.
      //Read the contents of Image File.
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e) => {
        let that = file
        //Initiate the JavaScript Image object.
        let image = new Image();
        //Set the Base64 string return from FileReader as source.
        image.src = _URL.createObjectURL(file);
        image.onload = () => {
          //Determine the Height and Width.
          let height = image.height;
          let width = image.width;
          swal({
            title: 'Uploading!',
            onOpen: () => {
              swal.showLoading();
              Promise.resolve(this.uploadFile(file)).then(location =>
                this.url = <any>location
              );
              timerInterval1 = setInterval(() => {
                if (this.url) {
                  swal.close();
                  clearInterval(timerInterval1);
                  // console.log('time1')
                }
              }, 100)
            },
            onClose: () => {
              clearInterval(timerInterval1)
            }
          }).then(ans => {
            clearInterval(timerInterval1);
          });
          return true;
        };
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }


  uploadFile(file) {
    let upload;
    let fileName = String(file.name);
    fileName = fileName.match(/[a-zA-Z0-9-_!.*'()]/gi).join('');
    if (!fileName) {
      fileName = 'sample';
    }
    if (this.categorySelection.selectedContentType === 'IMAGE') {
      upload = AWS.s3.upload({
        Key: this.previewService.organizationId.getValue() + '/content/images/' + new Date() + fileName,
        Bucket: AWS.bucketName, Body: file, ACL: 'public-read'
      });
    }
    else if (this.categorySelection.selectedContentType === 'VIDEO') {
      upload = AWS.s3.upload({
        Key: this.previewService.organizationId.getValue() + '/content/videos/' + new Date() + fileName,
        Bucket: AWS.bucketName, Body: file, ACL: 'public-read'
      });
    }
    else {

      // console.log('OCTET::', fileName.slice(fileName.length - this.sCurExtension.length, fileName.length).toLowerCase())
      if (fileName.slice(fileName.length - this.sCurExtension.length, fileName.length).toLowerCase() === '.pdf') {
        // console.log('pdfpdfpdf');
        /*
                upload = AWS.s3.putObject({
                  Key: this.previewService.organizationId.getValue() + '/content/info/' + new Date() + fileName, Bucket: AWS.bucketName, Body: file,
                  ACL: 'public-read', Metadata: {
                    "Content-Type": "application/pdf"
                  }, ContentType: "application/pdf", ContentEncoding: "UTF-8"
                })
          */
        upload = AWS.s3.upload({
          Key: (this.previewService.organizationId.getValue() + '/content/info/' + new Date().getTime() + fileName), Bucket: AWS.bucketName, Body: file,
          ACL: 'public-read', ContentType: 'application/pdf'
        });

      }
      else if (fileName.slice(fileName.length - this.sCurExtension.length, fileName.length).toLowerCase() === '.doc') {
        upload = AWS.s3.upload({ Key: (this.previewService.organizationId.getValue() + '/content/info/' + new Date().getTime() + fileName), Bucket: AWS.bucketName, Body: file, ACL: 'public-read', ContentType: 'application/msword' });
      }

      else if (fileName.slice(fileName.length - this.sCurExtension.length, fileName.length).toLowerCase() === '.docx') {
        upload = AWS.s3.upload({ Key: (this.previewService.organizationId.getValue() + '/content/info/' + new Date().getTime() + fileName), Bucket: AWS.bucketName, Body: file, ACL: 'public-read', ContentType: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' });
      }
      else if (fileName.slice(fileName.length - this.sCurExtension.length, fileName.length).toLowerCase() === '.ppt') {
        upload = AWS.s3.upload({ Key: (this.previewService.organizationId.getValue() + '/content/info/' + new Date().getTime() + fileName), Bucket: AWS.bucketName, Body: file, ACL: 'public-read', ContentType: 'application/vnd.ms-powerpoint' });
      }

      else if (fileName.slice(fileName.length - this.sCurExtension.length, fileName.length).toLowerCase() === '.pptx') {
        upload = AWS.s3.upload({ Key: (this.previewService.organizationId.getValue() + '/content/info/' + new Date().getTime() + fileName), Bucket: AWS.bucketName, Body: file, ACL: 'public-read', ContentType: 'application/vnd.openxmlformats-officedocument.presentationml.presentation' });
      }

      else if (fileName.slice(fileName.length - this.sCurExtension.length, fileName.length).toLowerCase() === '.xlsx') {
        upload = AWS.s3.upload({ Key: (this.previewService.organizationId.getValue() + '/content/info/' + new Date().getTime() + fileName), Bucket: AWS.bucketName, Body: file, ACL: 'public-read', ContentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      }

      else if (fileName.slice(fileName.length - this.sCurExtension.length, fileName.length).toLowerCase() === '.csv') {
        upload = AWS.s3.upload({ Key: (this.previewService.organizationId.getValue() + '/content/info/' + new Date().getTime() + fileName), Bucket: AWS.bucketName, Body: file, ACL: 'public-read', ContentType: 'text/csv' });
      }
      else {
        // console.log('eleselelselselselse');
        upload = AWS.s3.upload({ Key: (this.previewService.organizationId.getValue() + '/content/info/' + new Date().getTime() + fileName), Bucket: AWS.bucketName, Body: file, ACL: 'public-read' });
      }

    }
    let promise = upload.promise();
    return promise.then((data) => {
      // console.log(data);
      return data.Location;
    }, (err) => {
      // console.log(err);
    });
  }
  createContent() {
    if (!this.validateContentForm()) {
      return;
    }

    // console.log('sub:', this.categorySelection.selectedSubCategory);
    this._contentService.formDataObj.content = {
      contentType: this.contentTypeSelection.selectedType,
      targetCategory: this.categorySelection.selectedCategory,
      targetSubCategory: this.categorySelection.selectedSubCategory,
      colorTheme: this.contentToEdit.id,
      contentName: this.contentName,
      contentDescription: this.contentDescription,
      imageDetails: {
        url: this.url,
        name: this.imageName
      },
      mediaType: this.categorySelection.selectedContentType
    }
    // console.log(this._contentService.formDataObj);
    this.loadNext();
  }


  // ----------------------code for create content page 1 ends----------------------------------------------------

  // ----------------------code for new slide page 2 starts----------------------------------------------------
  @ViewChild('slideImageFileInput') slideImageInput: ElementRef;
  @ViewChild('slideVideoFileInput') slideVideoInput: ElementRef;
  @ViewChild('slideAudioFileInput') slideAudioInput: ElementRef;
  @ViewChild('slideDocFileInput') slideDocInput: ElementRef;
  @ViewChild('slideZipFileInput') slideZipInput: ElementRef;
  contentSlideUrl = '';
  slideImageName = '';
  slideVideoName = '';
  slideAudioName = '';
  slideDocName = '';
  slideTitleName = '';
  slideContentDescription = '';
  videoURL;
  slideZipName = '';
  videoUrl() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.contentSlideUrl);
  }
  browseSlideImageClicked() {
    let el: any = this.slideImageInput.nativeElement;
    el.click();
    this.contentImageUrl = '';
  }
  browseSlideVideoClicked() {
    let el: any = this.slideVideoInput.nativeElement;
    el.click();
    this.contentVideoUrl = '';
  }
  browseSlideAudioClicked() {
    let el: any = this.slideAudioInput.nativeElement;
    el.click();
    this.contentAudioUrl = '';
  }
  browseSlideZipClicked() {
    let el: any = this.slideZipInput.nativeElement;
    el.click();
    this.contentAudioUrl = '';
  }
  browseSlideDocClicked() {
    let el: any = this.slideDocInput.nativeElement;
    el.click();
    this.contentDocUrl = '';
  }
  selectContentType(type) {

    let contentTypeInput;
    contentTypeInput = document.getElementById('selectContentType');
    if (contentTypeInput.classList.contains('error')) {
      contentTypeInput.classList.remove('error');
    }
    this.categorySelection.selectedContentType = type;
    // console.log('ctype:', this.categorySelection.selectedContentType)
  }
  selectOperationType(type) {
    let contentTypeInput;
    contentTypeInput = document.getElementById('selectChecklistType');
    if (contentTypeInput.classList.contains('error')) {
      contentTypeInput.classList.remove('error');
    }
    this.categorySelection.selectedOperationType = type;
    this.categorySelection.operationTypePopupVisible = false;
  }

  contentSlideZipChanged(event) {
    let timerInterval;
    let timerInterval2;
    let _URL = window.URL;
    let error = false;
    const file = event.target.files[0];
    this.slideZipName = event.target.files[0].name;
    if (event.target.files && event.target.files[0]) {
      let sFileName = event.target.files[0].name;
      // console.log('sfilename: ', event.target.id);
      // console.log('sfilename Type: ', this.categorySelection.selectedContentType);


      let blnValid = false;
      for (let j = 0; j < this.validFileExtensionsZip.length; j++) {
        this.sCurExtension = this.validFileExtensionsZip[j];
        if (sFileName.slice(sFileName.length - this.sCurExtension.length, sFileName.length).toLowerCase() === this.sCurExtension.toLowerCase()) {
          blnValid = true;
          break;
        }
      }
      if (!blnValid) {
        swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensionsZip.join(','));
        return false;
      }
      // console.log('size:', event.target.files[0].size);
      if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 250000000)) {
        swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 250 Mb');
        return false;
      }
      this.uploader.uploadAll();

      swal({
        title: 'Uploading!',
        onOpen: () => {
          swal.showLoading();
          timerInterval2 = setInterval(() => {
            if (this.contentZipUrl) {
              // console.log('contentZipUrl: ', this.contentZipUrl);
              swal.close();
              clearInterval(timerInterval2);
              // console.log('time2');
            }
          }, 100)
        },
        onClose: () => {
          window.clearInterval(timerInterval2)
        }
      });
    }
  }
  contentSlideInfoChanged(event) {
    let timerInterval;
    let timerInterval2;
    let _URL = window.URL;
    let error = false;
    const file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      let sFileName = event.target.files[0].name;
      // console.log('sfilename: ', event.target.id);
      // console.log('sfilename Type: ', this.categorySelection.selectedContentType);
      if (event.target.id === 'contentSlidePhoto') {
        let blnValid = false;
        for (let j = 0; j < this.validFileExtensions.length; j++) {
          this.sCurExtension = this.validFileExtensions[j];
          if (sFileName.slice(sFileName.length - this.sCurExtension.length, sFileName.length).toLowerCase() === this.sCurExtension.toLowerCase()) {
            blnValid = true;
            break;
          }
        }
        if (!blnValid) {
          swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensions.join(','));
          return false;
        }
        // console.log('size:', event.target.files[0].size);
        if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 2000000)) {
          swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 2 Mb');
          return false;
        }
        let reader = new FileReader();
        // Initiate the FileReader object.
        // Read the contents of Image File.
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (e) => {
          let that = file
          //Initiate the JavaScript Image object.
          let image = new Image();
          //Set the Base64 string return from FileReader as source.
          image.src = _URL.createObjectURL(file);
          image.onload = () => {
            //Determine the Height and Width.
            let height = image.height;
            let width = image.width;
            swal({
              title: 'Uploading!',
              onOpen: () => {
                swal.showLoading();
                Promise.resolve(this.uploadFile(file)).then(location => {
                  this.slideImageName = event.target.files[0].name;
                  this.contentImageUrl = <any>location
                });
                timerInterval = setInterval(() => {
                  if (this.contentImageUrl) {
                    swal.close();
                    clearInterval(timerInterval);
                    // console.log('time');
                  }
                }, 100)
              },
              onClose: () => {
                window.clearInterval(timerInterval)
              }
            });
            return true;
          };
        }

      }

      if (event.target.id === 'contentSlideVideo') {
        let blnValid = false;
        for (let j = 0; j < this.validFileExtensionsVideo.length; j++) {
          this.sCurExtension = this.validFileExtensionsVideo[j];
          if (sFileName.slice(sFileName.length - this.sCurExtension.length, sFileName.length).toLowerCase() === this.sCurExtension.toLowerCase()) {
            blnValid = true;
            break;
          }
        }
        if (!blnValid) {
          swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensionsVideo.join(','));
          return false;
        }
        // console.log('size:', event.target.files[0].size);
        if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 25000000)) {
          swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 25 Mb');
          return false;
        }
        // console.log('contentSlideUrl', this.contentVideoUrl);
        swal({
          title: 'Uploading!',
          onOpen: () => {
            swal.showLoading();
            Promise.resolve(this.uploadFile(file)).then(location => {
              this.contentVideoUrl = <any>location;
              this.slideVideoName = event.target.files[0].name;
              // console.log('contentVideoUrl on loca', this.contentVideoUrl);
            }
            );
            timerInterval2 = setInterval(() => {
              if (this.contentVideoUrl) {
                // console.log('contentVideoUrl: ', this.contentVideoUrl);
                swal.close();
                clearInterval(timerInterval2);
                // console.log('time2');
              }
            }, 100)
          },
          onClose: () => {
            window.clearInterval(timerInterval2)
          }
        });
      }


      if (event.target.id === 'contentSlideAudio') {
        let blnValid = false;
        for (let j = 0; j < this.validFileExtensionsAudio.length; j++) {
          this.sCurExtension = this.validFileExtensionsAudio[j];
          if (sFileName.slice(sFileName.length - this.sCurExtension.length, sFileName.length).toLowerCase() === this.sCurExtension.toLowerCase()) {
            blnValid = true;
            break;
          }
        }
        if (!blnValid) {
          swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensionsAudio.join(','));
          return false;
        }
        // console.log('size:', event.target.files[0].size);
        if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 25000000)) {
          swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 20 Mb');
          return false;
        }
        // console.log('contentAudioUrl', this.contentAudioUrl);
        swal({
          title: 'Uploading!',
          onOpen: () => {
            swal.showLoading();
            Promise.resolve(this.uploadFile(file)).then(location => {
              this.contentAudioUrl = <any>location;
              this.slideAudioName = event.target.files[0].name;
              // console.log('contentAudioUrl on loca', this.contentAudioUrl);
            }
            );
            timerInterval2 = setInterval(() => {
              if (this.contentAudioUrl) {
                // console.log('contentAudioUrl: ', this.contentAudioUrl);
                swal.close();
                clearInterval(timerInterval2);
                // console.log('time2');
              }
            }, 100)
          },
          onClose: () => {
            window.clearInterval(timerInterval2)
          }
        });
      }

      if (event.target.id === 'contentSlideDoc') {
        let blnValid = false;
        for (let j = 0; j < this.validFileExtensionsDoc.length; j++) {
          this.sCurExtension = this.validFileExtensionsDoc[j];
          if (sFileName.slice(sFileName.length - this.sCurExtension.length, sFileName.length).toLowerCase() === this.sCurExtension.toLowerCase()) {
            blnValid = true;
            break;
          }
        }
        if (!blnValid) {
          swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensionsDoc.join(','));
          return false;
        }
        // console.log('size:', event.target.files[0].size);
        if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 25000000)) {
          swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 20 Mb');
          return false;
        }
        // console.log('contentDocUrl', this.contentDocUrl);
        swal({
          title: 'Uploading!',
          onOpen: () => {
            swal.showLoading();
            Promise.resolve(this.uploadFile(file)).then(location => {
              this.contentDocUrl = <any>location;
              this.slideDocName = event.target.files[0].name;
              // console.log('contentDocUrl on loca', this.contentDocUrl);
            }
            );
            timerInterval2 = setInterval(() => {
              if (this.contentDocUrl) {
                // console.log('contentDocUrl: ', this.contentDocUrl);
                swal.close();
                clearInterval(timerInterval2);
                // console.log('time2');
              }
            }, 100)
          },
          onClose: () => {
            window.clearInterval(timerInterval2)
          }
        });
      }
    }
  }

  contentSlideImageChanged(event) {
    let timerInterval;
    let timerInterval2;
    let _URL = window.URL;
    let error = false;
    const file = event.target.files[0];

    if (event.target.files && event.target.files[0]) {

      this.slideImageName = event.target.files[0].name;
      let sFileName = event.target.files[0].name;
      // console.log('sfilename: ', sFileName);
      // console.log('sfilename Type: ', this.categorySelection.selectedContentType);

      if (this.categorySelection.selectedContentType === 'BLOG' || this.categorySelection.selectedOperationType === 'IMAGE') {

        let blnValid = false;
        for (let j = 0; j < this.validFileExtensions.length; j++) {
          this.sCurExtension = this.validFileExtensions[j];
          if (sFileName.slice(sFileName.length - this.sCurExtension.length, sFileName.length).toLowerCase() === this.sCurExtension.toLowerCase()) {
            blnValid = true;
            break;
          }
        }
        if (!blnValid) {
          swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensions.join(','));
          return false;
        }
        // console.log('size:', event.target.files[0].size);
        if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 2500000)) {
          swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 2 Mb');
          return false;
        }

        let reader = new FileReader();


        // Initiate the FileReader object.
        // Read the contents of Image File.
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (e) => {
          let that = file
          //Initiate the JavaScript Image object.
          let image = new Image();
          //Set the Base64 string return from FileReader as source.
          image.src = _URL.createObjectURL(file);
          image.onload = () => {
            //Determine the Height and Width.
            let height = image.height;
            let width = image.width;

            swal({
              title: 'Uploading!',
              onOpen: () => {
                swal.showLoading();
                Promise.resolve(this.uploadFile(file)).then(location =>
                  this.contentSlideUrl = <any>location
                );
                timerInterval = setInterval(() => {
                  if (this.contentSlideUrl) {
                    swal.close();
                    clearInterval(timerInterval);
                    // console.log('time');
                  }
                }, 100)
              },
              onClose: () => {
                window.clearInterval(timerInterval)
              }
            });
            return true;
          };
        }

      }

      if (this.categorySelection.selectedContentType === 'VIDEO' || this.categorySelection.selectedOperationType === 'VIDEO') {

        let blnValid = false;
        for (let j = 0; j < this.validFileExtensionsVideo.length; j++) {
          this.sCurExtension = this.validFileExtensionsVideo[j];
          if (sFileName.slice(sFileName.length - this.sCurExtension.length, sFileName.length).toLowerCase() === this.sCurExtension.toLowerCase()) {
            blnValid = true;
            break;
          }
        }
        if (!blnValid) {
          swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensionsVideo.join(','));
          return false;
        }
        // console.log('size:', event.target.files[0].size);
        if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 25000000)) {
          swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 20 Mb');
          return false;
        }
        // console.log('contentSlideUrl', this.contentSlideUrl);
        swal({
          title: 'Uploading!',
          onOpen: () => {
            swal.showLoading();
            Promise.resolve(this.uploadFile(file)).then(location => {
              this.contentSlideUrl = <any>location;
              // console.log('contentSlideUrl on loca', this.contentSlideUrl);
            }
            );
            timerInterval2 = setInterval(() => {
              if (this.contentSlideUrl) {
                // console.log('contentSlideUrl: ', this.contentSlideUrl);
                swal.close();
                clearInterval(timerInterval2);
                // console.log('time2');
              }
            }, 100)
          },
          onClose: () => {
            window.clearInterval(timerInterval2)
          }
        });
      }


    }
  }
  createContentSlideObj(id) {
    return {
      slideTitleName: this.slideTitleName,
      slideContentDescription: this.slideContentDescription,
      slideImageDetails: {
        url: this.contentSlideUrl || this.slideUrl,
        name: this.slideImageName || 'file'
      },
      id: id,
      contentType: this.categorySelection.selectedContentType || this.categorySelection.selectedOperationType,
    }
  }

  updateContentSlide() {
    if (!this.validateContentForm2()) {
      return;
    }
    this._contentService.updateContentSlide(this.selectedSlide.id, this.createContentSlideObj(this.selectedSlide.id));

    let contentArr = this.contentArr;
    for (let i = 0; i < contentArr.length; i++) {
      if (contentArr[i].id === this.selectedSlide.id) {
        contentArr[i] = {
          slideTitleName: this.slideTitleName,
          slideContentDescription: this.slideContentDescription,
          type: 'contentSlide',
          id: this.selectedSlide.id,
          contentType: this.categorySelection.selectedContentType || this.categorySelection.selectedOperationType
        }
      }
    }

    this.slideTitleName = '';
    this.slideContentDescription = '';
    this.contentSlideUrl = '';
    this.slideImageName = '';

    this.editMode = false;
  }

  createContentSlide(id) {
    this._contentService.formDataObj.contentSlides.push({
      slideTitleName: this.slideTitleName,
      slideContentDescription: this.slideContentDescription,
      slideImageDetails: {
        url: this.contentSlideUrl || this.slideUrl,
        name: this.slideImageName || 'file'
      },
      id: id,
      contentType: this.categorySelection.selectedContentType || this.categorySelection.selectedOperationType,
      mandatory: this.mandatory
    });
  }

  createInfoContentSlide(id) {
    this._contentService.formDataObj.contentSlides.push({
      slideTitleName: this.slideTitleName,
      slideContentDescription: this.slideContentDescription,
      slideImageDetails: {
        url: this.contentImageUrl || this.imageUrl,
        name: this.slideImageName || 'file'
      },
      slideVideoDetails: {
        url: this.contentVideoUrl || this.videoUrls,
        name: this.slideVideoName || 'file'
      },
      slideAudioDetails: {
        url: this.contentAudioUrl || this.audioUrl,
        name: this.slideAudioName || 'file'
      },
      slideDocDetails: {
        url: this.contentDocUrl || this.docUrl,
        name: this.slideDocName || 'file'
      },
      id: id,
      contentType: this.categorySelection.selectedContentType || this.categorySelection.selectedOperationType,
      mandatory: this.mandatory
    });
    // console.log('infoCards:', this._contentService.formDataObj);
  }


  saveContentSlide() {
    if (!this.validateContentForm2()) {
      // console.log('Form :', this.validateContentForm2())
      return;
    }
    let id = this.slideTitleName + this.slideContentDescription + Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000;
    // console.log('slideTitleName1:', this.slideTitleName)
    this.notificationTitle = this.slideTitleName;
    this.contentArr.push({
      slideContentDescription: this.slideContentDescription,
      contentType: this.categorySelection.selectedContentType || this.categorySelection.selectedOperationType,
      slideTitleName: this.slideTitleName,
      slideImageDetails: {
        url: this.contentSlideUrl || this.slideUrl,
        name: this.slideImageName || 'file'
      },
      type: 'contentSlide',
      id: id
    })

    // console.log('carraY:', this.contentArr);

    this.createContentSlide(id);
    this.slideTitleName = '';
    this.slideContentDescription = '';
    this.contentSlideUrl = '';
    this.slideImageName = '';
    this.mandatory = false;
    this.slideUrl = '';
    this.categorySelection.selectedContentType = '';
    this.categorySelection.selectedFileInputType = '';
    this.el.innerHTML = '';
  }

  createHTMLSlide(id) {
    this._contentService.formDataObj.contentSlides.push({
      slideTitleName: this.slideTitleName,
      slideContentDescription: this.slideContentDescription,
      slideImageDetails: {
        url: this.contentSlideUrl || this.slideUrl,
        name: this.slideImageName || 'file'
      },
      slideZipUrl: this.contentZipUrl,
      id: id,
      contentType: this.categorySelection.selectedContentType || this.categorySelection.selectedOperationType,
      mandatory: this.mandatory
    });
  }

  saveHTMLSlide() {

    if (!this.validateContentForm2()) {
      return;
    }
    let id = this.slideTitleName + Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000;


    this.contentArr.push({
      slideContentDescription: this.slideContentDescription || '',
      contentType: this.categorySelection.selectedContentType || this.categorySelection.selectedOperationType,
      slideTitleName: this.slideTitleName,
      slideZipUrl: this.contentZipUrl,
      slideImageDetails: {
        url: this.contentSlideUrl || this.slideUrl,
        name: this.slideImageName || 'file'
      },
      type: 'contentSlide',
      id: id
    })

    // console.log('carraY:', this.contentArr);

    this.createHTMLSlide(id);
    this.slideTitleName = '';
    this.slideContentDescription = '';
    this.contentSlideUrl = '';
    this.slideImageName = '';
    this.mandatory = false;
    this.slideUrl = '';

  }

  updateHTMLSlide() {
    if (!this.validateContentForm2()) {
      return;
    }
    let id = this.contentToEdit.id;
    this._contentService.formDataObj.contentSlides = [];
    this.contentArr = [];

    this.contentArr.push({
      slideContentDescription: this.slideContentDescription || '',
      contentType: this.categorySelection.selectedContentType || this.categorySelection.selectedOperationType,
      slideTitleName: this.slideTitleName,
      slideZipUrl: this.contentZipUrl,
      slideImageDetails: {
        url: this.contentSlideUrl || this.slideUrl,
        name: this.slideImageName || 'file'
      },
      type: 'contentSlide',
      id: id
    })

    // console.log('carraY:', this.contentArr);

    this.createHTMLSlide(id);
    this.slideTitleName = '';
    this.slideContentDescription = '';
    this.contentSlideUrl = '';
    this.slideImageName = '';
    this.mandatory = false;
    this.slideUrl = '';

  }

  saveInfoCardSlide() {
    if (!this.validateContentForm2()) {
      return;
    }
    let id = this.slideTitleName + this.slideContentDescription + Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000;


    this.contentArr.push({
      slideContentDescription: this.slideContentDescription,
      contentType: this.categorySelection.selectedContentType || this.categorySelection.selectedOperationType,
      slideTitleName: this.slideTitleName,
      slideImageDetails: {
        url: this.contentImageUrl || this.imageUrl,
        name: this.slideImageName || 'file'
      },
      slideVideoDetails: {
        url: this.contentVideoUrl || this.videoUrls,
        name: this.slideVideoName || 'file'
      },
      slideAudioDetails: {
        url: this.contentAudioUrl || this.audioUrl,
        name: this.slideAudioName || 'file'
      },
      slideDocDetails: {
        url: this.contentDocUrl || this.docUrl,
        name: this.slideDocName || 'file'
      },
      type: 'contentSlide',
      id: id
    })

    // console.log('carraY:', this.contentArr);

    this.createInfoContentSlide(id);
    this.slideTitleName = '';
    this.slideContentDescription = '';
    this.contentImageUrl = '';
    this.slideImageName = '';
    this.contentVideoUrl = '';
    this.slideVideoName = '';
    this.contentAudioUrl = '';
    this.slideAudioName = '';
    this.contentDocUrl = '';
    this.slideDocName = '';
    this.mandatory = false;
  }

  updateInfoCardSlide() {
    if (!this.validateContentForm2()) {
      return;
    }
    let id = this.contentToEdit.id;
    this._contentService.formDataObj.contentSlides = [];
    this.contentArr = [];
    this.contentArr.push({
      slideContentDescription: this.slideContentDescription,
      contentType: this.categorySelection.selectedContentType || this.categorySelection.selectedOperationType,
      slideTitleName: this.slideTitleName,
      slideImageDetails: {
        url: this.contentImageUrl || this.imageUrl,
        name: this.slideImageName || 'file'
      },
      slideVideoDetails: {
        url: this.contentVideoUrl || this.videoUrls,
        name: this.slideVideoName || 'file'
      },
      slideAudioDetails: {
        url: this.contentAudioUrl || this.audioUrl,
        name: this.slideAudioName || 'file'
      },
      slideDocDetails: {
        url: this.contentDocUrl || this.docUrl,
        name: this.slideDocName || 'file'
      },
      type: 'contentSlide',
      id: id
    })


    // console.log('infoCards:', this._contentService.formDataObj);

    // console.log('carraY:', this.contentArr);

    this.createInfoContentSlide(id);
    this.slideTitleName = '';
    this.slideContentDescription = '';
    this.contentImageUrl = '';
    this.slideImageName = '';
    this.contentVideoUrl = '';
    this.slideVideoName = '';
    this.contentAudioUrl = '';
    this.slideAudioName = '';
    this.contentDocUrl = '';
    this.slideDocName = '';
    this.mandatory = false;
  }



  // ----------------------code for new slide page 2 ends----------------------------------------------------


  // ----------------------code for question slide page 3 starts----------------------------------------------------
  @ViewChild('QuestionImageFileInput') QuestionImageInput: ElementRef;
  questionImageName = '';
  questionTitleName = '';
  negativeMarks = 0;
  questionSubTitleName = '';
  correctAnswer = '';
  incorrectAnswers = [{ value: '' }];
  options = [{ value: '', marks: 0 }];
  timeLimit = '20';
  outOfTimeMessage = 'Out of Time';
  stars = '5';
  QuestionUrl = '';
  selectedSlide: any = {};
  editMode = false;
  question = {
    randomize: false,
    mandatory: false,
    anonymous: false,
    negative: false
  }
  accordian = {
    correct: false,
    incorrect: false,
    timer: false,
    stars: false,
    option: false,
    negative: false
  }
  negative = 0;
  questionTypes = ['IMAGE', 'TEXT', 'OPTION'];
  questionTypesQuiz = ['OPTION', 'MULTI-CORRECT OPTION'];

  selectQuestionType(type) {
    this.categorySelection.selectedQuestionType = type;
  }
  browseQuestionImageClicked() {
    let el: any = this.QuestionImageInput.nativeElement;
    el.click();
  }

  addOption(i, e) {
    this.incorrectAnswers[i].value = e.target.value;
  }

  addOptionQuiz(i, e) {
    this.options[i].value = e.target.value;
  }
  addOptionMarks(i, e) {
    this.options[i].marks = e.target.value;
  }
  randomizeCheckboxClicked() {
    this.question.randomize = !this.question.randomize;
  }

  addIncorrectAnswer() {
    this.incorrectAnswers.push({ value: '' });
  }

  addOptionField() {
    this.options.push({ value: '', marks: 0 });
  }

  deleteIncorrectAnswer(id) {
    this.incorrectAnswers.splice(id, 1);
  }

  deleteOption(id) {
    this.options.splice(id, 1);
  }

  createContentQuestionObj(id) {
    return {
      title: this.questionTitleName,
      subTitle: this.questionSubTitleName,
      randomize: this.question.randomize,
      mandatory: this.question.mandatory,
      anonymous: this.question.anonymous,
      negative: this.question.negative,
      options: this.options,
      correctAnswer: this.correctAnswer,
      incorrectAnswers: this.incorrectAnswers,
      timer: {
        timeLimit: this.timeLimit,
        outOfTimePrompt: this.outOfTimeMessage
      },
      stars: this.stars,
      id: id,
      qType: this.categorySelection.selectedQuestionType
    }
  }

  updateQuestionSlide() {
    this._contentService.updateQuestionSlide(this.selectedSlide.id, this.createContentQuestionObj(this.selectedSlide.id));

    let contentArr = this.contentArr;
    for (let i = 0; i < contentArr.length; i++) {
      if (contentArr[i].id === this.selectedSlide.id) {
        contentArr[i] = {
          slideTitleName: this.questionTitleName,
          slideContentDescription: this.questionSubTitleName,
          randomize: this.question.randomize,
          mandatory: this.question.mandatory,
          anonymous: this.question.anonymous,
          negative: this.question.negative,
          correctAnswer: this.correctAnswer,
          options: [...this.options],
          incorrectAnswers: [...this.incorrectAnswers],
          imageUrl: this.QuestionUrl,
          timer: {
            timeLimit: this.timeLimit,
            outOfTimePrompt: this.outOfTimeMessage
          },
          stars: this.stars,
          type: 'questionSlide',
          id: this.selectedSlide.id
        }
      }
    }

    this.questionTitleName = ''
    this.questionSubTitleName = '';
    this.question.randomize = false;
    this.question.mandatory = false
    this.question.anonymous = false;
    this.question.negative = false;
    this.correctAnswer = '';
    this.options = [{ value: '', marks: 0 }]
    this.incorrectAnswers = [{ value: '' }];
    this.timeLimit = '';
    this.outOfTimeMessage = '';
    this.stars = '';

    this.editMode = false;
  }

  QuestionImageChanged(event) {
    this.categorySelection.selectedContentType = 'IMAGE';
    let _URL = window.URL;
    let timerInterval;
    let error = false;
    if (event.target.files && event.target.files[0]) {

      this.questionImageName = event.target.files[0].name;
      let sFileName = event.target.files[0].name;
      // console.log('sfilename: ', sFileName);
      let blnValid = false;
      for (let j = 0; j < this.validFileExtensions.length; j++) {
        this.sCurExtension = this.validFileExtensions[j];
        if (sFileName.slice(sFileName.length - this.sCurExtension.length, sFileName.length).toLowerCase() === this.sCurExtension.toLowerCase()) {
          blnValid = true;
          break;
        }
      }
      if (!blnValid) {
        swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensions.join(', '));
        return false;
      }
      // console.log('size:', event.target.files[0].size);
      if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 2000000)) {
        swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 2 Mb');
        return false;
      }



      const file = event.target.files[0];




      let reader = new FileReader();


      //Initiate the FileReader object.
      //Read the contents of Image File.
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e) => {
        //Initiate the JavaScript Image object.
        let image = new Image();
        //Set the Base64 string return from FileReader as source.
        image.src = _URL.createObjectURL(file);
        image.onload = () => {
          //Determine the Height and Width.
          let height = image.height;
          let width = image.width;
          swal({
            title: 'Uploading!',
            onOpen: () => {
              swal.showLoading();
              Promise.resolve(this.uploadFile(file)).then(location =>
                this.QuestionUrl = <any>location
              );
              timerInterval = setInterval(() => {
                if (this.QuestionUrl) {
                  swal.close();
                  clearInterval(timerInterval);
                  // console.log('time');
                }
              }, 100)
            },
            onClose: () => {
              window.clearInterval(timerInterval)
            }
          });
          return true;
        };
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  enableHtml() {
    let contentTypeInput;
    contentTypeInput = document.getElementById('descriptionSlide');
    if (contentTypeInput.classList.contains('error')) {
      contentTypeInput.classList.remove('error');
    }
    this.htmlContent = document.getElementById('htmlContent');
    this.el = this.htmlContent.lastChild;
    this.el.innerHTML = this.slideContentDescription;
    //this.htmlContent.appendChild(this.el);
    if (this.slideContentDescription.length > 5000) {
      swal('Maximum Character Length Reached', 'Content can not be more than 5000 characters', 'error').then(a => {
        this.slideContentDescription = this.slideContentDescription.slice(0, 5000);
      })
    }
  }
  applyStyles() {
    this.htmlContent = document.getElementById('htmlContent');
    this.el = this.htmlContent.lastChild;
    this.el.innerHTML = this.slideContentDescription;
  }

  createContentQuestion(id) {
    this._contentService.formDataObj.contentQuestions.push({
      title: this.questionTitleName,
      subTitle: this.questionSubTitleName,
      randomize: this.question.randomize,
      mandatory: this.question.mandatory,
      anonymous: this.question.anonymous,
      negative: this.question.negative,
      correctAnswer: this.correctAnswer,
      options: this.options,
      incorrectAnswers: [...this.incorrectAnswers],
      imageUrl: this.QuestionUrl,
      timer: {
        timeLimit: this.timeLimit,
        outOfTimePrompt: this.outOfTimeMessage
      },
      stars: this.stars,
      id: id,
      qType: this.categorySelection.selectedQuestionType
    });
    // console.log(this._contentService.formDataObj);
    this.questionImageName = '';
    this.questionTitleName = '';
    this.questionSubTitleName = '';
    this.correctAnswer = '';
    this.question.negative = false;
    this.negative = 0;
    this.options = [{ value: '', marks: 0 }];
    this.incorrectAnswers = [{ value: '' }];
    this.timeLimit = '';
    this.outOfTimeMessage = '';
    this.stars = '';
    this.QuestionUrl = '';

  }

  saveQuestionSlide() {

    if (!this.validateContentForm3()) {
      return;
    }

    let id = this.questionTitleName + this.questionSubTitleName + Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000;

    this.contentArr.push({

      qType: this.categorySelection.selectedQuestionType,
      slideTitleName: this.questionTitleName,
      slideContentDescription: this.questionSubTitleName,
      randomize: this.question.randomize,
      mandatory: this.question.mandatory,
      anonymous: this.question.anonymous,
      negative: this.question.negative,
      negativeMarks: this.negative,
      correctAnswer: this.correctAnswer,
      incorrectAnswers: [...this.incorrectAnswers],
      options: [...this.options],
      imageUrl: this.QuestionUrl,
      timer: {
        timeLimit: this.timeLimit,
        outOfTimePrompt: this.outOfTimeMessage
      },
      stars: this.stars,
      id: id,
      type: 'questionSlide'
    })

    this.createContentQuestion(id);
  }

  // ----------------------code for question slide page 3 ends----------------------------------------------------





  //additional side bar functions
  addQuestionSlide() {
    this.questionTitleName = '';
    this.questionSubTitleName = '';
    this.correctAnswer = '';
    this.incorrectAnswers = [{ value: '' }];
    this.timeLimit = '';
    this.outOfTimeMessage = '';
    this.stars = '';

    this.question = {
      randomize: false,
      mandatory: false,
      anonymous: false,
      negative: false
    }
    this.accordian = {
      correct: false,
      incorrect: false,
      timer: false,
      stars: false,
      option: false,
      negative: false

    }

    this.pagesObj = {
      currentPage: 'questionSlide',
      pagesArr: ['newContent', 'contentSlide', 'questionSlide'],
      status: {
        newContent: false,
        contentSlide: false,
        questionSlide: true
      }
    }
  }

  addContentSlide() {
    this.contentSlideUrl = '';
    this.slideImageName = '';
    this.slideTitleName = '';
    this.slideContentDescription = '';

    this.pagesObj = {
      currentPage: 'contentSlide',
      pagesArr: ['newContent', 'contentSlide', 'questionSlide'],
      status: {
        newContent: false,
        contentSlide: true,
        questionSlide: false
      }
    }
    this.mandatory = false;
  }

  deleteSlide(id) {


    let deletedSlide: any = this.contentArr[id];
    this._contentService.deleted.push(deletedSlide);
    this.contentArr.splice(id, 1);
    if (deletedSlide.type === 'questionSlide') {
      this._contentService.deleteQuestionSlide(deletedSlide);
      if (this.selectedSlide.id === deletedSlide.id) {
        this.editMode = false;
        this.questionTitleName = ''
        this.questionSubTitleName = '';
        this.question.randomize = false;
        this.question.mandatory = false
        this.question.anonymous = false;
        this.question.negative = false;
        this.correctAnswer = '';
        this.incorrectAnswers = [{ value: '' }];
        this.timeLimit = '';
        this.outOfTimeMessage = '';
        this.stars = '';
      }
    } else {
      this._contentService.deleteContentSlide(deletedSlide);
      if (this.selectedSlide.id === deletedSlide.id) {
        this.editMode = false;
        this.slideTitleName = '';
        this.slideContentDescription = '';
        this.contentSlideUrl = '';
        this.slideImageName = '';
      }
    }
  }

  setInfoSlide(slide) {



    this.contentSlideUrl = slide.slideImageDetails.url || '';
    this.slideImageName = slide.slideImageDetails.name || '';
    this.slideTitleName = slide.slideTitleName || '';
    this.slideContentDescription = slide.slideContentDescription || '';
    this.categorySelection.selectedContentType = slide.contentType;
    this.categorySelection.selectedOperationType = slide.contentType;
    this.contentImageUrl = slide.slideImageDetails.url;
    this.contentAudioUrl = slide.slideAudioDetails.url;
    this.contentVideoUrl = slide.slideVideoDetails.url;
    this.contentDocUrl = slide.slideDocDetails.url;
    this.slideImageName = slide.slideImageDetails.name;
    this.slideVideoName = slide.slideVideoDetails.name;
    this.slideAudioName = slide.slideAudioDetails.name;
    this.slideDocName = slide.slideDocDetails.name
    this.pagesObj = {
      currentPage: 'contentSlide',
      pagesArr: ['newContent', 'contentSlide', 'questionSlide'],
      status: {
        newContent: false,
        contentSlide: true,
        questionSlide: false
      }
    }
  }

  selectSlide(i) {
    this.editMode = true;
    let selectedSlide = this.contentArr[i];
    this.selectedSlide = selectedSlide;
    if (selectedSlide.slideAudioDetails) {
      this.setInfoSlide(this._contentService.getContentSlide(selectedSlide.id));
    }
    if (selectedSlide.type === 'questionSlide') {
      this.setQuestionSlide(this._contentService.getQuestionSlide(selectedSlide.id));
    } else {
      this.setContentSlide(this._contentService.getContentSlide(selectedSlide.id));
    }
  }

  setQuestionSlide(slide) {
    // console.log('slide:', slide)
    this.questionTitleName = slide.title || '';
    this.questionSubTitleName = slide.subTitle || '';
    this.correctAnswer = slide.correctAnswer || '';
    this.negative = slide.negative;
    this.incorrectAnswers = slide.incorrectAnswers || [{ value: '' }];
    this.options = slide.options || [];
    this.timeLimit = (slide.timer && slide.timer.timeLimit !== '') ? slide.timer.timeLimit : '';
    this.outOfTimeMessage = (slide.timer && slide.timer.outOfTimePrompt !== '') ? slide.timer.outOfTimePrompt : '';
    this.stars = slide.stars || '';
    this.categorySelection.selectedQuestionType = slide.qType || '';
    this.question = {
      randomize: slide.randomize,
      mandatory: slide.mandatory,
      anonymous: slide.anonymous,
      negative: slide.negative
    }
    this.accordian = {
      correct: false,
      incorrect: false,
      timer: false,
      stars: false,
      option: false,
      negative: false

    }

    this.pagesObj = {
      currentPage: 'questionSlide',
      pagesArr: ['newContent', 'contentSlide', 'questionSlide'],
      status: {
        newContent: false,
        contentSlide: false,
        questionSlide: true
      }
    }
  }

  setContentSlide(slide) {
    // console.log('ARRAY', slide);
    this.contentSlideUrl = slide.slideImageDetails.url || '';
    this.slideImageName = slide.slideImageDetails.name || '';
    this.slideTitleName = slide.slideTitleName || '';
    this.slideContentDescription = slide.slideContentDescription || '';
    this.categorySelection.selectedContentType = slide.contentType;
    this.categorySelection.selectedOperationType = slide.contentType;
    this.contentZipUrl = slide.slideZipUrl || '';
    this.slideZipName = slide.slideTitleName || '';
    this.pagesObj = {
      currentPage: 'contentSlide',
      pagesArr: ['newContent', 'contentSlide', 'questionSlide'],
      status: {
        newContent: false,
        contentSlide: true,
        questionSlide: false
      }
    }
  }




  loadNext() {
    let currentIndex = 0;
    for (let i = 0; i < this.pagesObj.pagesArr.length; i++) {
      if (this.pagesObj.pagesArr[i] === this.pagesObj.currentPage) {
        currentIndex = i;
      }
    }
    if (currentIndex !== 0 && !this.saveData()) {
      return;
    }
    if (currentIndex === this.pagesObj.pagesArr.length - 1) {
      this.saveAndReset();
      return;
    }

    this.pagesObj.currentPage = this.pagesObj.pagesArr[currentIndex + 1];

    for (let key in this.pagesObj.status) {
      this.pagesObj.status[key] = false;
    }

    this.pagesObj.status[this.pagesObj.currentPage] = true;

    if (this.contentTypeSelection.selectedType === 'Survey' || this.contentTypeSelection.selectedType === 'Quiz' || this.contentTypeSelection.selectedType === 'Poll') {

      this.pagesObj.currentPage = 'questionSlide';
      for (let key in this.pagesObj.status) {
        this.pagesObj.status[key] = false;
      }
      this.pagesObj.status[this.pagesObj.currentPage] = true;
    }

  }

  saveData() {
    let retVal = false;
    if (this.pagesObj.status.contentSlide && this.slideTitleName && this.slideContentDescription) {
      this._contentService.contentArr.push({
        title: this.slideTitleName,
        description: this.slideContentDescription,
        type: 'contentSlide',
        contentType: this.categorySelection.selectedContentType || this.categorySelection.selectedOperationType,
        id: this.slideTitleName + this.slideContentDescription + Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000
      });
      //reset slide creation
      this.contentSlideUrl = '';
      this.slideImageName = '';
      this.slideTitleName = '';
      this.slideContentDescription = '';
      return true;
    }
    if (this.pagesObj.status.questionSlide && this.questionTitleName && this.questionSubTitleName) {
      this._contentService.contentArr.push({
        title: this.questionTitleName,
        description: this.questionSubTitleName,
        type: 'questionSlide',
        id: this.questionTitleName + this.questionSubTitleName + Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000,
        qType: this.categorySelection.selectedQuestionType
      });
      this.questionTitleName = '';
      this.questionSubTitleName = '';
      this.correctAnswer = '';
      this.incorrectAnswers = [{ value: '' }];
      this.timeLimit = '';
      this.outOfTimeMessage = '';
      this.stars = '';
      return true;
    }
    return false;
  }

  saveAndReset() {
    this.pagesObj = {
      currentPage: 'newContent',
      pagesArr: ['newContent', 'contentSlide', 'questionSlide'],
      status: {
        newContent: true,
        contentSlide: false,
        questionSlide: false
      }
    }
  }



  resetSlides() {
    //reset slide creation
    this.contentSlideUrl = '';
    this.slideImageName = '';
    this.slideTitleName = '';
    this.slideContentDescription = '';

    //reset question creation
    this.questionTitleName = '';
    this.questionSubTitleName = '';
    this.correctAnswer = '';
    this.incorrectAnswers = [{ value: '' }];
    this.timeLimit = '';
    this.outOfTimeMessage = '';
    this.stars = '';

    this.question = {
      randomize: false,
      mandatory: false,
      anonymous: false,
      negative: false
    }
    this.accordian = {
      correct: false,
      incorrect: false,
      timer: false,
      stars: false,
      option: false,
      negative: false

    }

    // reset create content page
    this.url = '';
    this.imageName = '';
    this.contentName = '';
    this.contentDescription = '';
    this.contentTypeSelection.selectedType = '';

    this.categorySelection = {
      expanded: false,
      categoryPopupVisible: false,
      subcategoryPopupVisible: false,
      contentTypePopupVisible: false,
      selectedCategory: '',
      selectedSubCategory: '',
      selectedContentType: ''
    };

    this.themeSelection = {
      expanded: false,
      selectedTheme: {
        theme: 'lightBlue',
        lightBlue: true,
        blue: false,
        green: false,
        purple: false
      }
    };

    this.contentArr = [];
    this._contentService.contentArr = [];
    this._contentService.deleted = [];
    // console.log(this._contentService.formDataObj)

  }

  getTokens(learners) {
    this.deviceService.getAllDevicesUsingGET().subscribe(res => {
      let result: any = res;
      // console.log('devices:', result);
      for (let i = 0; i < learners.length; i++) {
        for (let j = 0; j < result.length; j++) {
          if ((learners[i]['registeredNumber'] === result[j]['phoneNumber']) && result[j]['fcmToken']) {
            this.tokens.push(result[j]['fcmToken']);
            this.learnerIds.push(learners[i].id);
            // console.log('token+$i: ', result[j]['phoneNumber'], result[j]['fcmToken']);
          }
        }
      }
      do {
        let tok = this.tokens.splice(0, 1000);
        this.http.post(proxyApi + '/subscribe',
          {
            registrationTokens: tok,
            topic: this.previewService.organizationId.getValue()
          }, this.httpOptions).subscribe((response) => {
            // Response is a message ID string.
            // console.log('Successfully sent subscribe:', response);

          }, (error) => {
            // console.log('Error sending subscribe:', error);
          });

      } while ((this.tokens.length / 1000) > 0 || this.tokens.length);
      return this.tokens;
    });
  }

  resetFormObj() {
    this._contentService.formDataObj = {
      content: {
        contentType: '',
        targetCategory: '',
        targetSubCategory: '',
        colorTheme: '',
        contentName: '',
        contentDescription: '',
        imageDetails: {
          url: '',
          name: ''
        }
      },
      contentSlides: [],
      contentQuestions: []
    }
    this.contentArr = [];
  } 
   finishNewsVideo() {
    if (!this.validateContentForm2()) {
      return;
    }
    let deeplink;
    this.saveContentSlide();
    // Promise.resolve(response = await this._contentService.publishNewsVideo()).then(res => {
      // console.log('published:', response);
      // console.log('published res:', res);
      this.http.post(base + '/api/published-contents', {

      'audioLink': null,
      'author': null,
      'category': this._contentService.formDataObj.content.targetCategory,
      'content': this._contentService.formDataObj.contentSlides[0].slideContentDescription,
      'careerPathId': 0,
      'image': null,
      'level': 0,
      'maximumExperince': 0,
      'minimumEducation': 'NOT_APPLICABLE',
      'minimumExperince': 0,
      'postDate': new Date().toISOString(),
      'contentType': this._contentService.formDataObj.contentSlides[0].contentType,
      'copyRight': 'CREATIVE_COMMONS',
      'description': this._contentService.formDataObj.contentSlides[0].slideContentDescription || null,
      'language': 'ENGLISH',
      'moreContent': null,
      'name': this._contentService.formDataObj.content.contentName,
      'organizationId': this.previewService.organizationId.getValue(),
      'pdfLink': null,
      'previewImage': this._contentService.formDataObj.contentSlides[0].contentType.toUpperCase() === 'BLOG' ? this._contentService.formDataObj.contentSlides[0].slideImageDetails.url || null : '',
      'scheduled': null,
      'schemeUrl': this.previewService.Username.getValue(),
      'source': null,
      'state': 'DRAFT',
      'batTests': null,
      'expiry': new Date('1971-01-01'),
      'subCategory': this._contentService.formDataObj.content.targetSubCategory,
      'summary': this._contentService.formDataObj.content.contentDescription,
      'tags': null,
      'thumbnailImage': this._contentService.formDataObj.content.imageDetails.url || null,
      'type': 'REPACKED',
      'unlockScore': 0,
      'value': null,
      'videoLink': this._contentService.formDataObj.contentSlides[0].contentType.toUpperCase() === 'VIDEO' ? this._contentService.formDataObj.contentSlides[0].slideImageDetails.url || null : '',
    }, this.httpOptions).subscribe(res => {
      // this.sendFcmMessage(this.message);
      deeplink = "productiviseapp://productivise/published?category=" + this._contentService.formDataObj.content.targetCategory +"&newsId="+JSON.stringify(res).substring(6,JSON.stringify(res).indexOf(","));
    //  console.log("deeplink : "+ deeplink)
    deeplink = deeplink.split(" ").join("%20");

    //  console.log('slideTitleName:', this.slideTitleName)
     Promise.resolve(this.getTokens(this.learners)).then(tokens => {
      // console.log('tokens:', tokens);
      // console.log('tokensGlobal:', this.tokens);
    }).then(resolved => {
      // console.log('slideTitleName:', this.slideTitleName)
      this.sendUpdate(this.categorySelection.selectedCategory, this.categorySelection.selectedSubCategory, this.screenType);
      this.http.post(proxyApi + '/contentUpdate', {
        'topic': this.previewService.organizationId.getValue(),

        'data': {
          'silent': false,
          'save': true,
          'body': '"'+this.notificationTitle + '" added in '+this.categorySelection.selectedCategory,
          'campaignId': 82551,
          'ctaURL': 'string',
          'description':'Info',
          'image': null,
          'lifeCycleId': 257501,
          'microContentId': 1253,
          'name': this.categorySelection.selectedCategory,
          'scheduled': null,
          'screenType': 'news',
          'sequence': null,
          'sound': new Date().getTime(),
          'specialParameters': deeplink,
          'tags': 'Notify',
          'template': deeplink,
          'title': '',
          'orgId': this.previewService.organizationId.getValue(),
          'groupId': null,
          'userId': null
        }

      }, this.httpOptions).subscribe(res => {
          // console.log("push notification has been sent");
    });
    let url = base + '/api/notifications';
      let tok = this.tokens.splice(0, 10);
    this.http.post(url + String(tok.join()), JSON.parse(JSON.stringify({

      'body': '"'+this.notificationTitle + '" added in '+this.categorySelection.selectedCategory,
      'campaignId': 82551,
      'ctaURL': 'string',
      'description': this.themeSelection.selectedTheme.theme || 'Info',
      'image': null,
      'lifeCycleId': 257501,
      'microContentId': 1253,
      'name': this.categorySelection.selectedCategory,
      'scheduled': null,
      'screenType': 'news',
      'sequence': null,
      'sound': new Date().getTime(),
      'specialParameters': deeplink,
      'tags': 'Notify',
      'template': deeplink,
      'title': '',
      'orgId': this.previewService.organizationId.getValue(),
      'groupId': null,
      'userId': null

    })), this.httpOptions).subscribe(response => {

      // console.log("Successfully saved data to notification devapi");
      
      // console.log('response subscribe:', JSON.stringify(response));
    });
      this.saveAndReset();
      // setTimeout(this._contentService.resetFormObj(), 5000);
      this.resetSlides();
    });
      swal(
        
        this._contentService.formDataObj.content.contentType + ' has been created successfully.',
        'Your content was saved.',
        'success'
      ).then(a =>
        this.resetFormObj()
      );
      
        
    }, error => {
      swal(
        'Error',
        this._contentService.formDataObj.content.contentType + 'could not be created.',
        'error'
      )
    });
        
      

    // })

  }

  finishQuizCreation() {
    Promise.resolve(this._contentService.publishNewsVideo()).then(res => {
      // console.log('published:', res);
      Promise.resolve(this.getTokens(this.learners)).then(tokens => {
        // console.log('tokens:', tokens);
        // console.log('tokensGlobal:', this.tokens);
      }).then(resolved => {
        // console.log('Content:', resolved)
        this.sendUpdate(this.categorySelection.selectedCategory, this.categorySelection.selectedSubCategory, this.screenType);
        this.http.post(proxyApi + '/contentUpdate', {
          'topic': this.previewService.organizationId.getValue(),
  
          'data': {
            'silent': false,
            'save': true,
            'body': 'New quiz has been added in '+ this.categorySelection.selectedCategory + ' category.',
            'campaignId': 82551,
            'ctaURL': 'string',
            'description':'Info',
            'image': null,
            'lifeCycleId': 257501,
            'microContentId': 1253,
            'name': this.categorySelection.selectedCategory,
            'scheduled': null,
            'screenType': null,
            'sequence': null,
            'sound': new Date().getTime(),
            'specialParameters': null,
            'tags': 'Notify',
            'template': 'string',
            'title': '',
            'orgId': this.previewService.organizationId.getValue(),
            'groupId': null,
            'userId': null
          }
  
        }, this.httpOptions).subscribe(res => {
            // console.log("push notification has been sent");
      });
      let url = base + '/api/notifications';
        let tok = this.tokens.splice(0, 10);
      this.http.post(url + String(tok.join()), JSON.parse(JSON.stringify({

        'body': 'New content is added in ' + this.categorySelection.selectedCategory +' category',
        'campaignId': 82551,
        'ctaURL': 'string',
        'description': this.themeSelection.selectedTheme.theme || 'Info',
        'image': null,
        'lifeCycleId': 257501,
        'microContentId': 1253,
        'name': this.categorySelection.selectedCategory,
        'scheduled': null,
        'screenType': null,
        'sequence': null,
        'sound': new Date().getTime(),
        'specialParameters': null,
        'tags': 'Notify',
        'template': 'string',
        'title': '',
        'orgId': this.previewService.organizationId.getValue(),
        'groupId': null,
        'userId': null

      })), this.httpOptions).subscribe(response => {

        // console.log("Successfully saved data to notification devapi");
        
        // console.log('response subscribe:', response);
      });
        this.saveAndReset();
        // setTimeout(this._contentService.resetFormObj(), 5000);
        this.resetSlides();
      });

    })

  }

  finishHTMLCreation() {
    Promise.resolve(this._contentService.publishHTMLCourse(this._contentService.formDataObj)).then(res => {
      // console.log('published:', res);
      Promise.resolve(this.getTokens(this.learners)).then(tokens => {
        // console.log('tokens:', tokens);
        // console.log('tokensGlobal:', this.tokens);
      }).then(resolved => {
        // console.log('Content:', resolved)
        this.sendUpdate(this.categorySelection.selectedCategory, this.categorySelection.selectedSubCategory, this.screenType);
        this.http.post(proxyApi + '/contentUpdate', {
          'topic': this.previewService.organizationId.getValue(),
  
          'data': {
            'silent': false,
            'save': true,
            'body': 'New content is added in ' + this.categorySelection.selectedCategory +' category',
            'campaignId': 82551,
            'ctaURL': 'string',
            'description':'Info',
            'image': null,
            'lifeCycleId': 257501,
            'microContentId': 1253,
            'name': this.categorySelection.selectedCategory,
            'scheduled': null,
            'screenType': null,
            'sequence': null,
            'sound': new Date().getTime(),
            'specialParameters': null,
            'tags': 'Notify',
            'template': 'string',
            'title': '',
            'orgId': this.previewService.organizationId.getValue(),
            'groupId': null,
            'userId': null
          }
        }, this.httpOptions).subscribe(res => {
            // console.log("push notification has been sent");
      });
      let url = base + '/api/notifications';
        let tok = this.tokens.splice(0, 10);
      this.http.post(url + String(tok.join()), JSON.parse(JSON.stringify({

        'body': 'New content is added in ' + this.categorySelection.selectedCategory +' category',
        'campaignId': 82551,
        'ctaURL': 'string',
        'description': this.themeSelection.selectedTheme.theme || 'Info',
        'image': null,
        'lifeCycleId': 257501,
        'microContentId': 1253,
        'name': this.categorySelection.selectedCategory,
        'scheduled': null,
        'screenType': null,
        'sequence': null,
        'sound': new Date().getTime(),
        'specialParameters': null,
        'tags': 'Notify',
        'template': 'string',
        'title': '',
        'orgId': this.previewService.organizationId.getValue(),
        'groupId': null,
        'userId': null

      })), this.httpOptions).subscribe(response => {

        // console.log("Successfully saved data to notification devapi");
        
        // console.log('response subscribe:', response);
      });
        this.saveAndReset();
        // setTimeout(this._contentService.resetFormObj(), 5000);
        this.resetSlides();
      });

    })
  }

  finishContentCreation() {
    if(this._contentService.formDataObj.contentSlides.length == 0){
      swal(
        'Error',
        'Create atleast one content slide.',
        'error'
      )
    }else{
      Promise.resolve(this._contentService.publishMicroContent(this._contentService.formDataObj, this.coursePlans)).then(res => {
        // console.log('published:', res);
        if (Number(this.previewService.organizationId.getValue()) === 261351) {
          this.coursePlan = '211544';
        }
        let cards = [];
        let formDataObj = Object.assign({}, this._contentService.formDataObj);
        // console.log('formdataobj in : ', formDataObj);
        let slidesLength = formDataObj.contentSlides.length;
    
        this.http.post(base + '/api/contructs', {
    
          'description': null,
          'name': formDataObj.content.contentName
        }, this.httpOptions).subscribe(resp => {
          this.construct = <any>resp;
          // console.log('construct:', this.construct);
          for (let y = 0; y < formDataObj.contentQuestions.length; y++) {
            let questionObject = formDataObj.contentQuestions[y];
            // console.log('qOBJ:', formDataObj.contentQuestions[y]);
            this.http.post(base + '/api/questions', {
    
              'contructId': this.construct.id,
              'description': null,
              'imageURL': formDataObj.contentQuestions[y].imageUrl || null,
              'questionType': this.formDataObj.contentQuestions[y].qType,
              'questionFormat': this.formDataObj.contentQuestions[y].negative,
              'name': formDataObj.contentQuestions[y].subTitle,
    
            }, this.httpOptions).subscribe(res => {
              // console.log('question: ', res);
              this.questionId = res;
              // console.log('option1:', {
              //   'description': null,
              //   'imageURL': null,
              //   'name': formDataObj.contentQuestions[y].correctAnswer,
              //   'questionId': this.questionId.id,
              //   'score': 1
              // });
              if (formDataObj.contentQuestions[y].correctAnswer) {
                this.http.post(base + '/api/options', {
                  'description': null,
                  'imageURL': null,
                  'name': formDataObj.contentQuestions[y].correctAnswer,
                  'questionId': this.questionId.id,
                  'score': 1
                }, this.httpOptions).subscribe(optionResponse => console.log()/* console.log('optionResponse:', optionResponse)*/);
                for (let z = 0; z < formDataObj.contentQuestions[y].incorrectAnswers.length; z++) {
                  this.http.post(base + '/api/options', {
                    'description': null,
                    'imageURL': null,
                    'name': formDataObj.contentQuestions[y].incorrectAnswers[z].value,
                    'questionId': this.questionId.id,
                    'score': 0
                  }, this.httpOptions).subscribe(optionsResp => console.log()/* console.log(optionsResp)*/);
                }
              }
              else {
                for (let z = 0; z < formDataObj.contentQuestions[y].options.length; z++) {
                  this.http.post(base + '/api/options', {
                    'description': null,
                    'imageURL': null,
                    'name': formDataObj.contentQuestions[y].options[z].value,
                    'questionId': this.questionId.id,
                    'score': formDataObj.contentQuestions[y].options[z].marks
                  }, this.httpOptions).subscribe(optionsResp => console.log() /* console.log(optionsResp)*/);
                }
              }
    
            });
          }
    
          // console.log('formslides:', formDataObj.contentSlides);
    
          for (let x = 0; x < slidesLength; x++) {
    
            this.http.post(base + '/api/bat-tests', {
              'name': formDataObj.content.contentName,
              'description': null,
              'type': 'SURVEY',//feedback
              'maxTimeAllowed': 0,
              'intructionsImage': null,
              'organizationId': this.previewService.organizationId.getValue(),
              'contructs': [
                {
                  'id': this.construct.id,
                  'name': formDataObj.content.contentName,
                  'description': null,
                  'timeLimit': 0,
                  'multiLinguals': []
                }
              ],
              'contentCards': [],
              'multiLinguals': [],
              'microContentId': null
            }, this.httpOptions).subscribe(micoContentReponse => {
              // console.log(micoContentReponse);
    
              // console.log('publish body:', {
    
              //   'audioLink': null,
              //   'author': null,
              //   'category': formDataObj.content.targetCategory,
              //   'content': formDataObj.contentSlides[x].slideContentDescription,
              //   'careerPathId': 0,
              //   'image': null,
              //   'level': 0,
              //   'maximumExperince': 0,
              //   'minimumEducation': 'NOT_APPLICABLE',
              //   'minimumExperince': 0,
              //   'expiry': new Date('1971-01-01'),
              //   'contentType': formDataObj.contentSlides[x].contentType,
              //   'copyRight': 'CREATIVE_COMMONS',
              //   'description': formDataObj.contentSlides[x].slideContentDescription || null,
              //   'language': 'ENGLISH',
              //   'moreContent': null,
    
              //   'name': formDataObj.contentSlides[x].slideTitleName,
              //   'organizationId': this.previewService.organizationId.getValue(),
              //   'pdfLink': null,
              //   'previewImage': formDataObj.contentSlides[x].contentType.toUpperCase() === 'BLOG' ? formDataObj.contentSlides[x].slideImageDetails.url || null : '',
              //   'scheduled': null,
              //   'schemeUrl': this.previewService.Username.getValue(),
              //   'source': null,
              //   'state': 'DRAFT',
              //   'batTests': (x === slidesLength - 1) && formDataObj.contentQuestions.length ? [micoContentReponse] : null,
              //   'postDate': null,
              //   'subCategory': formDataObj.content.targetSubCategory,
              //   'summary': formDataObj.contentSlides[x].slideContentDescription,
              //   'tags': null,
              //   'thumbnailImage': formDataObj.content.imageDetails.url || null,
              //   'type': 'REPACKED',
              //   'unlockScore': 0,
              //   'value': null,
              //   'videoLink': formDataObj.contentSlides[x].contentType.toUpperCase() === 'VIDEO' ? formDataObj.contentSlides[x].slideImageDetails.url || null : '',
              // });
              this.http.post(base + '/api/published-contents', {
    
                'audioLink': null,
                'author': null,
                'category': formDataObj.content.targetCategory,
                'content': formDataObj.contentSlides[x].slideContentDescription,
                'careerPathId': 0,
                'image': null,
                'level': 0,
                'maximumExperince': 0,
                'minimumEducation': 'NOT_APPLICABLE',
                'minimumExperince': 0,
                'postDate': new Date().toISOString(),
                'contentType': formDataObj.contentSlides[x].contentType,
                'copyRight': 'CREATIVE_COMMONS',
                'description': formDataObj.contentSlides[x].slideContentDescription || null,
                'language': 'ENGLISH',
                'moreContent': null,
                'expiry': new Date('1971-01-01'),
                'name': formDataObj.contentSlides[x].slideTitleName,
                'organizationId': this.previewService.organizationId.getValue(),
                'pdfLink': null,
                'previewImage': formDataObj.contentSlides[x].contentType.toUpperCase() === 'BLOG' ? formDataObj.contentSlides[x].slideImageDetails.url || null : '',
                'scheduled': null,
                'schemeUrl': this.previewService.Username.getValue(),
                'source': null,
                'state': 'DRAFT',
                'batTests': (x === slidesLength - 1) && formDataObj.contentQuestions.length ? [micoContentReponse] : null,
    
                'subCategory': formDataObj.content.targetSubCategory,
                'summary': formDataObj.contentSlides[x].slideContentDescription,
                'tags': null,
                'thumbnailImage': formDataObj.content.imageDetails.url || null,
                'type': 'REPACKED',
                'unlockScore': 0,
                'value': null,
                'videoLink': formDataObj.contentSlides[x].contentType.toUpperCase() === 'VIDEO' ? formDataObj.contentSlides[x].slideImageDetails.url || null : '',
              }, this.httpOptions).subscribe(res => {
    
                // console.log(res);
                // console.log('contentslides:', formDataObj.contentSlides, x);
                this.http.post(base + '/api/content-cards',
                  { 'cards': [res], 'sequence': x, 'name': formDataObj.contentSlides[x].slideTitleName }, this.httpOptions)
                  .subscribe(responseCard => {
                    // console.log('card:', responseCard);
                    this.responseCards = responseCard;
                    // console.log('card in cards:', this.responseCards.cards[0]);
                    cards.push(this.responseCards);
                    // console.log('cardsArray:', this.cards);
                    // console.log('x before:', x, slidesLength);
    
                    if (x === slidesLength - 1) {
                      // console.log('x:', x, slidesLength);
                      let interval = setInterval(() => {
                        if (cards.length === slidesLength) {
                          // console.log('x in:', x, this.cards.length, slidesLength);
                          this.http.post(base + '/api/micro-contents', JSON.parse(JSON.stringify({
                            'about': null,
                            'cards': cards,
                            'category': formDataObj.content.targetCategory,
                            'description': formDataObj.content.contentDescription,
                            'postDate': new Date().toISOString(),
                            'message': null,
                            'name': formDataObj.content.contentName,
                            'organizationId': this.previewService.organizationId.getValue(),
                            'previewUrl': null,
                            'expiryDate': new Date('1971-01-01'),
                            'scheduled': null,
                            'subcategory': formDataObj.content.targetSubCategory,
                            'summary': formDataObj.content.contentDescription || null,
                            'tags': null,
                            'thumbnailUrl': formDataObj.content.imageDetails.url || null,
                            'unlockScore': 0,
                            'videoLink': null
                          })), this.httpOptions).subscribe(micro => {
                            // console.log('micro: ', micro);
                            let microContent = <any>micro;
                            for (let index = 0; index < this.coursePlans.length; index++) {
                              this.http.get(base + '/api/course-plans/' + this.coursePlans[index], this.httpOptions).subscribe(e => {
                                let courseResult = <any>e;
                                courseResult.microContents.push(microContent);
                                this.http.put(base + '/api/course-plans', courseResult, this.httpOptions).subscribe(putcourse => {
                                  console.log('putCourse:', putcourse);
                                  this.tabs = "";
                                  let micro = [];
                                  Promise.resolve(this.getTokens(this.learners)).then(tokens => {
                                    console.log('tokens:', tokens);
                                    // console.log('tokensGlobal:', this.tokens);

                                  }).then(resolved => {
                                    this.sendUpdate(this.categorySelection.selectedCategory, this.categorySelection.selectedSubCategory, this.screenType);
                                    console.log("inside resolve");
                                    let learningTabs = "";
                                    this.categories.forEach(element => {
                                      if (this.categorySelection.selectedCategory === element.value) {
                                        this.tabs = element.subCategory;
                                      }
                                    });
                                    let deeplink = "productiviseapp://productivise/learning?category="+this.categorySelection.selectedCategory+"&url="+this.tabs+"&tabName="+this.categorySelection.selectedSubCategory+"&courseId="+microContent.id+"&moduleId="+this.responseCards.id;
                                deeplink = deeplink.split(" ").join("%20");
                                    // let deeplink = `productiviseapp://productivise/learning?category=${this.categorySelection.selectedCategory}&tabs=${learningTabs.substring(0,learningTabs.length-1)}&tabName=${this.categorySelection.selectedSubCategory}&courseId=${this.selectedCourseId}&moduleId=${this.selectedModuleId}`;
                                    this.http.post(proxyApi + '/contentUpdate', {
                                      'topic': this.previewService.organizationId.getValue(),
                              
                                      'data': {
                                        'silent': false,
                                        'save': true,
                                        'body': 'New content is added in ' + this.categorySelection.selectedCategory +' category',
                                        'campaignId': 82551,
                                        'ctaURL': 'string',
                                        'description':'Info',
                                        'image': null,
                                        'lifeCycleId': 257501,
                                        'microContentId': 1253,
                                        'name': this.categorySelection.selectedCategory,
                                        'scheduled': null,
                                        'screenType': null,
                                        'sequence': null,
                                        'sound': new Date().getTime(),
                                        'specialParameters': null,
                                        'tags': 'Notify',
                                        'template': deeplink,
                                        'title': '',
                                        'orgId': this.previewService.organizationId.getValue(),
                                        'groupId': null,
                                        'userId': null
                                      }
                              
                                    }, this.httpOptions).subscribe(res => {
                                        // console.log("push notification has been sent");
                                  });
                                  let url = base + '/api/notifications';
                                  let tok = this.tokens.splice(0, 10);
                                this.http.post(url + String(tok.join()), JSON.parse(JSON.stringify({
                          
                                  'body': 'New content is added in ' + this.categorySelection.selectedCategory +' category',
                                  'campaignId': 82551,
                                  'ctaURL': 'string',
                                  'description': this.themeSelection.selectedTheme.theme || 'Info',
                                  'image': null,
                                  'lifeCycleId': 257501,
                                  'microContentId': 1253,
                                  'name': this.categorySelection.selectedCategory,
                                  'scheduled': null,
                                  'screenType': null,
                                  'sequence': null,
                                  'sound': new Date().getTime(),
                                  'specialParameters': null,
                                  'tags': 'Notify',
                                  'template': deeplink,
                                  'title': '',
                                  'orgId': this.previewService.organizationId.getValue(),
                                  'groupId': null,
                                  'userId': null
                          
                                })), this.httpOptions).subscribe(response => {
                          
                                  // console.log("Successfully saved data to notification devapi");
                                  
                                  // console.log('response subscribe:', response);
                                });
                                this.saveAndReset();
                                    // setTimeout(this._contentService.resetFormObj(), 5000);
                                    this.resetSlides();
                                    // console.log('Content:', resolved)
                                  });
                                })
                              }, err => {
                                swal(
                                  'Error',
                                  'Micro Learning could not be created.',
                                  'error'
                                )
                              })
                            }
    
                            
                            this.resetFormObj();
                            swal(
                              'Micro Learning has been created SuccessFully.',
                              'Your content was created.',
                              'success'
                            ).then(a =>
                              // console.log(a)
                              console.log()
                            );
    
                            // this.notificationSuccessfullySent(this.titles[0], this.microContent.name ? true : false);
                            // this.resetService();
    
    
                          });
                          clearInterval(interval);
                        }
                        else console.log() //console.log('cardsNOW: ', this.cards);
                      }, 500);
                    }
                  });
              }, err => {
                throw err;
              });
            }, err => {
              throw err;
            });
          }
        }, err => {
          // console.log(err);
          throw err;
    
        });
        

      })
  }

  }

  finishSurveyCreation() {
    Promise.resolve(this._contentService.publishSurveyContent(this._contentService.formDataObj)).then(res => {
      // console.log('published:', res);
      Promise.resolve(this.getTokens(this.learners)).then(tokens => {
        // console.log('tokens:', tokens);
        // console.log('tokensGlobal:', this.tokens);


      }).then(resolved => {
        this.sendUpdate(this.categorySelection.selectedCategory, this.categorySelection.selectedSubCategory, this.screenType);
        this.http.post(proxyApi + '/contentUpdate', {
          'topic': this.previewService.organizationId.getValue(),
  
          'data': {
            'silent': false,
            'save': true,
            'body': 'Please open the app to check it out.',
            'campaignId': 82551,
            'ctaURL': 'string',
            'description':'Info',
            'image': null,
            'lifeCycleId': 257501,
            'microContentId': 1253,
            'name': 'New ' + this.categorySelection.selectedCategory + ' added!',
            'scheduled': null,
            'screenType': null,
            'sequence': null,
            'sound': new Date().getTime(),
            'specialParameters': null,
            'tags': 'Notify',
            'template': 'string',
            'title': '',
            'orgId': this.previewService.organizationId.getValue(),
            'groupId': null,
            'userId': null
          }
  
        }, this.httpOptions).subscribe(res => {
            // console.log("push notification has been sent");
      });
        // console.log('Content:', resolved);
        this.saveAndReset();
        // setTimeout(this._contentService.resetFormObj(), 5000);
        this.resetSlides();
      });

    })


  }

  updateSurvey() {
    Promise.resolve(this._contentService.updateSurveyContent(this._contentService.formDataObj)).then(res => {
      // console.log('published:', res);
      Promise.resolve(this.getTokens(this.learners)).then(tokens => {
        // console.log('tokens:', tokens);
        // console.log('tokensGlobal:', this.tokens);


      }).then(resolved => {
        this.sendUpdate(this.categorySelection.selectedCategory, this.categorySelection.selectedSubCategory, this.screenType);
        // console.log('Content:', resolved);
        this.saveAndReset();
        this.resetSlides();
      });


      // setTimeout(this._contentService.resetFormObj(), 5000);
    })

  }

  finishOperationCreation() {
    Promise.resolve(this._contentService.publishOperationContent(null)).then(res => {
      // console.log('published:', res);
      Promise.resolve(this.getTokens(this.learners)).then(tokens => {
        // console.log('tokens:', tokens);
        // console.log('tokensGlobal:', this.tokens);


      }).then(resolved => {
        this.sendUpdate(this.categorySelection.selectedCategory, this.categorySelection.selectedSubCategory, this.screenType);
      //   this.http.post(proxyApi + '/contentUpdate', {
      //     'topic': this.previewService.organizationId.getValue(),
  
      //     'data': {
      //       'silent': false,
      //       'save': true,
      //       'body': 'Please open the app to check it out.',
      //       'campaignId': 82551,
      //       'ctaURL': 'string',
      //       'description':'Info',
      //       'image': null,
      //       'lifeCycleId': 257501,
      //       'microContentId': 1253,
      //       'name': 'New ' + this.categorySelection.selectedCategory + ' added!',
      //       'scheduled': null,
      //       'screenType': null,
      //       'sequence': null,
      //       'sound': new Date().getTime(),
      //       'specialParameters': null,
      //       'tags': 'Notify',
      //       'template': 'string',
      //       'title': '',
      //       'orgId': this.previewService.organizationId.getValue(),
      //       'groupId': null,
      //       'userId': null
      //     }
  
      //   }, this.httpOptions).subscribe(res => {
      //       // console.log("push notification has been sent");
      // });
        // console.log('Content:', resolved);
        this.saveAndReset();
        // setTimeout(this._contentService.resetFormObj(), 5000);
        this.resetSlides();
      });

    })

  }

  updateOperationCreation() {
    Promise.resolve(this._contentService.publishOperationContent(this.contentEditId)).then(res => {
      // console.log('published:', res);
      Promise.resolve(this.getTokens(this.learners)).then(tokens => {
        // console.log('tokens:', tokens);
        // console.log('tokensGlobal:', this.tokens);


      }).then(resolved => {
        this.sendUpdate(this.categorySelection.selectedCategory, this.categorySelection.selectedSubCategory, this.screenType);
        // console.log('Content:', resolved);
        this.saveAndReset();
        // setTimeout(this._contentService.resetFormObj(), 5000);
        this.resetSlides();
      });

    })

  }

  updateContentCreation() {
    Promise.resolve(this.deleteContent(this.contentEditId, this.coursePlan)).then((a => this._contentService.publishMicroContent(this._contentService.formDataObj, this.coursePlans))).then(res => {
      // console.log('published:', res);
      Promise.resolve(this.getTokens(this.learners)).then(tokens => {
        // console.log('tokens:', tokens);
        // console.log('tokensGlobal:', this.tokens);
      }).then(resolved => {
        this.sendUpdate(this.categorySelection.selectedCategory, this.categorySelection.selectedSubCategory, this.screenType);
        // console.log('Content:', resolved);
        this.saveAndReset();
        this.resetSlides();
      });

      // setTimeout(this._contentService.resetFormObj(), 5000);
    })

  }

  publishJobFamily(data) {
    return this.http.post(base + '/api/job-families', data, this.httpOptions).map(res => <any>res);
  }

  publishInfoCards() {
    // this.notificationTitle = this.slideTitleName;
    // Promise.resolve(this._contentService.publishInfoCards(this._contentService.formDataObj, this.org)).then(res => {
      // console.log('published:', res);
      let error;
      let arry = [];
      for (let i = 0; i < this._contentService.formDataObj.contentSlides.length; i++) {
        arry.push({
  
          "name": this._contentService.formDataObj.contentSlides[i].slideTitleName,
          "tags": this._contentService.formDataObj.content.targetCategory,
          "description": null,
          "url": null,
          "image": this._contentService.formDataObj.contentSlides[i].slideImageDetails['url'],
          "videoLink": this._contentService.formDataObj.contentSlides[i].slideVideoDetails['url'],
          "pdfLink": this._contentService.formDataObj.contentSlides[i].slideDocDetails['url'],
          "audioLink": this._contentService.formDataObj.contentSlides[i].slideAudioDetails['url'],
          "content": this._contentService.formDataObj.contentSlides[i].slideContentDescription,
          "bigImageLink": null,
          "displayOrder": null,
          "organizations": [this.org],
          "jobRoles": []
  
        })
      }
      // console.log('Arry:', arry);
      let source = of(arry);
      const example = source.pipe(mergeMap(q => forkJoin(...q.map(a => this.publishJobFamily(a)))));
      const subscribe = example.subscribe(val => {
        // console.log('VAL:', val);
        this.industry.createIndustryUsingPOST({
  
          "name": this._contentService.formDataObj.content.contentName,
          "tags": this._contentService.formDataObj.content.targetCategory,
          "description": null,
          "url": null,
          "images": null,
          "imageLink": this._contentService.formDataObj.content.imageDetails['url'],
          "videoLink": null,
          "pdfLink": null,
          "audioLink": null,
          "content": this._contentService.formDataObj.content.contentDescription,
          "displayOrder": 10,
          "extraData": [],
          "jobRoles": [],
          "organizations": [this.org],
          "jobFamilies": val,
          "additionalFiles": [],
          "multiLinguals": []
        }).subscribe(res => {
          this.http.get(base + '/api/app-configs/org/' + this.previewService.organizationId.getValue(), this.httpOptions).subscribe(resp => {
            let data = <any>resp;
            // console.log('data:', data);
            let tabs;
            data.forEach(element => {
                // console.log(JSON.stringify(element))
                if (element.category === 'DASHBOARD' && element.value === this.categorySelection.selectedCategory) {
                    tabs   = element.subCategory;
                }
              })
              tabs = tabs.split(', ').join(',');
              // console.log("configs : " + configs);
          let deeplink = "productiviseapp://productivise/industry?category="+this.categorySelection.selectedCategory+"&tabs="+tabs+"&tabName="+this._contentService.formDataObj.content.contentName+"&jobFamilyId="+JSON.stringify(res).substring(6,JSON.stringify(res).indexOf(","));
          // console.log("deeplink "+deeplink);
          deeplink = deeplink.split(' ').join('%20');
          // console.log("deeplink : "+deeplink);
          Promise.resolve(this.getTokens(this.learners)).then(tokens => {
            // console.log('tokens:', tokens);
            // console.log('tokensGlobal:', this.tokens);
    
    
          }).then(resolved => {
            // console.log(this.notificationTitle);
            this.sendUpdate(this.categorySelection.selectedCategory, this.categorySelection.selectedSubCategory, this.screenType);
            this.http.post(proxyApi + '/contentUpdate', {
              'topic': this.previewService.organizationId.getValue(),
      
              'data': {
                'silent': false,
                'save': true,
                'body': '"'+this.notificationTitle + '" added in '+this.categorySelection.selectedCategory,
                'campaignId': 82551,
                'ctaURL': 'string',
                'description':'Info',
                'image': null,
                'lifeCycleId': 257501,
                'microContentId': 1253,
                'name': this.categorySelection.selectedCategory,
                'scheduled': null,
                'screenType': 'industry',
                'sequence': null,
                'sound': new Date().getTime(),
                'specialParameters': deeplink,
                'tags': 'Notify',
                'template': deeplink,
                'title': '',
                'orgId': this.previewService.organizationId.getValue(),
                'groupId': null,
                'userId': null
              }
      
            }, this.httpOptions).subscribe(res => {
                // console.log("push notification has been sent");
          });
          let url = base + '/api/notifications';
            let tok = this.tokens.splice(0, 10);
          this.http.post(url + String(tok.join()), JSON.parse(JSON.stringify({
    
            'body': '"'+this.notificationTitle + '" added in '+this.categorySelection.selectedCategory,
            'campaignId': 82551,
            'ctaURL': 'string',
            'description': this.themeSelection.selectedTheme.theme || 'Info',
            'image': null,
            'lifeCycleId': 257501,
            'microContentId': 1253,
            'name': this.categorySelection.selectedCategory,
            'scheduled': null,
            'screenType': 'industry',
            'sequence': null,
            'sound': new Date().getTime(),
            'specialParameters': null,
            'tags': 'Notify',
            'template': deeplink,
            'title': '',
            'orgId': this.previewService.organizationId.getValue(),
            'groupId': null,
            'userId': null
    
          })), this.httpOptions).subscribe(response => {
    
            // console.log("Successfully saved data to notification devapi");
            
            // console.log('response subscribe:', response);
          });
            // console.log('Content:', resolved);
            this.saveAndReset();
            // setTimeout(this._contentService.resetFormObj(), 5000);
            this.resetSlides();
          });
          // console.log('res:', res);
          swal('Success', 'Your Info Card has been successfully created.', 'success');
        }, err => {
          error = true;
        });
      });
      });
      

    // })

  }

  updateInfoCards() {
    Promise.resolve(this._contentService.updateInfoCards(this._contentService.formDataObj, this.org)).then(res => {
      // console.log('published:', res);
      Promise.resolve(this.getTokens(this.learners)).then(tokens => {
        // console.log('tokens:', tokens);
        // console.log('tokensGlobal:', this.tokens);


      }).then(resolved => {
        this.sendUpdate(this.categorySelection.selectedCategory, this.categorySelection.selectedSubCategory, this.screenType);
        // console.log('Content:', resolved);
        this.saveAndReset();
        // setTimeout(this._contentService.resetFormObj(), 5000);
        this.resetSlides();
      });

    })

  }

  finishHTMLUpdate() {
    Promise.resolve(this._contentService.publishHTMLCourse(this._contentService.formDataObj)).then(res => {
      // console.log('published:', res);
      Promise.resolve(this.getTokens(this.learners)).then(tokens => {
        // console.log('tokens:', tokens);
        // console.log('tokensGlobal:', this.tokens);
        this.deleteContent(this.contentEditId, this.coursePlan);
      }).then(resolved => {
        // console.log('Content:', resolved);

        this.sendUpdate(this.categorySelection.selectedCategory, this.categorySelection.selectedSubCategory, this.screenType);
      //   this.http.post(proxyApi + '/contentUpdate', {
      //     'topic': this.previewService.organizationId.getValue(),
  
      //     'data': {
      //       'silent': false,
      //       'save': true,
      //       'body': 'Please open the app to check it out.',
      //       'campaignId': 82551,
      //       'ctaURL': 'string',
      //       'description':'Info',
      //       'image': null,
      //       'lifeCycleId': 257501,
      //       'microContentId': 1253,
      //       'name': 'New ' + this.categorySelection.selectedCategory + ' added!',
      //       'scheduled': null,
      //       'screenType': null,
      //       'sequence': null,
      //       'sound': new Date().getTime(),
      //       'specialParameters': null,
      //       'tags': 'Notify',
      //       'template': 'string',
      //       'title': '',
      //       'orgId': this.previewService.organizationId.getValue(),
      //       'groupId': null,
      //       'userId': null
      //     }
  
      //   }, this.httpOptions).subscribe(res => {
      //       // console.log("push notification has been sent");
      // });
        this.saveAndReset();
        // setTimeout(this._contentService.resetFormObj(), 5000);
        this.resetSlides();
      });

    })

  }

  deleteContent(contentEditId, coursePlan) {

    let timerInterval1;
    let deleteNow = false;
    if (Number(this.previewService.organizationId.getValue()) === 261351) {
      coursePlan = '211544';
    }
    /*
        if (Number(this.previewService.organizationId.getValue()) === 267502) {
          coursePlan = '211544';
        }
      */
    this.http.get(base + '/api/course-plans/' + coursePlan, this.httpOptions).subscribe(e => {
      let courseResult = <any>e;
      // console.log('1:', courseResult);
      let microList = courseResult.microContents;
      // console.log('e.id:', contentEditId)
      microList = microList.filter(element => element.id !== contentEditId);
      courseResult.microContents = microList;
      // console.log('2', courseResult)
      this.http.put(base + '/api/course-plans', courseResult, this.httpOptions).subscribe(putcourse => {
        // console.log('putCourse:', putcourse);
        if (this.coursePlans.length === 0) {
          deleteNow = true;
          // console.log('DELETE NOW:', putcourse);
        }
      })
    })

    for (let i = 0; i < this.coursePlans.length; i++) {

      // console.log('Course Plan In', this.coursePlans);
      this.http.get(base + '/api/course-plans/' + this.coursePlans[i], this.httpOptions).subscribe(e => {
        let courseResult = <any>e;
        // console.log('CoursePlans:', courseResult);
        let microList = courseResult.microContents;
        // console.log('e.id:', contentEditId)
        microList = microList.filter(element => element.id !== contentEditId);
        courseResult.microContents = microList;
        // console.log('CoursePlans 2:', courseResult)
        this.http.put(base + '/api/course-plans', courseResult, this.httpOptions).subscribe(putcourse => {
          // console.log(`putCourse =`, i, putcourse);
          // console.log('Course Plan In', this.coursePlans.length);
          if (i === this.coursePlans.length - 1) {
            deleteNow = true;
            // console.log('DELETE NOW:', putcourse);
          }
        })
      })
    }

    timerInterval1 = setInterval(() => {
      if (deleteNow) {
        clearInterval(timerInterval1);
        this.http.delete(base + '/api/micro-contents/' + contentEditId, this.httpOptions).subscribe(res => {
          // console.log('res:', res);
          swal(
            'SuccessFully Deleted Micro Learning',
            'Your content was deleted.',
            'success'
          ).then(a =>
            // console.log(a)
            console.log("")
          );
        });
        // console.log('time1')
      }
    }, 500)

  }

  /* publish MicroContent */

  sendNotification({ notificationName, cronExpression, notificationText, thumbnailImage, tags, tokens, specialParameters, screenType, title, orgId, sgroupId, cgroupId, userId }) {




    // console.log('notification:', JSON.parse(JSON.stringify({

    //   'notificationDTO': {
    //     'body': notificationText,
    //     'campaignId': 0,
    //     'ctaURL': 'string',
    //     'description': 'Info',
    //     'image': thumbnailImage,
    //     'lifeCycleId': 0,
    //     'microContentId': 0,
    //     'name': notificationName,
    //     'scheduled': cronExpression,
    //     'screenType': screenType,
    //     'sequence': sgroupId,
    //     'sound': new Date().getTime(),
    //     'specialParameters': specialParameters,
    //     'tags': tags,
    //     'template': 'string',
    //     'title': title,
    //     'orgId': orgId,
    //     'groupId': cgroupId,
    //     'userId': userId
    //   }, 'deviceID': tokens.join()
    // })));

    this.http.post(base + '/api/notifications/send?deviceID=' + String(tokens.join()), JSON.parse(JSON.stringify({


      'body': notificationText,
      'campaignId': 0,
      'ctaURL': 'string',
      'description': 'Info',
      'image': thumbnailImage,
      'lifeCycleId': 0,
      'microContentId': 0,
      'name': notificationName,
      'scheduled': cronExpression,
      'screenType': screenType,
      'sequence': sgroupId,
      'sound': new Date().getTime(),
      'specialParameters': specialParameters,
      'tags': tags,
      'template': 'string',
      'title': title,
      'orgId': orgId,
      'groupId': cgroupId,
      'userId': userId
    })),
      this.httpOptions).subscribe(response => {
        // console.log('response subscribe:', response);
        let result: any = response;
        this.http.post(base + '/api/notifications', {
          'body': notificationText,
          'campaignId': 82551,
          'ctaURL': 'string',
          'description': 'Info',
          'image': thumbnailImage,
          'lifeCycleId': 257501,
          'microContentId': 1253,
          'name': notificationName,
          'scheduled': cronExpression,
          'screenType': screenType,
          'sequence': sgroupId,
          'sound': new Date().getTime(),
          'specialParameters': specialParameters,
          'tags': tags,
          'template': 'string',
          'title': title,
          'orgId': orgId,
          'groupId': cgroupId,
          'userId': userId
        }, this.httpOptions)
          .subscribe(res => {
            // console.log('notification:', res)
            this.router.navigate(['/content/list'])
          });

        // this.notificationSuccessfullySent(this.pushNotificationName || this.popNotificationText, result.name ? true : false);
        // this.resetFields();
      });


  }
  sendUpdate(category, subcategory, screen) {

    let message = {
      topic: this.previewService.organizationId.getValue(),
      data: {
        silent: true,
        save: false,
        title: category,
        body: subcategory,
        screenType: screen,
        image: 'jpg',
        specialParameters: 'll',
        tags: 'Silent',
        orgId: this.organizationId,
        name: screen
      }

    }
    // console.log('message:', message);
    this.http.post(proxyApi + '/contentUpdate', message, this.httpOptions).subscribe((response) => {
      // Response is a message ID string.
      // console.log('Successfully sent message:', response);

    }, (error) => {
      // console.log('Error sending message:', error);
    });
  }


  contentValidationMessages = {
    contentName: {
      message: "",
      invalid: false
    },
    contentDescription: {
      message: "",
      invalid: false
    },
    contentImage: {
      message: "",
      invalid: false
    },
    targetCategory: {
      message: '',
      invalid: false
    },
    targetSubCategory: {
      message: '',
      invalid: false
    },
    contentType: {
      message: '',
      invalid: false
    }

  }

  validateContentForm3() {
    let isValid = true;
    let contentNameInput, contentTypeInput, contentDescriptionInput, categoryInput, subCategoryInput, imageInput, typeInput, negativeInput;
    if (this.contentTypeSelection.selectedType === 'Quiz' || this.contentTypeSelection.selectedType === 'MicroLearning' || this.contentTypeSelection.selectedType === 'Survey') {
      if (!this.categorySelection.selectedQuestionType) {
        typeInput = document.getElementById('questionType');
        typeInput.classList.add('error');
        isValid = false;
        return isValid;
      } else {
        typeInput = document.getElementById('questionType');
        typeInput.classList.remove('error');
        isValid = true;
      }
    }
    if (!this.questionTitleName || this.questionTitleName.trim() === "") {
      contentNameInput = document.getElementById('page3TitleName');
      contentNameInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      contentNameInput = document.getElementById('page3TitleName');
      contentNameInput.classList.remove('error');
      isValid = true;
    }
    if (document.getElementById('page2TitleName')) {
      if (!this.questionSubTitleName || this.questionSubTitleName.trim() === "") {
        contentDescriptionInput = document.getElementById('page2TitleName');
        contentDescriptionInput.classList.add('error');
        isValid = false;
        return isValid;
      } else {
        contentDescriptionInput = document.getElementById('page2TitleName');
        contentDescriptionInput.classList.remove('error');
        isValid = true;
      }
    }

    if (document.getElementById('negativeMarks')) {
      if (this.question.negative && !this.negativeMarks) {
        this.accordian.negative = true;
        negativeInput = document.getElementById('negativeMarks');
        negativeInput.classList.add('error');
        isValid = false;
        return isValid;
      } else if (this.question.negative && this.negativeMarks) {
        negativeInput = document.getElementById('negativeMarks');
        negativeInput.classList.remove('error');
        isValid = true;
      }
    }
    if (this.categorySelection.selectedQuestionType === 'MULTI-CORRECT OPTION') {
      for (let j = 0; j < this.options.length; j++) {

        if (!this.options[j].value) {
          this.accordian.option = true;
          categoryInput = document.getElementById('Option' + (j + 1));
          categoryInput.classList.add('error');
          isValid = false;
          return isValid;
        } else {
          categoryInput = document.getElementById('Option' + (j + 1));
          categoryInput.classList.remove('error');
          isValid = true;
        }

      }

    }

    if (this.categorySelection.selectedQuestionType === 'OPTION') {
      this.accordian.incorrect = true;
      if (this.incorrectAnswers.length < 1) {
        this.addIncorrectAnswer();
      }
      if (document.getElementById('correctAnswer')) {
        if (!this.correctAnswer) {
          this.accordian.correct = true;
          subCategoryInput = document.getElementById('correctAnswer');
          subCategoryInput.classList.add('error');
          isValid = false;
          return isValid;
        } else {
          subCategoryInput = document.getElementById('correctAnswer');
          subCategoryInput.classList.remove('error');
          isValid = true;
        }
      }
      for (let j = 0; j < this.incorrectAnswers.length; j++) {
        this.accordian.incorrect = true;
        if (!this.incorrectAnswers[j].value) {
          this.accordian.option = true;
          categoryInput = document.getElementById('Incorrect' + (j + 1));
          categoryInput.classList.add('error');
          isValid = false;
          return isValid;
        } else {
          categoryInput = document.getElementById('Incorrect' + (j + 1));
          categoryInput.classList.remove('error');
          isValid = true;
        }

      }

    }


    return isValid;


  }

  validateContentForm2() {
    let isValid = true;
    let contentNameInput, contentTypeInput, contentDescriptionInput, categoryInput, subCategoryInput, imageInput, typeInput;

    if (!this.slideTitleName || this.slideTitleName.trim() === "") {
      this.contentValidationMessages.contentName.invalid = true;
      this.contentValidationMessages.contentName.message = "";
      contentNameInput = document.getElementById('slideTitle');
      contentNameInput.focus();
      contentNameInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      this.contentValidationMessages.contentName.invalid = false;
      this.contentValidationMessages.contentName.message = "";
      contentNameInput = document.getElementById('slideTitle');
      contentNameInput.classList.remove('error');
      isValid = true;
    }
    if (this.contentTypeSelection.selectedType !== 'HTML5 Course') {
      if (!this.slideContentDescription || this.slideContentDescription.trim() === "") {
        contentDescriptionInput = document.getElementsByClassName('mce-container-body');
        contentDescriptionInput[0].focus();
        contentDescriptionInput[0].classList.add('error');
        isValid = false;
        return isValid;
      } else {
        contentDescriptionInput = document.getElementsByClassName('mce-container-body');
        contentDescriptionInput[0].classList.remove('error');
        isValid = true;
      }
    }

    if (this.contentTypeSelection.selectedType !== 'Standard Operating Procedure Checklist' &&
      this.contentTypeSelection.selectedType !== 'Info Cards' && this.contentTypeSelection.selectedType !== 'HTML5 Course') {
      if (!this.categorySelection.selectedContentType) {
        contentTypeInput = document.getElementById('selectContentType');
        contentTypeInput.classList.add('error');
        isValid = false;
        return isValid;
      } else {
        contentTypeInput = document.getElementById('selectContentType');
        contentTypeInput.classList.remove('error');
        isValid = true;
      }
    }

    if (this.contentTypeSelection.selectedType === 'Standard Operating Procedure Checklist') {
      if (!this.categorySelection.selectedOperationType) {
        categoryInput = document.getElementById('selectChecklistType');
        categoryInput.classList.add('error');
        isValid = false;
        return isValid;
      } else {
        categoryInput = document.getElementById('selectChecklistType');
        categoryInput.classList.remove('error');
        isValid = true;
      }
    }

    if (this.contentTypeSelection.selectedType === 'HTML5 Course') {
      if (!this.slideZipName || !this.contentZipUrl) {
        subCategoryInput = document.getElementById('zipBox');
        subCategoryInput.classList.add('error');
        isValid = false;
        return isValid;
      } else {
        subCategoryInput = document.getElementById('zipBox');
        subCategoryInput.classList.remove('error');
        isValid = true;
      }
    }
    if (this.contentTypeSelection.selectedType !== 'Info Cards' && this.contentTypeSelection.selectedType !== 'HTML5 Course') {
      if (!this.categorySelection.selectedFileInputType) {
        typeInput = document.getElementById('inputMethodBox');
        typeInput.classList.add('error');
        isValid = false;
        return isValid;
      } else {
        typeInput = document.getElementById('inputMethodBox');
        typeInput.classList.remove('error');
        isValid = true;
      }
    }
    if (this.contentTypeSelection.selectedType === 'Standard Operating Procedure Checklist' && this.categorySelection.selectedFileInputType === 'Upload a Local File') {
      if (!this.contentSlideUrl) {
        imageInput = document.getElementById('imageAudioVideoBox');
        imageInput.classList.add('error');
        isValid = false;
        return isValid;
      } else {
        imageInput = document.getElementById('imageAudioVideoBox');
        imageInput.classList.remove('error');
        isValid = true;
      }
    }
    /*
        if (this.contentTypeSelection.selectedType !== 'Standard Operating Procedure Checklist') {
          if (this.categorySelection.selectedFileInputType === 'Upload a Local File') {
            if (this.categorySelection.selectedContentType === 'BLOG' && !this.contentSlideUrl) {
              imageInput = document.getElementById('imageVideoBox');
              imageInput.classList.add('error');
              isValid = false;
              return isValid;
            }
    
            if (this.categorySelection.selectedContentType === 'BLOG' && this.contentSlideUrl) {
              imageInput = document.getElementById('imageVideoBox');
              imageInput.classList.add('error');
              isValid = false;
              return isValid;
            }
          }
          else if (!this.slideUrl && this.categorySelection.selectedFileInputType === 'Paste a Url') {
            imageInput = document.getElementById('slideUrl');
            imageInput.classList.add('error');
            isValid = false;
            return isValid;
          } else {
            imageInput = document.getElementById('imageVideoBox') || document.getElementById('slideUrl');
            if (imageInput.classList.contains('error')) {
              imageInput.classList.remove('error');
            }
            isValid = true;
          }
        }
        */
    return isValid;
  }

  validateContentForm() {
    let isValid = true;
    let contentNameInput, contentTypeInput, contentDescriptionInput, categoryInput, subCategoryInput, imageInput, jobRoleInput;

    if (!this.contentTypeSelection.selectedType) {
      this.contentValidationMessages.contentType.invalid = true;
      this.contentValidationMessages.contentType.message = "";
      contentTypeInput = document.getElementById('contentTypeInput');
      contentTypeInput.focus();
      contentTypeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      this.contentValidationMessages.contentType.invalid = false;
      this.contentValidationMessages.contentType.message = "";
      contentTypeInput = document.getElementById('contentTypeInput');
      contentTypeInput.classList.remove('error');
      isValid = true;
    }

    if (this.contentTypeSelection.selectedType === 'MicoLearning') {
      if (!this.jobRoleSelection.selectedCategory || (this.jobRoleSelection.selectedCategory.name !== 'All Industries' &&
        (!this.jobRoleSelection.selectedSubCategory || (this.jobRoleSelection.selectedSubCategory.name !== 'All Departments' && !this.jobRoleSelection.selectedJobRole)))) {
        this.contentValidationMessages.targetCategory.invalid = true;
        this.contentValidationMessages.targetCategory.message = "";
        jobRoleInput = document.getElementById('jobRoleInput');
        jobRoleInput.focus();
        jobRoleInput.classList.add('error');

        isValid = false;
        // console.log('Industries:', this.jobRoleSelection.selectedCategory)
        return isValid;
      } else {
        this.contentValidationMessages.targetCategory.invalid = false;
        this.contentValidationMessages.targetCategory.message = "";
        jobRoleInput = document.getElementById('jobRoleInput');
        jobRoleInput.classList.remove('error');
        isValid = true;
      }
    }

    if (!this.categorySelection.selectedCategory || this.categorySelection.selectedCategory === '') {
      this.contentValidationMessages.targetCategory.invalid = true;
      this.contentValidationMessages.targetCategory.message = "";
      categoryInput = document.getElementById('categoryInput');
      categoryInput.focus();
      categoryInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      this.contentValidationMessages.targetCategory.invalid = false;
      this.contentValidationMessages.targetCategory.message = "";
      categoryInput = document.getElementById('categoryInput');
      categoryInput.classList.remove('error');
      isValid = true;
    }


    if (!this.categorySelection.selectedSubCategory && this.contentTypeSelection.selectedType !== 'Survey' && this.contentTypeSelection.selectedType !== 'Info Cards') {
      this.contentValidationMessages.targetSubCategory.invalid = true;
      this.contentValidationMessages.targetSubCategory.message = "";
      subCategoryInput = document.getElementById('categoryInput');
      subCategoryInput.focus();
      subCategoryInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      this.contentValidationMessages.targetSubCategory.invalid = false;
      this.contentValidationMessages.targetSubCategory.message = "";
      subCategoryInput = document.getElementById('categoryInput');
      subCategoryInput.classList.remove('error');
      isValid = true;
    }

    if (!this.contentName || this.contentName.trim() === "") {
      this.contentValidationMessages.contentName.invalid = true;
      this.contentValidationMessages.contentName.message = "";
      contentNameInput = document.getElementById('contentName');
      contentNameInput.focus();
      contentNameInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      this.contentValidationMessages.contentName.invalid = false;
      this.contentValidationMessages.contentName.message = "";
      contentNameInput = document.getElementById('contentName');
      contentNameInput.classList.remove('error');
      isValid = true;
    }


    if (this.contentTypeSelection.selectedType !== 'Survey') {
      if (!this.contentDescription || this.contentDescription.trim() === "") {
        this.contentValidationMessages.contentDescription.invalid = true;
        this.contentValidationMessages.contentDescription.message = "";
        contentDescriptionInput = document.getElementById('contentDescriptionInput');
        contentDescriptionInput.classList.add('error');
        isValid = false;
        return isValid;
      } else {
        this.contentValidationMessages.contentDescription.invalid = false;
        this.contentValidationMessages.contentDescription.message = "";
        contentDescriptionInput = document.getElementById('contentDescriptionInput');
        contentDescriptionInput.classList.remove('error');
        isValid = true;
      }
    }

    if (this.contentTypeSelection.selectedType !== 'Survey') {


      if ((!this.url || this.url === "") && this.contentTypeSelection.selectedType !== 'Standard Operating Procedure Checklist') {
        this.contentValidationMessages.contentImage.invalid = true;
        this.contentValidationMessages.contentImage.message = "";
        imageInput = document.getElementById('imageInput');
        imageInput.classList.add('error');
        isValid = false;
        return isValid;
      } else {
        this.contentValidationMessages.contentImage.invalid = false;
        this.contentValidationMessages.contentImage.message = "";
        imageInput = document.getElementById('imageInput');
        imageInput.classList.remove('error');
        isValid = true;
      }
    }
    return isValid;
  }
}
