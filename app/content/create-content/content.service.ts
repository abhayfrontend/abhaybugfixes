import { Injectable, OnInit } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import { PreviewService } from '../../shared/auth/preview.service';
import { CampaignGroupResourceService, DeviceResourceService, UserResourceService, IndustryResourceService } from '../../sthaapak';
import { Router } from '@angular/router';
import { base } from '../../shared/configs/util';
import swal from 'sweetalert2';
import { SupportGroupResourceService } from '../../sthaapak/sdk/supportGroupResource.service';
import { SurveyResourceService } from '../../sthaapak/sdk/surveyResource.service';
import { SurveyQuestionResourceService } from '../../sthaapak/sdk/surveyQuestionResource.service';
import { TaskGroupResourceService } from '../../sthaapak/sdk/taskGroupResource.service';
import { mergeMap } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import { JSONType } from 'aws-sdk/clients/s3';
// import * as admin from 'firebase-admin';
// import * as google from 'google-auth-library';

@Injectable({
  providedIn: 'root'
})
export class ContentService implements OnInit {
  deleted = [];
  contentArr = [];
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };
  /* Variables */
  topic = 'd3Ob4X_mEGs:APA91bFEBHmn6gJZ9lFL9rZF4Z4lpSo2MzrS8uLD73HHrgIT_keoVQ5rX1JY89mcIGPgpzEu_6tqorr5YxTYfINtCiyTSvhbVfG1MdH36eDvfHwlpTimtPu6OfM0aa1ZxqVzeoVnlMZZ';
  // See documentation on defining a message payload.
  serverKey = 'AAAAYHBeiJU:APA91bGjHHhLr7Vb-9NOXwRL9xLWiu4IxQB-TNOGgYhDxOasRT9jZZIW7l780O1VPq4rWX_Ak75FUbjObzh45CBnqfYV8oq_NhC_-EQRgJaoayYqwhZjj6wRAxG_dRHEiZnXJP1OBPpW'
  httpOptionsFCM = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'key= ' + this.serverKey
    })
  };

  message = {
    notification: {
      title: '$GOOG up 1.43% on the day',
      body: '$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.'
    },
    data: {
      score: '850',
      time: '2:45'
    },
    to: this.topic
  };
  /*
  PROJECT_ID = 'yuva-191013';
  HOST = 'fcm.googleapis.com';
  PATH = '/v1/projects /' + this.PROJECT_ID + '/messages:send';
  google = require('googleapis');
  MESSAGING_SCOPE = 'https://www.googleapis.com/auth/firebase.messaging';
  SCOPES = [this.MESSAGING_SCOPE];
  FCM = require('fcm-push');
  fcm = new this.FCM(this.serverKey); */
  construct;
  questionObject;
  questionId;
  contentType;
  responseCards;
  cards = [];
  microContent;
  contentToEdit;
  /* End Variables */
  current;
  now = new Date();
  coursePlan = '266702';
  org;
  formDataObj: any = {
    content: {
      contentType: 'MicroLearning',
      targetCategory: '',
      targetSubCategory: '',
      colorTheme: '',
      contentName: '',
      contentDescription: '',
      imageDetails: {
        url: '',
        name: ''
      }
    },
    contentSlides: [],
    contentQuestions: []
  }
  /* app = admin.initializeApp({
     credential: admin.credential.cert({
       projectId: 'yuva-191013',
       privateKey: '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCUQxMs15y8hPCP\nMMdoT7a657I+HXn/kj1Wq72xVx3Q2DL4xYUUKJrAn4kVMk7tF3TGYMTUm8+nfjCr\nzhFrtfLqlpbhEp1kjPDzGKIW6R4Kg5drRey/oWu+QnToxXgVp3UwwHmsi7LwEF4i\n0AXetyuu+Qq9wV6E0qf6HRv9zBbtxKvHHimPetNJnJS2MFfZW2uLDLS5W72QvNcm\nGX494rtukAXZTJiK2zytR1zG4P2xqE8qYvurhXU8ZzCGNvpIuguXofb0+MLRuV3E\nS/CYldyF8BOGWoB5XOq8vM2GwKFVTPjZsf1TIZL6o2ng82Rl+FnUvZpqRtAZcQp8\nvQXYd/fNAgMBAAECggEAAWkkTvur4zOnn1Er6znrnO/UnMMxNJBAEv72LTBimVNe\npB/VrOlYVEdgVEo4K94v2gMJ3xUEHuL0Pxo/mlpZmfJTXrJg5YKdeKQDciYx3JCV\nJFaUErG27FFGZVWbAie9ACOrX2EtPhwFwp00mm0nQQEKdrTsO5Aj5t0X4FcXYRfy\nLW/XuQxoFGACfHE8xIAQ4WpHHNyyxqJjIOEH6lhu00fGIbldCmvEQ8Qu9simcdCn\n0R8bzZVnaU3fuD/beXar5TFq51BTFpWwpSjepm85UcGpxAecEzk5GVe8gpWjQuv5\nzz4NmdMeClTcQUKnK/J8iTpLzh2GVUJyx0KqPbKucQKBgQDLotA/cmMa+u2qfO4b\n6EOSZfHIqiPeylDkSg/T7vYZf/8BiLRBuO2wcrcYEpPOCUA071NQo/k5uW1eKDW2\nXO+/QZX0Jh0ucp9Uq4oX1/ts9tzdRAhXiXM7GiNlOLlh3+2DiSkV4vznPb1Hhs+j\nc7gxlmpUAtmFOpHntrjC6WY8/QKBgQC6YwnwuhFFGzpHVMZxAgcH/MYi15D3lAfO\n5bAvgf66eeadRcCQqBCA5DPN8AVi64WA/TRN/+b2N7KM+JtCWjtAhUQuBibH9usA\n+2Xuhb5sjm6mA+csdf/tdh/QYSAutHaGKM3M+YwTy07rvHceilJLvB6CI2rINIXU\ncpC7Ef8HEQKBgGUG944ir+x4xWSp6m/KAYwmK5hEYNttQEiiPcwFZ+8qw1b9fYCF\nwNajcxbsUIwNpt8cRRdi4oiy6AGorU02OnOGlGC2cA/VKhsVp8NAkipzJwxXblQP\niy2tQBY92Csor2tspweGv7XxuXA8/9K13JItb78SUtx4PF7bFjsNFH35AoGBALFM\n4PnTpfX9DpUoOZTqftHETJ58PPSmJj9UEyM+H5g3uX5GCgYROZHCgt0Vw6nzrQ2k\nz4qbMZNqLDkZjGez+cVIgDDfvShe4/INM0uQgjI101+tOsz6erfitCS/H6QOGulI\nC20+vVdfAPYSU6YDooFUtBjLpBpKjDwpty+c0OJhAoGBAIHLTLkSiKuRnkeRMGZA\n1lKfy2YKb1KBo8LjtvH2PLW3y2NYPr/D1LYSbxTvFdmQDH0DzSQD7cGpotKWdq5S\nZrDalQrE7KIlcGuMCkmTse2REbKsvrvDK0PRTW81xPXR2G3pM28mxXaQDGlkoDBb\nIexhR2tS6uVq6/eehPoFgV1g\n-----END PRIVATE KEY-----\n',
       clientEmail: 'firebase-adminsdk-x635x@yuva-191013.iam.gserviceaccount.com'
     })
   });
   */
  constructor(public groupService: CampaignGroupResourceService, public deviceService: DeviceResourceService, private router: Router, private surveyResource: SurveyResourceService,
    private http: HttpClient, private roleGuardService: RoleGuardService, public userResource: UserResourceService, public previewService: PreviewService,
    private surveyQues: SurveyQuestionResourceService, private taskGroup: TaskGroupResourceService, private industry: IndustryResourceService) {


  }


  ngOnInit() {


    this.cards = [];

    if (Number(this.previewService.organizationId.getValue()) === 261351) {
      this.coursePlan = '211544';
    }
    if (this.now.getMonth() === 11) {
      this.current = new Date(this.now.getFullYear() + 1, 0, 1).toISOString();
    } else {
      this.current = new Date(this.now.getFullYear(), this.now.getMonth() + 1, 1).toISOString();
    }


  }

  resetFormObj() {
    this.formDataObj = {
      content: {
        contentType: '',
        targetCategory: '',
        targetSubCategory: '',
        colorTheme: '',
        contentName: '',
        contentDescription: '',
        imageDetails: {
          url: '',
          name: ''
        }
      },
      contentSlides: [],
      contentQuestions: []
    }
    this.contentArr = [];
  }

  updateContentSlide(id, obj) {
    let contentArr = this.formDataObj.contentSlides;
    for (let i = 0; i < contentArr.length; i++) {
      if (contentArr[i].id === id) {
        contentArr[i] = obj;
      }
    }
  }

  updateQuestionSlide(id, obj) {
    let questionsArr = this.formDataObj.contentQuestions;
    for (let i = 0; i < questionsArr.length; i++) {
      if (questionsArr[i].id === id) {
        questionsArr[i] = obj;
      }
    }
  }

  deleteContentSlide(contentSlide) {
    let contentArr = this.formDataObj.contentSlides;
    for (let i = 0; i < contentArr.length; i++) {
      if (contentArr[i].id === contentSlide.id) {
        contentArr.splice(i, 1);
        return;
      }
    }
  }

  deleteQuestionSlide(questionSlide) {
    let questionsArr = this.formDataObj.contentQuestions;
    for (let i = 0; i < questionsArr.length; i++) {
      if (questionsArr[i].id === questionSlide.id) {
        questionsArr.splice(i, 1);
        return;
      }
    }
  }

  getContentSlide(id) {
    let contentArr = this.formDataObj.contentSlides;
    for (let i = 0; i < contentArr.length; i++) {
      if (contentArr[i].id === id) {
        return contentArr[i];
      }
    }
  }

  getQuestionSlide(id) {
    let questionsArr = this.formDataObj.contentQuestions;
    for (let i = 0; i < questionsArr.length; i++) {
      if (questionsArr[i].id === id) {
        return questionsArr[i];
      }
    }
  }

  publishJobFamily(data) {
    return this.http.post("https://loginapi.productivise.io" + '/api/job-families', data, this.httpOptions).map(res => <any>res);
  }

  publishInfoCards(formData, org) {

    // console.log('form:', formData, org);
    let error;
    let arry = [];
    for (let i = 0; i < formData.contentSlides.length; i++) {
      arry.push({

        "name": formData.contentSlides[i].slideTitleName,
        "tags": formData.content.targetCategory,
        "description": null,
        "url": null,
        "image": formData.contentSlides[i].slideImageDetails['url'],
        "videoLink": formData.contentSlides[i].slideVideoDetails['url'],
        "pdfLink": formData.contentSlides[i].slideDocDetails['url'],
        "audioLink": formData.contentSlides[i].slideAudioDetails['url'],
        "content": formData.contentSlides[i].slideContentDescription,
        "bigImageLink": null,
        "displayOrder": null,
        "organizations": [org],
        "jobRoles": []

      })
    }
    // console.log('Arry:', arry);
    let source = of(arry);
    const example = source.pipe(mergeMap(q => forkJoin(...q.map(a => this.publishJobFamily(a)))));
    const subscribe = example.subscribe(val => {
      // console.log('VAL:', val);
      this.industry.createIndustryUsingPOST({

        "name": formData.content.contentName,
        "tags": formData.content.targetCategory,
        "description": null,
        "url": null,
        "images": null,
        "imageLink": formData.content.imageDetails['url'],
        "videoLink": null,
        "pdfLink": null,
        "audioLink": null,
        "content": formData.content.contentDescription,
        "displayOrder": 10,
        "extraData": [],
        "jobRoles": [],
        "organizations": [org],
        "jobFamilies": val,
        "additionalFiles": [],
        "multiLinguals": []
      }).subscribe(res => {
        // console.log('res:', res);
        swal('Success', 'Your Info Card has been successfully created.', 'success');
      }, err => {
        error = true;
      });
    });

    /*
     this.industry.createIndustryUsingPOST({
 
       "name": formData.content.contetntName,
       "tags": formData.content.targetCategory,
       "description": null,
       "url": null,
       "images": null,
       "imageLink": formData.contentSlides[i].slideImageDetails['url'],
       "videoLink": formData.contentSlides[i].slideVideoDetails['url'],
       "pdfLink": formData.contentSlides[i].slideDocDetails['url'],
       "audioLink": formData.contentSlides[i].slideAudioDetails['url'],
       "content": formData.contentSlides[i].slideContentDescription,
       "displayOrder": 10,
       "extraData": [],
       "jobRoles": [],
       "organizations": [org],
       "jobFamilies": [
 
       ],
       "additionalFiles": [],
       "multiLinguals": []
     }).subscribe(res => {
       // console.log('res:', res);
     }, err => {
       error = true;
     });
     for (let i = 0; i < formData.contentSlides.length; i++) {
       if (i !== formData.contentSlides.length - 1) {
 
 
       }
 
 
       if (i === formData.contentSlides.length - 1) {
         this.industry.createIndustryUsingPOST({
 
           "name": formData.contentSlides[i].slideTitleName,
           "tags": formData.content.targetCategory,
           "description": null,
           "url": null,
           "images": null,
           "imageLink": formData.contentSlides[i].slideImageDetails['url'],
           "videoLink": formData.contentSlides[i].slideVideoDetails['url'],
           "pdfLink": formData.contentSlides[i].slideDocDetails['url'],
           "audioLink": formData.contentSlides[i].slideAudioDetails['url'],
           "content": formData.contentSlides[i].slideContentDescription,
           "displayOrder": 10,
           "extraData": [],
           "jobRoles": [],
           "organizations": [org],
           "jobFamilies": [],
           "additionalFiles": [],
           "multiLinguals": []
         }).subscribe(res => {
           // console.log('res:', res);
 
           if (error) {
             swal('Some Info Cards could not be created', '', 'error');
           }
           else {
             swal('Info Cards were created Successfully.', '', 'success');
           }
 
         }, err => {
           swal('Some Info Cards could not be created', '', 'error');
         });
 
       }
 
 
 
 
 
     }
 
 */
  }

  updateInfoCards(formData, org) {

    this.industry.updateIndustryUsingPUT({
      "id": formData.contentSlides[0]["id"],
      "name": formData.contentSlides[0].slideTitleName,
      "tags": formData.content.targetCategory,
      "description": null,
      "url": null,
      "images": null,
      "imageLink": formData.contentSlides[0].slideImageDetails['url'],
      "videoLink": formData.contentSlides[0].slideVideoDetails['url'],
      "pdfLink": formData.contentSlides[0].slideDocDetails['url'],
      "audioLink": formData.contentSlides[0].slideAudioDetails['url'],
      "content": formData.contentSlides[0].slideContentDescription,
      "displayOrder": 10,
      "extraData": [],
      "jobRoles": [],
      "organizations": [org],
      "jobFamilies": [],
      "additionalFiles": [],
      "multiLinguals": []
    }).subscribe(res => {
      // console.log('res:', res);


      swal('Info Card was updated Successfully.', '', 'success');

    }, err => {
      // console.log(err);
      swal('Info Card could not be updated', '', 'error');
    });




  }

   async publishNewsVideo():Promise<any> {
    let response ;
    // console.log('formobj:', this.formDataObj);
    // console.log('publish body:', {

    //   'audioLink': null,
    //   'author': null,
    //   'category': this.formDataObj.content.targetCategory,
    //   'content': this.formDataObj.contentSlides[0].slideContentDescription,
    //   'careerPathId': 0,
    //   'image': null,
    //   'level': 0,
    //   'maximumExperince': 0,
    //   'minimumEducation': 'NOT_APPLICABLE',
    //   'minimumExperince': 0,

    //   'contentType': this.formDataObj.contentSlides[0].contentType,
    //   'copyRight': 'CREATIVE_COMMONS',
    //   'description': this.formDataObj.contentSlides[0].slideContentDescription || null,
    //   'language': 'ENGLISH',
    //   'moreContent': null,

    //   'name': this.formDataObj.content.contentName,
    //   'organizationId': this.previewService.organizationId.getValue(),
    //   'pdfLink': null,
    //   'previewImage': this.formDataObj.contentSlides[0].contentType.toUpperCase() === 'BLOG' ? this.formDataObj.contentSlides[0].slideImageDetails.url || null : '',
    //   'scheduled': null,
    //   'schemeUrl': this.previewService.Username.getValue(),
    //   'source': null,
    //   'state': 'DRAFT',
    //   'batTests': null,
    //   'postDate': new Date().toISOString(),
    //   'subCategory': this.formDataObj.content.targetSubCategory,
    //   'summary': this.formDataObj.content.contentDescription,
    //   'tags': null,
    //   'thumbnailImage': this.formDataObj.content.imageDetails.url || null,
    //   'type': 'REPACKED',
    //   'unlockScore': 0,
    //   'value': null,
    //   'videoLink': this.formDataObj.contentSlides[0].contentType.toUpperCase() === 'VIDEO' ? this.formDataObj.contentSlides[0].slideImageDetails.url || null : '',
    // });
   await this.http.post("https://loginapi.productivise.io" + '/api/published-contents', {

      'audioLink': null,
      'author': null,
      'category': this.formDataObj.content.targetCategory,
      'content': this.formDataObj.contentSlides[0].slideContentDescription,
      'careerPathId': 0,
      'image': null,
      'level': 0,
      'maximumExperince': 0,
      'minimumEducation': 'NOT_APPLICABLE',
      'minimumExperince': 0,
      'postDate': new Date().toISOString(),
      'contentType': this.formDataObj.contentSlides[0].contentType,
      'copyRight': 'CREATIVE_COMMONS',
      'description': this.formDataObj.contentSlides[0].slideContentDescription || null,
      'language': 'ENGLISH',
      'moreContent': null,
      'name': this.formDataObj.content.contentName,
      'organizationId': this.previewService.organizationId.getValue(),
      'pdfLink': null,
      'previewImage': this.formDataObj.contentSlides[0].contentType.toUpperCase() === 'BLOG' ? this.formDataObj.contentSlides[0].slideImageDetails.url || null : '',
      'scheduled': null,
      'schemeUrl': this.previewService.Username.getValue(),
      'source': null,
      'state': 'DRAFT',
      'batTests': null,
      'expiry': new Date('1971-01-01'),
      'subCategory': this.formDataObj.content.targetSubCategory,
      'summary': this.formDataObj.content.contentDescription,
      'tags': null,
      'thumbnailImage': this.formDataObj.content.imageDetails.url || null,
      'type': 'REPACKED',
      'unlockScore': 0,
      'value': null,
      'videoLink': this.formDataObj.contentSlides[0].contentType.toUpperCase() === 'VIDEO' ? this.formDataObj.contentSlides[0].slideImageDetails.url || null : '',
    }, this.httpOptions).subscribe(res => {
      // this.sendFcmMessage(this.message);
      // console.log('news-vieo:', res);
      response = res;
       return res;
      swal(
        
        this.formDataObj.content.contentType + ' has been created successfully.',
        'Your content was saved.',
        'success'
      ).then(a =>
        this.resetFormObj()
      );
      
        
    }, error => {
      swal(
        'Error',
        this.formDataObj.content.contentType + 'could not be created.',
        'error'
      )
    });
    return response;
  }
  publishOperationContent(id) {
    // console.log('op:', this.formDataObj);
    if (id) {
      this.taskGroup.deleteTaskGroupUsingDELETE(id).subscribe(deletedResp => {
        // console.log('del:', deletedResp);
      })
    }
    this.http.post(base + '/api/task-groups', {
      'name': this.formDataObj.content.contentName,
      'description': this.formDataObj.content.contentDescription,
      'startDate': new Date().toISOString(),
      'endDate': this.current,
      'repeat': false,
      'cron': null,
      'imageUrl': this.formDataObj.contentSlides[0].contentType.toUpperCase() === 'IMAGE' ? this.formDataObj.contentSlides[0].slideImageDetails.url || null : null,
      'videoUrl': this.formDataObj.contentSlides[0].contentType.toUpperCase() === 'VIDEO' ? this.formDataObj.contentSlides[0].slideImageDetails.url || null : null,
      'type': this.formDataObj.contentSlides[0].contentType,
      'organizationId': this.previewService.organizationId.getValue()
    }, this.httpOptions).subscribe(res => {
      swal(
        'Checklist has been created successfully.',
        'Your content was saved.',
        'success'
      );
      let result = <any>res;
      // console.log('tskgrp:', result);
      for (let index = 0; index < this.formDataObj.contentSlides.length; index++) {

        this.http.post(base + '/api/tasks', {
          'name': this.formDataObj.contentSlides[index].slideTitleName,
          'priority': index + 1,
          'displayOrder': index + 1,
          'description': this.formDataObj.contentSlides[index].slideContentDescription || null,
          'startDate': null,
          'endDate': null,
          'repeat': this.formDataObj.contentSlides[index].mandatory,
          'cron': null,
          'imageUrl': this.formDataObj.contentSlides[index].contentType.toUpperCase() === 'IMAGE' ? this.formDataObj.contentSlides[index].slideImageDetails.url || null : null,
          'videoUrl': this.formDataObj.contentSlides[index].contentType.toUpperCase() === 'VIDEO' ? this.formDataObj.contentSlides[index].slideImageDetails.url || null : null,
          'category': this.formDataObj.content.targetCategory,
          'subCategory': this.formDataObj.content.targetSubCategory,
          'tags': this.formDataObj.contentSlides[index].mandatory ? 'Please complete task then move forward' : null,
          'type': this.formDataObj.contentSlides[index].contentType.toUpperCase() === 'TEXT' ? null : this.formDataObj.contentSlides[index].contentType.toUpperCase(),
          'taskGroupId': result.id
        }, this.httpOptions).subscribe(response => {

          // console.log('task:', response)
          this.resetFormObj();

        });

      }
    })
  }

  publishQuizContent() {

    this.http.post(base + '/api/contructs', {

      'description': null,
      'name': this.formDataObj.content.contentName
    }, this.httpOptions).subscribe(resp => {
      this.construct = <any>resp;
      // console.log('construct:', this.construct);

      for (let y = 0; y < this.formDataObj.contentQuestions.length; y++) {
        this.questionObject = this.formDataObj.contentQuestions[y];
        // console.log('qOBJ:', this.formDataObj.contentQuestions[y]);
        this.http.post(base + '/api/questions', {

          'contructId': this.construct.id,
          'description': null,
          'imageURL': this.formDataObj.contentQuestions[y].imageUrl || null,
          'questionType': this.formDataObj.contentQuestions[y].qType,
          'questionFormat': this.formDataObj.contentQuestions[y].negative,
          'name': this.formDataObj.contentQuestions[y].subTitle,

        }, this.httpOptions).subscribe(res => {
          // console.log('question: ', res);
          this.questionId = res;
          // console.log('option1:', {
          //   'description': null,
          //   'imageURL': null,
          //   'name': this.formDataObj.contentQuestions[y].correctAnswer,
          //   'questionId': this.questionId.id,
          //   'score': 1
          // });

          if (this.formDataObj.contentQuestions[y].correctAnswer) {
            this.http.post(base + '/api/options', {
              'description': null,
              'imageURL': null,
              'name': this.formDataObj.contentQuestions[y].correctAnswer,
              'questionId': this.questionId.id,
              'score': 1
            }, this.httpOptions).subscribe(optionResponse => console.log("")/*console.log('optionResponse:', optionResponse)*/);
            for (let z = 0; z < this.formDataObj.contentQuestions[y].incorrectAnswers.length; z++) {
              this.http.post(base + '/api/options', {
                'description': null,
                'imageURL': null,
                'name': this.formDataObj.contentQuestions[y].incorrectAnswers[z].value,
                'questionId': this.questionId.id,
                'score': 0
              }, this.httpOptions).subscribe(optionsResp => console.log("") /*console.log(optionsResp)*/);
            }
          }
          else {
            for (let z = 0; z < this.formDataObj.contentQuestions[y].options.length; z++) {
              this.http.post(base + '/api/options', {
                'description': null,
                'imageURL': null,
                'name': this.formDataObj.contentQuestions[y].options[z].value,
                'questionId': this.questionId.id,
                'score': this.formDataObj.contentQuestions[y].options[z].marks
              }, this.httpOptions).subscribe(optionsResp => console.log("")/*console.log(optionsResp)*/);
            }
          }

        });
      }

      for (let x = 0; x < this.formDataObj.contentSlides.length; x++) {
        this.http.post(base + '/api/bat-tests', {
          'name': this.formDataObj.content.contentName,
          'description': null,
          'type': 'SURVEY', // feedback
          'maxTimeAllowed': 0,
          'intructionsImage': null,
          'organizationId': this.previewService.organizationId.getValue(),
          'contructs': [
            {
              'id': this.construct.id,
              'name': this.formDataObj.content.contentName,
              'description': null,
              'timeLimit': 0,
              'multiLinguals': []
            }
          ],
          'contentCards': [],
          'multiLinguals': [],
          'microContentId': null
        }, this.httpOptions).subscribe(micoContentReponse => {
          // console.log(micoContentReponse);
          swal(
            'Quiz has been created successfully.',
            'Your content was saved.',
            'success'
          ).then(a =>
            this.resetFormObj()
          );

        }, error => {
          // console.log('Err:', error);
          swal('Error', 'Quiz could not be created', 'error');

        });
      }
    });
  }

  publishSurveyContent(formData) {
    console.log('survey:', {
      's': formData.contentQuestions[0].incorrectAnswers.map(e => e.value),

      "name": formData.content.contentName,
      "description": null,
      "category": formData.content.targetCategory,
      "region": null,
      "publishDate": null,
      "expiryDate": new Date('1971-01-01'),
      "eventName": null,
      "emailSubmit": null,
      "organizationId": this.previewService.organizationId.getValue()
    });
    console.log('q:', formData)

    this.surveyResource.createSurveyUsingPOST({

      "name": formData.content.contentName,
      "description": null,
      "category": formData.content.targetCategory,
      "region": null,
      "publishDate": null,
      "expiryDate": new Date('1971-01-01'),
      "eventName": null,
      "emailSubmit": null,
      "organizationId": Number(this.previewService.organizationId.getValue())
    }).subscribe(resp => {
      this.construct = <any>resp;
      // console.log('construct:', this.construct);
      swal('Survey has been created Successfully.', 'Your content was created', 'success');
      for (let i = 0; i < formData.contentQuestions.length; i++) {
        if (formData.contentQuestions[i].qType === 'IMAGE') {
          this.surveyQues.createSurveyQuestionUsingPOST({
            "name": formData.contentQuestions[i].title,
            "description": null,
            "displayOrder": i,
            "defaultData": null,
            "correctAnswer": null,
            "type": "IMAGE",
            "surveyId": this.construct.id
          }).subscribe(q => console.log()/* console.log(q)*/)
        }
        if (formData.contentQuestions[i].qType === 'TEXT') {
          this.surveyQues.createSurveyQuestionUsingPOST({
            "name": formData.contentQuestions[i].title,
            "description": null,
            "displayOrder": i,
            "defaultData": null,
            "correctAnswer": null,
            "type": "TEXT",
            "surveyId": this.construct.id
          }).subscribe(q => console.log()/* console.log(q)*/)
        }
        if (formData.contentQuestions[i].qType === 'OPTION') {
          this.surveyQues.createSurveyQuestionUsingPOST({
            "name": formData.contentQuestions[i].title,
            "description": null,
            "displayOrder": i,
            "defaultData": String(formData.contentQuestions[i].incorrectAnswers.map(e => e.value).join(',')),
            "correctAnswer": null,
            "type": "OPTION",
            "surveyId": this.construct.id
          }).subscribe(q => console.log() /* console.log(q)*/)
        }

      }

    }, error => {
      // console.log('Err:', error);
      swal('Error', 'Survey could not be created', 'error');

    });

  }

  updateSurveyContent(formData) {
    this.deleted.forEach(e => {
      this.http.delete(base + '/api/survey-questions/' + e.id, this.httpOptions).toPromise().then(res => console.log()/* console.log(e, res)*/)
    })
    // console.log('survey:', {
    //   "name": formData.content.contentName,
    //   "description": null,
    //   "category": formData.content.targetCategory,
    //   "region": null,
    //   "publishDate": null,
    //   "expiryDate": new Date('1971-01-01'),
    //   "eventName": null,
    //   "emailSubmit": null,
    //   "organizationId": this.previewService.organizationId.getValue()
    // });
    // console.log('q:', formData)

    this.surveyResource.updateSurveyUsingPUT({
      "id": formData.content.colorTheme,
      "name": formData.content.contentName,
      "description": null,
      "category": formData.content.targetCategory,
      "region": null,
      "publishDate": null,
      "expiryDate": new Date('1971-01-01'),
      "eventName": null,
      "emailSubmit": null,
      "organizationId": Number(this.previewService.organizationId.getValue())
    }).subscribe(resp => {
      this.construct = <any>resp;
      // console.log('construct:', this.construct);
      for (let i = 0; i < formData.contentQuestions.length; i++) {
        if (formData.contentQuestions[i].qType === 'IMAGE') {
          this.surveyQues.updateSurveyQuestionUsingPUT({
            "id": formData.contentQuestions[i].id,
            "name": formData.contentQuestions[i].title,
            "description": null,
            "displayOrder": i,
            "defaultData": null,
            "correctAnswer": null,
            "type": "IMAGE",
            "surveyId": this.construct.id
          }).subscribe(q => console.log() /* console.log(q)*/)
        }
        if (formData.contentQuestions[i].qType === 'TEXT') {
          this.surveyQues.updateSurveyQuestionUsingPUT({
            "id": formData.contentQuestions[i].id,
            "name": formData.contentQuestions[i].title,
            "description": null,
            "displayOrder": i,
            "defaultData": null,
            "correctAnswer": null,
            "type": "TEXT",
            "surveyId": this.construct.id
          }).subscribe(q => console.log())// console.log(q))
        }
        if (formData.contentQuestions[i].qType === 'OPTION') {
          this.surveyQues.updateSurveyQuestionUsingPUT({
            "id": formData.contentQuestions[i].id,
            "name": formData.contentQuestions[i].title,
            "description": null,
            "displayOrder": i,
            "defaultData": formData.contentQuestions[i].correctAnswer + ',' + String(formData.contentQuestions[i].incorrectAnswers.map(e => e.value).join(',')),
            "correctAnswer": null,
            "type": "OPTION",
            "surveyId": this.construct.id
          }).subscribe(q => console.log())// console.log(q))
        }

      }

    });


  }

  publishHTMLCourse(formData) {
    if (Number(this.previewService.organizationId.getValue()) === 261351) {
      this.coursePlan = '211544';
    }
    this.cards = [];
    let formDataObj = Object.assign({}, formData);
    // console.log('formdataobj in : ', formDataObj);
    let slidesLength = formDataObj.contentSlides.length;

    this.http.post(base + '/api/contructs', {

      'description': null,
      'name': formDataObj.content.contentName
    }, this.httpOptions).subscribe(resp => {
      this.construct = <any>resp;
      // console.log('construct:', this.construct);


      // console.log('formslides:', formDataObj.contentSlides);

      for (let x = 0; x < slidesLength; x++) {
        // console.log('contentslides:', formDataObj.contentSlides, x);
        this.http.post(base + '/api/content-cards',
          {
            'cards': [], 'sequence': x, 'name': formDataObj.contentSlides[x].slideTitleName,
            'description': x === 1 ? 'sify2.productivise.io.s3-website-us-east-1.amazonaws.com' : formDataObj.contentSlides[x].slideZipUrl, schemeUrl: 'webview'
          }, this.httpOptions)
          .subscribe(responseCard => {
            // console.log('card:', responseCard);
            this.responseCards = responseCard;
            // console.log('card in cards:', this.responseCards.cards[0]);
            this.cards.push(this.responseCards);
            // console.log('cardsArray:', this.cards);
            // console.log('x before:', x, slidesLength);

            if (x === slidesLength - 1) {
              // console.log('x:', x, slidesLength);
              let interval = setInterval(() => {
                if (this.cards.length === slidesLength) {
                  // console.log('x in:', x, this.cards.length, slidesLength);
                  this.http.post(base + '/api/micro-contents', JSON.parse(JSON.stringify({
                    'about': null,
                    'cards': this.cards,
                    'category': formDataObj.content.targetCategory,
                    'description': formDataObj.content.contentDescription,
                    'postDate': new Date().toISOString(),
                    'message': null,
                    'name': formDataObj.content.contentName,
                    'organizationId': this.previewService.organizationId.getValue(),
                    'previewUrl': null,
                    'expiryDate': new Date('1971-01-01'),
                    'scheduled': null,
                    'subcategory': formDataObj.content.targetSubCategory,
                    'summary': formDataObj.content.contentDescription || null,
                    'tags': null,
                    'thumbnailUrl': formDataObj.content.imageDetails.url || null,
                    'unlockScore': 0,
                    'videoLink': null
                  })), this.httpOptions).subscribe(micro => {
                    // console.log('micro: ', micro);
                    this.microContent = <any>micro;
                    this.http.get(base + '/api/course-plans/' + this.coursePlan, this.httpOptions).subscribe(e => {
                      let courseResult = <any>e;
                      courseResult.microContents.push(this.microContent);
                      this.http.put(base + '/api/course-plans', courseResult, this.httpOptions).subscribe(putcourse => {
                        // console.log('putCourse:', putcourse);
                        this.resetFormObj();
                        swal(
                          'HTML5 Course has been created SuccessFully',
                          'Your content was created.',
                          'success'
                        ).then(a =>
                          // console.log(a)
                          console.log()
                        );
                      })
                    }, err => {
                      swal(
                        'Error',
                        'HTML5 Course could not be created.',
                        'error'
                      )
                    })
                    // this.notificationSuccessfullySent(this.titles[0], this.microContent.name ? true : false);
                    // this.resetService();

                  });
                  clearInterval(interval);
                }
                else console.log();// console.log('cardsNOW: ', this.cards);
              }, 500);
            }
          });

      }
    }, err => {
      // console.log(err);
      throw err;

    });


  }




  publishMicroContent(formData, coursePlans) {
    if (Number(this.previewService.organizationId.getValue()) === 261351) {
      this.coursePlan = '211544';
    }
    this.cards = [];
    let formDataObj = Object.assign({}, formData);
    // console.log('formdataobj in : ', formDataObj);
    let slidesLength = formDataObj.contentSlides.length;

    this.http.post(base + '/api/contructs', {

      'description': null,
      'name': formDataObj.content.contentName
    }, this.httpOptions).subscribe(resp => {
      this.construct = <any>resp;
      // console.log('construct:', this.construct);
      for (let y = 0; y < formDataObj.contentQuestions.length; y++) {
        this.questionObject = formDataObj.contentQuestions[y];
        // console.log('qOBJ:', formDataObj.contentQuestions[y]);
        this.http.post(base + '/api/questions', {

          'contructId': this.construct.id,
          'description': null,
          'imageURL': formDataObj.contentQuestions[y].imageUrl || null,
          'questionType': this.formDataObj.contentQuestions[y].qType,
          'questionFormat': this.formDataObj.contentQuestions[y].negative,
          'name': formDataObj.contentQuestions[y].subTitle,

        }, this.httpOptions).subscribe(res => {
          // console.log('question: ', res);
          this.questionId = res;
          // console.log('option1:', {
          //   'description': null,
          //   'imageURL': null,
          //   'name': formDataObj.contentQuestions[y].correctAnswer,
          //   'questionId': this.questionId.id,
          //   'score': 1
          // });
          if (formDataObj.contentQuestions[y].correctAnswer) {
            this.http.post(base + '/api/options', {
              'description': null,
              'imageURL': null,
              'name': formDataObj.contentQuestions[y].correctAnswer,
              'questionId': this.questionId.id,
              'score': 1
            }, this.httpOptions).subscribe(optionResponse => console.log()/* console.log('optionResponse:', optionResponse)*/);
            for (let z = 0; z < formDataObj.contentQuestions[y].incorrectAnswers.length; z++) {
              this.http.post(base + '/api/options', {
                'description': null,
                'imageURL': null,
                'name': formDataObj.contentQuestions[y].incorrectAnswers[z].value,
                'questionId': this.questionId.id,
                'score': 0
              }, this.httpOptions).subscribe(optionsResp => console.log()/* console.log(optionsResp)*/);
            }
          }
          else {
            for (let z = 0; z < formDataObj.contentQuestions[y].options.length; z++) {
              this.http.post(base + '/api/options', {
                'description': null,
                'imageURL': null,
                'name': formDataObj.contentQuestions[y].options[z].value,
                'questionId': this.questionId.id,
                'score': formDataObj.contentQuestions[y].options[z].marks
              }, this.httpOptions).subscribe(optionsResp => console.log() /* console.log(optionsResp)*/);
            }
          }

        });
      }

      // console.log('formslides:', formDataObj.contentSlides);

      for (let x = 0; x < slidesLength; x++) {

        this.http.post(base + '/api/bat-tests', {
          'name': formDataObj.content.contentName,
          'description': null,
          'type': 'SURVEY',//feedback
          'maxTimeAllowed': 0,
          'intructionsImage': null,
          'organizationId': this.previewService.organizationId.getValue(),
          'contructs': [
            {
              'id': this.construct.id,
              'name': formDataObj.content.contentName,
              'description': null,
              'timeLimit': 0,
              'multiLinguals': []
            }
          ],
          'contentCards': [],
          'multiLinguals': [],
          'microContentId': null
        }, this.httpOptions).subscribe(micoContentReponse => {
          // console.log(micoContentReponse);

          // console.log('publish body:', {

          //   'audioLink': null,
          //   'author': null,
          //   'category': formDataObj.content.targetCategory,
          //   'content': formDataObj.contentSlides[x].slideContentDescription,
          //   'careerPathId': 0,
          //   'image': null,
          //   'level': 0,
          //   'maximumExperince': 0,
          //   'minimumEducation': 'NOT_APPLICABLE',
          //   'minimumExperince': 0,
          //   'expiry': new Date('1971-01-01'),
          //   'contentType': formDataObj.contentSlides[x].contentType,
          //   'copyRight': 'CREATIVE_COMMONS',
          //   'description': formDataObj.contentSlides[x].slideContentDescription || null,
          //   'language': 'ENGLISH',
          //   'moreContent': null,

          //   'name': formDataObj.contentSlides[x].slideTitleName,
          //   'organizationId': this.previewService.organizationId.getValue(),
          //   'pdfLink': null,
          //   'previewImage': formDataObj.contentSlides[x].contentType.toUpperCase() === 'BLOG' ? formDataObj.contentSlides[x].slideImageDetails.url || null : '',
          //   'scheduled': null,
          //   'schemeUrl': this.previewService.Username.getValue(),
          //   'source': null,
          //   'state': 'DRAFT',
          //   'batTests': (x === slidesLength - 1) && formDataObj.contentQuestions.length ? [micoContentReponse] : null,
          //   'postDate': null,
          //   'subCategory': formDataObj.content.targetSubCategory,
          //   'summary': formDataObj.contentSlides[x].slideContentDescription,
          //   'tags': null,
          //   'thumbnailImage': formDataObj.content.imageDetails.url || null,
          //   'type': 'REPACKED',
          //   'unlockScore': 0,
          //   'value': null,
          //   'videoLink': formDataObj.contentSlides[x].contentType.toUpperCase() === 'VIDEO' ? formDataObj.contentSlides[x].slideImageDetails.url || null : '',
          // });
          this.http.post(base + '/api/published-contents', {

            'audioLink': null,
            'author': null,
            'category': formDataObj.content.targetCategory,
            'content': formDataObj.contentSlides[x].slideContentDescription,
            'careerPathId': 0,
            'image': null,
            'level': 0,
            'maximumExperince': 0,
            'minimumEducation': 'NOT_APPLICABLE',
            'minimumExperince': 0,
            'postDate': new Date().toISOString(),
            'contentType': formDataObj.contentSlides[x].contentType,
            'copyRight': 'CREATIVE_COMMONS',
            'description': formDataObj.contentSlides[x].slideContentDescription || null,
            'language': 'ENGLISH',
            'moreContent': null,
            'expiry': new Date('1971-01-01'),
            'name': formDataObj.contentSlides[x].slideTitleName,
            'organizationId': this.previewService.organizationId.getValue(),
            'pdfLink': null,
            'previewImage': formDataObj.contentSlides[x].contentType.toUpperCase() === 'BLOG' ? formDataObj.contentSlides[x].slideImageDetails.url || null : '',
            'scheduled': null,
            'schemeUrl': this.previewService.Username.getValue(),
            'source': null,
            'state': 'DRAFT',
            'batTests': (x === slidesLength - 1) && formDataObj.contentQuestions.length ? [micoContentReponse] : null,

            'subCategory': formDataObj.content.targetSubCategory,
            'summary': formDataObj.contentSlides[x].slideContentDescription,
            'tags': null,
            'thumbnailImage': formDataObj.content.imageDetails.url || null,
            'type': 'REPACKED',
            'unlockScore': 0,
            'value': null,
            'videoLink': formDataObj.contentSlides[x].contentType.toUpperCase() === 'VIDEO' ? formDataObj.contentSlides[x].slideImageDetails.url || null : '',
          }, this.httpOptions).subscribe(res => {

            // console.log(res);
            // console.log('contentslides:', formDataObj.contentSlides, x);
            this.http.post(base + '/api/content-cards',
              { 'cards': [res], 'sequence': x, 'name': formDataObj.contentSlides[x].slideTitleName }, this.httpOptions)
              .subscribe(responseCard => {
                // console.log('card:', responseCard);
                this.responseCards = responseCard;
                // console.log('card in cards:', this.responseCards.cards[0]);
                this.cards.push(this.responseCards);
                // console.log('cardsArray:', this.cards);
                // console.log('x before:', x, slidesLength);

                if (x === slidesLength - 1) {
                  // console.log('x:', x, slidesLength);
                  let interval = setInterval(() => {
                    if (this.cards.length === slidesLength) {
                      // console.log('x in:', x, this.cards.length, slidesLength);
                      this.http.post(base + '/api/micro-contents', JSON.parse(JSON.stringify({
                        'about': null,
                        'cards': this.cards,
                        'category': formDataObj.content.targetCategory,
                        'description': formDataObj.content.contentDescription,
                        'postDate': new Date().toISOString(),
                        'message': null,
                        'name': formDataObj.content.contentName,
                        'organizationId': this.previewService.organizationId.getValue(),
                        'previewUrl': null,
                        'expiryDate': new Date('1971-01-01'),
                        'scheduled': null,
                        'subcategory': formDataObj.content.targetSubCategory,
                        'summary': formDataObj.content.contentDescription || null,
                        'tags': null,
                        'thumbnailUrl': formDataObj.content.imageDetails.url || null,
                        'unlockScore': 0,
                        'videoLink': null
                      })), this.httpOptions).subscribe(micro => {
                        // console.log('micro: ', micro);
                        this.microContent = <any>micro;
                        for (let index = 0; index < coursePlans.length; index++) {
                          this.http.get(base + '/api/course-plans/' + coursePlans[index], this.httpOptions).subscribe(e => {
                            let courseResult = <any>e;
                            courseResult.microContents.push(this.microContent);
                            this.http.put(base + '/api/course-plans', courseResult, this.httpOptions).subscribe(putcourse => {
                              // console.log('putCourse:', putcourse);

                            })
                          }, err => {
                            swal(
                              'Error',
                              'Micro Learning could not be created.',
                              'error'
                            )
                          })
                        }

                        this.resetFormObj();
                        swal(
                          'Micro Learning has been created SuccessFully.',
                          'Your content was created.',
                          'success'
                        ).then(a =>
                          // console.log(a)
                          console.log()
                        );

                        // this.notificationSuccessfullySent(this.titles[0], this.microContent.name ? true : false);
                        // this.resetService();


                      });
                      clearInterval(interval);
                    }
                    else console.log() //console.log('cardsNOW: ', this.cards);
                  }, 500);
                }
              });
          }, err => {
            throw err;
          });
        }, err => {
          throw err;
        });
      }
    }, err => {
      // console.log(err);
      throw err;

    });
  }



  /*
      this.fcm.send(this.message).then((response) => {
        // Response is a message ID string.
        // console.log('Successfully sent message:', response);
      }, (error) => {
        // console.log('Error sending message:', error);
      });
      */
}
  /*
    getAccessToken() {
      return new Promise(function (resolve, reject) {
        var key = require('../../shared/configs/yuva.json');
        var jwtClient = new this.google.auth.JWT(
          key.client_email,
          null,
          key.private_key,
          this.SCOPES,
          null
        );
        jwtClient.authorize(function (err, tokens) {
          if (err) {
            reject(err);
            return;
          }
          resolve(tokens.access_token);
        });
      });
    }
    sendFcmMessage(fcmMessage) {
      this.getAccessToken().then(function (accessToken) {
        let options = {
          hostname: this.HOST,
          path: this.PATH,
          method: 'POST',
          headers: {
            'Authorization': 'Bearer' + accessToken
          }
          // … plus the body of your notification or data message
        };
        let request = this.https.request(options, function (resp) {
          resp.setEncoding('utf8');
          resp.on('data', function (data) {
            // console.log('Message sent to Firebase for delivery, response: ');
            // console.log(data);
          });
        });
        this.request.on('error', function (err) {
          // console.log('Unable to send message to Firebase');
          // console.log(err);
        });
        request.write(JSON.stringify(fcmMessage));
        request.end();
      });
    }
  
  }
  */

  /*
  // Send a message to devices subscribed to the provided topic.
  admin.messaging().send(this.message)
    .then((response) => {
      // Response is a message ID string.
      // console.log('Successfully sent message:', response);
    })
    .catch((error) => {
      // console.log('Error sending message:', error);
    });
    
  
  function getAccessToken() {
    return new Promise(function (resolve, reject) {
      let key = require('./service-account.json');
      let jwtClient = new google.auth.(
        key.client_email,
        null,
        key.private_key,
        SCOPES,
        null
      );
      jwtClient.authorize(function (err, tokens) {
        if (err) {
          reject(err);
          return;
        }
        resolve(tokens.access_token);
      });
    });
    */

