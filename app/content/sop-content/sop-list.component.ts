import { Component, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable/release';
import { PublishedContentResourceService, MicroContentResourceService } from '../../sthaapak';
import { PreviewService } from '../../shared/auth/preview.service';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import swal from 'sweetalert2';
import { base } from '../../shared/configs/util';


@Component({
    selector: 'app-dt-sop',
    templateUrl: './sop-list.component.html',
    styleUrls: ['./sop-list.component.scss']
})

export class SOPListComponent implements OnInit {
    rows = [];
    temp = [];
    temp2 = [];
    filterOption;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.roleGuardService.token
        })
    };
    organizationId;
    // Table Column Titles
    columns = [
        { prop: 'name' },

        { prop: 'state' },
        { prop: 'postDate' },
        { prop: 'endDate' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private previewService: PreviewService, private router: Router,
        private microResource: MicroContentResourceService, private http: HttpClient, private roleGuardService: RoleGuardService) {
        if (!localStorage.getItem('token')) {
            this.router.navigateByUrl('/login');
        }
    }
    ngOnInit() {

        this.organizationId = this.previewService.organizationId.getValue();
        this.getContentList();
    }

    changeFilter(e) {
        // console.log('filter:', this.temp);
        this.filterOption = e.target.value;
        this.rows = this.temp;
    }

    changeFilterLater(e) {
        swal('Filter Unavailable',
            'Work in Progress. This filter will be available soon.',
            'info'

        )
    }

    getContentList() {
        this.temp = [];
        this.rows = [];
        this.temp2 = [];
        let cards = [];

        this.http.get(base + '/api/task-groups', this.httpOptions).subscribe(res => {
            let result = <any>res;
            // console.log('resil:', result);
            result = result.filter(e => {
                e.endDate = new Date(e.endDate).toDateString();
                e.postDate = new Date(e.postDate).toDateString();
                if (this.organizationId === e.organizationId) {

                    return true;
                }
                return false;
            })
            this.temp = [...result];
            this.rows = this.temp;
            //this.temp = [...this.temp, ...this.temp2];


            // console.log('temp:', this.temp);
        });
    }


    filterListByType(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {

            if (d.description === val) {
                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByAuthor(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            return d.author === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByStatus(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log(d.state);
            // console.log(val);
            return d.state === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByCreatedDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log('date:', new Date(d.startDate))
            if (new Date(d.startDate).getDate() === new Date(val).getDate() && new Date(d.startDate).getMonth() === new Date(val).getMonth() && new Date(d.startDate).getFullYear() === new Date(val).getFullYear()) {


                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPublDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {

            if (new Date(d.endDate).getDate() === new Date(val).getDate() && new Date(d.endDate).getMonth() === new Date(val).getMonth() && new Date(d.endDate).getFullYear() === new Date(val).getFullYear()) {

                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    reset() {
        this.filterOption = '';
        this.rows = this.temp;
    }

    scheduleContent(obj) {

        swal({
            title: 'Are you sure you want to schedule this content?',
            text: "This will take you to the Content Scheduler!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, schedule it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.previewService.editContentToSchedule(obj);
                this.router.navigate(['/scheduler/scheduler']);
            }
            else {
                swal(
                    'Cancelled',
                    'Your content was not scheduled.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your content was not scheduled.',
                    'error'
                )
            }
        })

    }

    deleteContent(row) {

        swal({
            title: 'Are you sure you want to delete this content?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.http.delete(base + '/api/task-groups/' + row.id, this.httpOptions).subscribe(res => {
                    // console.log('res:', res);
                    this.getContentList();
                });
            }
            else {
                swal(
                    'Cancelled',
                    'Your content was not deleted.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your content was not deleted.',
                    'error'
                )
            }
        })




    }

    editContent(obj) {

        let formData = {
            id: obj.id,
            contentType: 'Standard Operating Procedure Checklist',
            category: 'OPERATIONS',
            subcategory: 'PENDING',
            colorTheme: 'blue',
            name: obj.name,
            description: obj.description,
            imageDetails: {
                url: obj.imageUrl,
                name: 'image'
            },
            cards: []

        };

        swal({
            title: 'Are you sure you want to edit this content?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.http.get(base + '/api/tasks', this.httpOptions).subscribe(res => {
                    // console.log('res:', res);
                    let result = <any>res;
                    let i = 0, j = 0;

                    result = result.filter((element) => {
                        j++;
                        if (element.taskGroupId === obj.id) {
                            formData.cards.push({
                                slideContentDescription: element.name,
                                contentType: element.type,
                                slideTitleName: element.name,
                                slideImageDetails: {
                                    url: element.imageUrl,
                                    name: 'image'
                                },
                                id: element.priority
                            });
                            i++;
                            return true;
                        }
                        return false;
                    });
                    // console.log(i, result);
                    if (i === result.length) {
                        let interval = setInterval(() => {
                            // console.log('cards formdata:', formData.cards);
                            if (formData.cards.length === i) {
                                clearInterval(interval);
                                this.previewService.editContentToEdit(formData);
                                // console.log('final SOP:', formData, i, formData.cards.length);
                                this.router.navigate(['/content/sopEdit']);

                            }
                        }, 500);
                    }


                });
            }
            else {
                swal(
                    'Cancelled',
                    'Your content was not edited.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your content was not edited.',
                    'error'
                )
            }
        })





    }
    getCellClass({ row, column, value }): any {
        return {
            'is-unReview': value === 'Under Review',
            'is-published': value === 'Published',
            'is-approved': value === 'Approved',
            'is-expired': value === 'Expiry',
            'is-rejected': value === 'Rejected',
        };
    }

    disableContent(row) {
        row.enabled = false;
        this.http.put(base + '/api/tasks', row, this.httpOptions).toPromise().then(res => {
            // console.log(res);
            this.getContentList();
        })
    }
    enableContent(row) {
        row.enabled = true;
        this.http.put(base + '/api/tasks', row, this.httpOptions).toPromise().then(res => {
            // console.log(res);
            this.getContentList();
        })
    }
}
