import { Component, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable/release';
import { PublishedContentResourceService, MicroContentResourceService, CoursePlanResourceService } from '../../sthaapak';
import { PreviewService } from '../../shared/auth/preview.service';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import swal from 'sweetalert2';
import { base, proxyApi } from '../../shared/configs/util';


@Component({
    selector: 'app-dt-filter',
    templateUrl: './micro-content-list.component.html',
    styleUrls: ['./micro-content-list.component.scss']
})

export class MicroContentListComponent implements OnInit {
    rows = [];
    temp = [];
    temp2 = [];
    status = ['DRAFT', 'PUBLISHED', 'UNDER REVIEW', 'EXPIRED', 'APPROVED', 'REJECTED'];
    types = ['BLOG', 'VIDEO'];
    coursePlan = '266702';
    filterOption;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.roleGuardService.token
        })
    };
    organizationId;
    coursePlans = [];
    // Table Column Titles
    columns = [
        { prop: 'name' },
        { prop: 'author' },
        { prop: 'state' },
        { prop: 'postDate' },
        { prop: 'publishedDate' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private contentService: PublishedContentResourceService, private previewService: PreviewService, private router: Router,
        private microResource: MicroContentResourceService, private http: HttpClient, private roleGuardService: RoleGuardService,
        private course: CoursePlanResourceService) {
        if (!localStorage.getItem('token')) {
            this.router.navigateByUrl('/login');
        }
    }
    ngOnInit() {

        this.organizationId = this.previewService.organizationId.getValue();
        this.getContentList();
        this.course.getAllCoursePlansUsingGET().subscribe(course => {
            this.coursePlans = <any>course;
            // console.log('plans:', this.coursePlans);
        })
    }

    getContentList() {
        this.temp = [];
        this.rows = [];
        this.temp2 = [];
        let cards = [];
        this.http.get(base + '/api/micro-contents', this.httpOptions).subscribe(res => {

            let result = <any>res;
            // console.log('resil:', result);
            // console.log('cards', cards);
            result.sort((a, b) => { return new Date(b.postDate).getTime() - new Date(a.postDate).getTime() });
            result = result.filter(element => {
                if (element.organizationId === this.organizationId) {
                    element.publishedDate = new Date(element.publishedDate).toDateString();
                    element.postDate = new Date(element.postDate).toDateString();
                    return true;
                }
                return false;
            });
            this.temp = [...result];
            this.rows = this.temp;
            //this.temp = [...this.temp, ...this.temp2];


            // console.log('temp:', this.temp);
        });
    }

    changeFilter(e) {
        // console.log('filter:', this.temp);
        this.filterOption = e.target.value;
        this.rows = this.temp;
    }

    filterListByType(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            if (val === 'VIDEO' && d.videoLink) {
                return true;

            }
            if (val === 'BLOG' && d.previewImage) {
                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByAuthor(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            return d.author === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByStatus(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log(d.state);
            // console.log(val);
            return d.state === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPostDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log('date:', d.postDate)
            if (new Date(d.postDate).getDate() === new Date(val).getDate() && new Date(d.postDate).getMonth() === new Date(val).getMonth() && new Date(d.postDate).getFullYear() === new Date(val).getFullYear()) {


                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPublDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {

            if (new Date(d.publishedDate).getDate() === new Date(val).getDate() && new Date(d.publishedDate).getMonth() === new Date(val).getMonth() && new Date(d.publishedDate).getFullYear() === new Date(val).getFullYear()) {

                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }


    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    scheduleContent(obj) {

        swal({
            title: 'Are you sure you want to schedule this content?',
            text: "This will take you to the Content Scheduler!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, schedule it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.previewService.editContentToSchedule(obj);
                this.router.navigate(['/scheduler/scheduler']);
            }
            else {
                swal(
                    'Cancelled',
                    'Your content was not scheduled.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your content was not scheduled.',
                    'error'
                )
            }
        })

    }

    reset() {
        this.filterOption = '';
        this.rows = this.temp;
    }

    sendUpdate(category, subcategory, screen) {
        let message = {
          topic: this.previewService.organizationId.getValue(),
          data: {
            silent: true,
            save: false,
            title: category,
            body: subcategory,
            screenType: screen,
            image: 'jpg',
            specialParameters: 'll',
            tags: 'Silent',
            orgId: this.organizationId,
            name: screen
          }
    
        }
        console.log('message:', message);
        this.http.post(proxyApi + '/contentUpdate', message, this.httpOptions).subscribe((response) => {
          // Response is a message ID string.
          // console.log('Successfully sent message:', response);
    
        }, (error) => {
          // console.log('Error sending message:', error);
        });
      }

    deleteContent(row) {
        console.log("row.category : " + row.category);
        if (Number(this.previewService.organizationId.getValue()) === 261351) {
            this.coursePlan = '211544';
        }
        swal({
            title: 'Are you sure you want to delete this Micro-learning Module?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                let deleteNow;
                let timerInterval1;
                swal({
                    title: 'Deleting!',
                    onOpen: () => {
                        console.log("row.value : " + row.category);
                        swal.showLoading();
                        for (let i = 0; i < this.coursePlans.length; i++) {
                            // console.log('1:', this.coursePlans[i]);
                            let microList = this.coursePlans[i].microContents;
                            // console.log('e.id:', row.id)
                            microList = microList.filter(element => element.id !== row.id);
                            this.coursePlans[i].microContents = microList;
                            // console.log('2', this.coursePlans[i])
                            this.http.put(base + '/api/course-plans', this.coursePlans[i], this.httpOptions).subscribe(putcourse => {
                                // console.log('putCourse:', putcourse);
                                if (i === this.coursePlans.length - 1) {
                                    deleteNow = true;
                                }
                            })
                        }
                        console.log("row.value : " + row.category);
                        timerInterval1 = setInterval(() => {
                            if (deleteNow) {
                                clearInterval(timerInterval1);
                                this.microResource.deleteMicroContentUsingDELETE(row.id).subscribe(res => {
                                    console.log('res:', res);
                                    console.log("row.value : " + row.category);
                                    this.sendUpdate(row.category, row.subcategory, 'Microlearning');
                                    swal(
                                        'SuccessFully Deleted Micro Learning',
                                        'Your content was deleted.',
                                        'success'
                                    ).then(a =>
                                        // console.log(a)
                                        console.log("")
                                    );
                                    this.getContentList();
                                });
                                // console.log('time1')
                            }
                        }, 500)

                    },
                    onClose: () => {
                        clearInterval(timerInterval1)
                    }
                }).then(ans => {
                    clearInterval(timerInterval1);
                });

            }
            else {
                swal(
                    'Cancelled',
                    'Your Micro-learning Module was not deleted.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your Micro-learning Module was not deleted.',
                    'error'
                )
            }
        })


    }

    editContent(obj) {
        if (Number(this.previewService.organizationId.getValue()) === 261351) {
            this.coursePlan = '211544';
        }
        swal({
            title: 'Are you sure you want to edit this Micro-learning Module?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            // console.log('obj:', obj)
            if (confirm.value === true) {
                obj.questionSlides = [];
                obj.cards.forEach(element => {
                    if ((element.cards.length > 0) && element.cards[0].batTests.length) {
                        // console.log('element:', element);
                        let optionCount = 0, kk = 0;
                        this.http.get(base + '/api/questions', this.httpOptions).subscribe(questionResponse => {
                            let questions = <any>questionResponse;

                            questions.forEach((e, j) => {

                                let incorrect = [];
                                let correct = '';
                                if (Number(e.contructId) === Number(element.cards[0].batTests[0].contructs[0].id)) {
                                    optionCount++;
                                    this.http.get(base + '/api/options', this.httpOptions).subscribe(optionResponse => {
                                        let options = <any>optionResponse;

                                        options.forEach((elem, k) => {

                                            kk = k;
                                            if ((Number(elem.questionId) === Number(e.id)) && (Number(elem.score) === 0)) {
                                                incorrect.push({ value: elem.name })
                                            }
                                            if ((Number(elem.questionId) === Number(e.id)) && (Number(elem.score) === 1)) {
                                                correct = elem.name
                                            }

                                            if (k === options.length - 1) {

                                                obj.questionSlides.push({
                                                    title: obj.name,
                                                    subTitle: e.description || e.name,
                                                    randomize: e.randomize,
                                                    mandatory: e.mandatory,
                                                    anonymous: e.anonymous,
                                                    correctAnswer: correct,
                                                    incorrectAnswers: [...incorrect],
                                                    imageUrl: e.imageUrl,
                                                    timer: {
                                                        timeLimit: e.timeLimit,
                                                        outOfTimePrompt: e.outOfTimeMessage
                                                    },
                                                    stars: e.stars,
                                                    id: e.id
                                                });
                                            }


                                        });
                                        // console.log('object:', obj);
                                    });
                                }
                                if ((j === questions.length - 1)) {
                                    let interval = setInterval(() => {
                                        // console.log('optionCount:', optionCount);
                                        if (obj.questionSlides.length === optionCount) {
                                            clearInterval(interval);
                                            // console.log('Final object:', obj);
                                            this.previewService.editContentToEdit(obj);
                                            this.router.navigate(['/content/microEdit']);
                                        }
                                    }, 500);

                                }
                            })
                        })
                    }
                    else {
                        // console.log('Final object:', obj);
                        this.previewService.editContentToEdit(obj);
                        this.router.navigate(['/content/microEdit']);
                    }
                });
            }
            else {
                swal(
                    'Cancelled',
                    'Your Micro-learning Module was not edited.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your Micro-learning Module was not edited.',
                    'error'
                )
            }
        })

    }
    getCellClass({ row, column, value }): any {
        return {
            'is-unReview': value === 'Under Review',
            'is-published': value === 'Published',
            'is-approved': value === 'Approved',
            'is-expired': value === 'Expiry',
            'is-rejected': value === 'Rejected',
        };
    }


    disableContent(row) {
        row.enabled = false;
        this.http.put(base + '/api/micro-contents', row, this.httpOptions).toPromise().then(res => {
            // console.log(res);
            this.getContentList();
        })
    }
    enableContent(row) {
        row.enabled = true;
        this.http.put(base + '/api/micro-contents', row, this.httpOptions).toPromise().then(res => {
            // console.log(res);
            this.getContentList();
        })
    }
}
