import { Component, OnInit, OnDestroy } from '@angular/core';
import { QuillEditorComponent } from 'ngx-quill/src/quill-editor.component';
import { PreviewService } from '../../shared/auth/preview.service';
import { FormGroup, FormControl, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as AWS from '../../shared/configs/aws.config';
import { PublishedContentResourceService, AppConfigResourceService } from '../../sthaapak';
import { Router } from '@angular/router';
import { base } from '../../shared/configs/util';

@Component({
  selector: 'app-content-edit',
  templateUrl: './content-edit.component.html',
  styleUrls: ['./content-edit.component.scss']
})



export class ContentEditComponent implements OnInit, OnDestroy {

  categories = [];
  subCategories = [];
  contentTypes = ['BLOG', 'VIDEO'];
  organizationId;
  media = 'Image';
  obj;
  fileName;
  myform: FormGroup;
  name: FormControl;
  contentType: FormControl;
  description: FormControl;
  category: FormControl;
  subCategory: FormControl;
  logo: String;
  path: String;
  constructor(private contentService: PublishedContentResourceService, private previewService: PreviewService,
    private appConfigService: AppConfigResourceService, private router: Router) {
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }

  createFormControls() {
    this.name = new FormControl(this.obj.name, Validators.required);
    this.description = new FormControl(this.obj.description, Validators.required);
    this.category = new FormControl(this.obj.category, [
      Validators.required
    ]);
    this.subCategory = new FormControl(this.obj.subCategory, [
      Validators.required
    ]);
    this.contentType = new FormControl(this.obj.contentType, [
      Validators.required
    ]);
  }

  createForm() {
    this.myform = new FormGroup({
      name: this.name,
      description: this.description,
      category: this.category,
      subCategory: this.subCategory,
      contentType: this.contentType
    });
    this.path = this.obj.previewImage || this.obj.videoLink
    // console.log('path:', this.path);
    // console.log('media', this.media);
    this.myform.get('contentType').setValue(this.obj.contentType, { onlySelf: true });
    this.myform.get('category').setValue(this.obj.category, { onlySelf: true });
    // console.log('sub:', this.obj.subCategory);
    setTimeout(() => {
      this.selectSubCategory();
      this.myform.get('subCategory').setValue(this.obj.subCategory, { onlySelf: true })
    }, 1000);
  }


  ngOnInit() {

    this.obj = <any>this.previewService.contentToEdit.getValue();
    // console.log('ObJ: ', this.obj);
    this.previewService.organizationIdCast.subscribe(org => {
      this.organizationId = org;
      this.appConfigService.getAllAppConfigsByOrgUsingGET(Number(org)).subscribe(res => {
        let result = <any>res;
        this.categories = result.filter(e => {
          if (e.category === 'DASHBOARD') {
            return true;
          }
          return false;
        });

      });
      this.createFormControls();
      this.createForm();

    });

  }

  ngOnDestroy() {
    this.obj = {};
    this.previewService.editContentToEdit({});
  }

  onSubmit() {
    if (this.media === 'BLOG') {
      this.obj.previewImage = this.path;
      this.obj.videoLink = null;
    }
    if (this.media === 'VIDEO') {
      this.obj.videoLink = this.path;
      this.obj.previewImage = null;
    }
    // console.log('Form Submitted!');


    let DTO = Object.assign(this.obj, this.myform.value,
      {
        organizationId: this.previewService.organizationId.getValue(), postDate: new Date(this.obj.postDate).toISOString(),
        publishedDate: this.obj.publishedDate ? new Date(this.obj.publishedDate).toISOString() : null,
        expiry: this.obj.expiry ? new Date(this.obj.expiry).toISOString() : null
      });
    // console.log(DTO);
    this.contentService.updatePublishedContentUsingPUT(DTO).subscribe(res => {
      // console.log('response to put:', res);
      this.router.navigate(['/content/list']);
    })
    this.myform.reset();
  }

  fileEventThumbnail(fileInput: any) {


    const file = fileInput.target.files[0];

    let upload = AWS.s3.upload({ Key: file.name + new Date(), Bucket: AWS.bucketName, Body: file, ACL: 'public-read' });
    let promise = upload.promise();
    promise.then((data) => {
      // console.log(data);
      this.fileName = file.name;
      this.path = data.Location;
    }, (err) => {
      // console.log(err);
    });

  }
  selectSubCategory() {
    this.categories.forEach(element => {
      if (this.myform.controls['category'].value === element.value) {
        this.subCategories = element.subCategory.split(',');
      }
    });
  }


  changeContentType() {
    if (this.myform.controls['contentType'].value === 'BLOG') {
      this.media = 'Image';
    }
    if (this.myform.controls['contentType'].value === 'VIDEO') {
      this.media = 'Video';
    }

    this.path = '';

  }


}
