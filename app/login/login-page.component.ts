import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { User } from '../../models/user';
import { PreviewService } from '../shared/auth/preview.service';
import { AuthenticationService } from '../shared/auth/authentication-service.service';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import { AuthService } from '../shared/auth/auth.service';
import { base } from '../shared/configs/util';
@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent {


    f: FormGroup;
    obj;
    user;
    data;
    constructor(private router: Router, private previewService: PreviewService, private auth: AuthService, private route: ActivatedRoute,
        private authenticationService: AuthenticationService, private roleGuardService: RoleGuardService, private fb: FormBuilder) {

        this.user = new User();

        this.f = fb.group({
            'user_email': [null, Validators.required],
            'user_password': [null, Validators.required],
            'remember_me': [null]
        });
    }

    // On submit button click    

    addPost(post) {
        this.data = post;
    }

    authLogin(): void {
        this.user['username'] = this.f.controls['user_email'].value;
        this.user['password'] = this.f.controls['user_password'].value;
        this.user['rememberMe'] = this.f.controls['remember_me'].value;
        this.previewService.editUserID(this.user['username']);

        this.auth.login(this.user)
            .then((user) => {
                localStorage.setItem('token', user.id_token);
                //this.authVerify();

                this.router.navigateByUrl('/dashboard');
            })
            .catch((err) => {
                window.alert('Incorrect Login Information');
            });
    }

    authVerify(): void {
        let token = localStorage.getItem('token');
        this.auth.ensureAuthenticated(token)
            .then((user) => {
            })
            .catch((err) => {
            });
    }
    // On Forgot password link click
    onForgotPassword() {
        this.router.navigate(['forgotpassword'], { relativeTo: this.route.parent });
    }
    // On registration link click
    onRegister() {
        this.router.navigate(['register'], { relativeTo: this.route.parent });
    }
}



