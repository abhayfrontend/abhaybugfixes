import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../shared/auth/auth.service';
import { User } from '../../models/user';
import { Router } from '@angular/router';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import { AuthenticationService } from '../shared/auth/authentication-service.service';
import { PreviewService } from '../shared/auth/preview.service';
import swal from 'sweetalert2';
import { base } from '../shared/configs/util';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  myForm: FormGroup;
  post: any;
  token;
  user: User;
  loggedIn = false;
  obj;
  data;
  httpOptions;
  @ViewChild('map', { read: ElementRef }) map: ElementRef;

  constructor(private fb: FormBuilder, public http: HttpClient, private auth: AuthService, private previewService: PreviewService,
    private authenticationService: AuthenticationService, private router: Router, private roleGuardService: RoleGuardService) {
    this.user = new User();

    this.myForm = fb.group({
      'phone_number': [null, Validators.required],
      'user_password': [null, Validators.required],
      'remember_me': [null]
    });

    if (localStorage.getItem('token')) {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        })
      };
      this.http.get(base + '/api/authenticate', this.httpOptions).toPromise().then(respo => {
        let result = <any>respo;
        // console.log('ResultLog', result);
        this.router.navigateByUrl('/dashboard');
      }, err => {
        // console.log('ErrorLog:', err)
        localStorage.removeItem('token');
      })
    }
  }





  ngOnInit(): void {

  }

  addPost(post) {
    this.data = post;
  }

  authLogin(): void {
    this.user['username'] = this.myForm.controls['phone_number'].value;
    this.user['password'] = this.myForm.controls['user_password'].value;
    this.user['rememberMe'] = this.myForm.controls['remember_me'].value;
    this.previewService.editUserID(this.user['username']);

    this.auth.login(this.user)
      .then((user) => {

        localStorage.setItem('token', user.id_token);
        //this.authVerify();

        this.router.navigateByUrl('/dashboard');
      })
      .catch((err) => {
        swal('Incorrect Credentials', '', 'error');

      });
  }

  authVerify(): void {

    let token = localStorage.getItem('token');
    this.auth.ensureAuthenticated(token)
      .then((user) => {
      })
      .catch((err) => {
      });
  }
}
