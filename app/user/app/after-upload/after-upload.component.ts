import { Component, OnInit } from '@angular/core';
import * as AWS from '../../../shared/configs/aws.config';
import { PreviewService } from '../../../shared/auth/preview.service';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../../shared/auth/role-guard.service';
import * as moment from 'moment';
import { base, proxyApi } from '../../../shared/configs/util';
@Component({
  selector: 'app-after-upload',
  templateUrl: './after-upload.component.html',
  styleUrls: ['./after-upload.component.css']
})
export class AfterUploadComponent implements OnInit {
  message;
  contacts = [];
  sent = 0;
  password;
  emailMessage;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };
  constructor(public previewService: PreviewService, private router: Router, private http: HttpClient,
    private roleGuardService: RoleGuardService) {
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnInit() {

    this.contacts = this.previewService.SmsList.getValue();
  }

  EnableSend(e) {
    this.message = e;

  }

  sendSMS() {
    /*
        // console.log('Message Sent!!')
        // console.log('contacts:', '+91' + this.contacts[0][3]);
        this.message = `Hi! Pls download the Sifypedia (Productivise)App today:
        http://l.ead.me/bb6f1O
        Login details are sent on mail.For support, visit SEO Booth @Sify ABS.`
        const sns = AWS.sns;
        for (let i = 0; i < this.contacts.length; i++) {
          // this.message = `Please use your phone no. as username and password is- qwerty`;
          sns.publish({
            Message: this.message, PhoneNumber: '+91' + this.contacts[i][3], MessageAttributes: {
              'AWS.SNS.SMS.SenderID': {
                DataType: 'String',
                StringValue: 'S'
              },
              'AWS.SNS.SMS.MaxPrice': {
                DataType: 'Number',
                StringValue: '00.01'
              },
              'AWS.SNS.SMS.SMSType': {
                DataType: 'String',
                StringValue: 'Transactional'
              }
            }
          }).promise().then(res => {
            // console.log(res);
            this.sent = this.sent + 1;
          });
          /* this.http.get(base + '/api/users/' + this.contacts[i][3], this.httpOptions).subscribe(e => {
             let user = <any>e;
             user.userPassword = 'qwerty'
             this.resetPassword(user);
           })
          */
    // }

    this.register();
    this.router.navigate(['/user/bulk']);
  }

  resetPassword(user) {
    this.http.post(proxyApi + '/resetPassword', user, this.httpOptions).subscribe(res => { console.log()});// console.log(res) });

  }

  register() {
    for (let i = 0; i < this.contacts.length; i++) {
      let password = Math.random().toString(36).slice(2, 8);
      let emailMessage = `Hello Sifyite ` + this.contacts[i][0] + `,

Welcome aboard SIFYPEDIA, the mobile platform for Knowledge, News & Networking.

Download and login to explore the new world of Sify’s assisted Learning modules & latest Industry insides.

Login : ` + this.contacts[i][3] + `,
Password: ` + password + `

Reach out for Sifypedia volunteers or Productivise team at the SEO counter at ABS for any assistance.

Stay empowered!!!

Regards
Team Sifypedia`;
      // console.log('contacts:', this.contacts[i], emailMessage);

      // console.log('contacts:', this.contacts[i]);
      let newUser = <any>{
        'login': this.contacts[i][3],
        'firstName': this.contacts[i][0],
        'lastName': this.contacts[i][1] || null,
        'email': this.contacts[i][2] || (this.contacts[i][3] + '@gmail.com'),
        'imageUrl': null,
        'activated': true,
        'langKey': null,
        'createdBy': this.previewService.Username.getValue(),
        'createdDate': new Date().toISOString(),
        'lastModifiedBy': this.previewService.Username.getValue(),
        'lastModifiedDate': new Date().toISOString(),
        'authorities': [
          'ROLE_USER'
        ],
        'phone': this.contacts[i][3],
        'userId': null,
        'userType': 0,
        'orgId': this.previewService.organizationId.getValue(),
        'deviceID': Number(this.previewService.organizationId.getValue()) === 261351 ? 261701 : this.contacts[i][17],
        'geoHash': null,
        'location': this.contacts[i][5] || 'NOIDA',
        'aadharNumber': null,
        'userStatus': null,
        'userPassword': password
      };
      // console.log('newUser:', newUser);
      this.http.post(base + '/api/users/register', newUser, this.httpOptions).subscribe(resp => {
        // console.log('user:', resp);
        let result = <any>resp;
        // console.log('memberData:', result);
        // console.log('memberData:', emailMessage);

        this.sendEmail('Welcome to Sifypedia', emailMessage, result['email']);
        let memberData = {
          'name': result['firstName'] + ' ' + (result['lastName'] || ''),
          'userId': result['id'],
          'learnerId': result['learner']['id'],
          'category': this.previewService.organizationId.getValue(),
          'subCategory': this.contacts[i][9],
          'locationName': result['location'],
          'gehash1': result['geoHash'],
          'memberType': 'APPLICANT',
          'aadhaar': result['phone'] || null,
          'mobileNo': result['phone'] || this.contacts[i][3],
          'cardId': null
        };
        // console.log('memberData:', memberData);
        this.http.post(base + '/api/members', memberData, this.httpOptions).subscribe(response => {
          // console.log(response);
          let memberResponse = <any>response;
          // console.log('member:', memberResponse);
          newUser.id = result.id;
          newUser.learnerId = result['learner']['id'];
          newUser.langKey = memberResponse.id;
          newUser.authorities = ["ROLE_USER"];
          newUser.userPassword = password;
          // console.log('new', newUser)
          this.http.put(base + '/api/users', newUser, this.httpOptions).subscribe(respo => {
            // console.log('user:', respo);
            this.http.post(base + '/api/addresses', {
              'state': this.contacts[i][4] || 'UP',
              'district': this.contacts[i][5] || 'NOIDA',
              'name': result['learner']['id'],
              'constituency': null,
              'unitNumber': null,
              'buildingName': null,
              'addressLine1': null,
              'addressLine2': null,
              'village': null,
              'town': null,
              'block': null,
              'pincode': this.contacts[i][6] || '201301'
            }, this.httpOptions).subscribe(addrresp => {
              let address = <any>addrresp;
              this.http.post(base + '/api/extra-data', {
                'category': this.contacts[i][17] || 'a',
                'subCategory': this.contacts[i][12] || 'a',
                'strValue': this.contacts[i][13] || 'a',
                'tags': this.contacts[i][14] || 'a'
              }, this.httpOptions).subscribe(extraResponse => {
                let extraData = <any>extraResponse;
                this.http.put(base + '/api/learners', {

                  'id': result['learner']['id'],
                  'name': result['firstName'] + ' ' + (result['lastName'] || ''),
                  'orgId': this.previewService.organizationId.getValue(),
                  'tags': Number(this.previewService.organizationId.getValue()) === 261351 ? 261701 : this.contacts[i][17],
                  'description': memberResponse.id,
                  'imageLink': null,
                  'profileImage': '',
                  'email': this.contacts[i][2] || (this.contacts[i][3] + '@gmail.com'),
                  'userId': result['id'],
                  'phash': null,
                  'registeredNumber': this.contacts[i][3],
                  'phone': this.contacts[i][3],
                  'aadhaar': null,
                  'age': this.contacts[i][7] ? ((moment(moment.now()).diff(moment(this.contacts[i][7]), 'days')) / 365) : null,
                  'gender': this.contacts[i][13] || null,
                  'department': this.contacts[i][9] || null,
                  'jobRole': this.contacts[i][11] || null,
                  'doj': this.contacts[i][8] ? moment(this.contacts[i][8]) : new Date().toISOString(),
                  'city': this.contacts[i][5] || 'NOIDA',
                  'organizationId': this.previewService.organizationId.getValue(),
                  'addressId': address.id || null,
                  'extraData': [extraData],
                  'educations': [],
                  'interests': [],
                  'locations': [],
                  'groups': []

                }, this.httpOptions).subscribe(learnerResponse => {
                  // console.log('learnerResponse: ', learnerResponse);
                })
              })

            })
          });
        });
      });
    }

  }


  sendEmail(subject, message, email) {
    return this.http.post(proxyApi + '/sendMail', {
      message: message,
      email: email,
      subject: subject
    }).toPromise().then(mailRes => {
      let result = <any>mailRes;
      // this.sendSMS(this.message, user.phone)
    })
  }

}






