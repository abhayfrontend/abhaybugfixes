import { Component, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';

@Component({
  selector: 'app-next-btn',
  templateUrl: './next-btn.component.html',
  styleUrls: ['./next-btn.component.css']
})
export class NextBtnComponent implements OnInit, OnChanges {
  @Input() public myText: string;
  @Input() public target: string;
  @Input() public fileTarget: string;
  file = '';
  changeLog = [];
  constructor() { }

  ngOnInit() {
    this.file = this.fileTarget;
    // console.log('fileterget:', this.fileTarget);

    // console.log('file:', this.file);
  }
  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    let log: string[] = [];
    for (let propName in changes) {
      let changedProp = changes[propName];
      let to = JSON.stringify(changedProp.currentValue);
      if (changedProp.isFirstChange()) {
        log.push(`Initial value of ${propName} set to ${to}`);
      } else {
        let from = JSON.stringify(changedProp.previousValue);
        log.push(`${propName} changed from ${from} to ${to}`);
      }
    }
    this.changeLog.push(log.join(', '));
    // console.log('changes: ', this.changeLog);
    // console.log('fileterget:', this.fileTarget);
  }

}
