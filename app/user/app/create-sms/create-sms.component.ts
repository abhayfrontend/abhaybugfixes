import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PreviewService } from '../../../shared/auth/preview.service';

@Component({
  selector: 'app-create-sms',
  templateUrl: './create-sms.component.html',
  styleUrls: ['./create-sms.component.css']
})
export class CreateSmsComponent implements OnInit {

  @Output() messageReady = new EventEmitter;
  templates = [{
    name: 'Registration - English', message: `Hi You are invited by Surekha to join platform of working together
    Pwd:qwerty
https://play.google.com/store/apps/details?id=com.sthaapak.productivise&hl=en`
  }];




  templateName;
  nameOfTemplate;
  message;
  constructor(private previewService: PreviewService) { }

  ngOnInit() {


  }
  IsMessageReady() {
    if (this.message) {
      this.messageReady.emit(this.message);
    }
    else {
      this.messageReady.emit('');
    }
  }

  sendMsg() {
    // console.log('tn:', this.nameOfTemplate);
    // console.log('tn:', this.message);
    alert("Msg Sent");
  }

  changeTemplate() {
    // console.log('tn:', this.templateName);
    this.message = this.templateName;
    this.IsMessageReady();
  }


}
