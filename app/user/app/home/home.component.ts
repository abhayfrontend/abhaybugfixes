import { Component, OnInit, Input } from '@angular/core';
import { PreviewService } from '../../../shared/auth/preview.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // Variables

  file: boolean = true;

  // End Variables

  constructor(public previewService: PreviewService, private router: Router) {
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnInit() {
  }

  enableNext(e) {
    this.file = e.length > 0 ? false : true;
    this.previewService.editSmsList(e);
    // console.log('e:', e);
  }



}
