import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserAddComponent } from './user-add.component';
import { UserListComponent } from './user-list.component';
import { HomeComponent } from './app/home/home.component';
import { AfterUploadComponent } from './app/after-upload/after-upload.component';


const routes: Routes = [
  {
    path: '',
    children: [

      {
        path: 'add',
        component: UserAddComponent,
        data: {
          title: 'Add new User '
        }
      }, {
        path: 'list',
        component: UserListComponent,
        data: {
          title: 'List User'
        }
      },
      {
        path: 'bulk',
        component: HomeComponent,
        data: {
          title: 'Bulk Register User'
        }
      },
      {

        path: 'afterUpload',
        component: AfterUploadComponent

      },
      {

        path: 'edit',
        component: UserAddComponent

      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule { }
