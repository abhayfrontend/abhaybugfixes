import { Component, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { UserResourceService, CampaignGroupResourceService, DeviceResourceService } from '../sthaapak';
import { PreviewService } from '../shared/auth/preview.service';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import swal from 'sweetalert2';
import { base, proxyApi } from '../shared/configs/util';
import { SupportGroupResourceService } from '../sthaapak/sdk/supportGroupResource.service';
import { ChatHistoryResourceService } from '../sthaapak/sdk/chatHistoryResource.service';
import { Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
@Component({
    selector: 'app-dt-filter',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss']
})

export class UserListComponent implements OnInit {
    rows = [];
    filterOption;
    temp = [];
    data = [];
    groups = [];
    inputOption = {};
    createdBy = [];
    sgroup;
    cgroup;
    sgroups;
    status = ['Active', 'Inactive'];
    // Table Column Titles
    columns = [
        { prop: 'firstName' },
        { prop: 'lastName' },
        { prop: 'phone' },
        { prop: 'lastModifiedDate' }
    ];

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.roleGuardService.token
        })
    };

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private userService: UserResourceService, private previewService: PreviewService, private http: HttpClient, private chathistory: ChatHistoryResourceService,
        private roleGuardService: RoleGuardService, private groupService: CampaignGroupResourceService, private supportGroup: SupportGroupResourceService,
        private device: DeviceResourceService, private router: Router) {
        if (!localStorage.getItem('token')) {
            this.router.navigateByUrl('/login');
        }
    }
    ngOnInit() {
        this.getUsers();
        let result = <any>this.previewService.campaignGroups.getValue();
        result.forEach((e, i) => {
            this.inputOption[e.name] = e.name;
            // console.log('g:', this.inputOption);

        });
        // console.log('groups2:', this.groups);
        if (this.groups.length < 1) {
            this.getGroups();
        }
    }

    reset() {
        this.filterOption = '';
        this.rows = this.temp;
    }
    changeFilter(e) {
        // console.log('filter:', this.temp);
        this.filterOption = e.target.value;
        this.rows = this.temp;
    }

    getUsers() {
        this.temp = [];
        this.rows = [];
        this.data = [];
        this.http.get(base + '/api/support-groups', this.httpOptions).toPromise().then(res => {
            let sgroups = <any>res;
            this.sgroups = sgroups;
        });
        this.http.get(base + '/api/_search/users/filterby/' + this.previewService.organizationId.getValue() + '?size=10', this.httpOptions).subscribe(res => {
            let result = <any>res;
            // console.log('date1:', result[0]);
            // console.log('reult:', result)
            // console.log('org1: ', Number(this.previewService.organizationId.getValue()));
            result.forEach(element => {
                // console.log('org2: ', element.orgId)
                if (element.orgId && element.orgId === Number(this.previewService.organizationId.getValue()) && element.userType === 0) {
                    element.lastModifiedDate = new Date(element.lastModifiedDate).toDateString();


                    element.userType2 = 'End-User';
                    this.data.push(element);
                }
                if (element.orgId && element.orgId === Number(this.previewService.organizationId.getValue()) && (element.userType === 6 || element.userType === null)) {
                    element.lastModifiedDate = new Date(element.lastModifiedDate).toDateString();
                    this.createdBy.push(element.login);
                }
            });
            // console.log('data:', this.data);
            this.temp = [...this.data];
            this.rows.sort();
            this.rows = this.data;
            // console.log('temp:', this.temp);


        })
    }

    filterListByType(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            if (val === 'VIDEO' && d.videoLink) {
                return true;

            }
            if (val === 'BLOG' && d.previewImage) {
                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByCreatedBy(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            return d.createdBy === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByStatus(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log(d.activated);
            // console.log(val);
            if (val === 'Active') {
                return d.activated === true;
            }
            if (val === 'Inactive') {
                return d.activated === false;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByRegDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log('date:', val)
            if (new Date(d.lastModifiedDate).getDate() === new Date(val).getDate() && new Date(d.lastModifiedDate).getMonth() === new Date(val).getMonth() && new Date(d.lastModifiedDate).getFullYear() === new Date(val).getFullYear()) {


                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPublDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {

            if (new Date(d.publishedDate).getDate() === new Date(val).getDate() && new Date(d.publishedDate).getMonth() === new Date(val).getMonth() && new Date(d.publishedDate).getFullYear() === new Date(val).getFullYear()) {

                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }


    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.firstName.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }



    deleteContent(row) {
        console.log(JSON.stringify(row));
        swal({
            title: 'Are you sure you want to delete this user record?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then(async (confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                let isExpert;
                let isAdmin;
                isAdmin = false;
                isExpert = false;

                this.sgroups.forEach(element => {
                    if (String(element.email) === String(row.phone) && String(element.type) === "2") {
                        isExpert = [];
                        isExpert.push(element.name);
                    }
                    if (String(element.email) === String(row.phone) && String(element.type) === "3") {
                        isAdmin = [];
                        isAdmin.push(element.name);
                    }
                });

                if (isExpert) {
                    swal({
                        title: 'This User is an Expert.  Do you want to proceed?',
                        text: 'These groups will no longer have any expert assigned : ' + isExpert.join(),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#0CC27E',
                        cancelButtonColor: '#FF586B',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success btn-raised mr-5',
                        cancelButtonClass: 'btn btn-danger btn-raised',
                        buttonsStyling: false
                    }).then(async (confirm2) => {
                        if (confirm2.value === true) {

                            this.sgroups.forEach(element => {
                                if (isExpert) {
                                    this.deleteMemberFromGroup(row.langKey, element, row, row.login);
                                }
                                else {
                                    this.deleteMemberFromGroup(row.langKey, element, row);
                                }
                            });
                            if (isExpert) {
                                this.deleteFromCampaignGroupsAndDevice(row, row.login);
                            }
                            else {
                                this.deleteFromCampaignGroupsAndDevice(row);
                            }
                            this.http.delete(base + '/api/learners/' + row.learner.id, this.httpOptions).toPromise().then(res => {
                                console.log("user deleted from learners");
                            })
                        }
                    });
                }
                else if (isAdmin) {
                    swal({
                        title: 'This User is an Admin.  Do you want to proceed?',
                        text: 'These groups will no longer have any admin assigned : ' + isAdmin.join(),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#0CC27E',
                        cancelButtonColor: '#FF586B',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success btn-raised mr-5',
                        cancelButtonClass: 'btn btn-danger btn-raised',
                        buttonsStyling: false
                    }).then(async (confirm2) => {
                        if (confirm2.value === true) {

                            this.sgroups.forEach(element => {
                                if (isAdmin) {
                                    this.deleteMemberFromGroup(row.langKey, element, row, row.login);


                                }
                                else {
                                    this.deleteMemberFromGroup(row.langKey, element, row);


                                }
                            });
                            if (isAdmin) {
                                this.deleteFromCampaignGroupsAndDevice(row, row.login);
                            }
                            else {
                                this.deleteFromCampaignGroupsAndDevice(row);
                            }
                            this.http.delete(base + '/api/learners/' + row.learner.id, this.httpOptions).toPromise().then(res => {
                                console.log("user deleted from learners");
                                
                            })
                        }
                    });
                }
                else {
                    this.sgroups.forEach(element => {
                        this.deleteMemberFromGroup(row.langKey, element, row);

                    });

                    this.deleteFromCampaignGroupsAndDevice(row);
                    this.http.delete(base + '/api/learners/' + row.learner.id, this.httpOptions).toPromise().then(res => {
                            console.log("user deleted from learners");
                    })
                }
                


            }
            else {
                swal(
                    'Cancelled',
                    'The user record was not deleted.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'The user record was not deleted.',
                    'error'
                )
            }
        })


    }

    openDetails(row) {
        // console.log('row:', row);
        swal({
            html: `<table class="customers">
  <tr>
    <th>Property</th>
    <th>Value</th>
  </tr>
  <tr>
    <td>First Name</td>
    <td>` + row.firstName + `<td>
  </tr>
  <tr>
    <td>Last Name</td>
    <td>` + row.lastName + `</td>
  </tr>
  <tr>
    <td>Phone Number</td>
    <td>` + row.phone + `</td>
  </tr>
  <tr>
    <td>Role</td>
    <td>` + row.userType2 + `</td>
  </tr>
  <tr>
    <td>Created By</td>
    <td>` + row.createdBy + `</td>
  </tr>
  </table>`,
            customClass: 'table'
        })
    }


    sendUpdate(category, subcategory, screen) {


        let message = {
            topic: this.previewService.organizationId.getValue(),
            data: {
                silent: true,
                save: false,
                title: category,
                body: subcategory,
                screenType: screen,
                image: 'jpg',
                specialParameters: 'll',
                tags: 'Silent',
                orgId: this.previewService.organizationId.getValue(),
                name: screen
            }

        }
        // console.log('message:', message);
        this.http.post(proxyApi + '/contentUpdate', message, this.httpOptions).subscribe((response) => {
            // Response is a message ID string.
            // console.log('Successfully sent message:', response);

        }, (error) => {
            // console.log('Error sending message:', error);
        });



    }

    addToGroup(row) {
        swal.mixin({
            confirmButtonText: 'Next &rarr;',
            showCancelButton: true,
            progressSteps: ['1', '2', '3']
        }).queue([{
            title: 'Select a Group',
            input: 'select',
            inputOptions: this.inputOption,
            inputPlaceholder: 'Select a Group',
            showCancelButton: true,
            inputValidator: (value) => {
                return new Promise((resolve) => {
                    if (value) {
                        resolve()
                    } else {
                        resolve('You need to select one Group')
                    }
                })
            }
        }, {
            title: 'Are you sure you want to add this user to the group?',
            showCancelButton: true,
            showConfirmButton: true,

        }]).then(result => {
            let proceed = true;
            // console.log('form', result);
            if (result.value) {
                let campaignGroup = result.value[0];

                this.groups.forEach(e => {
                    // console.log("GROUP:", e.name);
                    // console.log("GROUPTOMATCH:", campaignGroup);
                    if (e.name === campaignGroup) {
                        this.cgroup = <any>e;
                        // console.log('cgr', this.cgroup);
                        this.cgroup.learners.forEach(element => {
                            if (element.id === row.learner.id) {
                                swal({
                                    title: 'Error',
                                    text: 'This user is already a part of this group.',
                                    onClose: () => swal.close()
                                })
                                proceed = false;
                                return;
                            }
                        });
                    }

                });


                let conf = this.cgroup;
                // console.log(conf);
                conf['learners'].push(row.learner);

                // console.log('2', conf);
                if (proceed) {
                    this.supportGroup.getAllSupportGroupsUsingGET().subscribe(el => {
                        let sgroups = <any>el;
                        // console.log(campaignGroup);
                        sgroups.forEach(element => {

                            if (campaignGroup === element.name && Number(element.category) === Number(this.previewService.organizationId.getValue())) {
                                this.sgroup = element;
                                // console.log(this.sgroup);
                            }
                        });

                        this.groupService.updateCampaignGroupUsingPUT(conf).subscribe(ans => {

                            this.http.get(base + '/api/users/' + row.phone, this.httpOptions).subscribe(user => {
                                let newuser = <any>user;
                                if (newuser.aadharNumber) {
                                    newuser.aadharNumber += ',' + conf.id;
                                }
                                else {
                                    newuser.aadharNumber = conf.id;
                                }
                                this.http.put(base + '/api/users', newuser, this.httpOptions).subscribe(userUp => {
                                    // console.log('userUp', userUp)
                                    this.sendUpdate(row.value, row.path, "GROUPS");
                                })
                            })

                            this.http.get(base + '/api/members/' + row.langKey, this.httpOptions).subscribe(member => {
                                let smember = <any>member;
                                // console.log('member:', smember);

                                // console.log('members in group:', this.sgroup['members']);
                                let newsgroup = this.sgroup;
                                newsgroup['members'].push(smember);
                                // console.log('members in group:', newsgroup['members']);

                                this.supportGroup.updateSupportGroupUsingPUT(newsgroup).subscribe(don => {
                                    // console.log(don);
                                    this.chathistory.createChatHistoryUsingPOST({
                                        "attachment": true,
                                        "attachmentLink": "string",
                                        "category": "string",
                                        "description": row['learner'].name + " has been added to " + this.sgroup.name + " group by " + this.previewService.Username.getValue(),
                                        "from": "string",
                                        "group": this.sgroup.id,
                                        "name": "string",
                                        "orgId": this.previewService.organizationId.getValue(),
                                        "subCategory": "broadcast",
                                        "tags": "string",
                                        "to": "string",
                                        "ts": new Date().getTime()
                                    }).subscribe(chat => {
                                        // console.log('CHATT:', chat);
                                        this.http.get(base + '/api/campaign-groups', this.httpOptions).subscribe(cgroups => {
                                            let campaignGroups = <any>cgroups;
                                            campaignGroups = campaignGroups.filter(e => e.orgId === this.previewService.organizationId.getValue())
                                            this.previewService.editCampaignGroup([]);
                                        })
                                        this.http.get(base + '/api/support-groups', this.httpOptions).subscribe(supgroups => {
                                            let supportGroups = <any>supgroups;
                                            supportGroups = supportGroups.filter(e => e.category === this.previewService.organizationId.getValue())
                                            this.previewService.editSupportGroup([]);
                                        })
                                        // console.log(chat);
                                        
                                        swal({
                                            title: 'User Added To Group',
                                            text: 'This user has been added to the group.',
                                            type: 'success',
                                            onClose: () => swal.close()
                                        })
                                    })
                                })
                            })
                        })

                    })

                }




            }
            else {
                swal('User not added in the Group', '', 'error')
            }
        });
    }

    getGroups() {
        this.groupService.getAllCampaignFororgidUsingGET(this.previewService.organizationId.getValue()).subscribe(res => {
            let result = <any>res;
            // console.log('groups:', result);
            this.groups = [];
            result.forEach((e, i) => {


                if (Number(e.orgId) === Number(this.previewService.organizationId.getValue())) {
                    this.groups.push(e);
                    this.inputOption[e.name] = e.name;
                    // console.log('g:', this.groups);
                }
            });
            // console.log('groups2:', this.groups);
        })
    }

    editContent(row) {
        this.previewService.editUser(row);
        this.router.navigate(['/user/edit'])
    }


    disableContent(row) {
        this.http.get(base + '/api/users/' + row.login, this.httpOptions).toPromise().then(res => {
            // console.log(res);
            let result = <any>res;
            result.activated = false;
            result.lastModifiedDate = result.lastModifiedDat
            this.http.put(base + '/api/users', result, this.httpOptions).toPromise().then(resp => {
                // console.log(resp);
                this.sendUpdate('User', row.id, row.login)
                this.getUsers();
            })
        })

    }
    enableContent(row) {
        this.http.get(base + '/api/users/' + row.login, this.httpOptions).toPromise().then(res => {
            // console.log(res);
            let result = <any>res;
            result.activated = true;
            result.lastModifiedDate = result.lastModifiedDat
            this.http.put(base + '/api/users', result, this.httpOptions).toPromise().then(resp => {
                // console.log(resp);
                this.sendUpdate('User', row.id, row.login)
                this.getUsers();
            })
        })
    }

    async deleteLearnerFromGroup(learnerId, group, name?: string) {
        // console.log(learnerId, group);
        let cgroup = <any>group;
        if (cgroup.selectQuery != null && cgroup.selectQuery === name) { cgroup.selectQuery = 'Deleted' }
        if (cgroup.selectQuery != null && cgroup.selectQuery.split('/')[0] === 'Broadcast' && cgroup.selectQuery.split('/')[1] === name) { cgroup.selectQuery = 'Removed' }
        cgroup.learners = cgroup.learners.filter(e => Number(e.id) !== Number(learnerId))
        const putRes = await this.http.put(base + '/api/campaign-groups/', cgroup, this.httpOptions).toPromise().then(res => res);
        return putRes;
    }

    async deleteMemberFromGroup(memberId, group, row, login?: string) {
        // console.log(memberId, group, login);
        let sgroup = <any>group;
        if (login && sgroup.email === login) {
            sgroup.email = null;
        }
        sgroup.members = sgroup.members.filter(e => {
            if (Number(e.id) === Number(memberId)) {
                this.chathistory.createChatHistoryUsingPOST({
                    "attachment": true,
                    "attachmentLink": "string",
                    "category": "string",
                    "description": row.firstName + " " + row.lastName + " has been removed by " + this.previewService.Username.getValue(),
                    "from": "string",
                    "group": sgroup.id,
                    "name": "string",
                    "orgId": this.previewService.organizationId.getValue(),
                    "subCategory": "broadcast",
                    "tags": "string",
                    "to": "string",
                    "ts": new Date().getTime()
                }).subscribe(chat => {
                    // console.log('chat:', chat);
                })
                return false;
            }
            else {
                return true;
            }
        });
        // console.log('AFTERDELETE', sgroup);
        const putRes = await this.http.put(base + '/api/support-groups/', sgroup, this.httpOptions).toPromise().then(res => res);
        this.http.delete(base + '/api/members/' + row.langKey, this.httpOptions).toPromise().then(res => console.log()); // console.log('memdel:', res));
        return putRes;
    }

    async deleteUser(row, name?: string) {
        this.http.get(base + '/api/devices/filterby/phone/' + row.phone, this.httpOptions).toPromise().then(res => {
            let device = <any>res;
            device.forEach(element => {
                this.http.delete(base + '/api/devices/' + element.id, this.httpOptions).toPromise().then(resp => console.log());// console.log(resp));
            });
        })
        const user = await this.http.get(base + '/api/campaign-groups/filterby/' + row.orgId, this.httpOptions).toPromise().then(async cgroup => {
            let campaignGroup = <any>cgroup;
            // console.log(campaignGroup);
            if (campaignGroup.length > 0) {
                let source = of(campaignGroup);
                const example = await source.pipe(mergeMap(q => forkJoin(...q.map(async a => await this.deleteLearnerFromGroup(row.learner.id, a, name)))));


                let subscribe = await example.subscribe(val => {
                    // console.log('VAL:', val);
                    return val;
                });
                return subscribe;
            }

        })
        return user;


    }

    async deleteFromCampaignGroupsAndDevice(row, name?: string) {


        const delres = await this.deleteUser(row, name);
        let timerInterval1 = setInterval(async () => {
            if (delres.closed) {
                clearInterval(timerInterval1);
                // console.log('time1', delres)
                // console.log('delres:', delres);
                row.authorities = [];
                row.createdDate = null;
                row.lastModifiedDate = null;
                const delres3 = await this.userService.updateUserUsingPUT(row).toPromise().then(res => { // console.log('de::', res);
                 return res });

                const delres2 = await this.userService.deleteUserUsingDELETE(row.login).toPromise().then(res => {
                    let result = <any>res;
                    // console.log('delte', result);
                    return result;
                })
                this.sendUpdate('User', row.id, row.login)

                swal('Success', 'User successfully deleted', 'success');
                this.getUsers();


            }
        }, 100)
    }
}
