import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { QuillEditorComponent } from 'ngx-quill/src/quill-editor.component';
import { PreviewService } from '../shared/auth/preview.service';
import { UserResourceService, IndustryResourceService } from '../sthaapak';
import { FormGroup, FormControl, ReactiveFormsModule, FormsModule, Validators } from '@angular/forms';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import swal from 'sweetalert2';
import * as AWS from '../shared/configs/aws.config';
import { base, proxyApi } from '../shared/configs/util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user)',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})

export class UserAddComponent implements AfterViewInit, OnInit, OnDestroy {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };
  firstNameValid = false;

  myform: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  email: FormControl;
  phone: FormControl;
  group: FormControl;
  userType: Number;
  location: FormControl;
  indus: FormControl;
  department: FormControl;
  jobRole: FormControl;
  selectedIndustry;
  industries = [];
  departments = [];
  selectedDepartment;
  jobRoles = [];
  userToEdit;
  organizationId;
  role;
  dep;
  ind;
  html;
  indusEl;
  password;
  emailMessage = '';

  message = `Hello, Welcome to Productivise. Click on the link below to get the App.` + proxyApi
    + `/create/com.sthaapak.productivise/` + this.previewService.organizationId.getValue();
  constructor(private previewService: PreviewService, private userService: UserResourceService, private http: HttpClient,
    private roleGuardService: RoleGuardService, private industry: IndustryResourceService, private router: Router) {
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }

  }

  createFormControls() {
    this.firstName = new FormControl('', Validators.required);
    this.lastName = new FormControl('');
    this.email = new FormControl('', [Validators.required, Validators.email]);
    this.location = new FormControl('', Validators.required);
    this.phone = new FormControl('', Validators.required);
    // this.group = new FormControl('', Validators.required);
    this.indus = new FormControl('', Validators.required);
    this.department = new FormControl('', Validators.required);
    this.jobRole = new FormControl('', Validators.required);
  }

  createForm() {
    this.myform = new FormGroup({
      firstName: this.firstName,
      phone: this.phone,
      // group: this.group,
      lastName: this.lastName,
      email: this.email,
      location: this.location,
      indus: this.indus,
      department: this.department,
      jobRole: this.jobRole
    });
  }

  setValues() {
    this.http.get(base + '/api/job-roles/' + this.userToEdit.deviceID, this.httpOptions).subscribe(jr => {
      let jobs = <any>jr;
      // console.log('role:', jobs);
      let dep = jobs.minimumExperince;
      this.http.get(base + '/api/job-families/' + dep, this.httpOptions).subscribe(jf => {
        let jobf = <any>jf;
        // console.log('dep:', jobf);
        let ind = jobf.url;
        this.myform.controls['indus'].setValue(ind);
        this.selectIndustry();
        this.myform.controls['department'].setValue(jobf.id);
        this.selectDepartment();
        this.jobRole.setValue(jobs.id);
        this.myform.controls['jobRole'].setValue(jobs.id);
        // console.log('JOBROLEID:', this.myform.controls['jobRole'].value)
      })
    });

  }

  ngAfterViewInit() {
    let usertoEdit = <any>this.previewService.usertoEdit.getValue();
    // console.log('userEdit', usertoEdit)
    if (usertoEdit.id) {

      this.indusEl = <HTMLInputElement>document.getElementById('name');
      /*
      // console.log('userEditName', this.indusEl)
      this.indusEl.value = usertoEdit.firstName;
      // console.log('userEditName', this.indusEl.value)

      this.indusEl.innerText = usertoEdit.firstName;
      */
      this.myform.controls['firstName'].setValue(usertoEdit.firstName);
      this.myform.controls['lastName'].setValue(usertoEdit.lastName);
      this.myform.controls['email'].setValue(usertoEdit.email);
      this.myform.controls['phone'].setValue(usertoEdit.phone);
      this.myform.controls['location'].setValue(usertoEdit.location);
    }
  }

  ngOnInit() {
    this.password = Math.random().toString(36).slice(2, 8);
    this.emailMessage = `Hello, Welcome to Productivise. Click on the link below to get the App.` + proxyApi
      + `/create/com.sthaapak.productivise/` + this.previewService.organizationId.getValue() + `\n Your password is: ` + this.password;
    this.createFormControls();
    this.createForm();
    this.userToEdit = <any>this.previewService.usertoEdit.getValue();
    if (!this.userToEdit.id) {
      this.router.navigate(['/user/add']);
    }
    this.organizationId = this.previewService.organizationId.getValue();
    this.industry.getAllIndustriesUsingGET().subscribe(e => {
      let result = <any>e;
      result.forEach(element => {
        // console.log('org2: ', element);
        element.organizations.forEach(el => {

          if (Number(el.id) === Number(this.previewService.organizationId.getValue()) && element.tags === 'newinfo') {
            this.industries.push(element);
            // console.log("ind1:", element);
          }
        });

        // console.log('jobFamilies:', element.jobFamilies);
      });
      if (this.userToEdit.id) {
        this.setValues();

      }
    })
  }

  NameValidation() {
    this.firstNameValid = this.firstName.value.trim().length > 0 ? true : false;
  }

  selectIndustry() {
    this.selectedIndustry = this.indus.value;
    // console.log('selectedIndustry:', this.selectedIndustry);

    this.industries.forEach(element => {
      // console.log('element:', element)
      if (element.tags === 'newinfo' && Number(element.id) === Number(this.selectedIndustry)) {
        this.ind = element.name;

        this.departments = element.jobFamilies;
      }
      // console.log('jobDepartments:', this.departments);
    });


  }


  selectDepartment() {
    this.selectedDepartment = this.department.value;
    // console.log('selectedDepartment:', this.selectedDepartment);

    this.departments.forEach(element => {
      // console.log('element:', element)
      if (element.tags === 'newinfo' && Number(element.id) === Number(this.selectedDepartment)) {
        this.dep = element.name;

        this.jobRoles = element.jobRoles;
        this.jobRole.setValue(this.jobRoles[0].id);
        this.role = this.jobRoles[0];
      }
      // console.log('jobFamilies:', this.jobRoles);
    });


  }

  selectJobRole() {
    this.jobRoles.forEach(element => {
      // console.log('element:', element)
      // console.log('jobRoles:', this.jobRoles)
      // console.log('jobRoles:', this.jobRole.value)
      if (element.tags === 'newinfo' && Number(element.id) === Number(this.jobRole.value)) {
        this.role = element;
      }
      // console.log('jobFamilies:', this.role);
    });

  }

  onSubmit() {

    if (this.myform.controls['firstName'].value && this.myform.controls['firstName'].value.trim() === "") {
      this.myform.controls['firstName'].setValue('');
    }
    if (this.myform.controls['lastName'].value && this.myform.controls['lastName'].value.trim() === "") {
      this.myform.controls['lastName'].setValue('');
    }
    if (!this.myform.valid && this.previewService.organizationId.getValue() !== '261351') {
      // console.log('Form', this.myform.status);
      this.myform.controls['firstName'].markAsTouched();
      this.myform.controls['lastName'].markAsTouched();
      this.myform.controls['email'].markAsTouched();
      this.myform.controls['phone'].markAsTouched();
      this.myform.controls['location'].markAsTouched();
      // this.myform.controls['group'].markAsTouched();
      this.myform.controls['indus'].markAsTouched();
      this.myform.controls['department'].markAsTouched();
      this.myform.controls['jobRole'].markAsTouched();
      return;
    }


    this.jobRoles.forEach(element => {
      // console.log('element:', element)
      // console.log('jobRoles:', this.jobRoles)
      // console.log('jobRoles:', this.jobRole.value)
      if (element.tags === 'newinfo' && Number(element.id) === Number(this.jobRole.value)) {
        this.role = element;
      }
      // console.log('jobFamilies:', this.role);
    });
    if (this.firstName.value.trim().length < 1) {
      swal('Invalid First Name', 'Empty First Name', 'error');
    }
    else if (!(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/.test(this.email.value))) {
      swal('Invalid Email', 'Error in email format', 'error');
    }
    else {

      if (!this.userToEdit.id) {


        let DTO = Object.assign({}, this.myform.value, {
          activated: true,
          orgId: this.previewService.organizationId.getValue(),
          group: null, userType: 0, login: this.phone.value, userPassword: this.password, langKey: null,
          organizationId: this.previewService.organizationId.getValue(),
          imageUrl: null, deviceID: Number(this.previewService.organizationId.getValue()) === 261351 ? 261701 : this.role.id
        })
        DTO = JSON.parse(JSON.stringify(DTO));
        // console.log('DTO:', DTO);
        if (this.organizationId !== '261351') {
          this.html = `<div><span><b>First Name:</b> ` + this.firstName.value + `</span><br/>` + `<span><b>Last Name: </b> ` + this.lastName.value + `</span > <br/>`
            + `<span><b>Email: </b> ` + this.email.value + `</span > <br/>` +
            `<span><b>Phone Number:</b>` + this.phone.value + `</span><br/>` + `<span><b>Location:</b>` + this.location.value + `</span><br/></div>`;
        }
        else {
          this.html = `<div><span><b>First Name:</b> ` + this.firstName.value + `</span><br/>` + `<span><b>Last Name: </b> ` + this.lastName.value + `</span > <br/>`
            + `<span><b>Email: </b> ` + this.email.value + `</span > <br/>` +
            `<span><b>Phone Number:</b>` + this.phone.value + `</span><br/>` + `<span><b>Location:</b>` + this.location.value + `</span><br/>` +
            `<span><b>Industry:</b>` + this.ind + `</span> <br/>` + `<span><b>Department:</b>` + this.dep + `</span> <br/>` + `<span><b>Job Role:</b>`
            + this.role.name + `</span> <br/></div>`
        }
        swal({
          title: 'Are you sure you want to create this user?',
          html: this.html,
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#0CC27E',
          cancelButtonColor: '#FF586B',
          confirmButtonText: 'Yes, create it!',
          cancelButtonText: 'No, cancel!',
          confirmButtonClass: 'btn btn-success btn-raised mr-5',
          cancelButtonClass: 'btn btn-danger btn-raised',
          buttonsStyling: false
        }).then((confirm) => {
          // console.log('con:', confirm)
          if (confirm.value) {
            this.http.post(base + '/api/users/register', DTO, this.httpOptions).subscribe(res => {
              // console.log('response to post:', res);
              let result = <any>res;
              // console.log('memberData:', result);

              let memberData = {
                'name': result['firstName'] + ' ' + (result['lastName'] || ''),
                'userId': result['id'],
                'learnerId': result['learner']['id'],
                'category': Number(this.previewService.organizationId.getValue()),
                'subCategory': this.dep,
                'locationName': result['location'],
                'gehash1': result['geoHash'],
                'memberType': 'APPLICANT',
                'aadhaar': result['phone'] || null,
                'mobileNo': result['phone'] || this.phone.value,
                'cardId': null
              };
              // console.log('memberData:', memberData);
              this.http.post(base + '/api/members', memberData, this.httpOptions).subscribe(response => {
                // console.log(response);
                let memberResponse = <any>response;
                // console.log('member:', memberResponse);
                result.learnerId = result['learner']['id'];
                result.langKey = memberResponse.id;
                result.userPassword = this.password;
                result.city = this.location.value;
                result.authorities = ["ROLE_USER"];
                // console.log('newuserput', result);
                let learnerObj = Object.assign({}, result.learner);
                learnerObj.tags = Number(this.previewService.organizationId.getValue()) === 261351 ? 261701 : result['deviceID'];
                learnerObj.city = result['location'];
                learnerObj.organizationId = Number(this.previewService.organizationId.getValue());
                learnerObj.description = memberResponse.id;
                learnerObj.department = this.dep;
                learnerObj.jobRole = this.role.name;
                this.http.put(base + '/api/users', result, this.httpOptions).subscribe(respo => {
                  // console.log('user:', respo);


                  this.http.put(base + '/api/learners', learnerObj, this.httpOptions).subscribe(learnerresp => {
                    // console.log('learner:', learnerresp);
                    console.log(this.emailMessage);
                    this.sendEmail('Welcome To Productivise', this.emailMessage, result['email']).then(a => this.myform.reset());
                    swal('User Created Successfully.', '', 'success');
                  })
                  this.http.get(base + '/api/campaign-groups', this.httpOptions).subscribe(groupResp => {
                    let groups = <any>groupResp;
                    groups.forEach(element => {
                      if (element.name === 'End-Users' && element.orgId === this.previewService.organizationId.getValue()) {
                        element.learners.push(result.learner);
                        this.http.put(base + '/api/campaign-groups', element, this.httpOptions).subscribe(putGroup => {
                          // console.log('putResp:', putGroup);
                        })
                      }
                    });
                  }, err => {
                    // console.log('Err', err);
                    swal(
                      'User Exists',
                      'Your User was not created.',
                      'error'
                    )
                  });
                }, err => {
                  // console.log('Err', err);

                  swal(
                    'User Exists',
                    'Your User was not created.',
                    'error'
                  )
                });
                this.http.get(base + '/api/support-groups', this.httpOptions).subscribe(groupResp => {
                  let groups = <any>groupResp;
                  groups.forEach(element => {
                    if (element.name === 'End-Users' && element.orgId === this.previewService.organizationId.getValue()) {
                      element.members.push(memberResponse);
                      this.http.put(base + '/api/support-groups', element, this.httpOptions).subscribe(putGroup => {
                        // console.log('putResp:', putGroup);
                      })
                    }
                  });
                });


              }, err => {
                swal(
                  'User Exists',
                  'Your User was not created.',
                  'error'
                )
              });
            }, err => {
              // console.log(err);
              swal(
                'User Exists',
                'Your User was not created.',
                'error'
              )
            })
            // console.log('Form Submitted!');
            // console.log(DTO);

          }
          else {
            swal(
              'Cancelled',
              'Your User was not created.',
              'error'
            )
          }
        }, (dismiss) => {
          // console.log('dismis:', dismiss)
          // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
          if (dismiss === 'cancel') {
            swal(
              'Cancelled',
              'Your User was not created.',
              'error'
            )
          }
        })
      }
    }

    if (this.userToEdit.id) {

      this.http.get(base + '/api/users/' + this.userToEdit.login, this.httpOptions).subscribe(us => {
        let user = <any>us;
        let DTO = Object.assign(user, {
          activated: true,
          firstName: this.firstName.value,
          lastName: this.lastName.value,
          location: this.location.value,
          orgId: this.previewService.organizationId.getValue(),
          organizationId: this.previewService.organizationId.getValue(),
          deviceID: Number(this.previewService.organizationId.getValue()) === 261351 ? '261701' : String(this.role.id)
        })
        DTO = JSON.parse(JSON.stringify(DTO));
        // console.log('DTO:', DTO);
        if (this.organizationId !== '261351') {
          this.html = `<div><span><b>First Name:</b> ` + this.firstName.value + `</span><br/>` + `<span><b>Last Name: </b> ` + this.lastName.value + `</span > <br/>`
            + `<span><b>Email: </b> ` + this.email.value + `</span > <br/>` +
            `<span><b>Phone Number:</b>` + this.phone.value + `</span><br/>` + `<span><b>Location:</b>` + this.location.value + `</span><br/></div>`;
        }
        else {
          this.html = `<div><span><b>First Name:</b> ` + this.firstName.value + `</span><br/>` + `<span><b>Last Name: </b> ` + this.lastName.value + `</span > <br/>`
            + `<span><b>Email: </b> ` + this.email.value + `</span > <br/>` +
            `<span><b>Phone Number:</b>` + this.phone.value + `</span><br/>` + `<span><b>Location:</b>` + this.location.value + `</span><br/>` +
            `<span><b>Industry:</b>` + this.ind + `</span> <br/>` + `<span><b>Department:</b>` + this.dep + `</span> <br/>` + `<span><b>Job Role:</b>`
            + this.role.name + `</span> <br/></div>`
        }
        swal({
          title: 'Are you sure you want to update this user?',
          html: this.html,
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#0CC27E',
          cancelButtonColor: '#FF586B',
          confirmButtonText: 'Yes, update it!',
          cancelButtonText: 'No, cancel!',
          confirmButtonClass: 'btn btn-success btn-raised mr-5',
          cancelButtonClass: 'btn btn-danger btn-raised',
          buttonsStyling: false
        }).then((confirm) => {
          // console.log('con:', confirm)
          if (confirm.value) {
            this.http.put(base + '/api/users', DTO, this.httpOptions).subscribe(res => {
              // console.log('response to post:', res);
              let result = <any>res;
              // console.log('memberData:', result);

              let memberData = {
                'id': result['langKey'],
                'name': result['firstName'] + ' ' + (result['lastName'] || ''),
                'userId': result['id'],
                'learnerId': result['learnerId'],
                'category': Number(this.previewService.organizationId.getValue()),
                'subCategory': this.dep,
                'locationName': result['location'],
                'gehash1': result['geoHash'],
                'memberType': 'APPLICANT',
                'aadhaar': result['phone'] || null,
                'mobileNo': result['phone'] || this.phone.value,
                'cardId': null
              };
              // console.log('memberData:', memberData);
              this.http.put(base + '/api/members', memberData, this.httpOptions).subscribe(response => {
                // console.log(response);
                let memberResponse = <any>response;
                // console.log('member:', memberResponse);
                result.learnerId = result['learnerId'];
                result.langKey = memberResponse.id;
                result.city = this.location.value;
                result.authorities = ["ROLE_USER"];
                // console.log('newuserput', result);
                let learnerObj = Object.assign({}, result.learner);
                learnerObj.tags = Number(this.previewService.organizationId.getValue()) === 261351 ? 261701 : String(this.role.id);
                learnerObj.city = result['location'];
                learnerObj.organizationId = Number(this.previewService.organizationId.getValue());
                learnerObj.description = memberResponse.id;
                learnerObj.department = this.dep;
                learnerObj.jobRole = this.role.name;
                this.http.put(base + '/api/learners', learnerObj, this.httpOptions).subscribe(learnerresp => {
                  // console.log('learner:', learnerresp);
                  this.sendSMS().then(a => {
                    this.myform.reset();
                    this.router.navigate(['/user/list']);
                  });
                  swal('User Updated Successfully.', '', 'success');
                })
                /*
                  this.http.get(base + '/api/campaign-groups', this.httpOptions).subscribe(groupResp => {
                    let groups = <any>groupResp;
                    groups.forEach(element => {
                      if (element.name === 'End-Users' && element.orgId === this.previewService.organizationId.getValue()) {
                        element.learners.push(result.learner);
                        this.http.put(base + '/api/campaign-groups', element, this.httpOptions).subscribe(putGroup => {
                          // console.log('putResp:', putGroup);
                        })
                      }
                    });
                  }, err => {
                    // console.log('Err', err);
                    swal(
                      'User Exists',
                      'Your User was not created.',
                      'error'
                    )
                  });
  
                  this.http.get(base + '/api/support-groups', this.httpOptions).subscribe(groupResp => {
                    let groups = <any>groupResp;
                    groups.forEach(element => {
                      if (element.name === 'End-Users' && element.orgId === this.previewService.organizationId.getValue()) {
                        element.members.push(memberResponse);
                        this.http.put(base + '/api/support-groups', element, this.httpOptions).subscribe(putGroup => {
                          // console.log('putResp:', putGroup);
                        })
                      }
                    });
                  });
  */

              });
            }, err => {
              // console.log(err);
              swal(
                'User Exists',
                'Your User was not created.',
                'error'
              )
            })
            // console.log('Form Submitted!');
            // console.log(DTO);

          }
          else {
            swal(
              'Cancelled',
              'Your User was not edited.',
              'error'
            )
          }
        }, (dismiss) => {
          // console.log('dismis:', dismiss)
          // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
          if (dismiss === 'cancel') {
            swal(
              'Cancelled',
              'Your User was not created.',
              'error'
            )
          }
        })
      })

    }
  }


  sendSMS() {

    // console.log('Message Sent!!')
    // console.log('contacts:', this.phone.value);
    const sns = AWS.sns;
    return sns.publish({
      Message: this.message, PhoneNumber: '+91' + this.phone.value, MessageAttributes: {
        'AWS.SNS.SMS.SenderID': {
          DataType: 'String',
          StringValue: 'PRDCTVSE'
        },
        'AWS.SNS.SMS.MaxPrice': {
          DataType: 'Number',
          StringValue: '00.01'
        },
        'AWS.SNS.SMS.SMSType': {
          DataType: 'String',
          StringValue: 'Transactional'
        }
      }
    }).promise().then(res => {
      // console.log(res);

    });

  }

  sendEmail(subject, message, email) {
    // console.log('message:', message);
    return this.http.post(proxyApi+'/sendemail', {
      template: message,
      address: email,
      subject: subject
    }, this.httpOptions).toPromise().then(mailRes => {
      let result = <any>mailRes;
      console.log("mail sent" + result);
      // this.sendSMS(this.message, user.phone)
    })
  }



  ngOnDestroy() {

    this.previewService.editUser({});
  }
}

