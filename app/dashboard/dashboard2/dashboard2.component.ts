import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from "ng-chartist/dist/chartist.component";
import { base } from '../../shared/configs/util';
import { baseAnalytics } from '../../shared/configs/util';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { PreviewService } from '../../shared/auth/preview.service';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import { Router } from '@angular/router';
import { SupportGroupResourceService } from '../../sthaapak/sdk/supportGroupResource.service';
import { DomSanitizer } from '@angular/platform-browser';
import { CampaignGroupResourceService } from '../../sthaapak';

declare var require: any;

const data: any = require('../../shared/data/chartist.json');


export interface Chart {
    type: ChartType;
    data: Chartist.IChartistData;
    options?: any;
    responsiveOptions?: any;
    events?: ChartEvent;
}

@Component({
    selector: 'app-dashboard2',
    templateUrl: './dashboard2.component.html',
    styleUrls: ['./dashboard2.component.scss']
})

export class Dashboard2Component implements OnInit {
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.roleGuardService.token
        })
    };

    // Line chart configuration Starts
    WidgetlineChart: Chart = {
        type: 'Line', data: data['WidgetlineChart2'],
        options: {
            axisX: {
                showGrid: false,
                showLabel: false,
                offset: 0,
            },
            axisY: {
                showGrid: false,
                low: 50,
                showLabel: false,
                offset: 0,
            },
            fullWidth: true
        },
    };
    // Line chart configuration Ends

    // Line chart configuration Starts
    WidgetlineChart1: Chart = {
        type: 'Line', data: data['WidgetlineChart3'],
        options: {
            axisX: {
                showGrid: false,
                showLabel: false,
                offset: 0,
            },
            axisY: {
                showGrid: false,
                low: 50,
                showLabel: false,
                offset: 0,
            },
            fullWidth: true,
            chartPadding: { top: 0, right: 0, bottom: 10, left: 0 }
        },
        events: {
            created(data: any): void {

                var defs = data.svg.elem('defs');
                defs.elem('linearGradient', {
                    id: 'widgradient',
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                }).elem('stop', {
                    offset: 0,
                    'stop-color': 'rgba(132, 60, 247, 1)'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-color': 'rgba(56, 184, 242, 1)'
                });

            },

        },
    };
    // Line chart configuration Ends

    // Line chart configuration Starts
    WidgetlineChart2: Chart = {
        type: 'Line', data: data['WidgetlineChart'],
        options: {
            axisX: {
                showGrid: true,
                showLabel: false,
                offset: 0,
            },
            axisY: {
                showGrid: false,
                low: 40,
                showLabel: false,
                offset: 0,
            },
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            fullWidth: true
        },
        events: {
            created(data: any): void {

                var defs = data.svg.elem('defs');
                defs.elem('linearGradient', {
                    id: 'widgradient1',
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                }).elem('stop', {
                    offset: 0,
                    'stop-color': 'rgba(0, 201, 255,1)'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-color': 'rgba(17,228,183, 1)'
                });
            },

        },
    };
    // Line chart configuration Ends

    // Donut chart configuration Starts
    DonutChart1: Chart = {
        type: 'Pie',
        data: data['DashboardDonut'],
        options: {
            donut: true,
            donutWidth: 3,
            startAngle: 0,
            chartPadding: 25,
            labelInterpolationFnc: function (value) {
                return '\ue9c9';
            }
        },
        events: {
            draw(data: any): void {
                if (data.type === 'label') {
                    if (data.index === 0) {
                        data.element.attr({
                            dx: data.element.root().width() / 2,
                            dy: (data.element.root().height() + (data.element.height() / 4)) / 2,
                            class: 'ct-label',
                            'font-family': 'feather'
                        });
                    } else {
                        data.element.remove();
                    }
                }
            }
        }
    };
    // Donut chart configuration Ends

    // Donut chart configuration Starts
    DonutChart2: Chart = {
        type: 'Pie',
        data: data['DashboardDonut'],
        options: {
            donut: true,
            donutWidth: 3,
            startAngle: 90,
            chartPadding: 25,
            labelInterpolationFnc: function (value) {
                return '\ue9e7';
            }
        },
        events: {
            draw(data: any): void {
                if (data.type === 'label') {
                    if (data.index === 0) {
                        data.element.attr({
                            dx: data.element.root().width() / 2,
                            dy: (data.element.root().height() + (data.element.height() / 4)) / 2,
                            class: 'ct-label',
                            'font-family': 'feather'
                        });
                    } else {
                        data.element.remove();
                    }
                }
            }
        }
    };
    // Donut chart configuration Ends

    // Donut chart configuration Starts
    DonutChart3: Chart = {
        type: 'Pie',
        data: data['DashboardDonut'],
        options: {
            donut: true,
            donutWidth: 3,
            startAngle: 270,
            chartPadding: 25,
            labelInterpolationFnc: function (value) {
                return '\ue964';
            }
        },
        events: {
            draw(data: any): void {
                if (data.type === 'label') {
                    if (data.index === 0) {
                        data.element.attr({
                            dx: data.element.root().width() / 2,
                            dy: (data.element.root().height() + (data.element.height() / 4)) / 2,
                            class: 'ct-label',
                            'font-family': 'feather'
                        });
                    } else {
                        data.element.remove();
                    }
                }
            }
        }
    };
    // Donut chart configuration Ends

    // Line area chart configuration Starts
    lineAreaChart: Chart = {
        type: 'Line',
        data: data['lineArea3'],
        options: {
            low: 0,
            showArea: true,
            fullWidth: true,
            onlyInteger: true,
            axisY: {
                low: 0,
                scaleMinSpace: 50,
            },
            axisX: {
                showGrid: false
            }
        },
        events: {
            created(data: any): void {
                var defs = data.svg.elem('defs');
                defs.elem('linearGradient', {
                    id: 'gradient',
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                }).elem('stop', {
                    offset: 0,
                    'stop-opacity': '0.2',
                    'stop-color': 'rgba(255, 255, 255, 1)'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-opacity': '0.2',
                    'stop-color': 'rgba(38, 198, 218, 1)'
                });
            },
            draw(data: any): void {

                var circleRadius = 6;
                if (data.type === 'point') {
                    var circle = new Chartist.Svg('circle', {
                        cx: data.x,
                        cy: data.y,
                        r: circleRadius,
                        class: 'ct-point-circle'
                    });
                    data.element.replace(circle);
                }
            }
        },
    };
    // Line area chart configuration Ends

    // Line chart configuration Starts
    lineChart2: Chart = {
        type: 'Line', data: data['line2'],
        options: {
            axisX: {
                showGrid: false,
            },
            axisY: {
                low: 0,
                scaleMinSpace: 50,
            },
            fullWidth: true,
        },
        responsiveOptions: [
            ['screen and (max-width: 640px) and (min-width: 381px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 2 === 0 ? value : null;
                    }
                }
            }],
            ['screen and (max-width: 380px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 3 === 0 ? value : null;
                    }
                }
            }]
        ],
        events: {
            draw(data: any): void {
                var circleRadius = 6;
                if (data.type === 'point') {
                    var circle = new Chartist.Svg('circle', {
                        cx: data.x,
                        cy: data.y,
                        r: circleRadius,
                        class: 'ct-point-circle'
                    });
                    data.element.replace(circle);
                }
                else if (data.type === 'label') {
                    // adjust label position for rotation
                    const dX = data.width / 2 + (30 - data.width)
                    data.element.attr({ x: data.element.attr('x') - dX })
                }
            }
        },

    };
    // Line chart configuration Ends

    // Line chart configuration Starts
    lineChart1: Chart = {
        type: 'Line', data: data['line1'],
        options: {
            axisX: {
                showGrid: false,
            },
            axisY: {
                low: 0,
                scaleMinSpace: 50,
            },
            fullWidth: true
        },
        events: {
            draw(data: any): void {
                if (data.type === 'label') {
                    // adjust label position for rotation
                    const dX = data.width / 2 + (30 - data.width)
                    data.element.attr({ x: data.element.attr('x') - dX })
                }
            }
        },
    };
    // Line chart configuration Ends
    DashboardItems = [];
    averageTimeSpent = [];
    learner;
    totalTimeSpentSum;
    modulesStarted = [];
    videosWatched = [];
    timeSpentOnVideos;
    modulesFinished = [];
    courseStarted = [];
    courseFinished = [];
    progressBar;
    constructor(private http: HttpClient, private previewService: PreviewService, private roleGuardService: RoleGuardService, private router: Router,
        private campaignGroups: CampaignGroupResourceService, private sanitizer: DomSanitizer, private supportGroups: SupportGroupResourceService) {
        if (!localStorage.getItem('token')) {
            this.router.navigateByUrl('/login');
        }
    }

    getModuleEvents() {
        this.courseStarted = [];
        this.courseFinished = [];
        this.videosWatched = [];
        /*
           this.http.get(base + '/api/_search/events?query=(name1%3DVideo_time_clicked)AND(userID%3D' + this.learner.registeredNumber + ')',
               this.httpOptions).subscribe(res => {
                   let results = <any>res;
   
                   results.forEach(element => {
                       let t1 = element.strValue.split(',')[0];
                       let t2 = element.strValue.split(',')[1];
                       let put = true;
                       if (this.videosWatched.length < 1 && t1 > 0 && t2 > t1 && element.value2.indexOf('&&')) {
                           this.videosWatched.push({
                               category: element.name, subcategory: element.category, name: element.value2.split('&&')[1],
                               count: 1, time: (t2 - t1)
                           })
                       }
                       if (this.videosWatched.length && t1 > 0 && t2 > t1 && element.value2.indexOf('&&')) {
                           this.videosWatched.forEach((el, i) => {
                               if (el.category === element.name && el.subcategory === element.category && el.name === element.value2.split('&&')[1]) {
                                   put = false;
   
                                   el.count++;
                                   el.time += (t2 - t1);
   
                               }
   
   
                           });
                           if (put && element.value2.indexOf('&&')) {
                               this.videosWatched.push({
                                   category: element.name, subcategory: element.category, name: element.value2.split('&&')[1],
                                   count: 1, time: (t2 - t1)
                               })
   
                           }
                       }
                   });
   
                   // console.log('videosWatched:', this.videosWatched);
                   this.videosWatched.sort((a, b) => {
                       return b.count - a.count;
                   });
                   this.videosWatched = this.videosWatched.slice(0, 5);
               }, error => {
                   this.videosWatched = [];
               });
   */

        this.http.get(baseAnalytics + '/api/_search/events?query=(name1%3DMODULE_clicked)AND(userID%3D' + this.learner.registeredNumber + ')',
            this.httpOptions).subscribe(res => {
                let results = <any>res;
                // console.log('MODULES CLICKED:', results);
                results.forEach(element => {

                    let put = true;
                    if (this.courseStarted.length < 1) {
                        this.courseStarted.push({
                            category: element.name, subcategory: element.category, name: element.value2
                        })
                    }
                    if (this.courseStarted.length) {
                        this.courseStarted.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2) {
                                put = false;
                            }


                        });
                        if (put) {
                            this.courseStarted.push({
                                category: element.name, subcategory: element.category, name: element.value2
                            })

                        }
                    }


                    let put2 = true;
                    if (this.modulesStarted.length < 1) {
                        this.modulesStarted.push({
                            category: element.name, subcategory: element.category, name: element.value2,
                            strValue: element.strValue
                        })
                    }
                    if (this.modulesStarted.length) {
                        this.modulesStarted.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2 && el.strValue === element.strValue) {
                                put2 = false;

                            }


                        });
                        if (put2) {
                            this.modulesStarted.push({
                                category: element.name, subcategory: element.category, name: element.value2,
                                strValue: element.strValue
                            })

                        }
                    }

                });

                // console.log('ModulesStarted:', this.courseStarted);
                this.courseStarted.sort((a, b) => {
                    return b.count - a.count;
                });
                this.courseStarted = this.courseStarted.slice(0, 5);
                this.modulesStarted.sort((a, b) => {
                    return b.count - a.count;
                });
                this.modulesStarted = this.modulesStarted.slice(0, 5);

            }, error => {
                this.courseStarted = [];
                this.modulesStarted = [];
            });


        this.http.get(baseAnalytics + '/api/_search/events?query=(name1%3Dmicrolearning_module_completed_clicked)AND(userID%3D' + this.learner.registeredNumber + ')',
            this.httpOptions).subscribe(res => {
                let results = <any>res;
                // console.log('MODULES Finished:', results);

                results.forEach(element => {

                    let put = true;
                    if (this.courseFinished.length < 1) {
                        this.courseFinished.push({
                            category: element.name, subcategory: element.category, name: element.value2,
                            count: 1, userID: [element.userID]
                        })
                    }
                    if (this.courseFinished.length) {
                        this.courseFinished.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2) {
                                put = false;
                                if (el.userID.indexOf(element.userID) < 0) {
                                    el.count++;
                                    el.userID.push(element.userID);
                                }
                            }


                        });
                        if (put) {
                            this.courseFinished.push({
                                category: element.name, subcategory: element.category, name: element.value2,
                                count: 1, userID: [element.userID]
                            })

                        }
                    }


                    let put2 = true;
                    if (this.modulesFinished.length < 1) {
                        this.modulesFinished.push({
                            category: element.name, subcategory: element.category, name: element.value2,
                            strValue: element.strValue, count: 1, userID: [element.userID]
                        })
                    }
                    if (this.modulesFinished.length) {
                        this.modulesFinished.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2 && el.strValue === element.strValue) {
                                put2 = false;
                                if (el.userID.indexOf(element.userID) < 0) {
                                    el.count++;
                                    el.userID.push(element.userID);
                                }
                            }


                        });
                        if (put2) {
                            this.modulesFinished.push({
                                category: element.name, subcategory: element.category, name: element.value2,
                                strValue: element.strValue, count: 1, userID: [element.userID]
                            })

                        }
                    }

                });

                // console.log('ModulesStarted:', this.courseFinished);
                this.courseFinished.sort((a, b) => {
                    return b.count - a.count;
                });
                this.courseFinished = this.courseFinished.slice(0, 5);
                this.modulesFinished.sort((a, b) => {
                    return b.count - a.count;
                });
                this.modulesFinished = this.modulesFinished.slice(0, 5);
            }, error => {
                this.courseFinished = [];
                this.modulesFinished = [];
            });


    }


    getContentMetrics2() {
        this.http.get(baseAnalytics + '/api/_search/events?query=(name%3DAppTimeSpent)AND(userID%3D' + this.learner.registeredNumber + ')',
            this.httpOptions).subscribe(res => {

                let results: any = res;
                // console.log('timespentt:', results);

                let TotalTimeSpent = [];
                let TotalTimeSpentBy = [];
                for (let index = 0; index < results.length; index++) {
                    if (TotalTimeSpentBy.indexOf(results[index]['userID']) < 0 && String(TotalTimeSpentBy.indexOf(results[index]['userID'])).length > 0) {
                        TotalTimeSpentBy.push(results[index]['userID']);
                        TotalTimeSpent.push(results[index]['strValue'] - results[index]['category']);
                    }
                    else {
                        TotalTimeSpent[TotalTimeSpentBy.indexOf(results[index]['userID'])] += (results[index]['strValue'] - results[index]['category']);
                    }
                }
                // console.log('Total Time Spent By count:', TotalTimeSpentBy);
                // console.log('Total Time Spent count:', TotalTimeSpent);
                this.totalTimeSpentSum = TotalTimeSpent.reduce((sum, e) => sum + e);
                this.totalTimeSpentSum /= 60000;
                this.totalTimeSpentSum = Math.round((this.totalTimeSpentSum + 0.00001) * 100) / 100
                // console.log('totalTimeSpentSum:', this.totalTimeSpentSum);
                this.totalTimeSpentSum = Math.round(this.totalTimeSpentSum);
                this.averageTimeSpent[0] = { screen: '', time: 0 };

                this.averageTimeSpent[0].screen = 'Total Time Spent on App';
                for (let j = 0; j < TotalTimeSpent.length; j++) {
                    this.averageTimeSpent[0].time += TotalTimeSpent[j];
                }
                this.averageTimeSpent[0].time = this.averageTimeSpent[0].time ? (this.averageTimeSpent[0].time / (60000)) : 0;
                this.averageTimeSpent[0].time = Math.round((this.averageTimeSpent[0].time + 0.00001) * 100) / 100;
                this.averageTimeSpent[0].time = Math.round(this.averageTimeSpent[0].time);
                // console.log('averageTimeSpent:', this.averageTimeSpent);
            }, error => {
                this.averageTimeSpent[0] = { screen: 'totalTimeSpent', time: 0 };

            });
        this.http.get(base + '/api/app-configs/org/' + this.previewService.organizationId.getValue(), this.httpOptions)
            .subscribe(res => {
                let result = <any>res;
                // console.log('DashboadItems:', res);
                for (let index = 0; index < result.length; index++) {
                    if ((this.previewService.organizationId.getValue() === result[index].organizationId) && (result[index]['category'] === 'DASHBOARD')) {
                        this.DashboardItems.push(result[index]['value']);
                    }
                }
                // console.log('items:', this.DashboardItems);
                this.http.get(baseAnalytics + '/api/_search/events?query=(name%3A*TimeSpent)AND(userID%3D' + this.learner.registeredNumber + ')',
                    this.httpOptions).subscribe(res => {
                        let results = <any>res;
                        // console.log('timespentBLOGSONLY:', results);

                        for (let i = 0; i < this.DashboardItems.length; i++) {
                            let TimeSpent = [];
                            let TimeSpentBy = [];
                            for (let index = 0; index < results.length; index++) {

                                if ((TimeSpentBy.indexOf(results[index]['userID']) < 0) && String(TimeSpentBy.indexOf(results[index]['userID'])).length > 0
                                    && results[index]['value1'] === this.DashboardItems[i]) {
                                    TimeSpentBy.push(results[index]['userID']);
                                    TimeSpent.push(results[index]['strValue'] - results[index]['category']);
                                }
                                else if ((TimeSpentBy.indexOf(results[index]['userID']) > 0) && String(TimeSpentBy.indexOf(results[index]['userID'])).length > 0
                                    && results[index]['value1'] === this.DashboardItems[i]) {
                                    TimeSpent[TimeSpentBy.indexOf(results[index]['userID'])] += (results[index]['strValue'] - results[index]['category']);
                                }
                            }

                            // console.log(this.DashboardItems[i] + ' Time Spent By count:', TimeSpentBy);
                            // console.log(this.DashboardItems[i] + 'Diary Time Spent count:', TimeSpent);
                            this.averageTimeSpent[i + 1] = { screen: '', time: 0 };
                            this.averageTimeSpent[i + 1].screen = this.DashboardItems[i];
                            for (let j = 0; j < TimeSpent.length; j++) {
                                this.averageTimeSpent[i + 1].time += TimeSpent[j];
                            }
                            this.averageTimeSpent[i + 1].time = this.averageTimeSpent[i + 1].time ? (this.averageTimeSpent[i + 1].time / (60000 * TimeSpent.length)) : 0;
                            this.averageTimeSpent[i + 1].time = this.averageTimeSpent[i + 1].time ? Math.round((this.averageTimeSpent[i + 1].time + 0.00001) * 100) / 100 : 0;
                            this.averageTimeSpent[i + 1].time = Math.round(this.averageTimeSpent[i + 1].time);
                            // console.log('averageTimeSpentD2:', this.averageTimeSpent);
                        }
                        // console.log('averageTimeSpentTotal:', this.averageTimeSpent);

                    }, error => {
                        this.averageTimeSpent[1] = { screen: this.DashboardItems[1], time: 0 };
                    });
            });
    }

    ngOnInit() {

        this.learner = <any>this.previewService.statsOf.getValue();
        this.getContentMetrics2();
        if (!this.learner.id) {
            this.router.navigate(['/dashboard/dashboard1']);
        }
        this.getModuleEvents();

        this.http.get(baseAnalytics + '/api/_search/events?query=(name%3DAppTimeSpent)AND(userID%3D' + this.learner.registeredNumber + ')',
            this.httpOptions).subscribe(res => {


                let results: any = res;
                // console.log('timespentt Indi:', results);
                if (results.length > 0) {
                    let TotalTimeSpent = [];
                    let TotalTimeSpentBy = [];
                    for (let index = 0; index < results.length; index++) {
                        TotalTimeSpent.push(results[index]['strValue'] - results[index]['category']);
                    }

                    // console.log('Total Time Spent count:', TotalTimeSpent);

                    this.totalTimeSpentSum = TotalTimeSpent.reduce((sum = 0, e) => sum + e);
                    this.totalTimeSpentSum /= 60000;
                    this.totalTimeSpentSum = Math.round((this.totalTimeSpentSum + 0.00001) * 100) / 100;
                    this.totalTimeSpentSum = Math.round((this.totalTimeSpentSum));
                    // console.log('totalTimeSpentSum:', this.totalTimeSpentSum);
                }
                else {
                    this.totalTimeSpentSum = 0;
                }


            }, error => {
                this.totalTimeSpentSum = 0;

            });
        /*
                this.http.get(base + '/api/_search/events?query=(name1%3DMODULE_clicked)AND(userID%3D' + this.learner.registeredNumber + ')',
                    this.httpOptions).subscribe(res => {
                        let results = <any>res;
                        // console.log("EVENTS:", results);
                        results = results.map(e => e.strValue);
                        results = results.filter((e, i) => results.indexOf(e) === i);
                        // console.log("RESULTS:", results);
                        this.modulesStarted = results.length;
                    }, error => {
                        this.modulesStarted = 0;
                    });
        */
        this.http.get(baseAnalytics + '/api/_search/events?query=(name1%3DVideo_time_clicked)AND(userID%3D' + this.learner.registeredNumber + ')',
            this.httpOptions).subscribe(res => {
                let results = <any>res;
                this.timeSpentOnVideos = 0;
                // console.log("VIDEOEVENTS:", results);
                results = results.filter((e, i) => {
                    if (e.strValue.split(',')[0] !== '0' && e.value2.indexOf('&&') >= 0) {
                        return true;
                    }
                    return false;
                });
                // console.log("VIDEORESULTS:", results);
                results.forEach(element => {

                    this.timeSpentOnVideos += Number(element.strValue.split(',')[1]) - Number(element.strValue.split(',')[0]);
                    // console.log("VIDEOTimeSoent:", this.timeSpentOnVideos);
                    let put = true;
                    if (this.videosWatched.length < 1 && element.value2.indexOf('&&')) {
                        this.videosWatched.push({ category: element.name, subcategory: element.category, name: element.value2.split('&&')[1] })
                    }
                    if (this.videosWatched.length) {
                        this.videosWatched.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2.split('&&')[1]) {
                                put = false;
                            }
                        });
                        if (put && element.value2.indexOf('&&') > -1) {
                            this.videosWatched.push({ category: element.name, subcategory: element.category, name: element.value2.split('&&')[1] })

                        }
                    }

                });
                this.timeSpentOnVideos /= 6000;
                this.timeSpentOnVideos = Math.round((this.timeSpentOnVideos + 0.00001) * 100) / 100;

            }, error => {
                this.timeSpentOnVideos = 0;
                this.videosWatched = [];
            });

        /*
    this.http.get(base + '/api/_search/events?query=(name1%3Dmicrolearning_module_completed_clicked)AND(userID%3D' + this.learner.registeredNumber + ')',
        this.httpOptions).subscribe(res => {
            let results = <any>res;
            results.forEach(element => {
 
                let put = true;
                if (this.modulesFinished.length < 1) {
                    this.modulesFinished.push({ category: element.name, subcategory: element.category, name: element.value2, strValue: element.strValue })
                }
                if (this.modulesFinished.length) {
                    this.modulesFinished.forEach((el, i) => {
                        if (el.category === element.name && el.subcategory === element.category && el.name === element.value2 && el.strValue === element.strValue) {
                            put = false;
                        }
                    });
                    if (put) {
                        this.modulesFinished.push({ category: element.name, subcategory: element.category, name: element.value2, strValue: element.strValue })
 
                    }
                }
 
            });
        }, error => {
            this.modulesFinished = [];
        });
*/
    }


}
