import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, AfterViewChecked } from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from "ng-chartist/dist/chartist.component";
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { PreviewService } from '../../shared/auth/preview.service';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import { Date } from 'core-js';
import { baseAnalytics, proxyApi } from '../../shared/configs/util';
import { base } from '../../shared/configs/util';
import { CampaignGroupResourceService } from '../../sthaapak';
import { SupportGroupResourceService } from '../../sthaapak/sdk/supportGroupResource.service';
import swal from 'sweetalert2';
import { DatatableComponent } from '@swimlane/ngx-datatable/release';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import { chart } from 'highcharts';
import * as Highcharts from 'highcharts';
declare var require: any;

const data: any = require('../../shared/data/chartist.json');

export interface Chart {
    type: ChartType;
    data: Chartist.IChartistData;
    options?: any;
    responsiveOptions?: any;
    events?: ChartEvent;
}

@Component({
    selector: 'app-dashboard1',
    templateUrl: './dashboard1.component.html',
    styleUrls: ['./dashboard1.component.scss', './styles2.css']
})

export class Dashboard1Component implements OnInit, AfterViewInit, AfterViewChecked {


    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('chartTarget') chartTarget: ElementRef;
    chart: any;

    topusers = [];
    department;
    topGroups = [];
    countGroups = [];
    rows = [];
    temp = [];
    BlogItems = [];
    MicroLearningItems = []
    FeedbackItems = []
    OperationItems = []
    IndustryItems = []
    NewsItems = [];
    QuizItems = [];
    loadChart = false;
    url;
    totalTimeSpentHourString;
    totalTimeSpentMinuteString
    active = 0;
    inactive = 100;
    countUsers = [];
    highActivityUsers = [];
    departments = [];
    columns = [
        { prop: 'name' },
        { prop: 'profileImage' }
    ];
    donutDashboard: {
        series: [
            {
                "name": "Active Users",
                "className": "ct-progress",
                "value": 0
            },
            {
                "name": "Not Active",
                "className": "ct-outstanding",
                "value": 100
            }

        ]
    };

    PieDashboard: {
        series: [
            {
                "name": String,
                "className": "ct-progress",
                "value": Number
            },
            {
                "name": String,
                "className": "ct-outstanding",
                "value": Number
            },
            {
                "name": String,
                "className": "ct-outstanding",
                "value": Number
            },
            {
                "name": String,
                "className": "ct-done",
                "value": Number
            },
            {
                "name": "outstanding",
                "className": "ct-outstanding",
                "value": 35
            },
            {
                "name": "started",
                "className": "ct-started",
                "value": 28
            }
        ]
    };

    lineArea2Data: {
        labels: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        series: [
            [
                5,
                30,
                25,
                55,
                45,
                65,
                60,
                50,
                80,
                10,
                10,
                50
            ],
            [
                80,
                95,
                87,
                55,
                40,
                47,
                30,
                80,
                60,
                75,
                65,
                20
            ]
        ]
    }


    // Line area chart configuration Starts
    lineArea: Chart = {
        type: 'Line',
        data: data['lineAreaDashboard'],
        options: {
            low: 0,
            showArea: true,
            fullWidth: true,
            onlyInteger: true,
            axisY: {
                low: 0,
                scaleMinSpace: 50,
            },
            axisX: {
                showGrid: false
            }
        },
        events: {
            created(data: any): void {
                var defs = data.svg.elem('defs');
                defs.elem('linearGradient', {
                    id: 'gradient',
                    x1: 0,
                    y1: 1,
                    x2: 1,
                    y2: 0
                }).elem('stop', {
                    offset: 0,
                    'stop-color': 'rgba(0, 201, 255, 1)'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-color': 'rgba(146, 254, 157, 1)'
                });

                defs.elem('linearGradient', {
                    id: 'gradient1',
                    x1: 0,
                    y1: 1,
                    x2: 1,
                    y2: 0
                }).elem('stop', {
                    offset: 0,
                    'stop-color': 'rgba(132, 60, 247, 1)'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-color': 'rgba(56, 184, 242, 1)'
                });
            },

        },
    };
    // Line area chart configuration Ends

    // Stacked Bar chart configuration Starts
    Stackbarchart: Chart = {
        type: 'Bar',
        data: data['Stackbarchart'],
        options: {
            stackBars: true,
            fullWidth: true,
            axisX: {
                showGrid: false,
            },
            axisY: {
                showGrid: false,
                showLabel: false,
                offset: 0
            },
            chartPadding: 30
        },
        events: {
            created(data: any): void {
                var defs = data.svg.elem('defs');
                defs.elem('linearGradient', {
                    id: 'linear',
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                }).elem('stop', {
                    offset: 0,
                    'stop-color': 'rgba(0, 201, 255,1)'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-color': 'rgba(17,228,183, 1)'
                });
            },
            draw(data: any): void {
                if (data.type === 'bar') {
                    data.element.attr({
                        style: 'stroke-width: 5px',
                        x1: data.x1 + 0.001
                    });

                }
                else if (data.type === 'label') {
                    data.element.attr({
                        y: 270
                    })
                }
            }
        },
    };
    // Stacked Bar chart configuration Ends

    // Line area chart 2 configuration Starts
    lineArea2: Chart = {
        type: 'Line',
        data: data['lineArea2'],
        options: {
            showArea: true,
            fullWidth: true,
            lineSmooth: Chartist.Interpolation.none(),
            axisX: {
                showGrid: false,
            },
            axisY: {
                low: 0,
                scaleMinSpace: 50,
            }
        },
        responsiveOptions: [
            ['screen and (max-width: 640px) and (min-width: 381px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 2 === 0 ? value : null;
                    }
                }
            }],
            ['screen and (max-width: 380px)', {
                axisX: {
                    labelInterpolationFnc: function (value, index) {
                        return index % 3 === 0 ? value : null;
                    }
                }
            }]
        ],
        events: {
            created(data: any): void {
                var defs = data.svg.elem('defs');
                defs.elem('linearGradient', {
                    id: 'gradient2',
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                }).elem('stop', {
                    offset: 0,
                    'stop-opacity': '0.2',
                    'stop-color': 'rgba(255, 255, 255, 1)'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-opacity': '0.2',
                    'stop-color': 'rgba(0, 201, 255, 1)'
                });

                defs.elem('linearGradient', {
                    id: 'gradient3',
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                }).elem('stop', {
                    offset: 0.3,
                    'stop-opacity': '0.2',
                    'stop-color': 'rgba(255, 255, 255, 1)'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-opacity': '0.2',
                    'stop-color': 'rgba(132, 60, 247, 1)'
                });
            },
            draw(data: any): void {
                var circleRadius = 4;
                if (data.type === 'point') {

                    var circle = new Chartist.Svg('circle', {
                        cx: data.x,
                        cy: data.y,
                        r: circleRadius,
                        class: 'ct-point-circle'
                    });
                    data.element.replace(circle);
                }
                else if (data.type === 'label') {
                    // adjust label position for rotation
                    const dX = data.width / 2 + (30 - data.width)
                    data.element.attr({ x: data.element.attr('x') - dX })
                }
            }
        },
    };
    // Line area chart 2 configuration Ends

    // Line chart configuration Starts
    lineChart: Chart = {
        type: 'Line', data: data['LineDashboard'],
        options: {
            axisX: {
                showGrid: false
            },
            axisY: {
                showGrid: false,
                showLabel: false,
                low: 0,
                high: 100,
                offset: 0,
            },
            fullWidth: true,
            offset: 0,
        },
        events: {
            draw(data: any): void {
                var circleRadius = 4;
                if (data.type === 'point') {
                    var circle = new Chartist.Svg('circle', {
                        cx: data.x,
                        cy: data.y,
                        r: circleRadius,
                        class: 'ct-point-circle'
                    });

                    data.element.replace(circle);
                }
                else if (data.type === 'label') {
                    // adjust label position for rotation
                    const dX = data.width / 2 + (30 - data.width)
                    data.element.attr({ x: data.element.attr('x') - dX })
                }
            }
        },

    };
    // Line chart configuration Ends

    // Donut chart configuration Starts
    DonutChart: Chart = {
        type: 'Pie',
        data: this.donutDashboard,
        options: {
            donut: true,
            startAngle: 0,
            labelInterpolationFnc: (value) => {
                var total = this.donutDashboard.series.reduce(function (prev, series) {
                    if (series.name = 'Active Users') {


                        return prev + series.value;
                    }
                }, 0);
                return total + '%';
            }
        },
        events: {
            draw(data: any): void {
                if (data.type === 'label') {
                    if (data.index === 0) {
                        data.element.attr({
                            dx: data.element.root().width() / 2,
                            dy: data.element.root().height() / 2
                        });
                    } else {
                        data.element.remove();
                    }
                }

            }
        }
    };
    // Donut chart configuration Ends

    // 3D PIE CHART CONFIG


    // END 3D PIE CHART CONFIG


    //  Bar chart configuration Starts
    BarChart: Chart = {
        type: 'Bar', data: data['DashboardBar'], options: {
            axisX: {
                showGrid: false,
            },
            axisY: {
                showGrid: false,
                showLabel: false,
                offset: 0
            },
            low: 0,
            high: 60, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
        },
        responsiveOptions: [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 5,
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ],
        events: {
            created(data: any): void {
                var defs = data.svg.elem('defs');
                defs.elem('linearGradient', {
                    id: 'gradient4',
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                }).elem('stop', {
                    offset: 0,
                    'stop-color': 'rgba(238, 9, 121,1)'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-color': 'rgba(255, 106, 0, 1)'
                });
                defs.elem('linearGradient', {
                    id: 'gradient5',
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                }).elem('stop', {
                    offset: 0,
                    'stop-color': 'rgba(0, 75, 145,1)'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-color': 'rgba(120, 204, 55, 1)'
                });

                defs.elem('linearGradient', {
                    id: 'gradient6',
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                }).elem('stop', {
                    offset: 0,
                    'stop-color': 'rgba(132, 60, 247,1)'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-color': 'rgba(56, 184, 242, 1)'
                });
                defs.elem('linearGradient', {
                    id: 'gradient7',
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                }).elem('stop', {
                    offset: 0,
                    'stop-color': 'rgba(155, 60, 183,1)'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-color': 'rgba(255, 57, 111, 1)'
                });

            },
            draw(data: any): void {
                var barHorizontalCenter, barVerticalCenter, label, value;
                if (data.type === 'bar') {

                    data.element.attr({
                        y1: 195,
                        x1: data.x1 + 0.001
                    });

                }
            }
        },

    };
    // Bar chart configuration Ends

    // line chart configuration Starts
    WidgetlineChart: Chart = {
        type: 'Line', data: data['WidgetlineChart'],
        options: {
            axisX: {
                showGrid: true,
                showLabel: false,
                offset: 0,
            },
            axisY: {
                showGrid: false,
                low: 40,
                showLabel: false,
                offset: 0,
            },
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            fullWidth: true,
        },
    };



    WidgetlineChart2: Chart = {
        type: 'Line', data: data['WidgetlineChart2'],
        options: {
            axisX: {
                showGrid: true,
                showLabel: false,
                offset: 0,
            },
            axisY: {
                showGrid: false,
                low: 40,
                showLabel: false,
                offset: 0,
            },
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            fullWidth: true,
        },
    };

    WidgetlineChart3: Chart = {
        type: 'Line', data: data['WidgetlineChart3'],
        options: {
            axisX: {
                showGrid: true,
                showLabel: false,
                offset: 0,
            },
            axisY: {
                showGrid: false,
                low: 40,
                showLabel: false,
                offset: 0,
            },
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            fullWidth: true,
        },
    };
    // Line chart configuration Ends

    // Variables
    organizationId;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.roleGuardService.token
        })
    };
    totalTimeSpentSum = 0;
    averageTimeSpent = [];
    DashboardItems = [];
    registeredUsers = 0;
    unregisteredUsers = 0;
    activeUsers = [];
    averageTimeSpentOnItems = [];
    loaded = false;
    modulesFinished = [];
    modulesStarted = [];
    mostViewedScreens: SafeResourceUrl;
    weeklyUniqueUsers: SafeResourceUrl;
    popularActivities: SafeResourceUrl;
    // end Variables

    data = {
        labels: ['Bananas', 'Apples', 'Grapes'],
        series: [20, 15, 40]
    };

    options = {
        labelInterpolationFnc: function (value) {
            return value[0]
        }
    };

    responsiveOptions = [
        ['screen and (min-width: 640px)', {
            chartPadding: 30,
            labelOffset: 100,
            labelDirection: 'explode',
            labelInterpolationFnc: function (value) {
                return value;
            }
        }],
        ['screen and (min-width: 1024px)', {
            labelOffset: 80,
            chartPadding: 20
        }]
    ];

    pie;
    optionsHigh;
    courseStarted = [];
    courseFinished = [];
    videosWatched = [];

    constructor(private http: HttpClient, private previewService: PreviewService, private roleGuardService: RoleGuardService, private router: Router,
        private campaignGroups: CampaignGroupResourceService, private sanitizer: DomSanitizer, private supportGroups: SupportGroupResourceService) {
        if (!localStorage.getItem('token')) {
            this.router.navigateByUrl('/login');
        }
    }

    ngAfterViewChecked() {
        let topcards = $('.card-block.pt-2');

        let newHeight = 0;
        if (Number($('#card1').height()) > newHeight) {
            newHeight = $('#card1').height();
        }
        if (Number($('#card2').height()) > newHeight) {
            newHeight = $('#card2').height()
        }
        if (Number($('#card3').height()) > newHeight) {
            newHeight = $('#card3').height()
        }
        if (Number($('#card4').height()) > newHeight) {
            newHeight = $('#card4').height()
        }
        $('#card1').height(newHeight);
        $('#card2').height(newHeight)
        $('#card3').height(newHeight)
        $('#card4').height(newHeight)
    }
    resize() {

        $('#card1').height('auto');
        $('#card2').height('auto')
        $('#card3').height('auto')
        $('#card4').height('auto')
    }

    ngAfterViewInit() {
        // this.resize();
        $(window).resize(this.resize);
        this.optionsHigh = {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: 'Browser market shares at a specific website, 2014'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Browser share',
                data: [
                    ['Firefox', 45.0],
                    ['IE', 26.8],
                    {
                        name: 'Chrome',
                        y: 12.8,
                        sliced: true,
                        selected: true
                    },
                    ['Safari', 8.5],
                    ['Opera', 6.2],
                    ['Others', 0.7]
                ]
            }]
        }
        
        if(this.chartTarget != null && this.chartTarget != undefined){
            this.chart = chart(this.chartTarget.nativeElement, this.optionsHigh);
        }
        // console.log("ICON:", $('#sidebarToggle'));
        $('#sidebarToggle').trigger('click');
        window.resizeTo(window.outerWidth, window.outerHeight);
    }


    ngOnInit() {
        //  this.pie = new Chartist.Pie('.ct-chart', this.data, this.options, this.responsiveOptions);




        let timerInterval1;
        let timerInterval2;
        let timerInterval3;
        this.loaded = false;
        this.organizationId = this.previewService.organizationId.getValue();
        this.weeklyUniqueUsers = this.sanitizer.bypassSecurityTrustResourceUrl(`https://data.productivise.io/app/kibana#/visualize/create?embed=true&type=area&indexPattern=event&_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-1y,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'orgID:` + this.organizationId + `')),uiState:(vis:(colors:(Count:%2370DBED,'Number%20of%20Users':%2370DBED))),vis:(aggs:!((id:'1',params:(customLabel:'Number%20of%20Users',field:userID),schema:metric,type:cardinality),(id:'2',params:(customLabel:'Time%20of%20the%20Day',field:date,ranges:!((from:now-4h%2Fh,to:now),(from:now-8h%2Fh,to:now-4h%2Fh),(from:now-12h%2Fh,to:now-8h%2Fh),(from:now-16h%2Fh,to:now-12h%2Fh),(from:now-20h%2Fh,to:now-16h%2Fh),(from:now-24h%2Fh,to:now-20h%2Fh))),schema:segment,type:date_range)),listeners:(),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,defaultYExtents:!f,interpolate:linear,mode:stacked,scale:linear,setYExtents:!f,shareYAxis:!t,smoothLines:!f,times:!(),yAxis:()),title:'New%20Visualization',type:area))`);
        this.mostViewedScreens = this.sanitizer.bypassSecurityTrustResourceUrl(`https://data.productivise.io/app/kibana#/visualize/create?embed=true&type=histogram&indexPattern=event&_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-1y,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'orgID:` + this.organizationId + `')),uiState:(spy:(mode:(fill:!f,name:!n)),vis:(colors:('Number%20of%20Views':%236ED0E0),legendOpen:!f)),vis:(aggs:!((id:'1',params:(customLabel:'Number%20of%20Views'),schema:metric,type:count),(id:'2',params:(customLabel:'Dashboard%20Items',exclude:(flags:!(CASE_INSENSITIVE),pattern:''),field:value1,include:(flags:!(UNIX_LINES),pattern:'(.*%5Ba-zA-Z%5D.*)'),size:10),schema:segment,type:significant_terms)),listeners:(),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,defaultYExtents:!f,mode:stacked,scale:linear,setYExtents:!f,shareYAxis:!t,times:!(),yAxis:()),title:'New%20Visualization',type:histogram))`);
        this.popularActivities = this.sanitizer.bypassSecurityTrustResourceUrl(`https://data.productivise.io/app/kibana#/visualize/create?embed=true&type=pie&indexPattern=event&_g=(filters:!(),refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-1y,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'orgID:` + this.organizationId + `')),uiState:(spy:(mode:(fill:!f,name:!n)),vis:(legendOpen:!f)),vis:(aggs:!((id:'1',params:(),schema:metric,type:count),(id:'2',params:(customLabel:'Popular%20Activities',exclude:(flags:!(MULTILINE),pattern:'(.*screen.*)%7C(.*timespent.*)%7C(readmore)'),field:value1,include:(flags:!(CASE_INSENSITIVE)),size:10),schema:segment,type:significant_terms)),listeners:(),params:(addLegend:!t,addTooltip:!t,isDonut:!f,shareYAxis:!t),title:'New%20Visualization',type:pie))`)
        // // this.getContentList();
        
        this.http.get(base + '/api/job-families/org/' + this.organizationId, this.httpOptions).toPromise().then(res => {
            let results = <any>res;
            // console.log('families:', results);
            this.departments = results.map(element => element.name);

            // for(let element of results){
            //             let put2 = true;
            //             if (this.departments.length < 1 && element.value4) {
            //                 this.departments.push(element.value4);
            //             }
            //             if (this.departments.length) {
            //                 this.departments.forEach((el, i) => {
            //                     if (el === element.value4) {
            //                         put2 = false;
            //                     }
            //                 });
            //                 if (put2 && element.value4) {
            //                     this.departments.push(element.value4)
            //                 }
            //             }
            //         }

            // console.log("DEPARTMENTS::"+ this.departments);
        });

        // this.getAllUsers();
        Promise.all([this.getDashboardItems(), this.getGroups(), this.getModuleEvents()]).then(res => {
            // console.log('all:', res)
            // console.log('before:', this.registeredUsers);

            Promise.resolve(this.getAllUsers()).then(location => {
                this.url = location;
                // console.log(location)
                // console.log("TIMESPENT :", this.averageTimeSpent);

            });
            timerInterval1 = setInterval(() => {
                if (this.registeredUsers) {
                    Promise.resolve(this.getActiveUsers()).then(resolve => {
                        // if (resolve) {
                            // console.log("ICON:", $('#sidebarToggle'));
                            $('#sidebarToggle').trigger('click');
                            $('#active').width('40%');
                            $('#inactive').width('60%');
                            window.resizeTo(window.outerWidth, window.outerHeight)
                        // }
                    });
                    this.getChatMetrics();
                    swal.close();
                    clearInterval(timerInterval1);
                    // console.log('time1')
                    // console.log('after:', this.registeredUsers);
                }
                if (this.loaded) {
                    $('#active').width('40%');
                    $('#inactive').width('60%');
                }
            }, 100);


            timerInterval2 = setInterval(() => {

                if (this.loaded) {
                    $('#active').width(this.active + '%');
                    $('#inactive').width(this.inactive + '%');
                    clearInterval(timerInterval2);
                }
            }, 100);



            timerInterval3 = setInterval(() => {

                if (this.loaded) {

                    clearInterval(timerInterval3);
                }
            }, 100);



        })
    }

    getModuleEvents() {
        this.courseStarted = [];
        this.courseFinished = [];
        this.videosWatched = [];
        this.http.get(baseAnalytics + '/api/_search/events?query=(name1%3DVideo_time_clicked)AND(orgID%3D' + this.organizationId + ')',
            this.httpOptions).subscribe(res => {
                let results = <any>res;
                results.forEach(element => {
                    let t1 = element.strValue.split(',')[0];
                    let t2 = element.strValue.split(',')[1];
                    let put = true;
                    if (this.videosWatched.length < 1 && t1 > 0 && t2 > t1 && element.value2.indexOf('&&') > -1) {
                        this.videosWatched.push({
                            category: element.name, subcategory: element.category, name: element.value2.split('&&')[1],
                            count: 1, userID: [element.userID]
                        })
                    }
                    if (this.videosWatched.length && t1 > 0 && t2 > t1 && element.value2.indexOf('&&') > -1) {
                        this.videosWatched.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2.split('&&')[1]) {
                                put = false;
                                if (el.userID.indexOf(element.userID) < 0) {
                                    el.count++;
                                    el.userID.push(element.userID);
                                }
                            }


                        });
                        if (put && element.value2.indexOf('&&') > -1) {
                            this.videosWatched.push({
                                category: element.name, subcategory: element.category, name: element.value2.split('&&')[1],
                                count: 1, userID: [element.userID]
                            })

                        }
                    }
                });

                // console.log('videosWatched:', this.videosWatched);
                this.videosWatched.sort((a, b) => {
                    return b.count - a.count;
                });
                this.videosWatched = this.videosWatched.slice(0, 5);
            }, error => {
                this.videosWatched = [];
            });


        this.http.get(baseAnalytics + '/api/_search/events?query=(name1%3DMODULE_clicked)AND(orgID%3D' + this.organizationId + ')',
            this.httpOptions).subscribe(res => {
                let results = <any>res;
                results.forEach(element => {

                    let put = true;
                    if (this.courseStarted.length < 1) {
                        this.courseStarted.push({
                            category: element.name, subcategory: element.category, name: element.value2,
                            count: 1, userID: [element.userID]
                        })
                    }
                    if (this.courseStarted.length) {
                        this.courseStarted.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2) {
                                put = false;
                                if (el.userID.indexOf(element.userID) < 0) {
                                    el.count++;
                                    el.userID.push(element.userID);
                                }
                            }


                        });
                        if (put) {
                            this.courseStarted.push({
                                category: element.name, subcategory: element.category, name: element.value2,
                                count: 1, userID: [element.userID]
                            })

                        }
                    }


                    let put2 = true;
                    if (this.modulesStarted.length < 1) {
                        this.modulesStarted.push({
                            category: element.name, subcategory: element.category, name: element.value2,
                            strValue: element.strValue, count: 1, userID: [element.userID]
                        })
                    }
                    if (this.modulesStarted.length) {
                        this.modulesStarted.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2 && el.strValue === element.strValue) {
                                put2 = false;
                                if (el.userID.indexOf(element.userID) < 0) {
                                    el.count++;
                                    el.userID.push(element.userID);
                                }
                            }


                        });
                        if (put2) {
                            this.modulesStarted.push({
                                category: element.name, subcategory: element.category, name: element.value2,
                                strValue: element.strValue, count: 1, userID: [element.userID]
                            })

                        }
                    }

                });

                // console.log('ModulesStarted:', this.courseStarted);
                this.courseStarted.sort((a, b) => {
                    return b.count - a.count;
                });
                this.courseStarted = this.courseStarted.slice(0, 5);
                this.modulesStarted.sort((a, b) => {
                    return b.count - a.count;
                });
                this.modulesStarted = this.modulesStarted.slice(0, 5);
            }, error => {
                this.courseStarted = [];
                this.modulesStarted = [];
            });


        this.http.get(baseAnalytics + '/api/_search/events?query=(name1%3Dmicrolearning_module_completed_clicked)AND(orgID%3D' + this.organizationId + ')',
            this.httpOptions).subscribe(res => {
                let results = <any>res;
                results.forEach(element => {

                    let put = true;
                    if (this.courseFinished.length < 1) {
                        this.courseFinished.push({
                            category: element.name, subcategory: element.category, name: element.value2,
                            count: 1, userID: [element.userID]
                        })
                    }
                    if (this.courseFinished.length) {
                        this.courseFinished.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2) {
                                put = false;
                                if (el.userID.indexOf(element.userID) < 0) {
                                    el.count++;
                                    el.userID.push(element.userID);
                                }
                            }


                        });
                        if (put) {
                            this.courseFinished.push({
                                category: element.name, subcategory: element.category, name: element.value2,
                                count: 1, userID: [element.userID]
                            })

                        }
                    }


                    let put2 = true;
                    if (this.modulesFinished.length < 1) {
                        this.modulesFinished.push({
                            category: element.name, subcategory: element.category, name: element.value2,
                            strValue: element.strValue, count: 1, userID: [element.userID]
                        })
                    }
                    if (this.modulesFinished.length) {
                        this.modulesFinished.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2 && el.strValue === element.strValue) {
                                put2 = false;
                                if (el.userID.indexOf(element.userID) < 0) {
                                    el.count++;
                                    el.userID.push(element.userID);
                                }
                            }


                        });
                        if (put2) {
                            this.modulesFinished.push({
                                category: element.name, subcategory: element.category, name: element.value2,
                                strValue: element.strValue, count: 1, userID: [element.userID]
                            })

                        }
                    }

                });

                // console.log('ModulesStarted:', this.courseFinished);
                this.courseFinished.sort((a, b) => {
                    return b.count - a.count;
                });
                this.courseFinished = this.courseFinished.slice(0, 5);
                this.modulesFinished.sort((a, b) => {
                    return b.count - a.count;
                });
                this.modulesFinished = this.modulesFinished.slice(0, 5);
            }, error => {
                this.courseFinished = [];
                this.modulesFinished = [];
            });


    }

    getContentList() {
        this.temp = [];
        this.rows = [];

        this.http.get(base + '/api/learners', this.httpOptions).subscribe(res => {

            let result = <any>res;
            // console.log('resil:', result);
            result = result.filter(element => {
                if (element.organizationId === this.organizationId || element.orgId === this.organizationId) {
                    if (!element['name']) {
                        // console.log('ERROR NAME:', element);
                    }
                    if (element['name'].split(' ')[1] === 'null') {
                        element.name = element.name.split(' ')[0];
                    }
                    return true;
                }
                return false;
            });
            this.temp = [...result];
            this.rows = this.temp;
            //this.temp = [...this.temp, ...this.temp2];
            // console.log('temp:', this.temp);
        });
    }

    async getGroups() {
        let campaignGroups = await this.campaignGroups.getAllCampaignFororgidUsingGET(this.organizationId).toPromise();
        this.previewService.editCampaignGroup(campaignGroups);
        let supportGroups = await this.supportGroups.getAllSupportGroupsUsingGET().subscribe(groups => {
            let sgroups = <any>groups;
            sgroups = sgroups.filter(element => {
                if (element.category === this.organizationId) {
                    return true;
                }
                return false;
            });
            this.previewService.editSupportGroup(sgroups);
            return sgroups;
        });

    }
    // get active users
    getActiveUsers() {
        let date = Date.now();
        // console.log(JSON.stringify(this.httpOptions));
        // console.log('now date:', date);
        // console.log('-1 day: ', (date - (24 * 60 * 60 * 1000 * 1000)));
        // console.log('query:', 'unixDate: [' + (date - (24 * 60 * 60 * 1000)) + ' TO ' + date + ']');
        this.http.get(baseAnalytics + '/api/events', this.httpOptions).subscribe(res => {
            let result = <any>res;
            console.log('users1:', result);
            result.forEach(element => {
                /*
                                let put2 = true;
                                if (this.departments.length < 1 && element.value4) {
                                    this.departments.push(element.value4);
                                }
                                if (this.departments.length) {
                                    this.departments.forEach((el, i) => {
                                        if (el === element.value4) {
                                            put2 = false;
                                        }
                                    });
                                    if (put2 && element.value4) {
                                        this.departments.push(element.value4)
                                    }
                                }
                */
                let put = true;
                if(element.extraData.orgId === this.organizationId)
                    this.activeUsers.push(element.userID);
                if (this.countUsers.length < 1) {
                    this.countUsers.push({ number: element.userID, count: 1 })
                }
                if (this.countUsers.length) {
                    this.countUsers.forEach((el, i) => {
                        if (el.number === element.userID) {
                            el.count++;
                            put = false;
                        }
                    });
                    if (put) {
                        this.countUsers.push({ number: element.userID, count: 1 })
                    }
                }
            });

            // console.log("DEpartments:'", this.departments);

            this.activeUsers = this.activeUsers.filter((e, i) => {

                if (e && this.activeUsers.indexOf(e) === i) {
                    return true;
                }
                this.highActivityUsers.push(e);
                return false;
            });
            this.countUsers.sort((a, b) => {
                return b.count - a.count;
            });
            // console.log('Mid active:', this.activeUsers);
            // console.log('Mid activeHigh:', this.highActivityUsers);
            // console.log('Count activeHigh:', this.countUsers);
            let length = (this.countUsers.length > 5) ? 5 : this.countUsers.length;
            for (let j = 0; j < length; j++) {
                this.http.get(base + '/api/users/' + this.countUsers[j].number, this.httpOptions).subscribe(s => {
                    let result = <any>s;
                    // console.log('s::', result);
                    this.http.get(base + '/api/learners/' + result.learnerId, this.httpOptions).subscribe(l => {
                        let res = <any>l;
                        // console.log('l::', res);
                        if (res.name.split(' ')[1] === 'null') {
                            res.name = res.name.split(' ')[0];
                        }
                        this.topusers[j] = res;
                    })
                })
            }

            // console.log('Topusers users:', this.topusers);

            // console.log('JHighest users:', this.countUsers);

            let donutDashboard = { series: [] };
            let serie = [];
            this.active = Math.floor(this.activeUsers.length * 100 / (this.registeredUsers + this.unregisteredUsers));
            this.inactive = Math.floor((this.registeredUsers + this.unregisteredUsers - this.activeUsers.length) * 100 / (this.registeredUsers + this.unregisteredUsers));
            //   this.donutDashboard.series[0].value = this.highActivityUsers.length * 100 / (this.registeredUsers + this.unregisteredUsers);
            serie.push({
                "name": "Active Users",
                "className": "ct-progress",
                "value": this.active
            });

            serie.push({
                "name": "Not Active",
                "className": "ct-outstanding",
                "value": this.inactive
            });

            donutDashboard.series = <any>serie;
            this.DonutChart.data = donutDashboard;
            this.donutDashboard = <any>donutDashboard;
            this.loadChart = true;
            // console.log('active: ', this.activeUsers);
            return this.loaded = true;
            // return serie;

        }, error => {


        });

    }



    // end get active users

    // Total Users
    getAllUsers() {

        // console.log("inside getting users");
        this.http.get(base + '/api/users/filterby/' + this.previewService.organizationId.getValue(), this.httpOptions).subscribe(res => {
            let result = <any>res;
            let learners = [];
            // console.log('users:', result);
            result.forEach(element => {
                if (element.activated === true && element.orgId === this.previewService.organizationId.getValue()) {
                    this.registeredUsers++;
                }
                if (element.activated === false && element.orgId === this.previewService.organizationId.getValue()) {
                    this.unregisteredUsers++;
                }


                if (!element['learner']['name']) {
                    // console.log('ERROR NAME:', element);
                }
                if (element['learner']['name'].split(' ')[1] === 'null') {
                    element['learner']['name'] = element['learner']['name'].split(' ')[0];
                }



                learners.push(element['learner']);
            });
            // console.log('registered:', this.registeredUsers);

            this.temp = [...learners];

            this.rows = this.temp;
            // console.log('rows:', this.rows);
            this.loaded = true;
        });

    }

    // end Total Users


    // Get Dashboard Items

    getDashboardItems() {
        return Promise.resolve(this.getContentMetrics2()).then(e => {


            // console.log("TIMESPENT :", this.averageTimeSpent);
        });

    }
    // end Dashboard Items



    // Analytics for top cards

    getChatMetrics() {
        let highGroups = [];
        this.http.get(base + '/api/chat-histories/filterby/org/' + this.organizationId, this.httpOptions).subscribe(e => {
            let chatEvents = <any>e;
            highGroups.push(e[0].group);
            chatEvents.forEach(element => {
                if (element.subCategory !== 'broadcast') {
                    let put = true;
                    if (this.countGroups.length < 1) {
                        this.countGroups.push({ number: element.group, count: 1, name: element.to })
                    }
                    if (this.countGroups.length) {
                        this.countGroups.forEach((el, i) => {
                            if (el.number === element.group) {
                                el.count++;
                                put = false;
                            }
                        });
                        if (put) {
                            this.countGroups.push({ number: element.group, count: 1, name: element.to })
                        }
                    }
                }
            });

            this.countGroups.sort((a, b) => {
                return b.count - a.count;
            })
            let length = (this.countGroups.length > 5) ? 5 : this.countGroups.length;
            for (let i = 0; i < length; i++) {
                this.http.get(base + '/api/support-groups/' + this.countGroups[i].number, this.httpOptions).subscribe(res => {
                    let group = <any>res;
                    this.topGroups[i] = group;
                }, err => {
                    // console.log('ERR::', err);
                    this.http.get(base + '/api/support-groups/' + this.countGroups[i + 1].number, this.httpOptions).subscribe(res => {
                        let group = <any>res;
                        this.topGroups[i] = group;
                    })
                    i++;
                    length++;
                })

            }

            // console.log('Active Groups:', this.countGroups);
        })
    }


    getItemTimeSpent(name, event) {

        this.http.post(proxyApi + '/esearch', {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match": {
                                "orgID": this.previewService.organizationId.getValue()
                            }
                        },
                        {
                            "match": {
                                "name": event
                            }

                        },
                        {
                            "match": {
                                "value1": name
                            }

                        }
                    ]
                }
            },
            "aggs": {
                "TIMESPENT": {
                    "scripted_metric": {
                        "init_script": "_agg['transactions'] = []",
                        "map_script": "_agg.transactions.add(Long.valueOf(doc['strValue'].value) - Long.valueOf(doc['category'].value))",
                        "combine_script": "profit = 0; for (t in _agg.transactions) { profit = Long.valueOf(profit) + Long.valueOf(t) }; return profit",
                        "reduce_script": "profit = 0; for (a in _aggs) { profit += a };  return profit;"

                    }
                }
            }
        }, this.httpOptions).toPromise().then(res => {
            let result = <any>res;
            // console.log('result::', name, result);
            result = result.data.aggregations['TIMESPENT'].value;
            result = result ? (result / (60000)) : 0;
            result = result ? Math.round((result + 0.00001) * 100) / 100 : 0;
            result = Math.round(result);
            this.averageTimeSpent.push({ screen: name, time: result })
            this.averageTimeSpent.sort((a, b) => b.time - a.time);
            this.averageTimeSpent = this.averageTimeSpent.slice(0, 5);
        })
    }

    getContentMetrics2() {
        this.http.get(base + '/api/app-configs/org/' + this.previewService.organizationId.getValue(), this.httpOptions)
            .subscribe(resp => {
                let result = <any>resp;
                // console.log('DashboadItems:', result);
                for (let index = 0; index < result.length; index++) {
                    if ((this.previewService.organizationId.getValue() === result[index].organizationId) && (result[index]['category'] === 'DASHBOARD')) {
                        this.DashboardItems.push(result[index]['value']);
                        if (result[index]['name'] === 'Blogs') {
                            this.BlogItems.push(result[index]['value']);
                            this.getItemTimeSpent(result[index]['value'], 'BlogsTimeSpent')
                        }
                        if (result[index]['name'] === 'MicroLearning') {
                            this.MicroLearningItems.push(result[index]['value']);
                            this.getItemTimeSpent(result[index]['value'], 'MicroLearningTimeSpent')
                        }
                        if (result[index]['name'] === 'Feedback') {
                            this.FeedbackItems.push(result[index]['value']);
                            this.getItemTimeSpent(result[index]['value'], 'FeedbackTimeSpent')
                        }
                        if (result[index]['name'] === 'Operations') {
                            this.OperationItems.push(result[index]['value']);
                            this.getItemTimeSpent(result[index]['value'], 'OperationsTimeSpent')
                        }
                        if (result[index]['name'] === 'Industry') {
                            this.IndustryItems.push(result[index]['value']);
                            this.getItemTimeSpent(result[index]['value'], 'IndustryTimeSpent')
                        }

                    }


                }
                /*
                
                GET event/_search
                {
                  "query": {
                    "bool": {
                      "must": [
                        {"match": {
                          "orgID": "207753"
                        }
                
                
                        },
                        {
                          "match": {
                            "name": "BlogsTimeSpent"
                          }
                        },{
                          "match": {
                            "value1": "News Updates"
                          }
                        }
                      ]
                    }
                  },
                  "size": 20,
                  "aggs": {
                        "TIMESPENT": {
                          "scripted_metric": {
                             "init_script" : "_agg['transactions'] = []",
                                "map_script" : "_agg.transactions.add(Long.valueOf(doc['strValue'].value) - Long.valueOf(doc['category'].value))",
                                "combine_script" : "profit = 0; for (t in _agg.transactions) { profit = Long.valueOf(profit) + Long.valueOf(t) }; return profit",
                                "reduce_script" : "profit = 0; for (a in _aggs) { profit += a }; return profit;"
                
                          }
                        }
                      }
                }
                
                
                */
                





                // console.log('items:', this.DashboardItems);
                this.averageTimeSpent = [];
                // console.log('AverageArr:', this.averageTimeSpent);
                this.http.get(baseAnalytics + '/api/_search/events?query=(name%3DAppTimeSpent)AND(orgID%3D' + this.previewService.organizationId.getValue() + ')',
                    this.httpOptions).subscribe(res => {

                        let results: any = res;
                        // console.log('timespentt:', results);

                        let TotalTimeSpent = [];
                        let TotalTimeSpentBy = [];
                        for (let index = 0; index < results.length; index++) {
                            if (TotalTimeSpentBy.indexOf(results[index]['userID']) < 0 && String(TotalTimeSpentBy.indexOf(results[index]['userID'])).length > 0
                                && results[index]['strValue'] > 0 && results[index]['category'] > 0) {
                                TotalTimeSpentBy.push(results[index]['userID']);
                                TotalTimeSpent.push(results[index]['strValue'] - results[index]['category']);
                                // console.log('strvalue category', results[index]['strValue'], results[index]['category']);
                            }
                            else {
                                if (results[index]['strValue'] > 0 && results[index]['category'] > 0) {
                                    TotalTimeSpent[TotalTimeSpentBy.indexOf(results[index]['userID'])] += (results[index]['strValue'] - results[index]['category']);
                                }
                            }
                        }
                        // console.log('Total Time Spent By count:', TotalTimeSpentBy);
                        // console.log('Total Time Spent count:', TotalTimeSpent);
                        this.totalTimeSpentSum = TotalTimeSpent.reduce((sum, e) => sum + e);
                        this.totalTimeSpentSum /= 60000;
                        this.totalTimeSpentSum = Math.round((this.totalTimeSpentSum + 0.00001) * 100) / 100
                        // console.log('totalTimeSpentSum:', this.totalTimeSpentSum);
                        this.totalTimeSpentSum = Math.round(this.totalTimeSpentSum);
                        this.totalTimeSpentHourString = Math.round(this.totalTimeSpentSum / 60) + ' hours ';
                        this.totalTimeSpentMinuteString = (this.totalTimeSpentSum % 60) + ' minutes';
                        this.averageTimeSpent.push({ screen: '', time: 0 });

                        this.averageTimeSpent[0]['screen'] = 'Total Time Spent on App';
                        for (let j = 0; j < TotalTimeSpent.length; j++) {
                            this.averageTimeSpent[0]['time'] += TotalTimeSpent[j];
                            // console.log('TimeSpent Ini:', this.averageTimeSpent[0]);
                            // console.log('TimeSpent After Sum:', TotalTimeSpent[j]);
                        }
                        this.averageTimeSpent[0]['time'] = this.averageTimeSpent[0]['time'] > 0 ? (this.averageTimeSpent[0]['time'] / (60000)) : 0;
                        this.averageTimeSpent[0]['time'] = Math.round((this.averageTimeSpent[0]['time'] + 0.00001) * 100) / 100;
                        this.averageTimeSpent[0]['time'] = Math.round(this.averageTimeSpent[0]['time']);
                        // console.log('averageTimeSpent:', this.averageTimeSpent[0]);
                        this.averageTimeSpent = this.averageTimeSpent.slice(0, 5);
                    }, error => {
                        this.averageTimeSpent[0] = { screen: 'totalTimeSpent', time: 0 };

                    });
                
                                this.http.get(baseAnalytics + '/api/_search/events?query=(name%3D*TimeSpent)AND(orgID%3D' + this.previewService.organizationId.getValue() + ')',
                                    this.httpOptions).subscribe(res => {
                                        let results = <any>res;
                                        // console.log('timespentBLOGSONLY:', results);
                
                                        for (let i = 0; i < this.DashboardItems.length; i++) {
                                            let newsTimeSpent = [];
                                            let newsTimeSpentBy = [];
                                            let TimeSpent = [];
                                            let TimeSpentBy = [];
                                            this.averageTimeSpent.push({ screen: this.DashboardItems[i], time: 0 });
                                            // console.log('AVERGA:', this.averageTimeSpent);
                                            for (let index = 0; index < results.length; index++) {
                
                                                if ((TimeSpentBy.indexOf(results[index]['userID']) < 0) && String(TimeSpentBy.indexOf(results[index]['userID'])).length > 0
                                                    && results[index]['value1'] === this.DashboardItems[i]) {
                                                    TimeSpentBy.push(results[index]['userID']);
                                                    TimeSpent.push(results[index]['strValue'] - results[index]['category']);
                                                }
                                                else if ((TimeSpentBy.indexOf(results[index]['userID']) > 0) && String(TimeSpentBy.indexOf(results[index]['userID'])).length > 0
                                                    && results[index]['value1'] === this.DashboardItems[i]) {
                                                    TimeSpent[TimeSpentBy.indexOf(results[index]['userID'])] += (results[index]['strValue'] - results[index]['category']);
                                                }
                                                if (this.DashboardItems[i] === 'Updates') {
                                                    newsTimeSpent = TimeSpent;
                                                    newsTimeSpentBy = TimeSpentBy;
                                                    this.previewService.editNewsTimeSpent(newsTimeSpent);
                                                    this.previewService.editNewsTimeSpentBy(newsTimeSpentBy);
                                                    // console.log('NEWS::', newsTimeSpent, newsTimeSpentBy)
                                                }
                                            }
                
                                            // console.log(this.DashboardItems[i] + ' Time Spent By count:', TimeSpentBy);
                                            // console.log(this.DashboardItems[i] + 'Diary Time Spent count:', TimeSpent);
                
                                            for (let j = 0; j < TimeSpent.length; j++) {
                                                this.averageTimeSpent[i]['time'] += TimeSpent[j];
                                            }
                                            this.averageTimeSpent[i]['time'] = this.averageTimeSpent[i]['time'] ? (this.averageTimeSpent[i]['time'] / (60000 * TimeSpent.length)) : 0;
                                            this.averageTimeSpent[i]['time'] = this.averageTimeSpent[i]['time'] ? Math.round((this.averageTimeSpent[i]['time'] + 0.00001) * 100) / 100 : 0;
                                            this.averageTimeSpent[i]['time'] = Math.round(this.averageTimeSpent[i]['time']);
                                            // console.log('averageTimeSpentD2:', this.averageTimeSpent[i]);
                                        }
                                        // console.log('averageTimeSpentTotal:', this.averageTimeSpent);
                                        for (let k = 0; k < this.averageTimeSpent.length; k++) {
                                            if (this.averageTimeSpent[k].time !== null) {
                                                // console.log('K::', this.averageTimeSpent[k].screen)
                                                this.averageTimeSpentOnItems.push(JSON.parse(JSON.stringify(this.averageTimeSpent[k])));
                                                // console.log('L::', this.averageTimeSpentOnItems);
                
                                            }
                                        }
                                        this.averageTimeSpentOnItems.sort((a, b) => b.time - a.time);
                                        this.averageTimeSpentOnItems = this.averageTimeSpentOnItems.slice(0, 5);
                
                                        return;
                                    }, error => {
                                        this.averageTimeSpent[1] = { screen: this.DashboardItems[1], time: 0 };
                                    });
                                    
            });
    }
    
        getContentMetrics() {
    
            this.http.get(baseAnalytics + '/api/_search/events?query=(name%3DAppTimeSpent)AND(orgID%3D' + this.organizationId + ')',
                this.httpOptions).subscribe(res => {
    
                    let results: any = res;
                    // console.log('timespentt:', results);
    
                    let TotalTimeSpent = [];
                    let TotalTimeSpentBy = [];
                    for (let index = 0; index < results.length; index++) {
                        if (TotalTimeSpentBy.indexOf(results[index]['userID']) < 0 && String(TotalTimeSpentBy.indexOf(results[index]['userID'])).length > 0) {
                            TotalTimeSpentBy.push(results[index]['userID']);
                            TotalTimeSpent.push(results[index]['strValue'] - results[index]['category']);
                        }
                        else {
                            TotalTimeSpent[TotalTimeSpentBy.indexOf(results[index]['userID'])] += (results[index]['strValue'] - results[index]['category']);
                        }
                    }
                    // console.log('Total Time Spent By count:', TotalTimeSpentBy);
                    // console.log('Total Time Spent count:', TotalTimeSpent);
                    this.totalTimeSpentSum = TotalTimeSpent.reduce((sum, e) => sum + e);
                    this.totalTimeSpentSum /= 60000;
                    this.totalTimeSpentSum = Math.round((this.totalTimeSpentSum + 0.00001) * 100) / 100
                    // console.log('totalTimeSpentSum:', this.totalTimeSpentSum);
                    this.totalTimeSpentSum = Math.round(this.totalTimeSpentSum);
                    this.averageTimeSpent[0] = { screen: '', time: 0 };
    
                    this.averageTimeSpent[0].screen = 'Total Time Spent on App';
                    for (let j = 0; j < TotalTimeSpent.length; j++) {
                        this.averageTimeSpent[0].time += TotalTimeSpent[j];
                    }
                    this.averageTimeSpent[0].time = this.averageTimeSpent[0].time ? (this.averageTimeSpent[0].time / (60000)) : 0;
                    this.averageTimeSpent[0].time = Math.round((this.averageTimeSpent[0].time + 0.00001) * 100) / 100;
                    this.averageTimeSpent[0].time = Math.round(this.averageTimeSpent[0].time);
                    // console.log('averageTimeSpent:', this.averageTimeSpent);
                }, error => {
                    this.averageTimeSpent[0] = { screen: 'totalTimeSpent', time: 0 };
    
                });
            for (let index = 0; index < this.DashboardItems.length; index++) {
                this.http.get(baseAnalytics + '/api/_search/events?query=(name%3ABlogsTimeSpent)AND(value1%3D' + this.DashboardItems[index] + ')AND(orgID%3D' + this.organizationId + ')',
                    this.httpOptions).subscribe(res => {
                        let results = <any>res;
                        // console.log('timespentBLOGS:', results);
                        let TimeSpent = [];
                        let TimeSpentBy = [];
                        for (let index = 0; index < results.length; index++) {
                            if ((TimeSpentBy.indexOf(results[index]['userID']) < 0) && String(TimeSpentBy.indexOf(results[index]['userID'])).length > 0) {
                                TimeSpentBy.push(results[index]['userID']);
                                TimeSpent.push(results[index]['strValue'] - results[index]['category']);
                            }
                            else {
                                TimeSpent[TimeSpentBy.indexOf(results[index]['userID'])] += (results[index]['strValue'] - results[index]['category']);
                            }
                        }
    
                        // console.log(this.DashboardItems[index] + ' Time Spent By count:', TimeSpentBy);
                        // console.log(this.DashboardItems[index] + 'Diary Time Spent count:', TimeSpent);
                        this.averageTimeSpent[index + 1] = { screen: '', time: 0 };
                        this.averageTimeSpent[index + 1].screen = this.DashboardItems[index];
                        for (let j = 0; j < TimeSpent.length; j++) {
                            this.averageTimeSpent[index + 1].time += TimeSpent[j];
                        }
                        this.averageTimeSpent[index + 1].time = this.averageTimeSpent[index + 1].time ? (this.averageTimeSpent[index + 1].time / (60000 * TimeSpent.length)) : 0;
                        this.averageTimeSpent[index + 1].time = this.averageTimeSpent[index + 1].time ? Math.round((this.averageTimeSpent[index + 1].time + 0.00001) * 100) / 100 : 0;
                        this.averageTimeSpent[index + 1].time = Math.round(this.averageTimeSpent[index + 1].time);
                        // console.log('averageTimeSpent:', this.averageTimeSpent);
    
                    }, error => {
                        this.averageTimeSpent[index + 1] = { screen: this.DashboardItems[index], time: 0 };
                    });
    
    
                this.http.get(baseAnalytics + '/api/_search/events?query=(name%3DIndustryTimeSpent)AND(value1%3D' + this.DashboardItems[index] + ')AND(orgID%3D' + this.organizationId + ')',
                    this.httpOptions).subscribe(res => {
                        let results = <any>res;
                        // console.log('timespentBLOGS:', results);
                        let TimeSpent = [];
                        let TimeSpentBy = [];
                        for (let index = 0; index < results.length; index++) {
                            if ((TimeSpentBy.indexOf(results[index]['userID']) < 0) && String(TimeSpentBy.indexOf(results[index]['userID'])).length > 0) {
                                TimeSpentBy.push(results[index]['userID']);
                                TimeSpent.push(results[index]['strValue'] - results[index]['category']);
                            }
                            else {
                                TimeSpent[TimeSpentBy.indexOf(results[index]['userID'])] += (results[index]['strValue'] - results[index]['category']);
                            }
                        }
    
                        // console.log(this.DashboardItems[index] + ' Time Spent By count:', TimeSpentBy);
                        // console.log(this.DashboardItems[index] + 'Diary Time Spent count:', TimeSpent);
                        this.averageTimeSpent[index + 1] = { screen: '', time: 0 };
    
                        this.averageTimeSpent[index + 1].screen = this.DashboardItems[index];
                        for (let j = 0; j < TimeSpent.length; j++) {
                            this.averageTimeSpent[index + 1].time += TimeSpent[j];
                        }
                        this.averageTimeSpent[index + 1].time = this.averageTimeSpent[index + 1].time ? (this.averageTimeSpent[index + 1].time / (60000 * TimeSpent.length)) : 0;
                        this.averageTimeSpent[index + 1].time = this.averageTimeSpent[index + 1].time ? Math.round((this.averageTimeSpent[index + 1].time + 0.00001) * 100) / 100 : 0;
                        this.averageTimeSpent[index + 1].time = Math.round(this.averageTimeSpent[index + 1].time);
    
                        // console.log('averageTimeSpent:', this.averageTimeSpent);
    
                    }, error => {
                        this.averageTimeSpent[index + 1] = { screen: this.DashboardItems[index], time: 0 };
                    });
    
                this.http.get(baseAnalytics + '/api/_search/events?query=(name%3DMicrolearningTimeSpent)AND(value1%3D' + this.DashboardItems[index] + ')AND(orgID%3D' + this.organizationId + ')',
                    this.httpOptions).subscribe(res => {
                        let results = <any>res;
                        // console.log('timespentBLOGS:', results);
                        let TimeSpent = [];
                        let TimeSpentBy = [];
                        for (let index = 0; index < results.length; index++) {
                            if ((TimeSpentBy.indexOf(results[index]['userID']) < 0) && String(TimeSpentBy.indexOf(results[index]['userID'])).length > 0) {
                                TimeSpentBy.push(results[index]['userID']);
                                TimeSpent.push(results[index]['strValue'] - results[index]['category']);
                            }
                            else {
                                TimeSpent[TimeSpentBy.indexOf(results[index]['userID'])] += (results[index]['strValue'] - results[index]['category']);
                            }
                        }
    
                        // console.log(this.DashboardItems[index] + ' Time Spent By count:', TimeSpentBy);
                        // console.log(this.DashboardItems[index] + 'Diary Time Spent count:', TimeSpent);
                        this.averageTimeSpent[index + 1] = { screen: '', time: 0 };
    
                        this.averageTimeSpent[index + 1].screen = this.DashboardItems[index];
                        for (let j = 0; j < TimeSpent.length; j++) {
                            this.averageTimeSpent[index + 1].time += TimeSpent[j];
                        }
                        this.averageTimeSpent[index + 1].time = this.averageTimeSpent[index + 1].time ? (this.averageTimeSpent[index + 1].time / (60000 * TimeSpent.length)) : 0;
                        this.averageTimeSpent[index + 1].time = this.averageTimeSpent[index + 1].time ? Math.round((this.averageTimeSpent[index + 1].time + 0.00001) * 100) / 100 : 0;
                        this.averageTimeSpent[index + 1].time = Math.round(this.averageTimeSpent[index + 1].time);
    
                        // console.log('averageTimeSpent:', this.averageTimeSpent);
    
                    }, error => {
                        this.averageTimeSpent[index + 1] = { screen: this.DashboardItems[index], time: 0 };
                    });
                this.http.get(baseAnalytics + '/api/_search/events?query=(name%3DFeedbackTimeSpent)AND(value1%3D' + this.DashboardItems[index] + ')AND(orgID%3D' + this.organizationId + ')',
                    this.httpOptions).subscribe(res => {
                        let results = <any>res;
                        // console.log('timespentBLOGS:', results);
                        let TimeSpent = [];
                        let TimeSpentBy = [];
                        for (let index = 0; index < results.length; index++) {
                            if ((TimeSpentBy.indexOf(results[index]['userID']) < 0) && String(TimeSpentBy.indexOf(results[index]['userID'])).length > 0) {
                                TimeSpentBy.push(results[index]['userID']);
                                TimeSpent.push(results[index]['strValue'] - results[index]['category']);
                            }
                            else {
                                TimeSpent[TimeSpentBy.indexOf(results[index]['userID'])] += (results[index]['strValue'] - results[index]['category']);
                            }
                        }
    
                        // console.log(this.DashboardItems[index] + ' Time Spent By count:', TimeSpentBy);
                        // console.log(this.DashboardItems[index] + 'Diary Time Spent count:', TimeSpent);
                        this.averageTimeSpent[index + 1] = { screen: '', time: 0 };
    
                        this.averageTimeSpent[index + 1].screen = this.DashboardItems[index];
                        for (let j = 0; j < TimeSpent.length; j++) {
                            this.averageTimeSpent[index + 1].time += TimeSpent[j];
                        }
                        this.averageTimeSpent[index + 1].time = this.averageTimeSpent[index + 1].time ? (this.averageTimeSpent[index + 1].time / (60000 * TimeSpent.length)) : 0;
                        this.averageTimeSpent[index + 1].time = this.averageTimeSpent[index + 1].time ? Math.round((this.averageTimeSpent[index + 1].time + 0.00001) * 100) / 100 : 0;
                        this.averageTimeSpent[index + 1].time = Math.round(this.averageTimeSpent[index + 1].time);
    
                        // console.log('averageTimeSpent:', this.averageTimeSpent);
    
                    }, error => {
                        this.averageTimeSpent[index + 1] = { screen: this.DashboardItems[index], time: 0 };
                    });
            }
            for (let k = 0; k < this.averageTimeSpent.length; k++) {
                this.averageTimeSpent[k].time = Math.round((this.averageTimeSpent[k]));
            }
    
            return;
        }
        

    selectDepartment(e) {
        this.department = e.target.value;
        // console.log("DEpaerment:", this.department);
        // console.log("QUESRY:", `(orgID%3D` + this.organizationId + `)AND(value4%3D` + this.department + `)`)
        if (this.department !== "All Departments") {
            this.weeklyUniqueUsers = this.sanitizer.bypassSecurityTrustResourceUrl(`https://data.productivise.io/app/kibana#/visualize/create?embed=true&type=area&indexPattern=event&_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-1y,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'(orgID:` + this.organizationId + `)AND(value4:` + this.department + `)')),uiState:(vis:(colors:(Count:%2370DBED,'Number%20of%20Users':%2370DBED))),vis:(aggs:!((id:'1',params:(customLabel:'Number%20of%20Users',field:userID),schema:metric,type:cardinality),(id:'2',params:(customLabel:'Time%20of%20the%20Day',field:date,ranges:!((from:now-4h%2Fh,to:now),(from:now-8h%2Fh,to:now-4h%2Fh),(from:now-12h%2Fh,to:now-8h%2Fh),(from:now-16h%2Fh,to:now-12h%2Fh),(from:now-20h%2Fh,to:now-16h%2Fh),(from:now-24h%2Fh,to:now-20h%2Fh))),schema:segment,type:date_range)),listeners:(),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,defaultYExtents:!f,interpolate:linear,mode:stacked,scale:linear,setYExtents:!f,shareYAxis:!t,smoothLines:!f,times:!(),yAxis:()),title:'New%20Visualization',type:area))`);
            this.mostViewedScreens = this.sanitizer.bypassSecurityTrustResourceUrl(`https://data.productivise.io/app/kibana#/visualize/create?embed=true&type=histogram&indexPattern=event&_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-1y,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'(orgID:` + this.organizationId + `)AND(value4:` + this.department + `)')),uiState:(spy:(mode:(fill:!f,name:!n)),vis:(colors:('Number%20of%20Views':%236ED0E0),legendOpen:!f)),vis:(aggs:!((id:'1',params:(customLabel:'Number%20of%20Views'),schema:metric,type:count),(id:'2',params:(customLabel:'Dashboard%20Items',exclude:(flags:!(CASE_INSENSITIVE),pattern:''),field:value1,include:(flags:!(UNIX_LINES),pattern:'(.*%5Ba-zA-Z%5D.*)'),size:10),schema:segment,type:significant_terms)),listeners:(),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,defaultYExtents:!f,mode:stacked,scale:linear,setYExtents:!f,shareYAxis:!t,times:!(),yAxis:()),title:'New%20Visualization',type:histogram))`);
            this.popularActivities = this.sanitizer.bypassSecurityTrustResourceUrl(`https://data.productivise.io/app/kibana#/visualize/create?embed=true&type=pie&indexPattern=event&_g=(filters:!(),refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-1y,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'(orgID:` + this.organizationId + `)AND(value4:` + this.department + `)')),uiState:(spy:(mode:(fill:!f,name:!n)),vis:(legendOpen:!f)),vis:(aggs:!((id:'1',params:(),schema:metric,type:count),(id:'2',params:(customLabel:'Popular%20Activities',exclude:(flags:!(MULTILINE),pattern:'(.*screen.*)%7C(.*timespent.*)%7C(readmore)'),field:value1,include:(flags:!(CASE_INSENSITIVE)),size:10),schema:segment,type:significant_terms)),listeners:(),params:(addLegend:!t,addTooltip:!t,isDonut:!f,shareYAxis:!t),title:'New%20Visualization',type:pie))`)

        }
        else {
            this.weeklyUniqueUsers = this.sanitizer.bypassSecurityTrustResourceUrl(`https://data.productivise.io/app/kibana#/visualize/create?embed=true&type=area&indexPattern=event&_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-1y,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'orgID:` + this.organizationId + `')),uiState:(vis:(colors:(Count:%2370DBED,'Number%20of%20Users':%2370DBED))),vis:(aggs:!((id:'1',params:(customLabel:'Number%20of%20Users',field:userID),schema:metric,type:cardinality),(id:'2',params:(customLabel:'Time%20of%20the%20Day',field:date,ranges:!((from:now-4h%2Fh,to:now),(from:now-8h%2Fh,to:now-4h%2Fh),(from:now-12h%2Fh,to:now-8h%2Fh),(from:now-16h%2Fh,to:now-12h%2Fh),(from:now-20h%2Fh,to:now-16h%2Fh),(from:now-24h%2Fh,to:now-20h%2Fh))),schema:segment,type:date_range)),listeners:(),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,defaultYExtents:!f,interpolate:linear,mode:stacked,scale:linear,setYExtents:!f,shareYAxis:!t,smoothLines:!f,times:!(),yAxis:()),title:'New%20Visualization',type:area))`);
            this.mostViewedScreens = this.sanitizer.bypassSecurityTrustResourceUrl(`https://data.productivise.io/app/kibana#/visualize/create?embed=true&type=histogram&indexPattern=event&_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-1y,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'orgID:` + this.organizationId + `')),uiState:(spy:(mode:(fill:!f,name:!n)),vis:(colors:('Number%20of%20Views':%236ED0E0),legendOpen:!f)),vis:(aggs:!((id:'1',params:(customLabel:'Number%20of%20Views'),schema:metric,type:count),(id:'2',params:(customLabel:'Dashboard%20Items',exclude:(flags:!(CASE_INSENSITIVE),pattern:''),field:value1,include:(flags:!(UNIX_LINES),pattern:'(.*%5Ba-zA-Z%5D.*)'),size:10),schema:segment,type:significant_terms)),listeners:(),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,defaultYExtents:!f,mode:stacked,scale:linear,setYExtents:!f,shareYAxis:!t,times:!(),yAxis:()),title:'New%20Visualization',type:histogram))`);
            this.popularActivities = this.sanitizer.bypassSecurityTrustResourceUrl(`https://data.productivise.io/app/kibana#/visualize/create?embed=true&type=pie&indexPattern=event&_g=(filters:!(),refreshInterval:(display:Off,pause:!f,value:0),time:(from:now-1y,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(query_string:(analyze_wildcard:!t,query:'orgID:` + this.organizationId + `')),uiState:(spy:(mode:(fill:!f,name:!n)),vis:(legendOpen:!f)),vis:(aggs:!((id:'1',params:(),schema:metric,type:count),(id:'2',params:(customLabel:'Popular%20Activities',exclude:(flags:!(MULTILINE),pattern:'(.*screen.*)%7C(.*timespent.*)%7C(readmore)'),field:value1,include:(flags:!(CASE_INSENSITIVE)),size:10),schema:segment,type:significant_terms)),listeners:(),params:(addLegend:!t,addTooltip:!t,isDonut:!f,shareYAxis:!t),title:'New%20Visualization',type:pie))`)

        }
        // console.log('e', e);
    }

    showStats(e) {
        // console.log('usere:', e);
        this.previewService.editStatsOf(e);
        this.router.navigate(['/dashboard/dashboard2']);

    }

    // end Analytics for top cards





    // ====================================================================== Analytics ==================================================================== //


    getAnalytics() {
        let newsTimeSpent = [];
        let newsTimeSpentBy = [];
        let TimeSpentQuiz = [];
        let TimeSpentByQuiz = [];
        let TimeSpentNews = [];
        let TimeSpentByNews = [];
        let TimeSpentFeedback = [];
        let TimeSpentByFeedback = [];
        let TimeSpentIndustry = [];
        let TimeSpentByIndustry = [];
        let TimeSpentMicro = [];
        let TimeSpentByMicro = [];
        let TimeSpentBlogs = [];
        let TimeSpentByBlogs = [];
        let TotalTimeSpent = [];
        let TotalTimeSpentBy = [];
        let TimeSpentBlogsTotal = 0;
        let TimeSpentMicroTotal = 0;
        let TimeSpentQuizTotal = 0;
        let TimeSpentNewsTotal = 0;
        let TimeSpentFeedbackTotal = 0;
        let TimeSpentIndustryTotal = 0;

        let date = Date.now();
        // console.log('now date:', date);
        // console.log('-1 day: ', (date - (24 * 60 * 60 * 1000 * 1000)));
        // console.log('query:', 'unixDate: [' + (date - (24 * 60 * 60 * 1000)) + ' TO ' + date + ']');
        this.http.get(baseAnalytics + '/api/_search/events?orgID%3D' + this.organizationId, this.httpOptions).subscribe(res => {
            let result = <any>res;
            // console.log('users2:', result);
            result.forEach((element, index) => {
                
                                let put2 = true;
                                if (this.departments.length < 1 && element.value4) {
                                    this.departments.push(element.value4);
                                }
                                if (this.departments.length) {
                                    this.departments.forEach((el, i) => {
                                        if (el === element.value4) {
                                            put2 = false;
                                        }
                                    });
                                    if (put2 && element.value4) {
                                        this.departments.push(element.value4)
                                    }
                                }
                
                // ======================================================================== active users ===============================================================

                let put = true;
                // this.activeUsers.push(element.userID);
                if (this.countUsers.length < 1) {
                    this.countUsers.push({ number: element.userID, count: 1 })
                }
                if (this.countUsers.length) {
                    this.countUsers.forEach((el, i) => {
                        if (el.number === element.userID) {
                            el.count++;
                            put = false;
                        }
                    });
                    if (put) {
                        this.countUsers.push({ number: element.userID, count: 1 })
                    }
                }
                // ====================================================================== end active users =============================================================

                // ====================================================================== videos =======================================================================

                if (element.name1 === 'Video_time_clicked') {
                    let t1 = element.strValue.split(',')[0];
                    let t2 = element.strValue.split(',')[1];
                    let put = true;
                    if (this.videosWatched.length < 1 && t1 > 0 && t2 > t1 && element.value2.indexOf('&&') > -1) {
                        this.videosWatched.push({
                            category: element.name, subcategory: element.category, name: element.value2.split('&&')[1],
                            count: 1, userID: [element.userID]
                        })
                    }
                    if (this.videosWatched.length && t1 > 0 && t2 > t1 && element.value2.indexOf('&&') > -1) {
                        this.videosWatched.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2.split('&&')[1]) {
                                put = false;
                                if (el.userID.indexOf(element.userID) < 0) {
                                    el.count++;
                                    el.userID.push(element.userID);
                                }
                            }


                        });
                        if (put && element.value2.indexOf('&&') > -1) {
                            this.videosWatched.push({
                                category: element.name, subcategory: element.category, name: element.value2.split('&&')[1],
                                count: 1, userID: [element.userID]
                            })

                        }
                    }
                }
                // ===================================================================== end videos ====================================================================

                // =================================================================== Modules Clicked =================================================================

                if (element.name1 === 'MODULE_clicked') {

                    let put = true;
                    if (this.courseStarted.length < 1) {
                        this.courseStarted.push({
                            category: element.name, subcategory: element.category, name: element.value2,
                            count: 1, userID: [element.userID]
                        })
                    }
                    if (this.courseStarted.length) {
                        this.courseStarted.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2) {
                                put = false;
                                if (el.userID.indexOf(element.userID) < 0) {
                                    el.count++;
                                    el.userID.push(element.userID);
                                }
                            }


                        });
                        if (put) {
                            this.courseStarted.push({
                                category: element.name, subcategory: element.category, name: element.value2,
                                count: 1, userID: [element.userID]
                            })

                        }
                    }


                    let put2 = true;
                    if (this.modulesStarted.length < 1) {
                        this.modulesStarted.push({
                            category: element.name, subcategory: element.category, name: element.value2,
                            strValue: element.strValue, count: 1, userID: [element.userID]
                        })
                    }
                    if (this.modulesStarted.length) {
                        this.modulesStarted.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2 && el.strValue === element.strValue) {
                                put2 = false;
                                if (el.userID.indexOf(element.userID) < 0) {
                                    el.count++;
                                    el.userID.push(element.userID);
                                }
                            }


                        });
                        if (put2) {
                            this.modulesStarted.push({
                                category: element.name, subcategory: element.category, name: element.value2,
                                strValue: element.strValue, count: 1, userID: [element.userID]
                            })

                        }
                    }
                }

                // ================================================================== End Modules Clicked ==============================================================


                // ================================================================ Modules Finished ===================================================================

                if (element.name1 === 'microlearning_module_completed_clicked') {

                    let put = true;
                    if (this.courseFinished.length < 1) {
                        this.courseFinished.push({
                            category: element.name, subcategory: element.category, name: element.value2,
                            count: 1, userID: [element.userID]
                        })
                    }
                    if (this.courseFinished.length) {
                        this.courseFinished.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2) {
                                put = false;
                                if (el.userID.indexOf(element.userID) < 0) {
                                    el.count++;
                                    el.userID.push(element.userID);
                                }
                            }


                        });
                        if (put) {
                            this.courseFinished.push({
                                category: element.name, subcategory: element.category, name: element.value2,
                                count: 1, userID: [element.userID]
                            })

                        }
                    }


                    let put2 = true;
                    if (this.modulesFinished.length < 1) {
                        this.modulesFinished.push({
                            category: element.name, subcategory: element.category, name: element.value2,
                            strValue: element.strValue, count: 1, userID: [element.userID]
                        })
                    }
                    if (this.modulesFinished.length) {
                        this.modulesFinished.forEach((el, i) => {
                            if (el.category === element.name && el.subcategory === element.category && el.name === element.value2 && el.strValue === element.strValue) {
                                put2 = false;
                                if (el.userID.indexOf(element.userID) < 0) {
                                    el.count++;
                                    el.userID.push(element.userID);
                                }
                            }


                        });
                        if (put2) {
                            this.modulesFinished.push({
                                category: element.name, subcategory: element.category, name: element.value2,
                                strValue: element.strValue, count: 1, userID: [element.userID]
                            })

                        }
                    }
                }
                // ============================================================== End Modules Finished =================================================================

                // ================================================================= AppTimeSpent ======================================================================

                if (element.name === 'AppTimeSpent') {
                    if (TotalTimeSpentBy.indexOf(element['userID']) < 0 && String(TotalTimeSpentBy.indexOf(element['userID'])).length > 0) {
                        TotalTimeSpentBy.push(element['userID']);
                        TotalTimeSpent.push(element['strValue'] - element['category']);
                    }
                    else {
                        TotalTimeSpent[TotalTimeSpentBy.indexOf(element['userID'])] += (element['strValue'] - element['category']);
                    }
                }

                // =============================================================== End AppTimeSpent ====================================================================

                // ================================================================ BlogsTimeSpent =====================================================================

                if (element.name === 'BlogsTimeSpent') {

                    for (let i = 0; i < this.BlogItems.length; i++) {




                        if ((TimeSpentByBlogs.indexOf(element['userID']) < 0) && String(TimeSpentByBlogs.indexOf(element['userID'])).length > 0
                            && element['value1'] === this.BlogItems[i]) {
                            TimeSpentByBlogs.push(element['userID']);
                            TimeSpentBlogs.push(element['strValue'] - element['category']);
                        }
                        else if ((TimeSpentByBlogs.indexOf(element['userID']) > 0) && String(TimeSpentByBlogs.indexOf(element['userID'])).length > 0
                            && element['value1'] === this.BlogItems[i]) {
                            TimeSpentBlogs[TimeSpentByBlogs.indexOf(element['userID'])] += (element['strValue'] - element['category']);
                        }
                        if (this.BlogItems[i] === 'Updates') {
                            newsTimeSpent = TimeSpentBlogs;
                            newsTimeSpentBy = TimeSpentByBlogs;
                            this.previewService.editNewsTimeSpent(newsTimeSpent);
                            this.previewService.editNewsTimeSpentBy(newsTimeSpentBy);
                            // console.log('NEWS::', newsTimeSpent, newsTimeSpentBy)
                        }


                        // console.log(this.BlogItems[i] + ' Time Spent By count:', TimeSpentByBlogs);
                        // console.log(this.BlogItems[i] + 'Diary Time Spent count:', TimeSpentBlogs);

                        for (let j = 0; j < TimeSpentBlogs.length; j++) {
                            TimeSpentBlogsTotal += TimeSpentBlogs[j];
                        }
                        TimeSpentBlogsTotal = TimeSpentBlogsTotal ? (TimeSpentBlogsTotal / (60000 * TimeSpentBlogs.length)) : 0;
                        TimeSpentBlogsTotal = TimeSpentBlogsTotal ? Math.round((TimeSpentBlogsTotal + 0.00001) * 100) / 100 : 0;
                        TimeSpentBlogsTotal = Math.round(TimeSpentBlogsTotal);

                        this.averageTimeSpent.push({ screen: this.BlogItems[i], time: TimeSpentBlogsTotal });
                        // console.log('AVERGA:', this.averageTimeSpent);
                        // console.log('averageTimeSpentD2:', this.averageTimeSpent[i]);
                    }

                }

                // ============================================================== End BlogsTimeSpent ===================================================================

                // ============================================================ MicroLearningTimeSpent =================================================================

                if (element.name === 'MicrolearningTimeSpent') {

                    for (let i = 0; i < this.MicroLearningItems.length; i++) {
                        if ((TimeSpentByMicro.indexOf(element['userID']) < 0) && String(TimeSpentByMicro.indexOf(element['userID'])).length > 0
                            && element['value1'] === this.MicroLearningItems[i]) {
                            TimeSpentByMicro.push(element['userID']);
                            TimeSpentMicro.push(element['strValue'] - element['category']);
                        }
                        else if ((TimeSpentByMicro.indexOf(element['userID']) > 0) && String(TimeSpentByMicro.indexOf(element['userID'])).length > 0
                            && element['value1'] === this.MicroLearningItems[i]) {
                            TimeSpentMicro[TimeSpentByMicro.indexOf(element['userID'])] += (element['strValue'] - element['category']);
                        }
                        if (this.MicroLearningItems[i] === 'Updates') {
                            newsTimeSpent = TimeSpentMicro;
                            newsTimeSpentBy = TimeSpentByMicro;
                            this.previewService.editNewsTimeSpent(newsTimeSpent);
                            this.previewService.editNewsTimeSpentBy(newsTimeSpentBy);
                            // console.log('NEWS::', newsTimeSpent, newsTimeSpentBy)
                        }


                        // console.log(this.MicroLearningItems[i] + ' Time Spent By count:', TimeSpentByMicro);
                        // console.log(this.MicroLearningItems[i] + 'Diary Time Spent count:', TimeSpentMicro);

                        for (let j = 0; j < TimeSpentMicro.length; j++) {
                            TimeSpentMicroTotal += TimeSpentMicro[j];
                        }
                        TimeSpentMicroTotal = TimeSpentMicroTotal ? (TimeSpentMicroTotal / (60000 * TimeSpentMicro.length)) : 0;
                        TimeSpentMicroTotal = TimeSpentMicroTotal ? Math.round((TimeSpentMicroTotal + 0.00001) * 100) / 100 : 0;
                        TimeSpentMicroTotal = Math.round(TimeSpentMicroTotal);

                        this.averageTimeSpent.push({ screen: this.MicroLearningItems[i], time: 0 });
                        // console.log('AVERGA:', this.averageTimeSpent);
                        // console.log('averageTimeSpentD2:', this.averageTimeSpent[i]);
                    }

                }
                // ========================================================== End MicroLearningTimeSpent ===============================================================

                // ============================================================ IndustryTimeSpent =================================================================

                if (element.name === 'IndustryTimeSpent') {

                    for (let i = 0; i < this.IndustryItems.length; i++) {

                        this.averageTimeSpent.push({ screen: this.IndustryItems[i], time: 0 });
                        // console.log('AVERGA:', this.averageTimeSpent);


                        if ((TimeSpentByIndustry.indexOf(element['userID']) < 0) && String(TimeSpentByIndustry.indexOf(element['userID'])).length > 0
                            && element['value1'] === this.IndustryItems[i]) {
                            TimeSpentByIndustry.push(element['userID']);
                            TimeSpentIndustry.push(element['strValue'] - element['category']);
                        }
                        else if ((TimeSpentByIndustry.indexOf(element['userID']) > 0) && String(TimeSpentByIndustry.indexOf(element['userID'])).length > 0
                            && element['value1'] === this.IndustryItems[i]) {
                            TimeSpentIndustry[TimeSpentByIndustry.indexOf(element['userID'])] += (element['strValue'] - element['category']);
                        }
                        if (this.IndustryItems[i] === 'Updates') {
                            newsTimeSpent = TimeSpentIndustry;
                            newsTimeSpentBy = TimeSpentByIndustry;
                            this.previewService.editNewsTimeSpent(newsTimeSpent);
                            this.previewService.editNewsTimeSpentBy(newsTimeSpentBy);
                            // console.log('NEWS::', newsTimeSpent, newsTimeSpentBy)
                        }


                        // console.log(this.IndustryItems[i] + ' Time Spent By count:', TimeSpentByIndustry);
                        // console.log(this.IndustryItems[i] + 'Diary Time Spent count:', TimeSpentIndustry);

                        for (let j = 0; j < TimeSpentIndustry.length; j++) {
                            this.averageTimeSpent[i]['time'] += TimeSpentIndustry[j];
                        }
                        this.averageTimeSpent[i]['time'] = this.averageTimeSpent[i]['time'] ? (this.averageTimeSpent[i]['time'] / (60000 * TimeSpentIndustry.length)) : 0;
                        this.averageTimeSpent[i]['time'] = this.averageTimeSpent[i]['time'] ? Math.round((this.averageTimeSpent[i]['time'] + 0.00001) * 100) / 100 : 0;
                        this.averageTimeSpent[i]['time'] = Math.round(this.averageTimeSpent[i]['time']);
                        // console.log('averageTimeSpentD2:', this.averageTimeSpent[i]);
                    }

                }
                // ========================================================== End IndustryTimeSpent ===============================================================

                // ============================================================ FeedbackTimeSpent =================================================================

                if (element.name === 'FeedbackTimeSpent') {

                    for (let i = 0; i < this.FeedbackItems.length; i++) {

                        this.averageTimeSpent.push({ screen: this.FeedbackItems[i], time: 0 });
                        // console.log('AVERGA:', this.averageTimeSpent);


                        if ((TimeSpentByFeedback.indexOf(element['userID']) < 0) && String(TimeSpentByFeedback.indexOf(element['userID'])).length > 0
                            && element['value1'] === this.FeedbackItems[i]) {
                            TimeSpentByFeedback.push(element['userID']);
                            TimeSpentFeedback.push(element['strValue'] - element['category']);
                        }
                        else if ((TimeSpentByFeedback.indexOf(element['userID']) > 0) && String(TimeSpentByFeedback.indexOf(element['userID'])).length > 0
                            && element['value1'] === this.FeedbackItems[i]) {
                            TimeSpentFeedback[TimeSpentByFeedback.indexOf(element['userID'])] += (element['strValue'] - element['category']);
                        }
                        if (this.FeedbackItems[i] === 'Updates') {
                            newsTimeSpent = TimeSpentFeedback;
                            newsTimeSpentBy = TimeSpentByFeedback;
                            this.previewService.editNewsTimeSpent(newsTimeSpent);
                            this.previewService.editNewsTimeSpentBy(newsTimeSpentBy);
                            // console.log('NEWS::', newsTimeSpent, newsTimeSpentBy)
                        }


                        // console.log(this.FeedbackItems[i] + ' Time Spent By count:', TimeSpentByFeedback);
                        // console.log(this.FeedbackItems[i] + 'Diary Time Spent count:', TimeSpentFeedback);

                        for (let j = 0; j < TimeSpentFeedback.length; j++) {
                            this.averageTimeSpent[i]['time'] += TimeSpentFeedback[j];
                        }
                        this.averageTimeSpent[i]['time'] = this.averageTimeSpent[i]['time'] ? (this.averageTimeSpent[i]['time'] / (60000 * TimeSpentBlogs.length)) : 0;
                        this.averageTimeSpent[i]['time'] = this.averageTimeSpent[i]['time'] ? Math.round((this.averageTimeSpent[i]['time'] + 0.00001) * 100) / 100 : 0;
                        this.averageTimeSpent[i]['time'] = Math.round(this.averageTimeSpent[i]['time']);
                        // console.log('averageTimeSpentD2:', this.averageTimeSpent[i]);
                    }

                }
                // ========================================================== End FeedbackTimeSpent ===============================================================

                // ============================================================ NewsTimeSpent =================================================================

                if (element.name === 'NewsTimeSpent') {

                    for (let i = 0; i < this.NewsItems.length; i++) {

                        this.averageTimeSpent.push({ screen: this.NewsItems[i], time: 0 });
                        // console.log('AVERGA:', this.averageTimeSpent);


                        if ((TimeSpentByNews.indexOf(element['userID']) < 0) && String(TimeSpentByNews.indexOf(element['userID'])).length > 0
                            && element['value1'] === this.NewsItems[i]) {
                            TimeSpentByNews.push(element['userID']);
                            TimeSpentNews.push(element['strValue'] - element['category']);
                        }
                        else if ((TimeSpentByNews.indexOf(element['userID']) > 0) && String(TimeSpentByNews.indexOf(element['userID'])).length > 0
                            && element['value1'] === this.NewsItems[i]) {
                            TimeSpentNews[TimeSpentByNews.indexOf(element['userID'])] += (element['strValue'] - element['category']);
                        }
                        if (this.NewsItems[i] === 'Updates') {
                            newsTimeSpent = TimeSpentNews;
                            newsTimeSpentBy = TimeSpentByNews;
                            this.previewService.editNewsTimeSpent(newsTimeSpent);
                            this.previewService.editNewsTimeSpentBy(newsTimeSpentBy);
                            // console.log('NEWS::', newsTimeSpent, newsTimeSpentBy)
                        }


                        // console.log(this.NewsItems[i] + ' Time Spent By count:', TimeSpentByNews);
                        // console.log(this.NewsItems[i] + 'Diary Time Spent count:', TimeSpentNews);

                        for (let j = 0; j < TimeSpentNews.length; j++) {
                            this.averageTimeSpent[i]['time'] += TimeSpentNews[j];
                        }
                        this.averageTimeSpent[i]['time'] = this.averageTimeSpent[i]['time'] ? (this.averageTimeSpent[i]['time'] / (60000 * TimeSpentNews.length)) : 0;
                        this.averageTimeSpent[i]['time'] = this.averageTimeSpent[i]['time'] ? Math.round((this.averageTimeSpent[i]['time'] + 0.00001) * 100) / 100 : 0;
                        this.averageTimeSpent[i]['time'] = Math.round(this.averageTimeSpent[i]['time']);
                        // console.log('averageTimeSpentD2:', this.averageTimeSpent[i]);
                    }

                }
                // ========================================================== End NewsTimeSpent ===============================================================

                // ============================================================ QuizTimeSpent =================================================================

                if (element.name === 'QuizTimeSpent') {

                    for (let i = 0; i < this.QuizItems.length; i++) {

                        this.averageTimeSpent.push({ screen: this.QuizItems[i], time: 0 });
                        // console.log('AVERGA:', this.averageTimeSpent);

                        if ((TimeSpentByQuiz.indexOf(element['userID']) < 0) && String(TimeSpentByQuiz.indexOf(element['userID'])).length > 0
                            && element['value1'] === this.QuizItems[i]) {
                            TimeSpentByQuiz.push(element['userID']);
                            TimeSpentQuiz.push(element['strValue'] - element['category']);
                        }
                        else if ((TimeSpentByQuiz.indexOf(element['userID']) > 0) && String(TimeSpentByQuiz.indexOf(element['userID'])).length > 0
                            && element['value1'] === this.QuizItems[i]) {
                            TimeSpentQuiz[TimeSpentByQuiz.indexOf(element['userID'])] += (element['strValue'] - element['category']);
                        }
                        if (this.QuizItems[i] === 'Updates') {
                            newsTimeSpent = TimeSpentQuiz;
                            newsTimeSpentBy = TimeSpentByQuiz;
                            this.previewService.editNewsTimeSpent(newsTimeSpent);
                            this.previewService.editNewsTimeSpentBy(newsTimeSpentBy);
                            // console.log('NEWS::', newsTimeSpent, newsTimeSpentBy)
                        }


                        // console.log(this.QuizItems[i] + ' Time Spent By count:', TimeSpentByQuiz);
                        // console.log(this.QuizItems[i] + 'Diary Time Spent count:', TimeSpentQuiz);

                        for (let j = 0; j < TimeSpentQuiz.length; j++) {
                            this.averageTimeSpent[i]['time'] += TimeSpentQuiz[j];
                        }
                        this.averageTimeSpent[i]['time'] = this.averageTimeSpent[i]['time'] ? (this.averageTimeSpent[i]['time'] / (60000 * TimeSpentQuiz.length)) : 0;
                        this.averageTimeSpent[i]['time'] = this.averageTimeSpent[i]['time'] ? Math.round((this.averageTimeSpent[i]['time'] + 0.00001) * 100) / 100 : 0;
                        this.averageTimeSpent[i]['time'] = Math.round(this.averageTimeSpent[i]['time']);
                        // console.log('averageTimeSpentD2:', this.averageTimeSpent[i]);
                    }

                }
                // ========================================================== End QuizTimeSpent ===============================================================

            });

            this.videosWatched.sort((a, b) => {
                return b.count - a.count;
            });
            this.videosWatched = this.videosWatched.slice(0, 5);

            // console.log("DEpartments:'", this.departments);

            this.activeUsers = this.activeUsers.filter((e, i) => {

                if (e && this.activeUsers.indexOf(e) === i) {
                    return true;
                }
                this.highActivityUsers.push(e);
                return false;
            });
            this.countUsers.sort((a, b) => {
                return b.count - a.count;
            });
            // console.log('Mid active:', this.activeUsers);
            // console.log('Mid activeHigh:', this.highActivityUsers);
            // console.log('Count activeHigh:', this.countUsers);
            let length = (this.countUsers.length > 5) ? 5 : this.countUsers.length;
            for (let j = 0; j < length; j++) {
                this.http.get(base + '/api/users/' + this.countUsers[j].number, this.httpOptions).subscribe(s => {
                    let result = <any>s;
                    // console.log('s::', result);
                    this.http.get(base + '/api/learners/' + result.learnerId, this.httpOptions).subscribe(l => {
                        let res = <any>l;
                        // console.log('l::', res);
                        if (res.name.split(' ')[1] === 'null') {
                            res.name = res.name.split(' ')[0];
                        }
                        this.topusers[j] = res;
                    })
                })
            }

            // console.log('Topusers users:', this.topusers);

            // console.log('JHighest users:', this.countUsers);

            let donutDashboard = { series: [] };
            let serie = [];
            this.active = Math.floor(this.activeUsers.length * 100 / (this.registeredUsers + this.unregisteredUsers));
            this.inactive = Math.floor((this.registeredUsers + this.unregisteredUsers - this.activeUsers.length) * 100 / (this.registeredUsers + this.unregisteredUsers));
            //   this.donutDashboard.series[0].value = this.highActivityUsers.length * 100 / (this.registeredUsers + this.unregisteredUsers);
            serie.push({
                "name": "Active Users",
                "className": "ct-progress",
                "value": this.active
            });

            serie.push({
                "name": "Not Active",
                "className": "ct-outstanding",
                "value": this.inactive
            });

            donutDashboard.series = <any>serie;
            this.DonutChart.data = donutDashboard;
            this.donutDashboard = <any>donutDashboard;
            this.loadChart = true;
            // console.log('active: ', this.activeUsers);
            return this.loaded = true;
            // return serie;

        }, error => {


        });                               

    }

    // ==================================================================== End Analytics ================================================================= //

}


/* ES QUERY
GET event/_search
{
  "query": {
    "bool": {
      "must": [
        {"match": {
          "orgID": "267502"
        }


        },
        {
          "match": {
            "name": "AppTimeSpent"
          }
        }
      ]
    }
  },
  "size": 20,
  "aggs": {
    "APPTIMESPENT": {
      "significant_terms": {
        "field": "userID"
      },
      "aggs": {
        "TIMESPENT": {
          "scripted_metric": {
            "init_script" : "state.transactions = []",
                "map_script" : "state.transactions.add(doc.strValue.value - doc.category.value)",
                "combine_script" : "double profit = 0; for (t in state.transactions) { profit += t } return profit",
                "reduce_script" : "double profit = 0; for (a in states) { profit += a } return profit"

          }
        }
      }
    }
  }
}

*/