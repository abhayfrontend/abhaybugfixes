import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import { NotificationComponent } from './notification.component';
import { NotificationRoutingModule } from './notification-routing.module';
import { NotificationListComponent } from './notification-list.component';
import { CronEditorModule } from 'cron-editor/cron-editor';

@NgModule({
  imports: [
    CommonModule,
    NotificationRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    CustomFormsModule,
    MatchHeightModule,
    NgbModule,
    NgxDatatableModule,
    CronEditorModule
  ],
  declarations: [NotificationComponent, NotificationListComponent]
})
export class NotificationModule { }
