import { Component, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { NotificationResourceService } from '../sthaapak';
import swal from 'sweetalert2';
import { base } from '../shared/configs/util';
import { PreviewService } from '../shared/auth/preview.service';
import { Router } from '@angular/router';


@Component({
    selector: 'app-dt-filter',
    templateUrl: './notification-list.component.html',
    styleUrls: ['./notification-list.component.scss']
})

export class NotificationListComponent implements OnInit {
    rows = [];
    filterOption;
    temp = [];
    types = ['Info', 'Todo', 'Deadline'];

    // Table Column Titles
    columns = [
        { prop: 'name' },
        { prop: 'description' },
        { prop: 'tags' },
        { prop: 'body' },
        { prop: 'image' },
        { prop: 'sound' }



    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private notificationService: NotificationResourceService, private previewService: PreviewService, private router: Router) {
        if (!localStorage.getItem('token')) {
            this.router.navigateByUrl('/login');
        }
    }

    ngOnInit() {
        this.getNotifications();
    }

    changeFilter(e) {
        // console.log('filter:', this.temp);
        this.filterOption = e.target.value;
        this.rows = this.temp;
    }

    reset() {
        this.filterOption = '';
        this.rows = this.temp;
    }

    changeFilterLater(e) {
        swal('Filter Unavailable',
            'Work in Progress. This filter will be available soon.',
            'info'

        )
    }

    getNotifications() {
        this.rows = [];

        this.temp = [];
        this.notificationService.getAllNotificationsUsingGET().subscribe(res => {
            let result = <any>res;
            // console.log(result);
            result = result.filter(element => {
                element.sound = new Date(Number(element.sound)).toDateString();
                if (Number(element.orgId) === Number(this.previewService.organizationId.getValue())) {
                    return true;
                }
                return false;
            });
            result.sort((a, b) => { return new Date(b.sound).getTime() - new Date(a.sound).getTime() });
            this.temp = [...result];
            this.rows = result;
        })
    }

    filterListByType(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {

            if (d.description === val) {
                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByAuthor(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            return d.author === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByStatus(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log(d.state);
            // console.log(val);
            return d.state === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByCreatedDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log('date:', new Date(d.sound).getMonth())
            if (new Date(d.sound).getDate() === new Date(val).getDate() && new Date(d.sound).getMonth() === new Date(val).getMonth() && new Date(d.sound).getFullYear() === new Date(val).getFullYear()) {


                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPublDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {

            if (new Date(d.publishedDate).getDate() === new Date(val).getDate() && new Date(d.publishedDate).getMonth() === new Date(val).getMonth() && new Date(d.publishedDate).getFullYear() === new Date(val).getFullYear()) {

                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    deleteContent(row) {

        swal({
            title: 'Are you sure you want to delete this notification?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.notificationService.deleteNotificationUsingDELETE(row.id).subscribe(res => {
                    let result = <any>res;
                    // console.log('delte', result)
                    this.getNotifications();

                })
            }
            else {
                swal(
                    'Cancelled',
                    'Your notification item was not deleted.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your notification item was not deleted.',
                    'error'
                )
            }
        })



    }
}
