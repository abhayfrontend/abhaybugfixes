import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as AWS from '../shared/configs/aws.config';
import { CampaignGroupResourceService, DeviceResourceService, UserResourceService } from '../sthaapak';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import { PreviewService } from '../shared/auth/preview.service';
import { CronOptions } from 'cron-editor/cron-editor';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { base, proxyApi } from '../shared/configs/util';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  @ViewChild('imageFileInput') imageInput: ElementRef;

  /* variables */
  validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
  date = new Date().toISOString().slice(0, 16).toString();
  futureDate = new Date(new Date().getTime() + 86400000 * 90).toISOString().slice(0, 16).toString();
  //minDate = this.date.getDate() + '/' + (this.date.getMonth() + 1 > 9 ? this.date.getMonth() + 1 : '0' + Number(this.date.getMonth() + 1)) + '/' + this.date.getFullYear();
  scheduleDate: any = '';
  radiovalue:any;
  max;
  min;
  groups = [];
  url;
  learnerIds = [];
  imageName;
  title;
  messageContent;
  thumbnailImage;
  groupsSelected = [];
  learners = [];
  tokens = [];
  cronExpression;
  notificationCronExpression;
  groupIds = [];
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };

  themeSelection: any = {
    expanded: false,
    selectedTheme: {
      theme: 'Info',
      Info: true,
      Deadline: false,
      Todo: false
    }
  };
  /* end variables */

  public cronOptions: CronOptions = {
    formInputClass: 'form-control cron-editor-input',
    formSelectClass: 'form-control cron-editor-select',
    formRadioClass: 'cron-editor-radio',
    formCheckboxClass: 'cron-editor-checkbox',

    defaultTime: "10:00:00",

    hideMinutesTab: false,
    hideHourlyTab: false,
    hideDailyTab: false,
    hideWeeklyTab: false,
    hideMonthlyTab: false,
    hideYearlyTab: false,
    hideAdvancedTab: true,
    use24HourTime: true,
    hideSeconds: false
  };


  groupSelection: any = {
    that: this,
    everyGroup: false,
    particularGroup: true,
    everyGroupClicked: function () {
      this.that.learners = [];
      this.everyGroup = true;
      this.particularGroup = false;
      /* this.that.http.get(base + '/api/users/filterby/' + this.that.previewService.organizationId.getValue(), this.that.httpOptions).subscribe(res => {
         let result = <any>res;
         // console.log('userResou:', result);
         result.forEach((element, i) => {
           if (element.orgId === this.that.previewService.organizationId.getValue() || element.organizationId === this.that.previewService.organizationId.getValue()) {
             this.that.http.get(base + '/api/learners/' + element.learnerId, this.that.httpOptions).subscribe(response => {
               let learnerObject = <any>response;
               // console.log('lobj:', learnerObject);
               this.that.learners.push(learnerObject);
 
             });
           }
           if (i === result.length - 1) {
             this.that.getTokens(this.that.learners);
           }
         });
 
 
       })
       */
    },
    particularGroupClicked: function () {
      this.that.learners = [];
      this.particularGroup = true;
      this.everyGroup = false;
    },
    searchVisible: false,
    selectedGroups: {
    },
    toggleListSelection: function (string) {
      this.searchVisible = !this.searchVisible;
      this.that.learners = [];
      this.that.groupsSelected = [];
      this.selectedGroups[string] = !this.selectedGroups[string];
      let groups = Object.keys(this.selectedGroups);
      // console.log('groupskeys:', groups);
      // console.log('groupsksele:', this.selectedGroups);
      groups.forEach(e => {
        // console.log('g1:', this.selectedGroups[e]);
        if (this.selectedGroups[e] === true) {
          this.that.groupsSelected.push(e)
          // console.log('groupselectd:', this.that.groupsSelected);
          this.that.groups.forEach((element, i) => {
            if (element.name === e && (this.that.groups.indexOf(element) === i)) {
              this.that.learners.push(...element.learners)
              // console.log('learners:', this.that.learners);
              this.that.groupIds.push(element.id);
            }
          });
        }
      });

      this.that.getTokens(this.that.learners);
    }
  }


  notificationSelection: any = {
    yes: true,
    no: false,
    yesClicked: function () {
      this.yes = true;
      this.no = false;
      this.step3Visible = true;
    },
    noClicked: function () {
      this.no = true;
      this.yes = false;
      this.step3Visible = false;
    },
    step3Visible: true,
  }

  scheduleSelection: any = {
    input: HTMLInputElement,
    min: new Date().toISOString().slice(0, 16).toString(),
    max: new Date(new Date().getTime() + 86400000 * 90).toISOString().slice(0, 16).toString(),
    now: true,
    later: false,
    once: true,
    repeat: false,
    nowClicked: function () {
      this.later = false;
      this.now = true;
      this.laterScheduleVisible = false;
    },
    laterClicked: function () {
      this.later = true;
      this.now = false;
      this.laterScheduleVisible = true;
      // console.log("ELEMRNT:", document.getElementById("date-field"), "2018-05-05T16:15");
      this.input = <HTMLInputElement>document.getElementById('date-field');
      console.log(JSON.stringify(this.input));
      this.input.min = this.min;
      this.input.max = this.max;
      // console.log('min:', this.min);
      // console.log('max:', this.max);
      // document.getElementById("date-field").max = this.max;
      // console.log("ELEMRNT:", document.getElementById("date-field"), "2018-05-05T16:15");
    },
    onceClicked: function () {
      this.once = true;
      this.repeat = false;
      this.repeatScheduleVisible = false;
    },
    repeatClicked: function () {
      if (this.now) {
        this.once = false;
        this.repeat = true;
        this.repeatScheduleVisible = true;
      }
    },
    repeatScheduleVisible: false,
    laterScheduleVisible: false,
  }

  closeDropdown() {
    if (this.groupSelection.searchVisible) {

      this.groupSelection.searchVisible = false;
    }
  }
  constructor(public groupService: CampaignGroupResourceService, public deviceService: DeviceResourceService, private router: Router,
    private http: HttpClient, private roleGuardService: RoleGuardService, public userResource: UserResourceService, public previewService: PreviewService) {
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnInit() {
    let datepick;
    this.min = this.date;
    // this.max = new Date('2020-06-12T19:30').toISOString();
    // console.log('min:', this.date.toString());
    this.groups = this.previewService.campaignGroups.getValue();
    if (this.groups.length < 1) {
      this.groupService.getAllCampaignGroupsUsingGET().subscribe(res => {
        let result = <any>res;
        result = result.filter(e => {
          if (Number(e.orgId) === Number(this.previewService.organizationId.getValue())) {
            return true;
          }
        })
        // console.log('groups:', result);
        this.groups = [...result]
      })
    }


  }

  browseImageClicked() {
    let el: any = this.imageInput.nativeElement;
    el.click();
  }

  validationOnTitle() {
    let elm = <HTMLInputElement>document.getElementById('title');
    if (elm.value.length < 1 || elm.value.length > 100) {
      swal('', 'Please enter atleast 1 character and atmost 100 characters.');
      // now focus it

    }
  }

  validationOnMessage() {
    let elm = <HTMLInputElement>document.getElementById('messageContent');
    if (elm.value.length < 1 || elm.value.length > 200) {
      swal('', 'Please enter atleast 1 character and atmost 200 characters.');
      // now focus it

    }
    // console.log('cron:', this.cronExpression);
  }

  selectTheme(theme) {
    this.radiovalue = theme;
    // console.log('type:', this.themeSelection.selectedTheme.theme);
  }

  deselectAllThemes() {
    this.themeSelection.selectedTheme = {
      theme: 'Info',
      Info: false,
      Deadline: false,
      Todo: false
    }
  }

  imageChanged(event) {

    let _URL = window.URL;
    let error = false;

    if (event.target.files && event.target.files[0]) {

      let sFileName = event.target.files[0].name;
      // console.log('sfilename: ', sFileName);
      let blnValid = false;
      for (let j = 0; j < this.validFileExtensions.length; j++) {
        let sCurExtension = this.validFileExtensions[j];
        if (sFileName.slice(sFileName.length - sCurExtension.length, sFileName.length).toLowerCase() === sCurExtension.toLowerCase()) {
          blnValid = true;
          break;
        }
      }
      if (!blnValid) {
        swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensions.join(','));
        return false;
      }
      // console.log('size:', event.target.files[0].size);
      if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 1000000)) {
        swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 1 Mb');
        return false;
      }



      const file = event.target.files[0];

      let reader = new FileReader();


      //Initiate the FileReader object.
      //Read the contents of Image File.
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e) => {
        //Initiate the JavaScript Image object.
        let image = new Image();
        //Set the Base64 string return from FileReader as source.
        image.src = _URL.createObjectURL(file);
        image.onload = () => {
          //Determine the Height and Width.
          let height = image.height;
          let width = image.width;

          swal('Valid File', "Uploaded image has valid Height and Width.").then(confirm => {
            Promise.resolve(this.uploadFile(file)).then(location => this.thumbnailImage = <any>location);
          });
          return true;
        };
      }



      this.imageName = event.target.files[0].name;

    }
  }

  uploadFile(file) {

    let upload = AWS.s3.upload({ Key: file.name + new Date(), Bucket: AWS.bucketName, Body: file, ACL: 'public-read' });
    let promise = upload.promise();
    return promise.then((data) => {
      // console.log(data);
      return data.Location;

    }, (err) => {
      // console.log(err);
    });

  }

  getTokens(learners) {
    this.deviceService.getAllDevicesUsingGET().subscribe(res => {
      let result: any = res;
      // console.log('devices:', result);
      for (let i = 0; i < learners.length; i++) {
        for (let j = 0; j < result.length; j++) {
          if (learners[i]['registeredNumber'] === result[j]['phoneNumber']) {
            this.tokens.push(result[j]['fcmToken']);
            this.learnerIds.push(learners[i].id);
            // console.log('token+$i: ', result[j]['phoneNumber']);
          }
        }
      }
      return this.tokens;
    });
  }


  sendNotifications() {

    if (!this.validateContentForm()) {
      return;
    }
    // console.log('Cron:', this.cronExpression);
    if (this.cronExpression) {
      this.cronExpression = this.cronExpression.split('? ').join('');
      // console.log('Cron:', this.cronExpression);
      this.cronExpression = this.cronExpression.replace(/[0-9]+\//gi, '*/');
      // console.log('Cron:', this.cronExpression);
    }
    if (this.scheduleSelection.later) {
      let dateObject = new Date(this.scheduleDate);
      // console.log('date:', dateObject);
      this.notificationCronExpression = "0" + " " + dateObject.getMinutes() + " " + dateObject.getHours() + " " + dateObject.getDate() + " " + Number(dateObject.getMonth()) + " *";
      // console.log('dateExpress:', this.notificationCronExpression);
      //    this.notificationCronExpression = dateObject;
    }
    if (this.scheduleSelection.now) {
      this.notificationCronExpression = null;
    }
    if (this.groupSelection.everyGroup) {

      this.http.post(proxyApi + '/contentUpdate', {
        'topic': this.previewService.organizationId.getValue(),

        'data': {
          'silent': false,
          'save': true,
          'body': this.messageContent,
          'campaignId': 0,
          'ctaURL': 'string',
          'description': this.radiovalue,
          'image': null,
          'lifeCycleId': 257501,
          'microContentId': 1253,
          'name': this.title,
          'scheduled': this.scheduleSelection.repeat ? this.cronExpression : this.notificationCronExpression,
          'screenType': null,
          'sequence': null,
          'sound': new Date().getTime(),
          'specialParameters': this.groupsSelected.length > 0 ? this.groupIds.join() : null,
          'tags': 'Notify',
          'template': 'string',
          'title': this.title,
          'orgId': this.previewService.organizationId.getValue(),
          'groupId': null,
          'userId': null

        }

      }, this.httpOptions).subscribe(res => {
        // console.log('sent notification to all', res);
        let url = base + '/api/notifications';
        let tok = this.tokens.splice(0, 10);
      this.http.post(url + String(tok.join()), JSON.parse(JSON.stringify({


        'body': this.messageContent,
        'campaignId': 82551,
        'ctaURL': 'string',
        'description': this.radiovalue,
        'image': null,
        'lifeCycleId': 257501,
        'microContentId': 1253,
        'name': this.title,
        'scheduled': this.scheduleSelection.repeat ? this.cronExpression : this.notificationCronExpression,
        'screenType': null,
        'sequence': null,
        'sound': new Date().getTime(),
        'specialParameters': this.groupsSelected.length > 0 ? this.groupIds.join() : null,
        'tags': 'Notify',
        'template': 'string',
        'title': this.title,
        'orgId': this.previewService.organizationId.getValue(),
        'groupId': null,
        'userId': null

      })), this.httpOptions).subscribe(response => {

        console.log("Successfully saved data to notification devapi");
        
        // console.log('response subscribe:', response);
      });
      
      swal('Successfully Sent Notification.', '', 'success');
      this.router.navigate(['/notification/list'])

      })
    } else if (this.groupSelection.particularGroup) {

      this.sendNotification({
        'notificationName': this.title, 'cronExpression': this.scheduleSelection.repeat ? this.cronExpression : this.notificationCronExpression,
        'notificationText': this.messageContent, 'thumbnailImage': this.thumbnailImage, 'tags': 'Notify', 'tokens': this.tokens,
        'specialParameters': this.groupsSelected.length > 0 ? this.groupIds.join() : null, title: null, screenType: 'Blogs',
        orgId: this.previewService.organizationId.getValue(), cgroupId: this.groupIds.join() || null, userId: this.learnerIds.join(), sgroupId: null
      });
    }

  };


  sendNotification({ notificationName, cronExpression, notificationText, thumbnailImage, tags, tokens, specialParameters, title, screenType, orgId, sgroupId, cgroupId, userId }) {

    let url = '';


    // console.log('notification:', JSON.parse(JSON.stringify({

    //   'notificationDTO': {
    //     'body': notificationText,
    //     'campaignId': 0,
    //     'ctaURL': 'string',
    //     'description': this.themeSelection.selectedTheme.theme || 'Info',
    //     'image': thumbnailImage,
    //     'lifeCycleId': 0,
    //     'microContentId': 0,
    //     'name': notificationName,
    //     'scheduled': cronExpression,
    //     'screenType': 'string',
    //     'sequence': this.previewService.organizationId.getValue(),
    //     'sound': new Date().getTime(),
    //     'specialParameters': specialParameters,
    //     'tags': tags,
    //     'template': 'string',
    //     'title': notificationName
    //   }, 'deviceID': tokens.join()
    // })));

    if (this.cronExpression) {
      url = base + '/api/notifications/schedule/send?deviceID='

    }
    else url = base + '/api/notifications/send?deviceID=';


    do {
      let tok = this.tokens.splice(0, 10);
      this.http.post(url + String(tok.join()), JSON.parse(JSON.stringify({


        'body': notificationText,
        'campaignId': 0,
        'ctaURL': 'string',
        'description': this.radiovalue,
        'image': thumbnailImage,
        'lifeCycleId': 257501,
        'microContentId': 1253,
        'name': notificationName,
        'scheduled': cronExpression,
        'screenType': screenType,
        'sequence': sgroupId,
        'sound': new Date().getTime(),
        'specialParameters': specialParameters,
        'tags': tags,
        'template': 'string',
        'title': notificationName,
        'orgId': orgId,
        'groupId': cgroupId,
        'userId': userId

      })), this.httpOptions).subscribe(response => {
        this.tokens.length /= 10;
        // console.log('response subscribe:', response);
      });
    } while (this.tokens.length);


    this.http.post(base + '/api/notifications', {
      'body': notificationText,
      'campaignId': 82551,
      'ctaURL': 'string',
      'description': this.radiovalue,
      'image': thumbnailImage,
      'lifeCycleId': null,
      'microContentId': null,
      'name': notificationName,
      'scheduled': cronExpression,
      'screenType': screenType,
      'sequence': sgroupId,
      'sound': new Date().getTime(),
      'specialParameters': specialParameters,
      'tags': tags,
      'template': 'string',
      'title': notificationName,
      'orgId': orgId,
      'groupId': cgroupId,
      'userId': userId
    }, this.httpOptions)
      .subscribe(res => {
        // console.log('notification:', res)
        swal('Successfully Sent Notification.', '', 'success');

      });

    //this.notificationSuccessfullySent(this.pushNotificationName || this.popNotificationText, result.name ? true : false);
    //this.resetFields();



  }

  resetSlides() {
    //reset slide creation
    this.title = '';
    this.messageContent = '';
    this.selectTheme('Info');

    //reset question creation
    this.groupSelection.particularGroupClicked();
    this.groups.forEach(e => {
      this.groupSelection.selectedGroups[e] = false;
    });
    this.groupsSelected = [];
    this.scheduleDate = '';
    this.scheduleSelection.nowClicked();
    this.scheduleSelection.onceClicked()
    this.cronExpression = null;

  }


  validateContentForm() {
    // console.log('Enter')
    let isValid = true;
    let contentNameInput, contentTypeInput, contentDescriptionInput, categoryInput, subCategoryInput, imageInput, typeInput, negativeInput;
    if (!this.title || this.title.trim() === "") {
      // console.log('Title', this.title);
      typeInput = document.getElementById('title');
      typeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      // console.log('Title', this.title);
      typeInput = document.getElementById('title');
      typeInput.classList.remove('error');
      isValid = true;
    }

    if (!this.messageContent || this.messageContent.trim() === "") {
      contentNameInput = document.getElementById('messageContent');
      contentNameInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      contentNameInput = document.getElementById('messageContent');
      contentNameInput.classList.remove('error');
      isValid = true;
    }

    if (this.groupSelection.particularGroup && !this.groupsSelected.length) {
      // console.log('Group', this.groupSelection.particularGroup);
      contentDescriptionInput = document.getElementById('groupList');
      contentDescriptionInput.classList.add('error');
      isValid = false;
      return isValid;
    } else if (this.groupSelection.particularGroup && this.groupsSelected.length) {
      // console.log('Group', this.groupSelection.particularGroup);
      contentDescriptionInput = document.getElementById('groupList');
      contentDescriptionInput.classList.remove('error');
      isValid = true;
    }

    if (this.scheduleSelection.later && !this.scheduleDate) {
      negativeInput = document.getElementById('date-field');
      negativeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else if (this.scheduleSelection.later && this.scheduleDate) {
      negativeInput = document.getElementById('date-field');
      negativeInput.classList.remove('error');
      isValid = true;
    }

    if (this.scheduleSelection.repeat && !this.cronExpression) {
      categoryInput = document.getElementById('cron');
      categoryInput.classList.add('error-text');
      isValid = false;
      return isValid;
    } else if (this.scheduleSelection.repeat && this.cronExpression) {
      categoryInput = document.getElementById('cron');
      categoryInput.classList.remove('error-text');
      isValid = true;
    }

    return isValid;
  }


}


