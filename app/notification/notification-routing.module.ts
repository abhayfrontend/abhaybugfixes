import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificationComponent } from './notification.component';
import { NotificationListComponent } from './notification-list.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add',
        component: NotificationComponent,
        data: {
          title: ''
        }
      },
      {
        path: 'list',
        component: NotificationListComponent,
        data: {
          title: ''
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationRoutingModule { }
