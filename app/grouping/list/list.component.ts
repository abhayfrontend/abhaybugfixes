import { Component, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable/release';
import { PublishedContentResourceService, MicroContentResourceService } from '../../sthaapak';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import { PreviewService } from '../../shared/auth/preview.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { base, proxyApi } from '../../shared/configs/util';
import { MatPaginator, MatTableDataSource, MatTableModule, MatDialog, MatSort, MatHorizontalStepper } from '@angular/material';
import { DataSource } from '@angular/cdk/table';
import { ChatHistoryResourceService } from '../../sthaapak/sdk/chatHistoryResource.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('stepper') stepper: MatHorizontalStepper;
  rows = [];
  temp = [];
  temp2 = [];
  filterOption;
  authors = [];
  organizationId;
  userarray;
  adduser;
  obj;
  dataSource;
  sgroups;
  CGroupId;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };


  status = ['DRAFT', 'PUBLISHED', 'UNDER REVIEW', 'EXPIRED', 'APPROVED', 'REJECTED'];
  types = ['BLOG', 'VIDEO'];
  // Table Column Titles
  columns = [
    { prop: 'name' },
    { prop: 'author' },
    { prop: 'state' },
    { prop: 'postDate' },
    { prop: 'publishedDate' }
  ];
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private chatHistory: ChatHistoryResourceService, private previewService: PreviewService, private router: Router,
    private roleGuardService: RoleGuardService, private http: HttpClient) {
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }

  async ngOnInit() {

    this.organizationId = this.previewService.organizationId.getValue();
    await this.getContentList();

  }

  reset() {
    this.filterOption = '';
    this.rows = this.temp;
  }


  getCellClass({ row, column, value }): any {
    return {
      'is-unReview': value === 'Under Review',
      'is-published': value === 'Published',
      'is-approved': value === 'Approved',
      'is-expired': value === 'Expiry',
      'is-rejected': value === 'Rejected',
    };
  }
  changeFilter(e) {
    // console.log('filter:', this.temp);
    this.filterOption = e.target.value;
    this.rows = this.temp;
  }

  async getContentList() {
    this.temp = [];
    this.rows = [];
    this.temp2 = [];
    let cards = [];
    this.obj = this.previewService.groupToEdit.getValue();
    this.CGroupId = <any>this.previewService.groupToEdit.getValue();
    // console.log(this.obj)

    if (!this.obj.name) {
      this.router.navigate(['/grouping/groups'])
    }
    else {
      let result = this.obj['learners'].map(el => {
        let name = el.name.split(' ');
        el.publishedDate = new Date(el.publishedDate).toDateString();
        el.postDate = new Date(el.postDate).toDateString();
        if (name[1] === 'null') {
          el.name = name[0];
        }
        if (name[2] === 'null') {
          el.name = name[0] + name[1];
        }
        return el;

      });
      // console.log(this.obj);
      //this.temp = [...this.temp, ...this.temp2];
      this.temp = [...result];
      this.rows = this.temp;

      // console.log('temp:', this.temp);
      this.sgroups = await this.http.get(base + '/api/support-groups', this.httpOptions).toPromise().then(gr => <any>gr);
      // console.log('SGROIPS:', this.sgroups);
    }
  }

  filterListByType(event) {
    // console.log('temptt:', this.temp);
    this.rows = this.temp;
    // console.log('temptr:', this.rows);

    const val = event.target.value;
    // filter our data
    const temp3 = this.temp.filter(function (d) {
      if (val === 'VIDEO' && d.videoLink) {
        return true;

      }
      if (val === 'BLOG' && d.previewImage) {
        return true;
      }
      return false;
    });

    // update the rows
    this.rows = temp3;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  filterListByAuthor(event) {
    // console.log('temptt:', this.temp);
    this.rows = this.temp;
    // console.log('temptr:', this.rows);

    const val = event.target.value;
    // filter our data
    const temp3 = this.temp.filter(function (d) {
      return d.author === val;
    });

    // update the rows
    this.rows = temp3;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  filterListByStatus(event) {
    // console.log('temptt:', this.temp);
    this.rows = this.temp;
    // console.log('temptr:', this.rows);

    const val = event.target.value;
    // filter our data
    const temp3 = this.temp.filter(function (d) {
      // console.log(d.state);
      // console.log(val);
      return d.state === val;
    });

    // update the rows
    this.rows = temp3;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  filterListByPostDate(event) {
    // console.log('temptt:', this.temp);
    this.rows = this.temp;
    // console.log('temptr:', this.rows);

    const val = event.target.value;
    // filter our data
    const temp3 = this.temp.filter(function (d) {
      // console.log('date:', d.postDate)
      if (new Date(d.postDate).getDate() === new Date(val).getDate() && new Date(d.postDate).getMonth() === new Date(val).getMonth() && new Date(d.postDate).getFullYear() === new Date(val).getFullYear()) {


        return true;
      }
      return false;
    });

    // update the rows
    this.rows = temp3;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  filterListByPublDate(event) {
    // console.log('temptt:', this.temp);
    this.rows = this.temp;
    // console.log('temptr:', this.rows);

    const val = event.target.value;
    // filter our data
    const temp3 = this.temp.filter(function (d) {

      if (new Date(d.publishedDate).getDate() === new Date(val).getDate() && new Date(d.publishedDate).getMonth() === new Date(val).getMonth() && new Date(d.publishedDate).getFullYear() === new Date(val).getFullYear()) {

        return true;
      }
      return false;
    });

    // update the rows
    this.rows = temp3;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  updateFilter(event) {

    const val = event.target.value.toLowerCase();

    // filter our data
    const temp3 = this.temp.filter(function (d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp3;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  scheduleContent(obj) {

    swal({
      title: 'Are you sure you want to publish this content?',
      text: "This will take you to the Content Scheduler!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Yes, edit it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then((confirm) => {
      // console.log('con:', confirm)
      if (confirm.value === true) {
        this.previewService.editContentToSchedule(obj);
        this.router.navigate(['/scheduler/scheduler']);
      }
      else {
        swal(
          'Cancelled',
          'Your content was not published.',
          'error'
        )
      }
    }, (dismiss) => {
      // console.log('dismis:', dismiss)
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Your content was not published.',
          'error'
        )
      }
    })

  }

  deleteContent(row) {


    swal({
      title: 'Are you sure you want to remove this user from this group?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then((confirm) => {
      let sgroup;
      let learnerName = row.name.split(' ')[0];
      let learnerLastName = row.name.split(' ')[1] === 'null' ? '' : row.name.split(' ')[1];
      // console.log('con:', confirm)
      if (confirm.value === true) {
        this.sgroups.forEach(element => {
          if (element.name === this.obj.name) {
            sgroup = element;
          }
        });
        this.obj.learners = this.obj.learners.filter(element => {
          if (element.id !== row.id) {
            return true;
          }

          else return false;
        });
        // console.log('delte', this.obj);
        this.http.put(base + '/api/campaign-groups', this.obj, this.httpOptions).subscribe(delRes => {
          // console.log('res:', delRes);
          this.http.get(base + '/api/users/filterby/' + this.organizationId, this.httpOptions).subscribe(users => {
            this.userarray = <any>users;
            this.userarray.forEach(element => {
              if (element['learner'].id === row.id) {
                sgroup.members = sgroup.members.filter(el => {
                  if (Number(el.id) === Number(element.langKey)) {
                    return false;
                  }
                  else return true;
                });
                this.http.put(base + '/api/support-groups', sgroup, this.httpOptions).subscribe(groups => {
                  this.sgroups = <any>groups;
                  this.sendUpdate(sgroup.id+"", this.obj.id+"", 'GROUPS');
                  this.chatHistory.createChatHistoryUsingPOST({
                    "attachment": true,
                    "attachmentLink": "string",
                    "category": "string",
                    "description": learnerName + ' ' + learnerLastName + " has been removed by " + this.previewService.Username.getValue(),
                    "from": "string",
                    "group": sgroup.id,
                    "name": "string",
                    "orgId": this.previewService.organizationId.getValue(),
                    "subCategory": "broadcast",
                    "tags": "string",
                    "to": "string",
                    "ts": new Date().getTime()
                  }).subscribe(chat => {
                    // console.log(chat);
                    /* this.http.get(base + '/api/campaign-groups', this.httpOptions).subscribe(cgroups => {
                       let campaignGroups = <any>cgroups;
                       campaignGroups = campaignGroups.filter(e => e.orgId === this.previewService.organizationId.getValue())
                       this.previewService.editCampaignGroup([]);
                     })
                     this.http.get(base + '/api/support-groups', this.httpOptions).subscribe(sgroups => {
                       let supportGroups = <any>sgroups;
                       supportGroups = supportGroups.filter(e => e.category === this.previewService.organizationId.getValue())
                       this.previewService.editSupportGroup([]);
                     })
                     */
                  })
                  this.getContentList();
                })
              }
            });
          })
        })

      }
      else {
        swal(
          'Cancelled',
          'Your content was not deleted.',
          'error'
        )
      }
    }, (dismiss) => {
      // console.log('dismis:', dismiss)
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Your content was not deleted.',
          'error'
        )
      }
    })




  }

  openDetails(row) {
    // console.log('row:', row);
    swal({
      html: `<table class="customers">
  <tr>
    <th>Property</th>
    <th>Value</th>
  </tr>
  <tr>
    <td> Name</td>
    <td>` + row.name + `<td>
  </tr>
  <tr>
    <td>Phone Number</td>
    <td>` + row.registeredNumber + `</td>
  </tr>
  </table>`,
      customClass: 'table'
    })
  }


  makeExpert(obj) {

    swal({
      title: 'Are you sure you want to make ' + obj.name + ' Expert for this group?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Yes!',
      cancelButtonText: 'No!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then(async (confirm) => {
      // console.log('con:', confirm)
      if (confirm.value === true) {

        let getGroup = await this.http.get(base + '/api/campaign-groups/' + this.CGroupId.id, this.httpOptions).toPromise().then(res => <any>res);
        getGroup.selectQuery = obj.registeredNumber;
        let putCGroup = this.http.put(base + '/api/campaign-groups', getGroup, this.httpOptions).toPromise().then(res => {
          // console.log(res)
          swal(
            'Success',
            obj.name + ' was assigned Expert for this group.',
            'success'
          )
          this.previewService.editGroup(res);
          this.getContentList();
        });
        let getSGroup = await this.http.get(base + '/api/support-groups', this.httpOptions).toPromise().then(res => <any>res);
        getSGroup.forEach(element => {
          if (element.name === this.CGroupId.name) {
            element.email = obj.registeredNumber;
            let putSGroup = this.http.put(base + '/api/support-groups', element, this.httpOptions).toPromise().then(res => {
              // console.log(res)
              this.sendUpdate(element.id, this.CGroupId.id, 'groups');

            });

          }
        });

      }
      else {
        swal(
          'Cancelled',
          'Your content was not edited.',
          'error'
        )
      }
    }, (dismiss) => {
      // console.log('dismis:', dismiss)
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Your content was not edited.',
          'error'
        )
      }
    })
  }

  makeAdmin(obj) {

    swal({
      title: 'Are you sure you want to make ' + obj.name + ' Admin for this group?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Yes!',
      cancelButtonText: 'No!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then(async (confirm) => {
      // console.log('con:', confirm)
      if (confirm.value === true) {

        let getGroup = await this.http.get(base + '/api/campaign-groups/' + this.CGroupId.id, this.httpOptions).toPromise().then(res => <any>res);
        getGroup.selectQuery = 'Broadcast/' + obj.registeredNumber;
        let putCGroup = this.http.put(base + '/api/campaign-groups', getGroup, this.httpOptions).toPromise().then(res => {
          // console.log(res)
          swal(
            'Success',
            obj.name + ' was assigned Admin for this group.',
            'success'
          )
          this.previewService.editGroup(res);
          this.getContentList();
        });
        let getSGroup = await this.http.get(base + '/api/support-groups', this.httpOptions).toPromise().then(res => <any>res);
        getSGroup.forEach(element => {
          if (element.name === this.CGroupId.name) {
            element.email = obj.registeredNumber;
            let putSGroup = this.http.put(base + '/api/support-groups', element, this.httpOptions).toPromise().then(res => {
              // console.log(res)

            });

          }
        });

      }
      else {
        swal(
          'Cancelled',
          'Your content was not edited.',
          'error'
        )
      }
    }, (dismiss) => {
      // console.log('dismis:', dismiss)
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Your content was not edited.',
          'error'
        )
      }
    })
  }


  sendUpdate(category, subcategory, screen) {


    let message = {
      topic: this.previewService.organizationId.getValue(),
      data: {
        silent: true,
        save: false,
        title: category,
        body: subcategory,
        screenType: screen,
        image: 'jpg',
        specialParameters: 'll',
        tags: 'Silent',
        orgId: this.previewService.organizationId.getValue(),
        name: screen
      }

    }
    // console.log('message:', message);
    this.http.post(proxyApi + '/contentUpdate', message, this.httpOptions).subscribe((response) => {
      // Response is a message ID string.
      console.log('Successfully sent message:', response);

    }, (error) => {
      // console.log('Error sending message:', error);
    });



  }

}
