import { Component, OnInit } from '@angular/core';
import { UserResourceService, CampaignGroupResourceService } from '../../sthaapak';
import { PreviewService } from '../../shared/auth/preview.service';
import swal from 'sweetalert2';
import { base } from '../../shared/configs/util';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import { Router } from '@angular/router';
import { SupportGroupResourceService } from '../../sthaapak/sdk/supportGroupResource.service';
import { proxyApi } from '../../shared/configs/util';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {
  groups = [];
  inputOption = {};
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };

  constructor(private router: Router, public groupService: CampaignGroupResourceService, private previewService: PreviewService,
    private roleGuardService: RoleGuardService, private http: HttpClient, private supportGroup: SupportGroupResourceService) {
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnInit() {
    let result = <any>this.previewService.campaignGroups.getValue();
        result.forEach((e, i) => {
            this.inputOption[e.name] = e.name;
            // console.log('g:', this.inputOption);

        });
        // console.log('groups2:', this.groups);
        if (this.groups.length < 1) {
            this.getGroups();
        }
    // this.groups = this.previewService.campaignGroups.getValue();
    // this.groups = this.groups.map(e => {
    //   e.filters = [];
    //   return e;
    // });
    // console.log('groups2:', this.groups);
    // if (this.groups.length < 1) {
      this.getGroups();
    // }

  }

  getGroups() {
    this.groupService.getAllCampaignFororgidUsingGET(this.previewService.organizationId.getValue()).subscribe(res => {
      let result = <any>res;
      console.log('orgid:', this.previewService.organizationId.getValue());
      this.groups = [];
      result.forEach((e, i) => {

          if (Number(e.orgId) === Number(this.previewService.organizationId.getValue())) {
              this.groups.push(e);
              this.inputOption[e.name] = e.name;
              // console.log('g:', this.groups);
          }
      });
      console.log('groups2:', this.groups);
  })
  }

  deleteGroup(row) {

    swal({
      title: 'Are you sure you want to delete this group?',
      text: "This will delete the group. You will no longer be able to use this group for chat, campaigns and notifications. ",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then((confirm) => {
      // console.log('con:', confirm)
      if (confirm.value === true) {

        this.groupService.deleteCampaignGroupUsingDELETE(row.id).subscribe(res => {
          let result = <any>res;
          console.log('Deleted:', result);
          this.supportGroup.getAllSupportGroupsUsingGET().subscribe(resp => {
            let sgroups = <any>resp;
            sgroups.forEach(element => {
              if (element.name === row.name) {
                this.supportGroup.deleteSupportGroupUsingDELETE(element.id).subscribe(delsgroup => {
                  console.log('dels:', delsgroup);
                  console.log(element.id + " " + row.id + " " + element.value + " " + row.path);
                  this.sendUpdate(element.id+"", row.id+"", 'GROUPS');
                  this.getGroups();
                })
              }
            });
          })


        })
      }
      else {
        swal(
          'Cancelled',
          'Your group was not deleted.',
          'error'
        )
      }
    }, (dismiss) => {
      // console.log('dismis:', dismiss)
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Your group was not deleted.',
          'error'
        )
      }
    })

  }

  viewUsers(e) {
    this.previewService.editGroup(e);
    this.router.navigate(['/grouping/groupUsers']);
  }


  sendUpdate(category, subcategory, screen) {


    let message = {
      topic: this.previewService.organizationId.getValue(),
      data: {
        silent: true,
        save: false,
        title: category,
        body: subcategory,
        screenType: screen,
        image: 'jpg',
        specialParameters: 'll',
        tags: 'Silent',
        orgId: this.previewService.organizationId.getValue(),
        name: screen
      }

    }
    console.log('message:', message);
    this.http.post(proxyApi + '/contentUpdate', message, this.httpOptions).subscribe((response) => {
      // Response is a message ID string.
      console.log('Successfully sent message:', response);

    }, (error) => {
      // console.log('Error sending message:', error);
    });



  }

}



