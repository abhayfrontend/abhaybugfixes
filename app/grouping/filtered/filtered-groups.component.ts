import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { CampaignResourceService, CampaignGroupResourceService, UserResourceService, DeviceResourceService, IndustryResourceService } from '../../sthaapak';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import { PreviewService } from '../../shared/auth/preview.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { base, proxyApi } from '../../shared/configs/util';
import { ChatHistoryResourceService } from '../../sthaapak/sdk/chatHistoryResource.service';
import * as AWS from '../../shared/configs/aws.config';
import { baseAnalytics } from '../../shared/configs/util';
import { by } from 'protractor';
import { group } from '@angular/animations';
@Component({
  selector: 'app-filtered-groups',
  templateUrl: './filtered-groups.component.html',
  styleUrls: ['./filtered-groups.component.scss']
})
export class FilteredGroupsComponent implements OnInit, AfterViewInit {

  display = false;
  filterOption;
  temp = [];
  data = [];
  groups = [];
  inputOption = {};
  createdBy = [];
  groupToBeCreated;
  groupCreated;
  sgroup;
  cgroup;
  status = ['Active', 'Inactive'];
  // Table Column Titles
  columns2 = [
    { prop: 'name' },
    { prop: 'imageLink' },
    { prop: 'content' },
    { prop: 'id' }
  ];

  columns = [
    { prop: 'firstName' },
    { prop: 'lastName' },
    { prop: 'lastactive' }
  ];


  rows = [];
  screen;
  timeSpan;
  value;
  jobRole;
  metric;
  values = [];
  groupName = "";
  groupDescription = "";
  orArrays = [];
  totalTimeSpentSum;
  averageTimeSpent;
  orArraysFiltered = [];
  result;
  jobRoles = [];
  numericValue;
  operator;
  departments = ['Technical', 'Sales', 'Faculty'];
  genders = ['MALE', 'FEMALE', 'OTHER'];
  locations = ['NOIDA',
    'DELHI',
    'ALLAHABAD',
    'MEERUT',
    'GURGAON',
    'LUCKNOW',
    'BAREILLY',
    'PUNE',
    'MUMBAI',
    'CHENNAI'
  ];
  types = ['Peer-To-Peer', 'Expert Chat'];
  operators = ['>', '<', '='];
  finalArray = [];
  filtersArray = [
    {
      id: 1,
      filters: [
        {
          id: 1,
          title: 'TOTAL DURATION',
          description: `Total number of minutes the user's device has had your app open`,
          totalDurationOperator: '>',
          icon: 'clock-o',
          type: 'totalDuration'
        }
      ]
    }

  ];
  imageurl;
  imageName;
  learnerIds = [];
  DashboardItems = [];
  metrics = [{ name: 'Time Spent On', value: '*TimeSpent' }, { name: 'Completed', value: 'microlearning_module_completed_clicked' },
  { name: 'Not Completed', value: 'microlearning_module_completed_clicked' }, { name: 'Scored More than 80% in', value: '' },
  { name: 'Scored Less than 40% in', value: '' }]
  groupSelection = {
    selectedTypeName: 'Select a Group Type',
    selectedType: null,
    TypePopupVisible: false,
    selectedExpertName: 'Select an Expert',
    selectedExpert: null,
    ExpertPopupVisible: false
  }
  groupInput;
  metricInput;
  valueInput;
  timeInput;
  noFiltersApplied = true;
  initialButtonClicked = false;
  row = [];
  Operators = [{ name: '>', value: '>' },
  { name: '<', value: '<' },
  { name: '=', value: '==' }];
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token

    })
  };


  timeSpans = [
    { name: '1 day', value: 24 * 60 * 60 * 1000 },
    { name: '1 week', value: 7 * 24 * 60 * 60 * 1000 },
    { name: '2 week', value: 14 * 24 * 60 * 60 * 1000 },
    { name: '1 month', value: 30 * 24 * 60 * 60 * 1000 },
    { name: '3 month', value: 90 * 24 * 60 * 60 * 1000 },
    { name: '1 year', value: 365 * 24 * 60 * 60 * 1000 }
  ]


  constructor(private groupService: CampaignGroupResourceService, private userService: UserResourceService, private router: Router, private chatHistory: ChatHistoryResourceService,
    private roleGuardService: RoleGuardService, private http: HttpClient, private previewService: PreviewService, private Industry: IndustryResourceService,
    private deviceService: DeviceResourceService) {
    if (this.filtersArray.length > 0) {
      this.noFiltersApplied = false;
    }
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }
  getDashboardItems() {
    this.http.get(base + '/api/app-configs/org/' + this.previewService.organizationId.getValue(), this.httpOptions)
      .subscribe(resp => {
        let result = <any>resp;
        // console.log('DashboadItems:', result);
        for (let index = 0; index < result.length; index++) {
          if ((this.previewService.organizationId.getValue() === result[index].organizationId) && (result[index]['category'] === 'DASHBOARD')) {
            this.DashboardItems.push(result[index]['value']);
          }
        }


      })
  }

  ngAfterViewInit() {

  }


  getGroups() {
    this.http.get(base + '/api/campaign-groups/filterby/' + this.previewService.organizationId.getValue(), this.httpOptions).toPromise().then(res => {
      let results = <any>res;
      this.groups = results;
      this.groups.unshift({ 'id': 1, 'name': 'All Users' })
    })
  }

  getJobRoles() {
    this.http.get(base + '/api/job-roles/org/' + this.previewService.organizationId.getValue(), this.httpOptions).toPromise().then(res => {
      let results = <any>res;
      this.jobRoles = results;
    })
  }
  ngOnInit() {

    this.getGroups();
    // this.getDashboardItems();
    this.getJobRoles();
    this.getValues();

    this.groupInput = document.getElementById('groupName');
    this.metricInput = document.getElementById('metric');
    this.valueInput = document.getElementById('value');
    this.timeInput = document.getElementById('timeSpan');

  }



  sendNotification({ notificationName, cronExpression, notificationText, thumbnailImage, tags, tokens, specialParameters, screenType, title, orgId, sgroupId, cgroupId, userId }) {

    // console.log('notification:', JSON.parse(JSON.stringify({

    //   'notificationDTO': {
    //     'body': notificationText,
    //     'campaignId': 0,
    //     'ctaURL': 'string',
    //     'description': 'Info',
    //     'image': thumbnailImage,
    //     'lifeCycleId': 0,
    //     'microContentId': 0,
    //     'name': notificationName,
    //     'scheduled': cronExpression,
    //     'screenType': screenType,
    //     'sequence': sgroupId,
    //     'sound': new Date().getTime(),
    //     'specialParameters': specialParameters,
    //     'tags': tags,
    //     'template': 'string',
    //     'title': title,
    //     'orgId': orgId,
    //     'groupId': cgroupId,
    //     'userId': userId
    //   }, 'deviceID': tokens.join()
    // })));

    this.http.post(base + '/api/notifications/send?deviceID=' + String(tokens.join()), JSON.parse(JSON.stringify({
      'body': notificationText,
      'campaignId': 0,
      'ctaURL': 'string',
      'description': 'Info',
      'image': thumbnailImage,
      'lifeCycleId': 0,
      'microContentId': 0,
      'name': notificationName,
      'scheduled': cronExpression,
      'screenType': screenType,
      'sequence': sgroupId,
      'sound': new Date().getTime(),
      'specialParameters': specialParameters,
      'tags': tags,
      'template': 'string',
      'title': title,
      'orgId': orgId,
      'groupId': cgroupId,
      'userId': userId
    })),
      this.httpOptions).subscribe(response => {
        // console.log('response subscribe:', response);
        let result: any = response;
        this.http.post(base + '/api/notifications', {
          'body': notificationText,
          'campaignId': 82551,
          'ctaURL': 'string',
          'description': 'Info',
          'image': thumbnailImage,
          'lifeCycleId': 257501,
          'microContentId': 1253,
          'name': notificationName,
          'scheduled': cronExpression,
          'screenType': screenType,
          'sequence': sgroupId,
          'sound': new Date().getTime(),
          'specialParameters': specialParameters,
          'tags': tags,
          'template': 'string',
          'title': title,
          'orgId': orgId,
          'groupId': cgroupId,
          'userId': userId
        }, this.httpOptions)
          .subscribe(res => {
            // console.log('notification:', res);

            this.router.navigate(['/content/list'])
          });
        // this.notificationSuccessfullySent(this.pushNotificationName || this.popNotificationText, result.name ? true : false);
        // this.resetFields();
      });


  }

  getResults() {
    /* if (!this.validateContentForm()) {
       return false;
     }
     */
    this.data = [];
    this.temp = [];
    this.rows = [];
    // console.log('temp:', this.temp);
    // console.log('results:', this.groupName, this.metric, this.value, this.timeSpan, this.jobRole)

    if (this.groupName && this.metric && this.value && this.timeSpan) {
      this.values.forEach(e => {
        if (e.value === this.value) {
          this.screen = e.name;
        }
      });
      if (this.metric === '*TimeSpent' && this.value === 'APP') {
        this.getAppTimeSpentEvents();
      }
      if (this.metric === '*TimeSpent' && this.value !== 'APP') {
        switch (this.screen) {
          case 'MicroLearning':
            this.screen = 'MicrolearningTimeSpent'
            break;
          case 'Blogs':
            this.screen = 'BlogsTimeSpent'
            break;
          case 'Industry':
            this.screen = 'IndustryTimeSpent'
            break;
          case 'Feedback':
            this.screen = 'FeedbackTimeSpent'
            break;
        }
        // console.log('value:', this.screen)
        this.getTimeSpentEvents(this.screen);
      }
      this.groupToBeCreated = true;
      this.groupCreated = false;
    }
    else {
      if (!this.groupName) {
        this.groupInput.classList.add('error')
      }
      if (!this.metric) {
        this.metricInput.classList.add('error')
      }
      if (!this.value) {
        this.valueInput.classList.add('error')
      }
      if (!this.timeSpan) {
        this.timeInput.classList.add('error')
      }

    }

  }

  selectGroup(e) {
    this.groupName = e.target.value;
    if (e.target.value && this.groupInput.classList.contains('error')) {
      this.groupInput.classList.remove('error')
    }
  }
  selectMetric(e) {
    this.metric = e.target.value;
    if (e.target.value && this.metricInput.classList.contains('error')) {
      this.metricInput.classList.remove('error')
    }
  }

  selectValue(e) {
    this.value = <any>e.target.value;
    // console.log('selectValue:', this.value);
    if (e.target.value && this.valueInput.classList.contains('error')) {
      this.valueInput.classList.remove('error')
    }
  }

  selectTimeSpan(e) {
    if (e.target.value && this.timeInput.classList.contains('error')) {
      this.timeInput.classList.remove('error')
    }
    this.timeSpan = e.target.value;
  }

  selectJobRole(e) {
    this.jobRole = e.target.value;
  }

  selectOperator(e) {
    this.operator = e.target.value;
  }


  validateContentForm() {
    // console.log('Enter')
    let isValid = true;
    let contentNameInput, contentDescriptionInput, categoryInput, typeInput;
    if (!this.groupName || this.groupName.trim() === "") {
      // console.log('groupName', this.groupName);
      typeInput = document.getElementById('groupName');
      typeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      // console.log('groupName', this.groupName);
      typeInput = document.getElementById('groupName');
      if (typeInput.classList.contains('error')) {
        typeInput.classList.remove('error');
      } isValid = true;
    }

    if (!this.metric) {
      contentNameInput = document.getElementById('metric');
      contentNameInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      contentNameInput = document.getElementById('metric');
      if (contentNameInput.classList.contains('error')) {
        contentNameInput.classList.remove('error');
      }
      isValid = true;
    }

    if (!this.value) {
      // console.log('Group', this.groupSelection);
      contentDescriptionInput = document.getElementById('value');
      contentDescriptionInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      // console.log('Group', this.groupSelection);
      contentDescriptionInput = document.getElementById('value');
      if (contentDescriptionInput.classList.contains('error')) {
        contentDescriptionInput.classList.remove('error');
      }
      isValid = true;
    }

    if (!this.timeSpan) {
      // console.log('Group', this.groupSelection);
      categoryInput = document.getElementById('timeSpan');
      categoryInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      // console.log('Group', this.groupSelection);
      categoryInput = document.getElementById('timeSpan');
      if (categoryInput.classList.contains('error')) {
        categoryInput.classList.remove('error');
      }
      isValid = true;
    }
    return isValid;
  }

  getValues() {
    this.http.get(base + '/api/app-configs/org/' + this.previewService.organizationId.getValue(), this.httpOptions)
      .subscribe(resp => {
        let result = <any>resp;
        // console.log('DashboadItems:', result);
        for (let index = 0; index < result.length; index++) {
          if ((this.previewService.organizationId.getValue() === result[index].organizationId) && (result[index]['category'] === 'DASHBOARD')) {
            this.values.push(result[index]);
          }
        }
        this.values.unshift({ id: 1, value: 'APP' })
      });
  }

  getCompletedValues() {
    this.http.get(base + '/api/micro-contents/org/' + this.previewService.organizationId.getValue(), this.httpOptions).toPromise().then(res => {
      let courses = <any>res;
      this.values = courses;
    })
  }

  publishGroup() {
    let nameField = <HTMLInputElement>document.getElementById('name');
    if (nameField && nameField.value.trim() === '') {
      nameField.classList.add('error');
      return;
    }
    if (nameField && nameField.value.trim() !== '' && nameField.classList.contains('error')) {
      nameField.classList.remove('error');
    }
    this.http.post(base + '/api/campaign-groups/', {
      'learners': this.finalArray, 'name': this.groupName,
      'description': this.groupName, 'orgId': this.previewService.organizationId.getValue()
    }, this.httpOptions).toPromise().then(res => {
      let group = <any>res;
      // console.log('group:', group)
    })
    this.row = [{
      'name': 1,
      'groupName': nameField.value,
      'users': this.rows.length,
      'list': this.rows
    }];
    this.groupCreated = true;
    this.groupToBeCreated = false;
  }
  validateGroupName() {
    let nameField = <HTMLInputElement>document.getElementById('name');
    if (nameField && nameField.value.trim() === '') {
      nameField.classList.add('error');
    }

    if (nameField && nameField.value.trim() !== '' && nameField.classList.contains('error')) {
      nameField.classList.remove('error');
    }
  }

  getAppTimeSpentEvents() {
    this.http.post(proxyApi + '/esearch', {
      "query": {
        "bool": {
          "must": [
            {
              "match": {
                "orgID": this.previewService.organizationId.getValue()
              }
            },
            {
              "match": {
                "name": "AppTimeSpent"
              }

            }, {
              "range": {
                "date": {
                  "gte": new Date().getTime() - this.timeSpan,
                  "lte": new Date().getTime()
                }
              }
            }
          ]
        }
      },
      "aggs": {
        "APPTIMESPENT": {
          "significant_terms": {
            "field": "userID"
          },
          "aggs": {
            "TIMESPENT": {
              "scripted_metric": {
                "init_script": "_agg['transactions'] = []",
                "map_script": "_agg.transactions.add(Long.valueOf(doc['strValue'].value) - Long.valueOf(doc['category'].value))",
                "combine_script": "profit = 0; for (t in _agg.transactions) { profit = Long.valueOf(profit) + Long.valueOf(t) }; return profit",
                "reduce_script": "profit = 0; for (a in _aggs) { profit += a }; if(profit " + this.operator + " Long.valueOf(" + this.numericValue + ")) { return profit} else { return 0;}"

              }
            }
          }
        }
      }
    }, this.httpOptions).toPromise().then(res => {
      let result = <any>res;
      // console.log('result:', result);
      result = result.data.aggregations['APPTIMESPENT'].buckets;
      this.result = Number(result.length);
      result.forEach(element => {
        this.http.get(base + '/api/users/' + element.key, this.httpOptions).toPromise().then(e => {
          let user = <any>e;
          this.http.post(proxyApi + '/esearch', {
            "query": {
              "bool": {
                "must": [
                  {
                    "match": {
                      "orgID": this.previewService.organizationId.getValue()
                    }


                  },
                  {
                    "match": {
                      "userID": element.key
                    }
                  }

                ]
              }
            },
            "sort": [
              {
                "date": {
                  "order": "desc"
                }
              }
            ]
          }).toPromise().then(last => {
            let lastactive = <any>last;
            lastactive = lastactive.data.hits.hits[0]['_source']['date'];

            user.lastactive = new Date(lastactive);

            this.data.push(user);
            // console.log('lastactive:', lastactive)
          })

          this.finalArray.push(user)
        }, err => {
          // console.log('err:', err);
          this.result--;
        })
      });

      // console.log('data:', this.data);
      this.temp = [...this.data];
      this.rows = this.data;
      // console.log('temp:', this.temp);
      this.display = true;
    })
  }


  getTimeSpentEvents(item) {
    // console.log('item:', item)
    this.http.post(proxyApi + '/esearch', {
      "query": {
        "bool": {
          "must": [
            {
              "match": {
                "orgID": this.previewService.organizationId.getValue()
              }
            },
            {
              "match": {
                "name": item
              }

            },
            {
              "match": {
                "value1": this.value
              }

            }, {
              "range": {
                "date": {
                  "gte": new Date().getTime() - this.timeSpan,
                  "lte": new Date().getTime()
                }
              }
            }
          ]
        }
      },
      "aggs": {
        "APPTIMESPENT": {
          "significant_terms": {
            "field": "userID"
          },
          "aggs": {
            "TIMESPENT": {
              "scripted_metric": {
                "init_script": "_agg['transactions'] = []",
                "map_script": "_agg.transactions.add(Long.valueOf(doc['strValue'].value) - Long.valueOf(doc['category'].value))",
                "combine_script": "profit = 0; for (t in _agg.transactions) { profit = Long.valueOf(profit) + Long.valueOf(t) }; return profit",
                "reduce_script": "profit = 0; for (a in _aggs) { profit += a }; return profit"

              }
            }
          }
        }
      }
    }, this.httpOptions).toPromise().then(res => {
      let result = <any>res;
      // console.log('result:', result);
      result = result.data.aggregations['APPTIMESPENT'].buckets;
      this.result = result.length;
      result.forEach(element => {
        this.http.get(base + '/api/users/' + element.key, this.httpOptions).toPromise().then(e => {
          let user = <any>e
          // console.log('object:', user, typeof (user))
          this.http.post(proxyApi + '/esearch', {
            "query": {
              "bool": {
                "must": [
                  {
                    "match": {
                      "orgID": this.previewService.organizationId.getValue()
                    }


                  },
                  {
                    "match": {
                      "userID": element.key
                    }
                  }
                ]
              }
            },
            "sort": [
              {
                "date": {
                  "order": "desc"
                }
              }
            ]
          }).toPromise().then(last => {
            let lastactive = <any>last;
            lastactive = lastactive.data.hits.hits[0]['_source']['date'];
            if (user) {
              let item = { 'firstName': user.firstName, 'lastName': user.lastName, 'lastactive': new Date(lastactive) }

              this.data.push(item);
              // console.log('lastactive:', lastactive)
            }
          })
          if (user) {
            this.finalArray.push(user)
          }
        }, err => {
          // console.log('err:', err);
          this.result.length--;
        })
      });

      // console.log('data:', this.data);
      this.temp = [...this.data];
      this.rows = this.data;
      // console.log('temp:', this.temp);

      this.display = true;

    })
  }

  getCompletedEvents(item) {
    // console.log('item:', item)
    this.http.post(proxyApi + '/esearch', {
      "query": {
        "bool": {
          "must": [
            {
              "match": {
                "orgID": this.previewService.organizationId.getValue()
              }
            },
            {
              "match": {
                "name": item
              }

            },
            {
              "match": {
                "value1": this.value
              }

            }, {
              "range": {
                "date": {
                  "gte": new Date().getTime() - this.timeSpan,
                  "lte": new Date().getTime()
                }
              }
            }
          ]
        }
      },
      "aggs": {
        "APPTIMESPENT": {
          "significant_terms": {
            "field": "userID"
          },
          "aggs": {
            "TIMESPENT": {
              "scripted_metric": {
                "init_script": "_agg['transactions'] = []",
                "map_script": "_agg.transactions.add(Long.valueOf(doc['strValue'].value) - Long.valueOf(doc['category'].value))",
                "combine_script": "profit = 0; for (t in _agg.transactions) { profit = Long.valueOf(profit) + Long.valueOf(t) }; return profit",
                "reduce_script": "profit = 0; for (a in _aggs) { profit += a }; return profit"

              }
            }
          }
        }
      }
    }, this.httpOptions).toPromise().then(res => {
      let result = <any>res;
      // console.log('result:', result);
      result = result.data.aggregations['APPTIMESPENT'].buckets;
      this.result = result.length;
      result.forEach(element => {
        this.http.get(base + '/api/users/' + element.key, this.httpOptions).toPromise().then(e => {
          let user = <any>e
          // console.log('object:', user, typeof (user))
          this.http.post(proxyApi + '/esearch', {
            "query": {
              "bool": {
                "must": [
                  {
                    "match": {
                      "orgID": this.previewService.organizationId.getValue()
                    }


                  },
                  {
                    "match": {
                      "userID": element.key
                    }
                  }
                ]
              }
            },
            "sort": [
              {
                "date": {
                  "order": "desc"
                }
              }
            ]
          }).toPromise().then(last => {
            let lastactive = <any>last;
            lastactive = lastactive.data.hits.hits[0]['_source']['date'];
            if (user) {
              let item = { 'firstName': user.firstName, 'lastName': user.lastName, 'lastactive': new Date(lastactive) }

              this.data.push(item);
              // console.log('lastactive:', lastactive)
            }
          })
          if (user) {
            this.finalArray.push(user)
          }
        })
      });

      // console.log('data:', this.data);
      this.temp = [...this.data];
      this.rows = this.data;
      // console.log('temp:', this.temp);

      this.display = true;

    })
  }

}



