import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupsComponent } from './groups/groups.component';
import { CreateGroupComponent } from './create-group/create-group.component';
import { ListComponent } from './list/list.component';
import { FilteredGroupsComponent } from './filtered/filtered-groups.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'groups',
        component: GroupsComponent,
        data: {
          title: ''
        }
      },
      {
        path: 'createGroup',
        component: CreateGroupComponent,
        data: {
          title: ''
        }
      },
      {
        path: 'groupUsers',
        component: ListComponent,
        data: {
          title: ''
        }
      },
      {
        path: 'filteredGroups',
        component: FilteredGroupsComponent,
        data: {
          title: ''
        }

      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GroupingRoutingModule { }
