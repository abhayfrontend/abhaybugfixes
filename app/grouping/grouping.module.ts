import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CustomFormsModule } from 'ng2-validation';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import { GroupsComponent } from './groups/groups.component';
import { CreateGroupComponent } from './create-group/create-group.component';
import { GroupingRoutingModule } from './grouping-routing.module'
import { NgxEditorModule } from 'ngx-editor';

import { FilterPopupComponent } from './filter-popup/filter-popup.component';
import { ListComponent } from './list/list.component';
import { FilteredGroupsComponent } from './filtered/filtered-groups.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    GroupingRoutingModule,
    CustomFormsModule,
    MatchHeightModule,
    NgbModule,
    NgxDatatableModule,
    NgxEditorModule
  ],
  declarations: [GroupsComponent, CreateGroupComponent, FilterPopupComponent, ListComponent, FilteredGroupsComponent]
})
export class GroupingModule { }

