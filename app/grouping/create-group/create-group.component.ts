import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CampaignResourceService, CampaignGroupResourceService, UserResourceService, DeviceResourceService } from '../../sthaapak';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import { PreviewService } from '../../shared/auth/preview.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { base , proxyApi} from '../../shared/configs/util';
import { ChatHistoryResourceService } from '../../sthaapak/sdk/chatHistoryResource.service';
import * as AWS from '../../shared/configs/aws.config';
import { baseAnalytics } from '../../shared/configs/util';
import { by } from 'protractor';
// import { base, proxyApi } from '../shared/configs/util';
@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.scss']
})
export class CreateGroupComponent implements OnInit {



  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };


  courseStarted = [];
  courseFinished = [];
  videosWatched = [];
  modulesStarted = [];
  modulesFinished = [];
  @ViewChild('imageFileInput') imageInput: ElementRef;
  validFileExtensions = ['.jpg', '.jpeg', '.bmp', '.gif', '.png'];
  tokens = [];
  memberArray = [];
  groupName = "";
  groupDescription = "";
  orArrays = [];
  totalTimeSpentSum;
  averageTimeSpent;
  orArraysFiltered = [];
  result = [];
  departments = ['Technical', 'Sales', 'Faculty'];
  genders = ['MALE', 'FEMALE', 'OTHER'];
  locations = ['NOIDA',
    'DELHI',
    'ALLAHABAD',
    'MEERUT',
    'GURGAON',
    'LUCKNOW',
    'BAREILLY',
    'PUNE',
    'MUMBAI',
    'CHENNAI',
    'LONDON'
  ];
  types = ['Peer-To-Peer', 'Expert Chat', 'Broadcast'];
  operators = ['>', '<', '='];
  finalArray = [];
  filtersArray = [
    {
      id: 1,
      filters: [
        {
          id: 1,
          title: 'TOTAL DURATION',
          description: `Total number of minutes the user's device has had your app open`,
          totalDurationOperator: '>',
          icon: 'clock-o',
          type: 'totalDuration'
        }
      ]
    }

  ];
  imageurl;
  imageName;
  learnerIds = [];
  groupSelection = {
    selectedTypeName: 'Select a Group Type',
    selectedType: null,
    TypePopupVisible: false,
    selectedExpertName: 'Select an Expert',
    selectedExpert: null,
    ExpertPopupVisible: false,
    selectedAdmin: null,
    selectedAdminName: 'Select an Admin'
  }

  noFiltersApplied = true;
  initialButtonClicked = false;

  constructor(private groupService: CampaignGroupResourceService, private userService: UserResourceService, private router: Router, private chatHistory: ChatHistoryResourceService,
    private roleGuardService: RoleGuardService, private http: HttpClient, private previewService: PreviewService, private deviceService: DeviceResourceService) {
    if (this.filtersArray.length > 0) {
      this.noFiltersApplied = false;
    }
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }

  selectExpert(expert) {
    this.groupSelection.selectedExpert = expert.registeredNumber;
    this.groupSelection.selectedExpertName = expert.name;
  }

  selectAdmin(Admin) {
    this.groupSelection.selectedAdmin = Admin.registeredNumber;
    this.groupSelection.selectedAdminName = Admin.name;
  }

  selectType(type) {
    switch (type) {
      case 'Peer-To-Peer':
        this.groupSelection.selectedType = 1;
        this.groupSelection.selectedTypeName = 'Peer-To-Peer';
        break;

      case 'Expert Chat':
        this.groupSelection.selectedType = 2;
        this.groupSelection.selectedTypeName = 'Expert Chat'
        break;
      case 'Broadcast':
        this.groupSelection.selectedType = 3;
        this.groupSelection.selectedTypeName = 'Broadcast'
        break;
      default:
        break;
    }
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';

  }

  ngOnInit() {


    this.http.get(base + '/api/learners/filterby/orgid/' + this.previewService.organizationId.getValue(), this.httpOptions).subscribe(async (res) => {
      let result = <any>res;
      // console.log('userResou:', result);
      const results = await <any>this.getAppTime();
      const startedEvents = await <any>this.getModuleStartedEvents();
      const finishedEvents = await <any>this.getModuleFinishedEvents();
      // console.log('Results:', results);
      // console.log('Results2:', startedEvents);
      // console.log('Results3:', finishedEvents);
      let newsTimeSpent = this.previewService.newsTimeSpent.getValue();
      let newsTimeSpentBy = this.previewService.newsTimeSpentBy.getValue();
      // console.log("ORGID:", this.previewService.organizationId.getValue())
      result = result.filter(e => {
        let sum1, sum2, sum3, sum4 = 0;
        if (Number(e.orgId) === Number(this.previewService.organizationId.getValue()) || Number(e.organizationId) === Number(this.previewService.organizationId.getValue())) {

          e.userAppTime = results.spent[results.by.indexOf(e.registeredNumber)] || 0;
          if (startedEvents.modulesStarted.length) {
            e.modulesStarted = startedEvents.modulesStarted.map(el => {
              if (el.userID === e.registeredNumber) { return 1 }
              else { return 0; }
            }).reduce((sum1, ele) => sum1 + ele);
          }
          else { e.modulesStarted = 0 }
          if (startedEvents.courseStarted.length) {
            e.courseStarted = startedEvents.courseStarted.map(el => {
              if (el.userID === e.registeredNumber) { return 1 }
              else { return 0; }
            }).reduce((sum2, ele) => sum2 + ele);
          }
          else {
            e.courseStarted = 0;
          }
          if (finishedEvents.modulesFinished.length) {
            e.modulesFinished = finishedEvents.modulesFinished.map(el => {
              if (el.userID === e.registeredNumber) { return 1 }
              else { return 0; }
            }).reduce((sum3, ele) => sum3 + ele);
          } else { e.modulesFinished = 0 }
          if (finishedEvents.courseFinished.length) {
            e.courseFinished = finishedEvents.courseFinished.map(el => {
              if (el.userID === e.registeredNumber) { return 1 }
              else { return 0; }
            }).reduce((sum4, ele) => sum4 + ele);
          } else { e.courseFinished = 0; }
          e.newsTimeSpent = newsTimeSpent[newsTimeSpentBy.indexOf(e.registeredNumber)] || 0;
          this.finalArray.push(e);
          return true;
        }
        return false;
      });


      // console.log('userResou:', result);
      // console.log('finalArray:', this.finalArray);
      this.orArrays.push(result);
      // console.log(this.orArrays);
      this.orArraysFiltered.push(result);
      this.result = result;
    });

    this.http.get(base + '/api/members', this.httpOptions).subscribe(res => {
      let result = <any>res;
      result = result.filter(e => Number(e.category) === Number(this.previewService.organizationId.getValue()));
      // console.log('members:', result);
      this.memberArray = result;

    });

  }

  itemSelected(event, filterGroup) {
    if (this.noFiltersApplied) {
      this.noFiltersApplied = false;
      let groupObj = {
        id: 1,
        filters: []
      }
      groupObj.filters.push(event);
      this.filtersArray.push(groupObj);
    } else if (filterGroup !== null) {
      this.addToArray(event, filterGroup.id);
      filterGroup.visible = false;
    }
  }

  validationOngroupName() {
    let elm = <HTMLInputElement>document.getElementById('groupName');
    if (elm.value.length < 1 || elm.value.length > 100) {
      swal('', 'Please enter atleast 1 character and atmost 100 characters.');
      // now focus it

    }
  }

  validationOndescriptionGroup() {
    // console.log('Eorking');
    let elm = <HTMLInputElement>document.getElementById('groupDescription');
    if (elm.value.length < 1 || elm.value.length > 200) {
      swal('', 'Please enter atleast 1 character and atmost 200 characters.');
      // now focus it

    }
  }

  addToArray(filterObj, filterGroupId) {
    for (let i = 0; i < this.filtersArray.length; i++) {
      if (this.filtersArray[i].id === filterGroupId) {
        let id = this.filtersArray[i].filters.length + 1
        filterObj.id = id;
        this.filtersArray[i].filters.push(filterObj);
        return;
      }
    }
  }

  createNewgroup() {
    this.orArrays.push(this.result);
    this.orArraysFiltered.push(this.result);
    let groupObj = {
      id: this.filtersArray.length + 1,
      filters: []
    }
    this.filtersArray.push(groupObj);
    // console.log('Arrays:', this.orArrays);
    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  removeFilter(filterGroup, filterItem) {
    let row = null;
    let col = null;
    let canColBeDeleted = false;
    let canRowBeDeleted = false;
    for (let i = 0; i < this.filtersArray.length; i++) {
      if (this.filtersArray[i].id === filterGroup.id) {
        for (let j = 0; j < this.filtersArray[i].filters.length; j++) {
          if (this.filtersArray[i].filters[j].id === filterItem.id) {
            row = i;
            col = j;
            canColBeDeleted = true;
            if (this.filtersArray[i].filters.length === 1) {
              canRowBeDeleted = true;

            }
          }
        }
      }
    }

    if (canColBeDeleted) {
      this.filtersArray[row].filters.splice(col, 1);
    }

    if (canRowBeDeleted) {
      this.filtersArray.splice(row, 1);
      this.orArrays.splice(row, 1);
      this.orArraysFiltered.splice(row, 1);
    }



    // console.log(this.filtersArray);
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  filterArray(filterGroup) {
    this.filtersArray.forEach((ele, i) => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          // console.log('filternumber:', e);
        });
      }
    })
  }

  changeAgeOperator(filterGroup, filterItem, event) {
    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.ageOperator = event.target.value;

          }

        });

      }
    })
    // console.log('filterArray:', this.filtersArray);
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  changeAgeFilter(filterGroup, filterItem, event) {

    this.filtersArray.forEach((ele, i) => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.ageFilter = event.target.value;

            let newArray = this.orArrays[i].filter(user => {
              // console.log('user:', user.age);
              switch (e.ageOperator) {
                case '>':
                  if (user.age && (user.age >= event.target.value)) {
                    return true;
                  }
                  break;
                case '<':
                  if (user.age && (user.age <= event.target.value)) {
                    return true;
                  }
                  break;
                case '=':
                  if (user.age && (Number(user.age) === Number(event.target.value))) {
                    return true;
                  }
                  break;

              }

            })
            this.orArraysFiltered[i] = newArray;
            // console.log("ORARRAYS", this.orArraysFiltered);
            // console.log('NEWARR:', newArray);
          }

        });

      }
    })
    // console.log('filterArray:', this.filtersArray);
    // console.log('arrrays:', this.orArrays);

    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  changemodulesStartedOperator(filterGroup, filterItem, event) {
    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.modulesStartedOperator = event.target.value;

          }

        });

      }
    })
    // console.log('filterArray:', this.filtersArray);
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  changemodulesStartedFilter(filterGroup, filterItem, event) {

    this.filtersArray.forEach((ele, i) => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.modulesStartedFilter = event.target.value;

            let newArray = this.orArrays[i].filter(user => {
              // console.log('user:', user.modulesStarted);
              switch (e.modulesStartedOperator) {
                case '>':
                  if (user.modulesStarted && (user.modulesStarted >= event.target.value)) {
                    return true;
                  }
                  break;
                case '<':
                  if (user.modulesStarted && (user.modulesStarted <= event.target.value)) {
                    return true;
                  }
                  break;
                case '=':
                  if (user.modulesStarted && (Number(user.modulesStarted) === Number(event.target.value))) {
                    return true;
                  }
                  break;

              }

            })
            this.orArraysFiltered[i] = newArray;
            // console.log("ORARRAYS", this.orArraysFiltered);
            // console.log('NEWARR:', newArray);
          }

        });

      }
    })
    // console.log('filterArray:', this.filtersArray);
    // console.log('arrrays:', this.orArrays);

    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }
  changemodulesFinishedOperator(filterGroup, filterItem, event) {
    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.modulesFinishedOperator = event.target.value;

          }

        });

      }
    })
    // console.log('filterArray:', this.filtersArray);
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  changemodulesFinishedFilter(filterGroup, filterItem, event) {

    this.filtersArray.forEach((ele, i) => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.modulesFinishedFilter = event.target.value;

            let newArray = this.orArrays[i].filter(user => {
              // console.log('user:', user.modulesFinished);
              switch (e.modulesFinishedOperator) {
                case '>':
                  if (user.modulesFinished && (user.modulesFinished >= event.target.value)) {
                    return true;
                  }
                  break;
                case '<':
                  if (user.modulesFinished && (user.modulesFinished <= event.target.value)) {
                    return true;
                  }
                  break;
                case '=':
                  if (user.modulesFinished && (Number(user.modulesFinished) === Number(event.target.value))) {
                    return true;
                  }
                  break;

              }

            })
            this.orArraysFiltered[i] = newArray;
            // console.log("ORARRAYS", this.orArraysFiltered);
            // console.log('NEWARR:', newArray);
          }

        });

      }
    })
    // console.log('filterArray:', this.filtersArray);
    // console.log('arrrays:', this.orArrays);

    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  changecourseStartedOperator(filterGroup, filterItem, event) {
    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.courseStartedOperator = event.target.value;
          }
        });

      }
    })
    // console.log('filterArray:', this.filtersArray);
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  changecourseStartedFilter(filterGroup, filterItem, event) {

    this.filtersArray.forEach((ele, i) => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.courseStartedFilter = event.target.value;

            let newArray = this.orArrays[i].filter(user => {
              // console.log('user:', user.courseStarted);
              switch (e.courseStartedOperator) {
                case '>':
                  if (user.courseStarted && (user.courseStarted >= event.target.value)) {
                    return true;
                  }
                  break;
                case '<':
                  if (user.courseStarted && (user.courseStarted <= event.target.value)) {
                    return true;
                  }
                  break;
                case '=':
                  if (user.courseStarted && (Number(user.courseStarted) === Number(event.target.value))) {
                    return true;
                  }
                  break;

              }

            })
            this.orArraysFiltered[i] = newArray;
            // console.log("ORARRAYS", this.orArraysFiltered);
            // console.log('NEWARR:', newArray);
          }

        });

      }
    })
    // console.log('filterArray:', this.filtersArray);
    // console.log('arrrays:', this.orArrays);

    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
  }

  changecourseFinishedOperator(filterGroup, filterItem, event) {
    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.courseFinishedOperator = event.target.value;

          }

        });

      }
    })
    // console.log('filterArray:', this.filtersArray);
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  changecourseFinishedFilter(filterGroup, filterItem, event) {

    this.filtersArray.forEach((ele, i) => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.courseFinishedFilter = event.target.value;

            let newArray = this.orArrays[i].filter(user => {
              // console.log('user:', user.courseFinished);
              switch (e.courseFinishedOperator) {
                case '>':
                  if (user.courseFinished && (user.courseFinished >= event.target.value)) {
                    return true;
                  }
                  break;
                case '<':
                  if (user.courseFinished && (user.courseFinished <= event.target.value)) {
                    return true;
                  }
                  break;
                case '=':
                  if (user.courseFinished && (Number(user.courseFinished) === Number(event.target.value))) {
                    return true;
                  }
                  break;

              }

            })
            this.orArraysFiltered[i] = newArray;
            // console.log("ORARRAYS", this.orArraysFiltered);
            // console.log('NEWARR:', newArray);
          }

        });

      }
    })
    // console.log('filterArray:', this.filtersArray);
    // console.log('arrrays:', this.orArrays);

    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  changeGender(filterGroup, filterItem, event) {
    // console.log('arrrays:', this.orArrays);

    this.filtersArray.forEach((ele, i) => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            // console.log('i:', i)
            e.genderFilter = event.target.value;
            let newArray = this.orArrays[i].filter(user => {
              // console.log('user:', user.gender);
              if (user.gender && (user.gender === event.target.value)) {
                return true;
              }
            })
            this.orArraysFiltered[i] = newArray;
          }
        });
      }
    })

    // console.log('filterArray:', this.filtersArray);
    // console.log('arrrays:', this.orArrays);

    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })
    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }


  changedepartment(filterGroup, filterItem, event) {
    // console.log('arrrays:', this.orArrays);

    this.filtersArray.forEach((ele, i) => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            // console.log('i:', i)
            e.locationFilter = event.target.value;
            let newArray = this.orArrays[i].filter(user => {
              // console.log('user:', user.department);
              if (user.department && (user.department === event.target.value)) {
                return true;
              }
            })
            this.orArraysFiltered[i] = newArray;
          }
        });
      }
    })

    // console.log('filterArray:', this.filtersArray);
    // console.log('arrrays:', this.orArrays);

    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })
    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }


  changelocation(filterGroup, filterItem, event) {
    // console.log('arrrays:', this.orArrays);

    this.filtersArray.forEach((ele, i) => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            // console.log('i:', i)
            e.locationFilter = event.target.value;
            let newArray = this.orArrays[i].filter(user => {
              // console.log('user:', user.city);
              if (user.city && (user.city === event.target.value)) {
                return true;
              }
            })
            this.orArraysFiltered[i] = newArray;
          }
        });
      }
    })

    // console.log('filterArray:', this.filtersArray);
    // console.log('arrrays:', this.orArrays);

    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })
    this.finalArray = finalArray;
    // console.log('FinalArray:', finalArray)
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  changetotalDurationOperator(filterGroup, filterItem, event) {
    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.totalDurationOperator = event.target.value;
          }
        });
      }
    })
    // console.log('filterArray:', this.filtersArray);
    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })
    this.finalArray = finalArray;

    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  changetotalDurationFilter(filterGroup, filterItem, event) {
    this.filtersArray.forEach((ele, i) => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach((e) => {
          if (filterItem.id === e.id) {
            e.totalDurationFilter = event.target.value;
            // console.log(JSON.stringify(this.orArrays));
            let newArray = this.orArrays[i].filter((user) => {
              // console.log('user:', user.registeredNumber);


              // console.log('USERAPPTIME:', user.userAppTime);
              switch (e.totalDurationOperator) {
                case '>':
                  if (user.userAppTime > event.target.value) {
                    return true;
                  }
                  break;
                case '<':
                  if (user.userAppTime < event.target.value) {
                    return true;
                  }
                  break;
                case '=':
                  if (Number(user.userAppTime) === Number(event.target.value)) {
                    return true;
                  }
                  break;

              }
            });
            this.orArraysFiltered[i] = newArray;
            // console.log("ORARRAYS", this.orArraysFiltered);
            // console.log('NEWARR:', newArray);
          }
        });
      }
    })
    // console.log('filterArray:', this.filtersArray);
    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })
    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';

  }

  changenewsDurationOperator(filterGroup, filterItem, event) {
    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.newsDurationOperator = event.target.value;
          }
        });
      }
    })
    // console.log('filterArray:', this.filtersArray);
    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })
    this.finalArray = finalArray;

    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  changenewsDurationFilter(filterGroup, filterItem, event) {
    this.filtersArray.forEach((ele, i) => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach((e) => {
          if (filterItem.id === e.id) {
            e.newsDurationFilter = event.target.value;
            let newArray = this.orArrays[i].filter((user) => {
              // console.log('user:', user.registeredNumber);
              // console.log('USERNEWSTIME:', user.newsTimeSpent);
              switch (e.newsDurationOperator) {
                case '>':
                  if (user.newsTimeSpent > event.target.value) {
                    return true;
                  }
                  break;
                case '<':
                  if (user.newsTimeSpent < event.target.value) {
                    return true;
                  }
                  break;
                case '=':
                  if (Number(user.newsTimeSpent) === Number(event.target.value)) {
                    return true;
                  }
                  break;

              }
            });
            this.orArraysFiltered[i] = newArray;
            // console.log("ORARRAYS", this.orArraysFiltered);
            // console.log('NEWARR:', newArray);
          }
        });
      }
    })
    // console.log('filterArray:', this.filtersArray);
    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })
    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';

  }

  changeSessionCountOperator(filterGroup, filterItem, event) {

    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.SessionCountOperator = event.target.value;
          }
        });
      }
    })
    // console.log('filterArray:', this.filtersArray);
    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';

  }

  changeSessionCountFilter(filterGroup, filterItem, event) {

    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.SessionCountFilter = event.target.value;
          }
        });
      }
    })
    // console.log('filterArray:', this.filtersArray);
    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';

  }

  changedojFilter(filterGroup, filterItem, event) {
    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.dojFilter = event.target.value;
          }
        });
      }
    })
    // console.log('filterArray:', this.filtersArray);
    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';

  }

  changedojOperator(filterGroup, filterItem, event) {
    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.dojOperator = event.target.value;
          }
        });
      }
    })
    // console.log('filterArray:', this.filtersArray);
    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;

    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }




  changelastSessionFilter(filterGroup, filterItem, event) {
    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.lastSessionFilter = event.target.value;
          }
        });
      }
    })
    // console.log('filterArray:', this.filtersArray);
    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;

    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  changelastSessionOperator(filterGroup, filterItem, event) {
    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.lastSessionOperator = event.target.value;
          }
        });
      }
    })
    // console.log('filterArray:', this.filtersArray);
    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;

    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  changefirstSessionOperator(filterGroup, filterItem, event) {
    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.firstSessionOperator = event.target.value;
          }
        });
      }
    })
    // console.log('filterArray:', this.filtersArray);
    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';

  }

  changefirstSessionFilter(filterGroup, filterItem, event) {
    this.filtersArray.forEach(ele => {
      if (ele.id === filterGroup.id) {
        filterGroup.filters.forEach(e => {
          if (filterItem.id === e.id) {
            e.firstSessionFilter = event.target.value;
          }
        });
      }
    })
    // console.log('filterArray:', this.filtersArray);
    let finalArray = [];
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        return true;
      }
    })

    this.finalArray = finalArray;
    this.groupSelection.selectedExpert = null;
    this.groupSelection.selectedExpertName = 'Select an Expert';
    this.groupSelection.selectedAdmin = null;
    this.groupSelection.selectedAdminName = 'Select an Admin';
  }

  getTokens(learners) {
    this.deviceService.getAllDevicesUsingGET().subscribe(res => {
      let result: any = res;
      // console.log('devices:', result);
      for (let i = 0; i < learners.length; i++) {
        for (let j = 0; j < result.length; j++) {
          if (learners[i]['registeredNumber'] === result[j]['phoneNumber']) {
            this.tokens.push(result[j]['fcmToken']);
            this.learnerIds.push(learners[i].id);
            // console.log('token+$i: ', result[j]['phoneNumber'], result[j]['fcmToken']);
          }
        }
      }
      return this.tokens;
    });
  }
  browseImageClicked() {
    let el: any = this.imageInput.nativeElement;
    el.click();
  }

  GroupImageChanged(event) {
    let timerInterval1;
    let _URL = window.URL;
    let error = false;
    if (event.target.files && event.target.files[0]) {

      this.imageName = event.target.files[0].name;




      let sFileName = event.target.files[0].name;
      // console.log('sfilename: ', sFileName);
      let blnValid = false;
      for (let j = 0; j < this.validFileExtensions.length; j++) {
        let sCurExtension = this.validFileExtensions[j];
        if (sFileName.slice(sFileName.length - sCurExtension.length, sFileName.length).toLowerCase() === sCurExtension.toLowerCase()) {
          blnValid = true;
          break;
        }
      }
      if (!blnValid) {
        swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensions.join(', '));
        return false;
      }
      // console.log('size:', event.target.files[0].size);
      if ((event.target.files[0].size < 1000) || (event.target.files[0].size > 2000000)) {
        swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 2 Mb');
        return false;
      }



      const file = event.target.files[0];

      let reader = new FileReader();


      //Initiate the FileReader object.
      //Read the contents of Image File.
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e) => {
        let that = file
        //Initiate the JavaScript Image object.
        let image = new Image();
        //Set the Base64 string return from FileReader as source.
        image.src = _URL.createObjectURL(file);
        image.onload = () => {
          //Determine the Height and Width.
          let height = image.height;
          let width = image.width;
          swal({
            title: 'Uploading!',
            onOpen: () => {
              swal.showLoading();
              Promise.resolve(this.uploadFile(file)).then(location =>
                this.imageurl = <any>location
              );
              timerInterval1 = setInterval(() => {
                if (this.imageurl) {
                  swal.close();
                  clearInterval(timerInterval1);
                  // console.log('time1')
                }
              }, 100)
            },
            onClose: () => {
              clearInterval(timerInterval1)
            }
          }).then(ans => {
            clearInterval(timerInterval1);
          });

          return true;
        };
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }


  uploadFile(file) {
    let upload;
    upload = AWS.s3.upload({ Key: this.previewService.organizationId.getValue() + '/group/image/' + new Date() + file.name, Bucket: AWS.bucketName, Body: file, ACL: 'public-read' });
    let promise = upload.promise();
    return promise.then((data) => {
      // console.log(data);
      return data.Location;

    }, (err) => {
      // console.log(err);
    });

  }
  sendUpdate(category, subcategory, screen) {

    let message = {
      topic: this.previewService.organizationId.getValue(),
      data: {
        silent: true,
        save: false,
        title: category,
        body: subcategory,
        screenType: screen,
        image: 'jpg',
        specialParameters: 'll',
        tags: 'Silent',
        orgId: this.previewService.organizationId.getValue(),
        name: screen
      }

    }
    // console.log('message:', message);
    this.http.post(proxyApi + '/contentUpdate', message, this.httpOptions).subscribe((response) => {
      // Response is a message ID string.
      // console.log('Successfully sent message:', response);

    }, (error) => {
      // console.log('Error sending message:', error);
    });



  }

  publishGroup() {

    if (!this.validateContentForm()) {
      return;
    }
    // console.log("inside function");
    let finalArray = [];
    let learnerIds = [];
    let gtype = null;
    let expert = null;
    let admin = null;
    let expertMessage = '';
    let adminMessage = '';
    this.orArraysFiltered.forEach(e => {
      finalArray = [...finalArray, ...e]
    });

    if (this.orArraysFiltered.length === 0) {
      finalArray = this.result;
    }

    finalArray = finalArray.filter((e, i) => {
      if (finalArray.indexOf(e) === i) {
        learnerIds.push(e.id)
        return true;
      }
    })

    // console.log('membersAraay', this.memberArray);
    // console.log('learnerIds:', this.learnerIds);

    this.finalArray = finalArray;
    let memberArray = [];

    this.memberArray.forEach(e => {
      if (learnerIds.indexOf(Number(e.learnerId)) > -1) {
        memberArray.push(e);
      };
    });
    if (this.groupSelection.selectedType) {
      gtype = this.groupSelection.selectedType;
      expertMessage = 'You have been added to "' +this.groupName.trim() +'" '+'group';
    }
    if (this.groupSelection.selectedExpert) {
      expert = this.groupSelection.selectedExpert;
      expertMessage = 'You have been added to the expert channel of "' +this.groupName.trim() +'" '+ 'group';
    }

    if (this.groupSelection.selectedAdmin) {
      admin = this.groupSelection.selectedAdmin;
      adminMessage = 'You have been added to the Admin channel of "'+this.groupName.trim() +'" '+ 'group';
    }
    // console.log('learnerIds:', learnerIds);
    // console.log('fin:', finalArray);
    // console.log('mem:', memberArray);
    // console.log('filterArrays:', this.filtersArray);
    swal({
      title: 'Are you sure you want to create this group?',
      text: "This will create a group " + this.groupName + " with " + finalArray.length + " users",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Yes, create it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then((confirm) => {
      // console.log('con:', confirm)
      
      if (confirm.value === true) {
        let obj = {
          'learners': this.finalArray, 'name': this.groupName.trim(), selectQuery: expert ? expert : admin ? 'Broadcast/' + admin : null,
          'description': this.groupDescription, 'orgId': this.previewService.organizationId.getValue(), 'value':'', 'path':'true'
        }
        this.http.post(base + '/api/campaign-groups', obj,
          this.httpOptions).subscribe(res => {
            let result = <any>res;
            this.sendUpdate(obj.value, obj.path, "GROUPS");
            // console.log('group:', result);
            Promise.resolve(this.getTokens(this.finalArray)).then(tokens => {
              // console.log('tokens:', tokens);
              // console.log('tokensGlobal:', this.tokens);
            }).then(resolved => console.log());// console.log('Content:', resolved));
            let obj1 = {
              'members': memberArray, 'name': this.groupName.trim(), type: gtype, email: expert || admin,
              'description': this.imageurl, 'category': this.previewService.organizationId.getValue(),'value':'',path:'true'
            } 
            this.http.post(base + '/api/support-groups', obj1,
              this.httpOptions).subscribe(resp => {
                let result1 = <any>resp;
                this.sendUpdate(obj.value, obj.path, "GROUPS");
                // console.log('supportgroup:', result1);
                this.http.get(base + '/api/campaign-groups/filterby/'+this.previewService.organizationId.getValue(), this.httpOptions).subscribe(cgroups => {
                  let campaignGroups = <any>cgroups;
                  // console.log(JSON.stringify(campaignGroups));
                  this.previewService.editCampaignGroup(campaignGroups);
                })
                this.http.get(base + '/api/support-groups', this.httpOptions).subscribe(sgroups => {
                  let supportGroups = <any>sgroups;
                  // console.log(JSON.stringify(supportGroups));
                  this.previewService.editSupportGroup(supportGroups);
                })

                let deeplink = 'productiviseapp://chatGroups?type='+this.groupSelection.selectedType;
                // console.log("deeplink : " + deeplink);

                this.sendNotification({
                  'notificationName': 'You are a member of a group now.', 'cronExpression': null,
                  'notificationText': expertMessage || adminMessage,
                  'thumbnailImage': 'Info', 'tags': 'Notify', 'tokens': this.tokens,
                  'specialParameters': deeplink, screenType: 'Chat', title: 'Chat', orgId: this.previewService.organizationId.getValue(),
                  cgroupId: result.id, userId: this.learnerIds.join(), sgroupId: result1.id,'template': deeplink
                })
                finalArray.forEach(learner => {
                  this.http.get(base + '/api/users/' + learner.registeredNumber, this.httpOptions).subscribe(user => {
                    let userList = <any>user;
                    if (userList.aadharNumber) {
                      userList.aadharNumber += ',' + result.id;
                    }
                    else {
                      userList.aadharNumber = result.id;
                    }
                    this.http.put(base + '/api/users', userList, this.httpOptions).subscribe(putUser => {
                      // console.log(putUser)
                    })
                  })

                  let learnerName = learner.name.split(' ')[0];
                  let learnerLastName = learner.name.split(' ')[1] === 'null' ? '' : learner.name.split(' ')[1];
                  this.chatHistory.createChatHistoryUsingPOST({
                    "attachment": true,
                    "attachmentLink": "string",
                    "category": "string",
                    "description": learnerName + ' ' + learnerLastName + " has been added to the group.",
                    "from": "string",
                    "group": result1.id,
                    "name": "string",
                    "orgId": this.previewService.organizationId.getValue(),
                    "subCategory": "broadcast",
                    "tags": "string",
                    "to": "string",
                    "ts": new Date().getTime()
                  }).subscribe(chat => {
                    // console.log(chat);
                        console.log();
                  })
                })


              });
            swal(
              'Group Created Successfully',
              'Your group was created.'
            ).then((done) => {
              if (done) {
                this.router.navigate(['/grouping/groups'])
              }

            })

          });
      }
      else {
        swal(
          'Cancelled',
          'Your group was not created.',
          'error'
        )
      }
    }, (dismiss) => {
      // console.log('dismis:', dismiss)
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Your group was not created.',
          'error'
        )
      }
    })
  }


  sendNotification({ notificationName, cronExpression, notificationText, thumbnailImage, tags, tokens, specialParameters, screenType, title, orgId, sgroupId, cgroupId, userId,template }) {

    // console.log('notification:', JSON.parse(JSON.stringify({

    //   'notificationDTO': {
    //     'body': notificationText,
    //     'campaignId': 0,
    //     'ctaURL': 'string',
    //     'description': 'Info',
    //     'image': thumbnailImage,
    //     'lifeCycleId': 0,
    //     'microContentId': 0,
    //     'name': notificationName,
    //     'scheduled': cronExpression,
    //     'screenType': screenType,
    //     'sequence': sgroupId,
    //     'sound': new Date().getTime(),
    //     'specialParameters': specialParameters,
    //     'tags': tags,
    //     'template': 'string',
    //     'title': title,
    //     'orgId': orgId,
    //     'groupId': cgroupId,
    //     'userId': userId
    //   }, 'deviceID': tokens.join()
    // })));

    this.http.post(base + '/api/notifications/send?deviceID=' + String(tokens.join()), JSON.parse(JSON.stringify({


      'body': notificationText,
      'campaignId': 0,
      'ctaURL': 'string',
      'description': 'Info',
      'image': thumbnailImage,
      'lifeCycleId': 0,
      'microContentId': 0,
      'name': notificationName,
      'scheduled': cronExpression,
      'screenType': screenType,
      'sequence': sgroupId,
      'sound': new Date().getTime(),
      'specialParameters': specialParameters,
      'tags': tags,
      'template': template,
      'title': title,
      'orgId': orgId,
      'groupId': cgroupId,
      'userId': userId
    })),
      this.httpOptions).subscribe(response => {
        // console.log('response subscribe:', response);
        let result: any = response;
        this.http.post(base + '/api/notifications', {
          'body': notificationText,
          'campaignId': 82551,
          'ctaURL': 'string',
          'description': 'Info',
          'image': thumbnailImage,
          'lifeCycleId': 257501,
          'microContentId': 1253,
          'name': notificationName,
          'scheduled': cronExpression,
          'screenType': screenType,
          'sequence': sgroupId,
          'sound': new Date().getTime(),
          'specialParameters': specialParameters,
          'tags': tags,
          'template': 'string',
          'title': title,
          'orgId': orgId,
          'groupId': cgroupId,
          'userId': userId
        }, this.httpOptions)
          .subscribe(res => {
            // console.log('notification:', res);


          });
        // this.notificationSuccessfullySent(this.pushNotificationName || this.popNotificationText, result.name ? true : false);
        // this.resetFields();
      });


  }


  validateContentForm() {
    // console.log('Enter')
    let isValid = true;
    let contentNameInput, contentTypeInput, contentDescriptionInput, categoryInput, subCategoryInput, imageInput, typeInput, negativeInput;
    if (!this.groupName || this.groupName.trim() === "") {
      // console.log('groupName', this.groupName);
      typeInput = document.getElementById('groupName');
      typeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      // console.log('groupName', this.groupName);
      typeInput = document.getElementById('groupName');
      typeInput.classList.remove('error');
      isValid = true;
    }

    if (!this.groupDescription || this.groupDescription.trim() === "") {
      contentNameInput = document.getElementById('groupDescription');
      contentNameInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      contentNameInput = document.getElementById('groupDescription');
      contentNameInput.classList.remove('error');
      isValid = true;
    }

    if (!this.groupSelection.selectedType) {
      // console.log('Group', this.groupSelection);
      contentDescriptionInput = document.getElementById('groupType');
      contentDescriptionInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      // console.log('Group', this.groupSelection);
      contentDescriptionInput = document.getElementById('groupType');
      if (contentDescriptionInput.classList.contains('error')) {
        contentDescriptionInput.classList.remove('error');
      }
      isValid = true;
    }

    if (this.groupSelection.selectedType === 2 && !this.groupSelection.selectedExpert) {
      // console.log('Group', this.groupSelection);
      categoryInput = document.getElementById('expert');
      categoryInput.classList.add('error');
      isValid = false;
      return isValid;
    } else if (this.groupSelection.selectedType === 2 && this.groupSelection.selectedExpert) {
      // console.log('Group', this.groupSelection);
      categoryInput = document.getElementById('expert');
      if (categoryInput.classList.contains('error')) {
        categoryInput.classList.remove('error');
      }
      isValid = true;
    }

    if (!this.imageName || !this.imageurl) {
      // console.log('Group', this.groupSelection);
      categoryInput = document.getElementById('imageUploader');
      categoryInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      // console.log('Group', this.groupSelection);
      categoryInput = document.getElementById('imageUploader');
      categoryInput.classList.remove('error');
      isValid = true;
    }



    return isValid;
  }
  getAppTime() {
    return this.http.get(baseAnalytics + '/api/_search/events?query=(name%3DAppTimeSpent)AND(orgID%3D' + this.previewService.organizationId.getValue() + ')',
      this.httpOptions).toPromise().then(res => {

        let results: any = res;
        // console.log('timespentt:', results);

        let TotalTimeSpent = [];
        let TotalTimeSpentBy = [];
        for (let index = 0; index < results.length; index++) {
          if (TotalTimeSpentBy.indexOf(results[index]['userID']) < 0 && String(TotalTimeSpentBy.indexOf(results[index]['userID'])).length > 0) {
            TotalTimeSpentBy.push(results[index]['userID']);
            TotalTimeSpent.push(results[index]['strValue'] - results[index]['category']);
          }
          else {
            TotalTimeSpent[TotalTimeSpentBy.indexOf(results[index]['userID'])] += (results[index]['strValue'] - results[index]['category']);
          }
        }
        // console.log('Total Time Spent By count:', TotalTimeSpentBy);
        // console.log('Total Time Spent count:', TotalTimeSpent);
        for (let j = 0; j < TotalTimeSpent.length; j++) {
          TotalTimeSpent[j] = TotalTimeSpent[j] / 60000;
          TotalTimeSpent[j] = Math.round((TotalTimeSpent[j] + 0.00001) * 100) / 100;
          TotalTimeSpent[j] = Math.round(TotalTimeSpent[j]);
          // console.log('TimeSpent After Sum:', TotalTimeSpent[j]);

        }
        return { by: TotalTimeSpentBy, spent: TotalTimeSpent };

      }, error => {
        return [[], []];
      });
  }


  getModuleStartedEvents() {
    this.courseStarted = [];
    this.courseFinished = [];
    this.videosWatched = [];
    /*
       this.http.get(base + '/api/_search/events?query=(name1%3DVideo_time_clicked)AND(userID%3D' + this.learner.registeredNumber + ')',
           this.httpOptions).subscribe(res => {
               let results = <any>res;
 
               results.forEach(element => {
                   let t1 = element.strValue.split(',')[0];
                   let t2 = element.strValue.split(',')[1];
                   let put = true;
                   if (this.videosWatched.length < 1 && t1 > 0 && t2 > t1 && element.value2.indexOf('&&')) {
                       this.videosWatched.push({
                           category: element.name, subcategory: element.category, name: element.value2.split('&&')[1],
                           count: 1, time: (t2 - t1)
                       })
                   }
                   if (this.videosWatched.length && t1 > 0 && t2 > t1 && element.value2.indexOf('&&')) {
                       this.videosWatched.forEach((el, i) => {
                           if (el.category === element.name && el.subcategory === element.category && el.name === element.value2.split('&&')[1]) {
                               put = false;
 
                               el.count++;
                               el.time += (t2 - t1);
 
                           }
 
 
                       });
                       if (put && element.value2.indexOf('&&')) {
                           this.videosWatched.push({
                               category: element.name, subcategory: element.category, name: element.value2.split('&&')[1],
                               count: 1, time: (t2 - t1)
                           })
 
                       }
                   }
               });
 
               // console.log('videosWatched:', this.videosWatched);
               this.videosWatched.sort((a, b) => {
                   return b.count - a.count;
               });
               this.videosWatched = this.videosWatched.slice(0, 5);
           }, error => {
               this.videosWatched = [];
           });
*/

    return this.http.get(baseAnalytics + '/api/_search/events?query=(name1%3DMODULE_clicked)AND(orgID%3D' + this.previewService.organizationId.getValue() + ')',
      this.httpOptions).toPromise().then(res => {
        let results = <any>res;
        // console.log('MODULES CLICKED:', results);
        results.forEach(element => {

          let put = true;
          if (this.courseStarted.length < 1) {
            this.courseStarted.push({
              category: element.name, subcategory: element.category, name: element.value2, userID: element.userID
            })
          }
          if (this.courseStarted.length) {
            this.courseStarted.forEach((el, i) => {
              if (el.category === element.name && el.subcategory === element.category && el.name === element.value2 && el.userID === element.userID) {
                put = false;
              }
            });
            if (put) {
              this.courseStarted.push({
                category: element.name, subcategory: element.category, name: element.value2, userID: element.userID
              })

            }
          }


          let put2 = true;
          if (this.modulesStarted.length < 1) {
            this.modulesStarted.push({
              category: element.name, subcategory: element.category, name: element.value2, userID: element.userID,
              strValue: element.strValue
            })
          }
          if (this.modulesStarted.length) {
            this.modulesStarted.forEach((el, i) => {
              if (el.category === element.name && el.subcategory === element.category && el.name === element.value2 && el.strValue === element.strValue && el.userID === element.userID) {
                put2 = false;

              }


            });
            if (put2) {
              this.modulesStarted.push({
                category: element.name, subcategory: element.category, name: element.value2,
                strValue: element.strValue, userID: element.userID
              })

            }
          }

        });

        // console.log('courseStarted:', this.courseStarted);

        return { courseStarted: this.courseStarted, modulesStarted: this.modulesStarted }


      }, error => {
        this.courseStarted = [];
        this.modulesStarted = [];
      });
  }
  getModuleFinishedEvents() {
    this.courseStarted = [];
    this.courseFinished = [];
    this.videosWatched = [];

    return this.http.get(baseAnalytics + '/api/_search/events?query=(name1%3Dmicrolearning_module_completed_clicked)AND(orgID%3D' +
      this.previewService.organizationId.getValue() + ')', this.httpOptions).toPromise().then(res => {
        let results = <any>res;
        // console.log('MODULES Finished:', results);

        results.forEach(element => {

          let put = true;
          if (this.courseFinished.length < 1) {
            this.courseFinished.push({
              category: element.name, subcategory: element.category, name: element.value2,
              count: 1, userID: element.userID
            })
          }
          if (this.courseFinished.length) {
            this.courseFinished.forEach((el, i) => {
              if (el.category === element.name && el.subcategory === element.category && el.name === element.value2 && el.userID === element.userID) {
                put = false;
              }
            });
            if (put) {
              this.courseFinished.push({
                category: element.name, subcategory: element.category, name: element.value2,
                count: 1, userID: element.userID
              })

            }
          }


          let put2 = true;
          if (this.modulesFinished.length < 1) {
            this.modulesFinished.push({
              category: element.name, subcategory: element.category, name: element.value2,
              strValue: element.strValue, count: 1, userID: element.userID
            })
          }
          if (this.modulesFinished.length) {
            this.modulesFinished.forEach((el, i) => {
              if (el.category === element.name && el.subcategory === element.category && el.name === element.value2 && el.strValue === element.strValue && el.userID === element.userID) {
                put2 = false;
              }
            });
            if (put2) {
              this.modulesFinished.push({
                category: element.name, subcategory: element.category, name: element.value2,
                strValue: element.strValue, count: 1, userID: element.userID
              })

            }
          }

        });
        return { courseFinished: this.courseFinished, modulesFinished: this.modulesFinished }


      }, error => {
        this.courseFinished = [];
        this.modulesFinished = [];
      });


  }




}



