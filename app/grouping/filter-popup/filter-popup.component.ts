import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter-popup',
  templateUrl: './filter-popup.component.html',
  styleUrls: ['./filter-popup.component.scss']
})
export class FilterPopupComponent implements OnInit {

  @Output() itemSelected = new EventEmitter();

  filterList = [
    /*  {
        title: 'FIRST SESSION',
        description: `The first date/time the user's device communicated with Productivise server`,
        firstSessionOperator: '>',
        icon: 'sign-in',
        type: 'firstSession'
      },
      {
        title: 'LAST SESSION',
        description: `The most recent date/time the users device communicated with Productivise server`,
        icon: 'sign-out',
        lastSessionOperator: '>',
        type: 'lastSession'
      },
      {
        title: 'SESSION COUNT',
        description: `The number of times the user's device has opened your app`,
        icon: 'list-ol',
        SessionCountOperator: '>',
        type: 'sessionCount'
      },
      */
    {
      title: 'TOTAL DURATION',
      description: `Total number of minutes the user's device has had your app open`,
      totalDurationOperator: '>',
      icon: 'clock-o',
      type: 'totalDuration'
    },
    {
      title: 'NEWS DURATION ',
      description: `Total number of minutes the user's device spent on Updates`,
      totalDurationOperator: '>',
      icon: 'clock-o',
      type: 'totalNewsTimeSpent'
    },
    {
      title: 'MODULES FINISHED ',
      description: `Total number of modules the user has finished`,
      totalDurationOperator: '>',
      icon: 'clock-o',
      type: 'modulesFinished'
    },
    {
      title: 'MODULES STARTED ',
      description: `Total number of modules the user started`,
      totalDurationOperator: '>',
      icon: 'clock-o',
      type: 'modulesStarted'
    },
    {
      title: 'COURSE FINISHED ',
      description: `Total number of courses the user finished`,
      totalDurationOperator: '>',
      icon: 'clock-o',
      type: 'courseFinished'
    },
    {
      title: 'COURSE STARTED',
      description: `Total number of courses the user started`,
      totalDurationOperator: '>',
      icon: 'clock-o',
      type: 'courseStarted'
    },
    {
      title: 'Date of Joining',
      description: `Date of Joining of the user`,
      dojOperator: '>',
      icon: 'registered',
      type: 'doj'
    },
    {
      title: 'AGE',
      description: `Age of the user`,
      ageOperator: '>',
      icon: 'user',
      type: 'age'
    },
    {
      title: 'GENDER',
      description: `Gender of the user`,
      icon: 'venus-mars',
      type: 'gender'

    },
    {
      title: 'Location',
      description: `Location of the user`,
      icon: 'globe',
      type: 'location'
    },
    {
      title: 'Department',
      description: `Department of the user`,
      icon: 'users',
      type: 'department'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

  listItemSelected(item) {
    this.itemSelected.emit(item);
  }

}
