import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { FullLayoutComponent } from "./layouts/full/full-layout.component";
import { ContentLayoutComponent } from "./layouts/content/content-layout.component";

import { Full_ROUTES } from "./shared/routes/full-layout.routes";
import { CONTENT_ROUTES } from "./shared/routes/content-layout.routes";

import { AuthGuardService } from './shared/auth/auth-guard.service';
import { RoleGuardService } from './shared/auth/role-guard.service';
import { LoginComponent } from './login/login.component';
import { LinkComponent } from './link/link.component';
import { PreviewService } from './shared/auth/preview.service';
import { InviteComponent } from './invite/invite.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AnalyticsComponent } from './analytics/analytics.component';

const appRoutes: Routes = [
  {
    path: 'invite/:id/:phone/:name/:org/:invt/:t',
    component: InviteComponent,
  },
  {
    path: 'create/:id/:referrer/:lid',
    component: LinkComponent,
  },
  {
    path: 'create/:id/:referrer',
    component: LinkComponent,
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent
  },
  {
    path: 'login',
    component: LoginComponent
  }
  , {
    path: 'dashboard',
    canActivate: [RoleGuardService],
    redirectTo: 'dashboard/dashboard1',
    pathMatch: 'full',
  },
  { path: '', component: FullLayoutComponent, data: { title: 'full Views' }, children: Full_ROUTES, canActivate: [RoleGuardService] },
  { path: '', component: ContentLayoutComponent, data: { title: 'content Views' }, children: CONTENT_ROUTES, canActivate: [RoleGuardService] }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})



export class AppRoutingModule {

  constructor(private previewService: PreviewService) {

  }

}



