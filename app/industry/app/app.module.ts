import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatIconModule } from '@angular/material/icon';
import { TopNavComponent } from './top-nav/top-nav.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { HomeComponent } from './home/home.component';
import { StepsComponent } from './steps/steps.component';
import { DownloadSampleComponent } from './download-sample/download-sample.component';
import { DownloadBtnComponent } from './download-btn/download-btn.component';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { UploadCsvBtnComponent } from './upload-csv-btn/upload-csv-btn.component';
import { ManuallyAddBtnComponent } from './manually-add-btn/manually-add-btn.component';
import { NextBtnComponent } from './next-btn/next-btn.component';
import { AfterUploadComponent } from './after-upload/after-upload.component';
import { UploadComponent } from './upload/upload.component';
import { CreateSmsComponent } from './create-sms/create-sms.component';
import { DownloadIncorrectFileComponent } from './download-incorrect-file/download-incorrect-file.component';
import { UploadIncorrectContactComponent } from './upload-incorrect-contact/upload-incorrect-contact.component';
import { UploadBtnComponent } from './upload-btn/upload-btn.component';
import { NgxEditorModule } from 'ngx-editor';
@NgModule({
  declarations: [
    TopNavComponent,
    BreadcrumbsComponent,
    HomeComponent,
    StepsComponent,
    DownloadSampleComponent,
    DownloadBtnComponent,
    UploadFileComponent,
    UploadCsvBtnComponent,
    ManuallyAddBtnComponent,
    NextBtnComponent,
    AfterUploadComponent,
    UploadComponent,
    CreateSmsComponent,
    DownloadIncorrectFileComponent,
    UploadIncorrectContactComponent,
    UploadBtnComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    MatIconModule,
    NgxEditorModule
  ]


})
export class AppModule { }
