import { Component, OnInit } from '@angular/core';
import { QuillEditorComponent } from 'ngx-quill/src/quill-editor.component';
import { PreviewService } from '../shared/auth/preview.service';
import { IndustryResourceService } from '../sthaapak';
import { FormGroup, FormControl, ReactiveFormsModule, FormsModule, Validators } from '@angular/forms';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import swal from 'sweetalert2';
import * as AWS from '../shared/configs/aws.config';
import { base } from '../shared/configs/util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-industry)',
  templateUrl: './industry-add.component.html',
  styleUrls: ['./industry-add.component.scss']
})

export class IndustryAddComponent implements OnInit {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };
  alternateName: FormControl;
  myform: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  email: FormControl;
  phone: FormControl;
  group: FormControl;
  userType: Number;
  location: FormControl;
  audio: FormControl;
  pdf: FormControl;
  organization;
  slideImageName;
  contentImageUrl;
  contentVideoUrl;
  slideVideoName;
  contentAudioUrl;
  slideAudioName;
  contentDocUrl;
  slideDocName;
  validFileExtensions = ['.jpg', '.jpeg', '.bmp', '.gif', '.png'];
  validFileExtensionsVideo = ['.mp4', '.avi', '.mov', '.mwv', '.flv'];
  validFileExtensionsZip = ['.zip'];
  validFileExtensionsDoc = ['.doc', '.docx', '.xlsx', '.csv', '.ppt', '.pptx', '.txt', '.pdf'];
  validFileExtensionsAudio = ['.aac', '.mp3', '.m4a', '.wav', '.wma', '.vox', '.ogg', '.m4p'];

  constructor(private previewService: PreviewService, private industry: IndustryResourceService, private http: HttpClient,
    private roleGuardService: RoleGuardService, private router: Router) {
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
    this.http.get(base + '/api/organizations/' + this.previewService.organizationId.getValue(), this.httpOptions).subscribe(e => { this.organization = e });

  }

  contentSlideInfoChanged(event) {
    let timerInterval;
    let timerInterval2;
    let _URL = window.URL;
    let error = false;
    // console.log('FILE CHANGED:', event);
    const file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      let sFileName = event.target.files[0].name;
      // console.log('sfilename: ', event.target.id);
      if (event.target.id === 'image') {
        let blnValid = false;
        for (let j = 0; j < this.validFileExtensions.length; j++) {
          let sCurExtension = this.validFileExtensions[j];
          if (sFileName.slice(sFileName.length - sCurExtension.length, sFileName.length).toLowerCase() === sCurExtension.toLowerCase()) {
            blnValid = true;
            break;
          }
        }
        if (!blnValid) {
          swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensions.join(','));
          return false;
        }
        // console.log('size:', event.target.files[0].size);
        if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 2000000)) {
          swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 2 Mb');
          return false;
        }
        let reader = new FileReader();
        // Initiate the FileReader object.
        // Read the contents of Image File.
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (e) => {
          let that = file
          //Initiate the JavaScript Image object.
          let image = new Image();
          //Set the Base64 string return from FileReader as source.
          image.src = _URL.createObjectURL(file);
          image.onload = () => {
            //Determine the Height and Width.
            let height = image.height;
            let width = image.width;
            swal({
              title: 'Uploading!',
              onOpen: () => {
                swal.showLoading();
                Promise.resolve(this.uploadFile(file, event.target.id)).then(location => {
                  this.slideImageName = event.target.files[0].name;
                  this.contentImageUrl = <any>location
                  this.email.setValue(this.slideImageName);
                });
                timerInterval = setInterval(() => {
                  if (this.contentImageUrl) {
                    swal.close();
                    clearInterval(timerInterval);
                    // console.log('time');
                  }
                }, 100)
              },
              onClose: () => {
                window.clearInterval(timerInterval)
              }
            });
            return true;
          };
        }

      }

      if (event.target.id === 'video') {
        let blnValid = false;
        for (let j = 0; j < this.validFileExtensionsVideo.length; j++) {
          let sCurExtension = this.validFileExtensionsVideo[j];
          if (sFileName.slice(sFileName.length - sCurExtension.length, sFileName.length).toLowerCase() === sCurExtension.toLowerCase()) {
            blnValid = true;
            break;
          }
        }
        if (!blnValid) {
          swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensionsVideo.join(','));
          return false;
        }
        // console.log('size:', event.target.files[0].size);
        if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 25000000)) {
          swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 25 Mb');
          return false;
        }
        // console.log('contentSlideUrl', this.contentVideoUrl);
        swal({
          title: 'Uploading!',
          onOpen: () => {
            swal.showLoading();
            Promise.resolve(this.uploadFile(file, event.target.id)).then(location => {
              this.contentVideoUrl = <any>location;
              this.slideVideoName = event.target.files[0].name;
              this.phone.setValue(this.slideVideoName);

              // console.log('contentVideoUrl on loca', this.contentVideoUrl);
            }
            );
            timerInterval2 = setInterval(() => {
              if (this.contentVideoUrl) {
                // console.log('contentVideoUrl: ', this.contentVideoUrl);
                swal.close();
                clearInterval(timerInterval2);
                // console.log('time2');
              }
            }, 100)
          },
          onClose: () => {
            window.clearInterval(timerInterval2)
          }
        });
      }


      if (event.target.id === 'audio') {
        let blnValid = false;
        for (let j = 0; j < this.validFileExtensionsAudio.length; j++) {
          let sCurExtension = this.validFileExtensionsAudio[j];
          if (sFileName.slice(sFileName.length - sCurExtension.length, sFileName.length).toLowerCase() === sCurExtension.toLowerCase()) {
            blnValid = true;
            break;
          }
        }
        if (!blnValid) {
          swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensionsAudio.join(','));
          return false;
        }
        // console.log('size:', event.target.files[0].size);
        if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 25000000)) {
          swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 20 Mb');
          return false;
        }
        // console.log('contentAudioUrl', this.contentAudioUrl);
        swal({
          title: 'Uploading!',
          onOpen: () => {
            swal.showLoading();
            Promise.resolve(this.uploadFile(file, event.target.id)).then(location => {
              this.contentAudioUrl = <any>location;
              this.slideAudioName = event.target.files[0].name;
              // console.log('contentAudioUrl on loca', this.contentAudioUrl);
            }
            );
            timerInterval2 = setInterval(() => {
              if (this.contentAudioUrl) {
                // console.log('contentAudioUrl: ', this.contentAudioUrl);
                swal.close();
                clearInterval(timerInterval2);
                // console.log('time2');
              }
            }, 100)
          },
          onClose: () => {
            window.clearInterval(timerInterval2)
          }
        });
      }

      if (event.target.id === 'pdf') {
        let blnValid = false;
        for (let j = 0; j < this.validFileExtensionsDoc.length; j++) {
          let sCurExtension = this.validFileExtensionsDoc[j];
          if (sFileName.slice(sFileName.length - sCurExtension.length, sFileName.length).toLowerCase() === sCurExtension.toLowerCase()) {
            blnValid = true;
            break;
          }
        }
        if (!blnValid) {
          swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensionsDoc.join(','));
          return false;
        }
        // console.log('size:', event.target.files[0].size);
        if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 25000000)) {
          swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 20 Mb');
          return false;
        }
        // console.log('contentDocUrl', this.contentDocUrl);
        swal({
          title: 'Uploading!',
          onOpen: () => {
            swal.showLoading();
            Promise.resolve(this.uploadFile(file, event.target.id)).then(location => {
              this.contentDocUrl = <any>location;
              this.slideDocName = event.target.files[0].name;
              // console.log('contentDocUrl on loca', this.contentDocUrl);
            }
            );
            timerInterval2 = setInterval(() => {
              if (this.contentDocUrl) {
                // console.log('contentDocUrl: ', this.contentDocUrl);
                swal.close();
                clearInterval(timerInterval2);
                // console.log('time2');
              }
            }, 100)
          },
          onClose: () => {
            window.clearInterval(timerInterval2)
          }
        });
      }
    }
  }

  uploadFile(file, type) {
    let upload;
    if (type === 'image') {
      upload = AWS.s3.upload({ Key: this.previewService.organizationId.getValue() + '/industry/images/' + new Date() + file.name, Bucket: AWS.bucketName, Body: file, ACL: 'public-read' });
    }
    else if (type === 'video') {
      upload = AWS.s3.upload({ Key: this.previewService.organizationId.getValue() + '/industry/videos/' + new Date() + file.name, Bucket: AWS.bucketName, Body: file, ACL: 'public-read' });
    }
    else if (type === 'audio') {
      upload = AWS.s3.upload({ Key: this.previewService.organizationId.getValue() + '/industry/audio/' + new Date() + file.name, Bucket: AWS.bucketName, Body: file, ACL: 'public-read' });
    }
    else if (type === 'pdf') {
      upload = AWS.s3.upload({ Key: this.previewService.organizationId.getValue() + '/industry/doc/' + new Date() + file.name, Bucket: AWS.bucketName, Body: file, ACL: 'public-read' });
    }
    let promise = upload.promise();
    return promise.then((data) => {
      // console.log(data);
      return data.Location;
    }, (err) => {
      // console.log(err);
    });
  }

  createFormControls() {
    this.firstName = new FormControl('', Validators.required);
    this.lastName = new FormControl('', Validators.required);
    this.email = new FormControl('');
    this.location = new FormControl('');
    this.phone = new FormControl('');
    this.group = new FormControl('');
    this.audio = new FormControl('');
    this.pdf = new FormControl('');
    this.alternateName = new FormControl('');
  }

  createForm() {
    this.myform = new FormGroup({
      firstName: this.firstName,
      phone: this.phone,
      group: this.group,
      lastName: this.lastName,
      email: this.email,
      location: this.location,
      audio: this.audio,
      pdf: this.pdf,
      alternateName: this.alternateName
    });
  }


  ngOnInit() {
    this.createFormControls();
    this.createForm();
    this.email.valueChanges.subscribe(e => {
      // console.log(e);
    })
  }

  onSubmit() {
    if (this.myform.controls['firstName'].value.trim() === "") {
      this.myform.controls['firstName'].setValue('');
    }
    if (this.myform.controls['lastName'].value.trim() === "") {
      this.myform.controls['lastName'].setValue('');
    }
    if (!this.myform.valid) {
      // console.log('Form', this.myform.status);
      this.myform.controls['firstName'].markAsTouched();
      this.myform.controls['lastName'].markAsTouched();
      // this.myform.controls['indus'].markAsTouched();
      // this.myform.controls['department'].markAsTouched();

      // this.myform.controls['indus'].markAsTouched();
      // this.myform.controls['department'].markAsTouched();
      // this.myform.controls['jobRole'].markAsTouched();
      return;
    }

    swal({
      title: 'Are you sure you want to create this Industry?',
      html: `<div><span><b>Name:</b> ` + this.firstName.value + `</span><br/>` + `<span><b>Description: </b> ` + this.lastName.value + `</span > <br/></div>`
      ,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Yes, create it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then((confirm) => {
      // console.log('con:', confirm)
      if (confirm.value === true) {
        this.industry.createIndustryUsingPOST({
          'name': this.firstName.value,
          'tags': "newinfo",
          'description': null,
          'url': null,
          'imageLink': this.contentImageUrl,
          'videoLink': this.contentVideoUrl,
          'pdfLink': this.contentDocUrl,
          'audioLink': this.contentAudioUrl,
          'content': this.lastName.value,
          'images': this.alternateName.value,
          'organizations': [this.organization]
        }).subscribe(ind => {
          // console.log('Industry:', ind)
          swal(
            'Industry has been created Successfully.',
            '',
            'success'
          )
        });
        this.myform.reset();


      }
      else {
        swal(
          'Cancelled',
          'The Industry was not created.',
          'error'
        )
      }
    }, (dismiss) => {
      // console.log('dismis:', dismiss)
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        swal(
          'Cancelled',
          'The Industry was not created.',
          'error'
        )
      }
    })
  }



}

