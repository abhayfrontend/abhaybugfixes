import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndustryAddComponent } from './industry-add.component';
import { IndustryListComponent } from './industry-list.component';
import { HomeComponent } from './app/home/home.component';
import { AfterUploadComponent } from './app/after-upload/after-upload.component';


const routes: Routes = [
  {
    path: '',
    children: [

      {
        path: 'add',
        component: IndustryAddComponent,
        data: {
          title: 'Add new Industry '
        }
      }, {
        path: 'list',
        component: IndustryListComponent,
        data: {
          title: 'List Industry'
        }
      },
      {
        path: 'bulk',
        component: HomeComponent,
        data: {
          title: 'Bulk Register Industry'
        }
      },
      {

        path: 'afterUpload',
        component: AfterUploadComponent

      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IndustryRoutingModule { }
