import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { IndustryAddComponent } from './industry-add.component';
import { IndustryListComponent } from './industry-list.component';
import { IndustryRoutingModule } from "./industry-routing.module";
import { TopNavComponent } from './app/top-nav/top-nav.component';
import { BreadcrumbsComponent } from './app/breadcrumbs/breadcrumbs.component';
import { HomeComponent } from './app/home/home.component';
import { StepsComponent } from './app/steps/steps.component';
import { DownloadSampleComponent } from './app/download-sample/download-sample.component';
import { DownloadBtnComponent } from './app/download-btn/download-btn.component';
import { UploadFileComponent } from './app/upload-file/upload-file.component';
import { UploadCsvBtnComponent } from './app/upload-csv-btn/upload-csv-btn.component';
import { ManuallyAddBtnComponent } from './app/manually-add-btn/manually-add-btn.component';
import { NextBtnComponent } from './app/next-btn/next-btn.component';
import { AfterUploadComponent } from './app/after-upload/after-upload.component';
import { UploadComponent } from './app/upload/upload.component';
import { CreateSmsComponent } from './app/create-sms/create-sms.component';
import { DownloadIncorrectFileComponent } from './app/download-incorrect-file/download-incorrect-file.component';
import { UploadIncorrectContactComponent } from './app/upload-incorrect-contact/upload-incorrect-contact.component';
import { UploadBtnComponent } from './app/upload-btn/upload-btn.component';



@NgModule({
    imports: [
        CommonModule,

        ReactiveFormsModule,
        FormsModule,
        IndustryRoutingModule,

        CustomFormsModule,
        MatchHeightModule,
        NgbModule,
        NgxDatatableModule
    ],
    declarations: [
        IndustryAddComponent,
        IndustryListComponent,
        TopNavComponent,
        BreadcrumbsComponent,
        HomeComponent,
        StepsComponent,
        DownloadSampleComponent,
        DownloadBtnComponent,
        UploadFileComponent,
        UploadCsvBtnComponent,
        ManuallyAddBtnComponent,
        NextBtnComponent,
        AfterUploadComponent,
        UploadComponent,
        CreateSmsComponent,
        DownloadIncorrectFileComponent,
        UploadIncorrectContactComponent,
        UploadBtnComponent
    ]
})
export class IndustryModule { }
