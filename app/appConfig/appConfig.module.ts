import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CustomFormsModule } from 'ng2-validation';
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { AppconfigAddComponent } from './appConfig-add.component';
import { AppconfigRoutingModule } from "./appConfig-routing.module";



@NgModule({
    imports: [
        CommonModule,
       
        ReactiveFormsModule,
        FormsModule,
        AppconfigRoutingModule,
       
        CustomFormsModule,
        MatchHeightModule,
        NgbModule
    ],
    declarations: [
       
        AppconfigAddComponent
    ]

    
})
export class  AppconfigModule { }
