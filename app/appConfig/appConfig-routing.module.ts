import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppconfigAddComponent } from './appConfig-add.component';


const routes: Routes = [
  {
    path: '',
    children: [

      {
        path: 'add',
        component: AppconfigAddComponent,
        data: {
          title: 'App Config'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppconfigRoutingModule { }
