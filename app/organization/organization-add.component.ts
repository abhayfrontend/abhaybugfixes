import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { QuillEditorComponent } from 'ngx-quill/src/quill-editor.component';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import { PreviewService } from '../shared/auth/preview.service';
import * as AWS from '../shared/configs/aws.config';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { base } from '../shared/configs/util';
import { SupportGroupDTO } from '../sthaapak/model/supportGroupDTO';
import { SupportGroupResourceService } from '../sthaapak/sdk/supportGroupResource.service';
import { UserResourceService } from '../sthaapak';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-organization)',
  templateUrl: './organization-add.component.html',
  styleUrls: ['./organization-add.component.scss']
})


export class OrganizationAddComponent implements OnInit, OnDestroy {

  // Variables
  obj;
  objid;
  organizationImage;
  organizationType = 'Select Organization type';
  cin;
  adminName;
  organization;
  organizationTypeSelected;
  validFileExtensions = ['.jpg', '.jpeg', '.bmp', '.gif', '.png', '.ico'];
  // End Variables

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };

  myform: FormGroup;
  organizationNamef: FormControl;
  lastName: FormControl;
  emailf: FormControl;
  phone: FormControl;
  group: FormControl;
  userType: Number;
  location: FormControl;
  organizationTypef: FormControl;
  organizationDescriptionf: FormControl;
  organizationWebsitef: FormControl;
  organizationImagef: FormControl;
  organizationCIN: FormControl;
  primary_contactf: FormControl;
  adminNamef: FormControl;
  adminPhonef: FormControl;
  adminEmail: FormControl;
  licensef: FormControl;

  createFormControls() {
    this.organizationNamef = new FormControl('', Validators.required);
    this.organizationTypef = new FormControl('', Validators.required);
    this.organizationDescriptionf = new FormControl('', Validators.required);
    this.organizationWebsitef = new FormControl('', Validators.required);
    this.organizationImagef = new FormControl('');
    this.organizationCIN = new FormControl('', Validators.required);
    this.primary_contactf = new FormControl('', Validators.required);
    this.adminNamef = new FormControl('', Validators.required);
    this.adminPhonef = new FormControl('', Validators.required);
    this.adminEmail = new FormControl('', Validators.required);
    this.licensef = new FormControl('', Validators.required);
    this.lastName = new FormControl('', Validators.required);
    this.emailf = new FormControl('', Validators.required);
    // this.location = new FormControl('', Validators.required);
    // this.phone = new FormControl('', Validators.required);
    // this.group = new FormControl('', Validators.required);

  }

  createForm() {
    this.myform = new FormGroup({
      organizationNamef: this.organizationNamef,
      organizationTypef: this.organizationTypef,
      organizationDescriptionf: this.organizationDescriptionf,
      organizationWebsitef: this.organizationWebsitef,
      organizationImagef: this.organizationImagef,
      organizationCIN: this.organizationCIN,
      primary_contactf: this.primary_contactf,
      adminNamef: this.adminNamef,
      adminPhonef: this.adminPhonef,
      adminEmail: this.adminEmail,
      licensef: this.licensef
    });
  }


  constructor(private supportGroup: SupportGroupResourceService, private http: HttpClient, private roleGuardService: RoleGuardService,
    public previewService: PreviewService, private router: Router, private userResource: UserResourceService) {

    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }


  imageChanged(event) {
    let timerInterval;
    let _URL = window.URL;
    let error = false;

    if (event.target.files && event.target.files[0]) {

      let sFileName = event.target.files[0].name;
      // console.log('sfilename: ', sFileName);
      let blnValid = false;
      for (let j = 0; j < this.validFileExtensions.length; j++) {
        let sCurExtension = this.validFileExtensions[j];
        if (sFileName.slice(sFileName.length - sCurExtension.length, sFileName.length).toLowerCase() === sCurExtension.toLowerCase()) {
          blnValid = true;
          break;
        }
      }
      if (!blnValid) {
        swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensions.join(','));
        return false;
      }
      // console.log('size:', event.target.files[0].size);
      if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 2000000)) {
        swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 2 Mb');
        return false;
      }



      const file = event.target.files[0];

      let reader = new FileReader();


      //Initiate the FileReader object.
      //Read the contents of Image File.
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e) => {
        //Initiate the JavaScript Image object.
        let image = new Image();
        //Set the Base64 string return from FileReader as source.
        image.src = _URL.createObjectURL(file);
        image.onload = () => {
          //Determine the Height and Width.
          let height = image.height;
          let width = image.width;
          swal({
            title: 'Uploading!',
            onOpen: () => {
              swal.showLoading();
              Promise.resolve(this.uploadFile(file)).then(location =>
                this.organizationImage = <any>location
              );
              timerInterval = setInterval(() => {
                if (this.organizationImage) {
                  let orglogo = document.getElementById('orglogo');
                  if (orglogo.classList.contains('error')) {
                    orglogo.classList.remove('error');
                  }
                  swal.close();
                  clearInterval(timerInterval);
                }
              }, 100)
            },
            onClose: () => {
              clearInterval(timerInterval)
            }
          });
          return true;
        };
      }


    }
  }

  uploadFile(file) {
    let upload = AWS.s3.upload({ Key: this.previewService.organizationId.getValue() + '/org/images/' + new Date() + file.name, Bucket: AWS.bucketName, Body: file, ACL: 'public-read' });
    let promise = upload.promise();
    return promise.then((data) => {
      // console.log(data);
      return data.Location;
    }, (err) => {
      // console.log(err);
    });
  }
  createOrganizationTest() {
    let timerInterval1;
    // console.log('1:', {

      // 'description': this.organizationDescriptionf.value,
      // 'images': this.organizationImage,
      // 'name': this.organizationNamef.value,
      // 'type': this.organizationTypef.value,
      // 'tags': this.organizationCIN.value,
      // 'url': this.organizationWebsitef.value


    // })
    // console.log('2:', {
      // 'login': this.adminEmail.value,
      // 'organizationName': this.adminNamef.value.split(' ')[0],
      // 'lastName': this.adminNamef.value.split(' ')[1],
      // 'email': this.adminEmail.value,
      // 'activated': true,
      // 'langKey': null,
      // 'imageUrl': null,
      // 'phone': this.adminPhonef.value,
      // 'userType': 6,
      // 'userPassword': 'qwerty',
      // 'authorities': ['ROLE_USER', 'ROLE_ADMIN']
    // })
  }

  createOrganization() {
    // console.log('license:', this.licensef.value);
    if (this.myform.controls['organizationNamef'].value.trim() === "") {
      this.myform.controls['organizationNamef'].setValue('');
    }
    if (this.myform.controls['organizationDescriptionf'].value.trim() === "") {
      this.myform.controls['organizationDescriptionf'].setValue('');
    }
    if (this.myform.controls['organizationCIN'].value.trim() === "") {
      this.myform.controls['organizationCIN'].setValue('');
    }

    if (!this.myform.valid) {
      // console.log('Form', this.myform.value);

      this.myform.controls['organizationNamef'].markAsTouched();
      this.myform.controls['organizationTypef'].markAsTouched();
      this.myform.controls['organizationDescriptionf'].markAsTouched();
      this.myform.controls['organizationWebsitef'].markAsTouched();
      this.myform.controls['organizationImagef'].markAsTouched();
      this.myform.controls['organizationCIN'].markAsTouched();
      this.myform.controls['primary_contactf'].markAsTouched();
      this.myform.controls['adminNamef'].markAsTouched();
      this.myform.controls['adminEmail'].markAsTouched();

      this.myform.controls['adminPhonef'].markAsTouched();

      this.myform.controls['licensef'].markAsTouched();

      // this.myform.controls['indus'].markAsTouched();
      // this.myform.controls['department'].markAsTouched();
      // this.myform.controls['jobRole'].markAsTouched();
      return;
    }
    if (!this.organizationImage) {
      let orglogo = document.getElementById('orglogo');
      orglogo.classList.add('error');
      return
    }

    let timerInterval1;
    swal({
      title: 'Are you sure you want to create this organization?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Yes, create it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false,
      html: `<div><span><b>Organization Name:</b> ` + this.organizationNamef.value + `</span><br/>` + `<span><b>Organizzation Type: </b> ` +
        this.organizationTypef.value + `</span > <br/>`
        + `<span><b>Primary Contact of Organization: </b> ` + this.primary_contactf.value + `</span > <br/>` +
        `<span><b>Organization Admin:</b>` + this.adminNamef.value + `</span><br/>` + `<span><b>Organization Admin Login:</b>` + this.adminEmail.value + `</span><br/></div>`
    }).then(confirm => {
      // console.log('org:', confirm.value);
      if (confirm.value) {
        this.http.post(base + '/api/organizations', {

          'description': this.organizationDescriptionf.value,
          'images': this.organizationImage,
          'name': this.organizationNamef.value,
          'type': this.organizationTypef.value,
          'tags': this.cin,
          'url': this.organizationWebsitef.value

        }, this.httpOptions).subscribe(res => {
          // console.log('org:', res);
          let res1 = <any>res;
          const orgId = res1.id;
          this.http.post(base + '/api/contacts', {
            'email': this.adminEmail.value,
            'phone': this.adminPhonef.value,
            'landline': this.primary_contactf.value
          }, this.httpOptions).subscribe(contact => {
            // console.log('contact:', contact);
            let contactResult = <any>contact;
            this.http.put(base + '/api/organizations', {
              'description': this.organizationDescriptionf.value,
              'images': this.organizationImage,
              'name': this.organizationNamef.value,
              'type': this.organizationTypef.value,
              'tags': this.organizationCIN.value,
              'url': this.organizationWebsitef.value,
              'businessContactId': contactResult.id,
              'id': orgId

            }, this.httpOptions).subscribe(orgRes => {
              // console.log('orgres:', orgRes);
            });


            this.http.post(base + '/api/app-configs', {
              "name": this.organizationNamef.value,
              "description": null,
              "category": "ABOUT",
              "value": "About Us\t",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": 10,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "Industry",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/sales_kit.png",
              "category": "DASHBOARD",
              "value": "Sales Kit",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": 2,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "Rewards",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/ic-rewards.png",
              "category": "DASHBOARD",
              "value": "Rewards",
              "subCategory": "Earn, Claim, Leaderboard",
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": 8,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "toolbarBackground\t",
              "description": null,
              "category": "toolbarBackground",
              "value": "#e0eff9",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": null,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "Profile_Field_1",
              "description": null,
              "category": "PROFILE",
              "value": null,
              "subCategory": "TEXT",
              "tags": null,
              "type": null,
              "numValue": 1,
              "path": null,
              "icon": null,
              "displayOrder": 1,
              "organizationId": orgId,
              "extraData": [],
              "multiLinguals": []
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "Profile_Field_2",
              "description": null,
              "category": "PROFILE",
              "value": null,
              "subCategory": "TEXT",
              "tags": null,
              "type": null,
              "numValue": 2,
              "path": null,
              "icon": null,
              "displayOrder": 2,
              "organizationId": orgId,
              "extraData": [],
              "multiLinguals": []
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "Profile_Field_3",
              "description": null,
              "category": "PROFILE",
              "value": null,
              "subCategory": "TEXT",
              "tags": null,
              "type": null,
              "numValue": 3,
              "path": null,
              "icon": null,
              "displayOrder": 3,
              "organizationId": orgId,
              "extraData": [],
              "multiLinguals": []
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "Profile_Field_4",
              "description": null,
              "category": "PROFILE",
              "value": null,
              "subCategory": "TEXT",
              "tags": null,
              "type": null,
              "numValue": 4,
              "path": null,
              "icon": null,
              "displayOrder": 4,
              "organizationId": orgId,
              "extraData": [],
              "multiLinguals": []
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "Profile_Field_5",
              "description": null,
              "category": "PROFILE",
              "value": null,
              "subCategory": "TEXT",
              "tags": null,
              "type": null,
              "numValue": 5,
              "path": null,
              "icon": null,
              "displayOrder": 5,
              "organizationId": orgId,
              "extraData": [],
              "multiLinguals": []
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "Change Password",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/change-password.png",
              "category": "SIDEMENU",
              "value": "Change Password",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": "",
              "displayOrder": 9,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "splashBackground",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/sify_splash_screen.png",
              "category": "splashBackground",
              "value": "",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": null,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "splashLogoBackground",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/sify_splash_screen.png",
              "category": "splashBackground",
              "value": null,
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": null,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "MicroLearning",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/sidebar-self-development.png",
              "category": "SIDEMENU",
              "value": "Product Training",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": "",
              "displayOrder": 4,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "primary_color_dark\t",
              "description": null,
              "category": "APPTHEME",
              "value": "#000000",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": null,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "Notification",
              "description": null,
              "category": "TOOLBAR",
              "value": "Notification",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": null,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "primary_color",
              "description": null,
              "category": "APPTHEME",
              "value": "#000000",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": null,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "About Us",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/about.png",
              "category": "SIDEMENU",
              "value": "About Us",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": 9,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "Blogs",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/Home-Updates.png",
              "category": "DASHBOARD",
              "value": "Updates",
              "subCategory": "HEALTH,NEW",
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": "",
              "displayOrder": 1,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "Blogs",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/sidebar-updates.png",
              "category": "SIDEMENU",
              "value": "Updates",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": 1,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "Chat",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/Home-chat.png",
              "category": "DASHBOARD",
              "value": "Chat",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": "",
              "displayOrder": 6,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "Feedback",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/sop.png",
              "category": "SIDEMENU",
              "value": "SOPs",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": "",
              "displayOrder": 5,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "dashboardBackground",
              "description": null,
              "category": "dashboardBackground",
              "value": "#e0eff9",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": null,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "Chat",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/chat.png",
              "category": "SIDEMENU",
              "value": "Chat",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": "",
              "displayOrder": 6,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              'name': this.organizationNamef.value,
              'description': this.organizationImage,
              'category': 'APPNAME',
              'value': this.organizationNamef.value,
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": null,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "Logout",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/sidebar-logout.png",
              "category": "SIDEMENU",
              "value": "Logout",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": 10,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "MicroLearning",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/self-development.png",
              "category": "DASHBOARD",
              "value": "Product Training",
              "subCategory": "TECHNICAL,MANAGERIAL,POLICIES,SALES,WORK ETHICS,SAFETY,PRODUCTS,AWS PLUS,GOINFINIT BACKUP",
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": "",
              "displayOrder": 4,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "Industry",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/sidebar_saleskit.png",
              "category": "SIDEMENU",
              "value": "Sales Kit",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": 2,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "Profile",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/sidebar-profile.png",
              "category": "SIDEMENU",
              "value": "Profile",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": 8,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "sideMenuBackground",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Ativitti/sidebar-bg.png",
              "category": "sideMenuBackground",
              "value": null,
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": null,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "microLearningBackground",
              "description": null,
              "category": "microLearningBackground",
              "value": "#6b70b8",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": null,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "Chat",
              "description": null,
              "category": "TOOLBAR",
              "value": "Chat",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": null,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "Feedback",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/Home-sop.png",
              "category": "DASHBOARD",
              "value": "SOPs",
              "subCategory": "ON BOARDING,LOAN REQUEST,LEAVE REQUEST",
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": "",
              "displayOrder": 5,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "Rewards",
              "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Sify/sidebar-updates.png",
              "category": "SIDEMENU",
              "value": "Rewards",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": 7,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

            this.http.post(base + '/api/app-configs', {
              "name": "accent_color",
              "description": null,
              "category": "APPTHEME",
              "value": "#000000",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": null,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });
            this.http.post(base + '/api/app-configs', {
              "name": "statusBarBackground\t",
              "description": null,
              "category": "statusBarBackground",
              "value": "#d8e0e5",
              "subCategory": null,
              "tags": null,
              "type": null,
              "numValue": null,
              "path": null,
              "icon": null,
              "displayOrder": null,
              'organizationId': orgId
            }, this.httpOptions).subscribe(configRes => {
              // console.log('config:', configRes)

            });

          });
          // console.log('userAdmin:', {
          //   'login': this.adminEmail.value,
          //   'firstName': this.adminName.split(' ')[0],
          //   'lastName': this.adminName.split(' ')[1] || null,
          //   'email': this.adminEmail.value,
          //   'activated': true,
          //   'langKey': null,
          //   'imageUrl': null,
          //   'phone': this.adminPhonef.value,
          //   'userType': 6,
          //   'userPassword': 'qwerty',
          //   'authorities': ['ROLE_ADMIN', 'ROLE_USER'],
          //   'orgId': orgId
          // });

          this.http.post(base + '/api/users/register', {
            'login': this.adminEmail.value,
            'firstName': this.adminName.split(' ')[0],
            'lastName': this.adminName.split(' ')[1] || null,
            'email': this.adminEmail.value,
            'activated': true,
            'langKey': null,
            'imageUrl': null,
            'phone': this.adminPhonef.value,
            'userType': 6,
            'userPassword': 'qwerty',
            'orgId': orgId,
            'location': null
          }, this.httpOptions).subscribe(response => {
            // console.log('Admin:', response);
            let result: any = response;
            // console.log('memberData:', result);
            let memberData = {
              'name': result['firstName'] + ' ' + (result['lastName'] || ''),
              'userId': result['id'],
              'learnerId': result['learner']['id'],
              'category': null,
              'subCategory': null,
              'locationName': result['location'],
              'gehash1': result['geoHash'],
              'memberType': 'APPLICANT',
              'aadhaar': result['phone'] || null,
              'mobileNo': result['phone'] || this.adminPhonef.value,
              'cardId': null
            };
            // console.log('memberData:', memberData);
            this.http.post(base + '/api/members', memberData, this.httpOptions).subscribe(responseData => {
              // console.log(responseData);
              let memberResponse = <any>responseData;
              // console.log('member:', memberResponse);
              result.learnerId = result['learner']['id'];
              result.langKey = memberResponse.id;
              result.userPassword = result['firstName'] + result['phone']
              result.id = result.id;
              result.authorities = ['ROLE_ADMIN', 'ROLE_USER'];
              this.http.put(base + '/api/users', result, this.httpOptions).subscribe(respo => {
                // console.log('user:', respo);
              });

              /* this.http.put(base+'/api/users', {
                 'id': result['id'],
                 'login': this.email.value,
                 'firstName': this.adminName.split(' ')[0],
                 'lastName': this.adminName.split(' ')[1] || null,
                 'email': this.email.value,
                 'activated': true,
                 'langKey': null,
                 'imageUrl': null,
                 'learnerId': result['learner'].id,
                 'phone': this.admin_phone.value,
                 'userType': 6,
                 'userPassword': this.email.value + this.admin_phone.value,
                 'authorities': ['ROLE_USER', 'ROLE_ADMIN'],
                 'orgId': orgId,
                 'group': null,
                 'location': null
               }, this.httpOptions).subscribe(userPut => {
                 // console.log('userPut:', userPut);
               });*/
              this.supportGroup.createSupportGroupUsingPOST({
                'members': [], 'name': 'End-Users',
                'category': orgId
              }).subscribe(don => {
                // console.log(don);
              });
              this.supportGroup.createSupportGroupUsingPOST({
                'members': [], 'name': 'Content-Managers',
                'category': orgId
              }).subscribe(don => {
                // console.log(don);
              });
              this.supportGroup.createSupportGroupUsingPOST({
                'members': [], 'name': 'Administrators',
                'category': orgId
              }).subscribe(don => {
                // console.log(don);
              });
              this.supportGroup.createSupportGroupUsingPOST({
                'members': [], 'name': 'Campaign-Managers',
                'category': orgId
              }).subscribe(don => {
                // console.log(don);
              });
              this.supportGroup.createSupportGroupUsingPOST({
                'members': [], 'name': 'HR Managers',
                'category': orgId
              }).subscribe(don => {
                // console.log(don);
              });
              this.supportGroup.createSupportGroupUsingPOST({
                'members': [memberResponse], 'name': 'Super-Users',
                'category': orgId
              }).subscribe(don => {
                // console.log(don);
              });
              this.http.post(base + '/api/campaign-groups', {
                'learners': [], 'name': 'End-Users',
                'description': `This is a pre-defined group which should contain End-Users.`,
                'orgId': orgId
              },
                this.httpOptions).subscribe(resp => {
                  // console.log('group:', resp);
                });
              this.http.post(base + '/api/campaign-groups', {
                'learners': [], 'name': 'Content-Managers',
                'description': `This is a pre-defined group which should contain Content Managers.`,
                'orgId': orgId
              },
                this.httpOptions).subscribe(resp => {
                  // console.log('group:', resp);
                });
              this.http.post(base + '/api/campaign-groups', {
                'learners': [], 'name': 'Administrators',
                'description': `This is a pre-defined group which should contain Administrators.`,
                'orgId': orgId
              },
                this.httpOptions).subscribe(resp => {
                  // console.log('group:', resp);
                });
              this.http.post(base + '/api/campaign-groups', {
                'learners': [], 'name': 'Campaign-Managers',
                'description': `This is a pre-defined group which should contain Campaign-Managers.`,
                'orgId': orgId
              },
                this.httpOptions).subscribe(resp => {
                  // console.log('group:', resp);
                });
              this.http.post(base + '/api/campaign-groups', {
                'learners': [], 'name': 'HR Managers',
                'description': `This is a pre-defined group which should contain Human Resource Managers.`,
                'orgId': orgId
              },
                this.httpOptions).subscribe(resp => {
                  // console.log('group:', resp);
                });
              this.http.post(base + '/api/campaign-groups', {
                'learners': [response['learner']], 'name': 'Super-Users',
                'description': `This is a pre-defined group which should contain Organization Owners.`,
                'orgId': orgId
              },
                this.httpOptions).subscribe(resp => {
                  // console.log('group:', resp);
                  let finalCall = <any>resp;

                  swal({
                    title: 'Creating Organization!',
                    onOpen: () => {
                      swal.showLoading();

                      timerInterval1 = setInterval(() => {
                        if (finalCall.id) {
                          swal.close();
                          clearInterval(timerInterval1);
                          // console.log('time1')
                        }
                      }, 100)
                    },
                    onClose: () => {
                      clearInterval(timerInterval1)
                    }
                  }).then(ans => {
                    clearInterval(timerInterval1);
                  });



                });
            });
          });
        });
      }
    });
  }

  updateOrganization() {
    this.http.put(base + '/api/organizations', {
      'id': this.objid,
      'description': (<HTMLInputElement>document.getElementById('organizationDescription')).value,
      'images': this.organizationImage,
      'name': (<HTMLInputElement>document.getElementById('Organizationname')).value,
      'type': (<HTMLInputElement>document.getElementById('organizationType')).value,
      'tags': this.cin,
      'url': (<HTMLInputElement>document.getElementById('organizationWebsite')).value

    }, this.httpOptions).subscribe(res => {
      // console.log('org:', res);
      this.router.navigate(['/organization/list']);
    });
  }


  clearForm() {
    (<HTMLInputElement>document.getElementById('Organizationname')).value = '';
    (<HTMLInputElement>document.getElementById('organizationWebsite')).value = '';
    (<HTMLInputElement>document.getElementById('organizationType')).value = 'Select Organization type';
    this.organizationImage = '';
    (<HTMLInputElement>document.getElementById('organizationDescription')).value = '';
    (<HTMLInputElement>document.getElementById('primary_contact')).value = '';
    (<HTMLInputElement>document.getElementById('email')).value = '';
    this.adminName = '';
    (<HTMLInputElement>document.getElementById('admin_phone')).value = '';
  }
  ngOnInit() {
    this.obj = <any>this.previewService.Organization.getValue();
    this.createFormControls();
    this.createForm();
    this.objid = this.obj.id;
    // console.log('org:', this.obj);
    setTimeout(() => {
      if (this.obj.id) {
        (<HTMLInputElement>document.getElementById('Organizationname')).value = this.obj.name;
        this.organization = this.obj.name;
        (<HTMLInputElement>document.getElementById('organizationType')).value = this.obj.type;
        (<HTMLInputElement>document.getElementById('organizationWebsite')).value = this.obj.url;
        (<HTMLInputElement>document.getElementById('organizationDescription')).value = this.obj.description;
        this.cin = this.obj.tags;
        (<HTMLInputElement>document.getElementById('organizationType')).value = this.obj.type;
        // console.log('oooo', this.organizationNamef.value);
        this.organizationImage = this.obj.images;
        this.http.get(base + '/api/contacts/' + this.obj.businessContactId, this.httpOptions).subscribe(res => {
          let result = <any>res;
          this.http.get(base + '/api/users/' + result.email, this.httpOptions).subscribe(response => {
            let userResponse = <any>response;
            // console.log('u:', userResponse);
            (<HTMLInputElement>document.getElementById('email')).value = result.email;
            this.adminName = userResponse.firstName + (userResponse.lastName || '');
            (<HTMLInputElement>document.getElementById('admin_phone')).value = result.phone;
            (<HTMLInputElement>document.getElementById('primary_contact')).value = result.landline;
          });

        })

      }
    }, 0);

  }



  ngOnDestroy() {
    this.previewService.editOrganization({});
  }


}
