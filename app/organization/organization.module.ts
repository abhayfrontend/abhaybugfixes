import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CustomFormsModule } from 'ng2-validation';
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { OrganizationAddComponent } from './organization-add.component';
import { OrganizationListComponent } from './organization-list.component';
import { OrganizationRoutingModule } from "./organization-routing.module";



@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        OrganizationRoutingModule,
        CustomFormsModule,
        MatchHeightModule,
        NgbModule,
        NgxDatatableModule
    ],
    declarations: [
        OrganizationAddComponent,
        OrganizationListComponent
    ]
})
export class OrganizationModule { }
