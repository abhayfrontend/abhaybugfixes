import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import { PreviewService } from '../shared/auth/preview.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { base } from '../shared/configs/util';

@Component({
    selector: 'app-dt-filter',
    templateUrl: './organization-list.component.html',
    styleUrls: ['./organization-list.component.scss']
})

export class OrganizationListComponent implements OnInit {
    rows = [];
    filterOption;
    temp = [];

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.roleGuardService.token
        })
    };

    types = ["TRAINING_PARTNERS", "FACILITIES_MANAGEMENT", "MANUFACTURER", "RETAIL", "HOSPITALITY", "3RD_PARTY_SERVICE", "TRANSPORT_FLEET_OPERATORS", "OEM_SPONSORED_AUTHORISED_SERVICE_PARTNERS_AND_DEALERS"];

    // Table Column Titles
    columns = [
        { prop: 'id' },
        { prop: 'name' },
        { prop: 'description' },
        { prop: 'type' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private http: HttpClient, private roleGuardService: RoleGuardService, private previewService: PreviewService,
        private router: Router) {
        if (!localStorage.getItem('token')) {
            this.router.navigateByUrl('/login');
        }
    }

    ngOnInit() {
        this.getOrganizations();
    }

    changeFilter(e) {
        // console.log('filter:', this.temp);
        this.filterOption = e.target.value;
        this.rows = this.temp;
    }

    changeFilterLater(e) {
        swal('Filter Unavailable',
            'Work in Progress. This filter will be available soon.',
            'info'

        )
    }
    getOrganizations() {
        this.rows = [];

        this.temp = [];
        this.http.get(base + '/api/organizations', this.httpOptions).subscribe(res => {
            let data = <any>res;
            this.temp = [...data];
            this.rows = data;
        });
    }


    filterListByType(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {

            if (d.type === val) {
                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByAuthor(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            return d.author === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByStatus(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log(d.state);
            // console.log(val);
            return d.state === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPostDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log('date:', d.postDate)
            if (new Date(d.postDate).getDate() === new Date(val).getDate() && new Date(d.postDate).getMonth() === new Date(val).getMonth() && new Date(d.postDate).getFullYear() === new Date(val).getFullYear()) {


                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPublDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {

            if (new Date(d.publishedDate).getDate() === new Date(val).getDate() && new Date(d.publishedDate).getMonth() === new Date(val).getMonth() && new Date(d.publishedDate).getFullYear() === new Date(val).getFullYear()) {

                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    deleteOrganization(row) {

        swal({
            title: 'Are you sure you want to delete this Organization?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.http.get(base + '/api/app-configs/org/' + row.id, this.httpOptions).subscribe(res => {
                    let result = <any>res;
                    for (let i = 0; i < result.length - 1; i++) {
                        this.http.delete(base + '/api/app-configs/' + result[i].id, this.httpOptions).subscribe(response => {
                            // console.log(response)
                        });

                    }
                    if (result.length) {
                        this.http.delete(base + '/api/app-configs/' + result[result.length - 1].id, this.httpOptions).subscribe(response => {
                            // console.log(response)
                            this.http.delete(base + '/api/organizations/' + row.id, this.httpOptions).subscribe(orgres => {
                                // console.log('res:', orgres);
                                this.getOrganizations();
                            });
                        });
                    }
                    else {
                        this.http.delete(base + '/api/organizations/' + row.id, this.httpOptions).subscribe(orgres => {
                            // console.log('res:', orgres);
                            this.getOrganizations();
                        });

                    }





                });
            }
            else {
                swal(
                    'Cancelled',
                    'Your Organization was not deleted.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your Organization was not deleted.',
                    'error'
                )
            }
        })



    }

    editOrganization(row) {

        swal({
            title: 'Are you sure you want to edit this Organization?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.previewService.editOrganization(row);
                this.router.navigate(['/organization/edit']);
            }
            else {
                swal(
                    'Cancelled',
                    'Your Organization was not edited.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your Organization was not edited.',
                    'error'
                )
            }
        })

    }
}
