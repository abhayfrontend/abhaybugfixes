import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganizationAddComponent } from './organization-add.component';
import { OrganizationListComponent } from './organization-list.component';


const routes: Routes = [
  {
    path: '',
    children: [

      {
        path: 'add',
        component: OrganizationAddComponent,
        data: {
          title: 'Create a organization and its primary user'
        }
      },
      {
        path: 'list',
        component: OrganizationListComponent,
        data: {
          title: 'List Organization'
        }
      },
      {
        path: 'edit',
        component: OrganizationAddComponent,
        data: {
          title: 'Edit Organization'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrganizationRoutingModule { }
