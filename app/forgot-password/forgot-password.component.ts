import { Component, OnInit } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../shared/auth/auth.service';
import { User } from '../../models/user';
import { Router } from '@angular/router';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import { AuthenticationService } from '../shared/auth/authentication-service.service';
import { PreviewService } from '../shared/auth/preview.service';
import swal from 'sweetalert2';
import { base, proxyApi, adminUser, adminPassword } from '../shared/configs/util';
import * as AWS from '../shared/configs/aws.config';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  myForm: FormGroup;
  post: any;
  token;
  user: User;
  email;
  obj;
  data;
  message;
  httpOptions;
  otp;
  subject;
  userBody;
  useremail;
  otpValue;
  password;
  confirmpassword;

  constructor(private fb: FormBuilder, public http: HttpClient, private previewService: PreviewService,
    private router: Router) {

    this.user = new User();

    if (localStorage.getItem('token')) {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        })
      };
      this.http.get(base + '/api/authenticate', this.httpOptions).toPromise().then(respo => {
        let result = <any>respo;
        // console.log('ResultLog', result);
        this.router.navigateByUrl('/dashboard');
      }, err => {
        // console.log('ErrorLog:', err)
        localStorage.removeItem('token');
      })
    }

  }

  sendSMS(message, phone) {

    // console.log('Message Sent!!')
    // console.log('contacts:', phone);
    const sns = AWS.sns;
    return sns.publish({
      Message: message, PhoneNumber: '+91' + phone, MessageAttributes: {
        'AWS.SNS.SMS.SenderID': {
          DataType: 'String',
          StringValue: 'PRDCTVSE'
        },
        'AWS.SNS.SMS.MaxPrice': {
          DataType: 'Number',
          StringValue: '00.01'
        },
        'AWS.SNS.SMS.SMSType': {
          DataType: 'String',
          StringValue: 'Transactional'
        }
      }
    }).promise().then(res => {
      // console.log(res);

    });

  }


  sendEmail() {
    // console.log('email:', this.useremail);
    this.http.post(base + '/api/authenticate', { username: adminUser, password: adminPassword }).toPromise().then(tok => {
      let token = <any>tok;
      token = token.id_token;
      // console.log('token:', token)
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        })
      };
      this.http.get(base + '/api/users/' + this.useremail, this.httpOptions).toPromise().then(use => {
        let user = <any>use;
        this.userBody = user;
        // console.log('user:', this.userBody)
        // console.log('message:', this.otpValue)
        this.http.post(proxyApi + '/sendMail', {
          message: this.message,
          email: user.email,
          subject: this.subject
        }).toPromise().then(mailRes => {
          let result = <any>mailRes;
          this.email = user.email;
          // this.sendSMS(this.message, user.phone)
        })
      })
    })

  }


  ngOnInit(): void {
    this.otpValue = Math.round(Math.random() * 1000000);
    this.message = 'Hi. Your OTP is ' + this.otpValue + '. Please do not share with anyone.';
    this.subject = 'Forgot Password Request'

  }

  addPost(post) {
    this.data = post;
    if (!this.email) {
      this.sendEmail();
      this.myForm = this.fb.group({
        'otp': [null, Validators.required],
        'password': [null, Validators.required],
        'confirmpassword': [null, Validators.required]
      });
    }
    if (this.email) { }
  }

  resetPassword() {
    // console.log('pass:', this.otpValue, this.otp)
    if (!this.otp || String(this.otp).length < 5 || (String(this.otp) !== String(this.otpValue))) {
      swal('Error', 'Incorrect OTP. Please Enter the OTP sent to your registered email and phone number.', 'error');
      return false;
    }

    if (String(this.password).length < 5) {
      swal('Password Length too short', 'Password should have atleast 5 characters', 'error');
      return false;
    }
    if (String(this.password) !== String(this.confirmpassword)) {
      swal('Password Mismatch', 'Your Password does not match the confirm password', 'error')
      return false;
    }

    if (String(this.password).length > 5 && String(this.confirmpassword).length === String(this.password).length && String(this.otp) === String(this.otpValue)) {
      this.userBody.userPassword = this.password;
      this.http.put(base + '/api/users/saas/reset', this.userBody, this.httpOptions).toPromise().then(res => {
        let result = <any>res;
        // console.log('reset:', result);
        swal('Success', 'Your password has been reset. You can log in now.', 'success').then(a => this.router.navigate['/login']);
      })

    }


  }
  /*
    authLogin(): void {
      this.user['username'] = this.myForm.controls['phone_number'].value;
      this.user['password'] = this.myForm.controls['user_password'].value;
      this.user['rememberMe'] = this.myForm.controls['remember_me'].value;
      this.previewService.editUserID(this.user['username']);
  
  
      this.auth.login(this.user)
        .then((user) => {
  
          localStorage.setItem('token', user.id_token);
          //this.authVerify();
  
          this.router.navigateByUrl('/dashboard');
        })
        .catch((err) => {
          swal('Incorrect Credentials', '', 'error');
  
        });
    }
  
    authVerify(): void {
  
      let token = localStorage.getItem('token');
      this.auth.ensureAuthenticated(token)
        .then((user) => {
        })
        .catch((err) => {
        });
    }
  */
}
