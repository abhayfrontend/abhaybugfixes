/**
 * sthaapakx API
 * sthaapakx API documentation
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { CareerPathDTO } from './careerPathDTO';
import { ExtraDataDTO } from './extraDataDTO';
import { FileDTO } from './fileDTO';
import { LanguageConfigDTO } from './languageConfigDTO';
import { OrganizationDTO } from './organizationDTO';
import { SkillDTO } from './skillDTO';


export interface JobRoleDTO {
    additionalFiles?: Array<FileDTO>;
    audioLink?: string;
    careerPaths?: Array<CareerPathDTO>;
    content?: string;
    description?: string;
    displayOrder?: number;
    extraData?: Array<ExtraDataDTO>;
    id?: number;
    image?: string;
    level?: number;
    maximumExperince?: number;
    minimumEducation?: JobRoleDTO.MinimumEducationEnum;
    minimumExperince?: number;
    multiLinguals?: Array<LanguageConfigDTO>;
    name?: string;
    organizations?: Array<OrganizationDTO>;
    pdfLink?: string;
    skills?: Array<SkillDTO>;
    tags?: string;
    trainingDuration?: string;
    url?: string;
    videoLink?: string;
}
export namespace JobRoleDTO {
    export type MinimumEducationEnum = 'NOT_APPLICABLE' | 'PRIMARY' | 'MIDDLE' | 'METRIC' | 'SECONDARY' | 'DIPLOMA' | 'UNDER_GRADUATE' | 'POST_GRADUATE' | 'PHD' | 'VOCATIONAL_TRAINING';
    export const MinimumEducationEnum = {
        NOTAPPLICABLE: 'NOT_APPLICABLE' as MinimumEducationEnum,
        PRIMARY: 'PRIMARY' as MinimumEducationEnum,
        MIDDLE: 'MIDDLE' as MinimumEducationEnum,
        METRIC: 'METRIC' as MinimumEducationEnum,
        SECONDARY: 'SECONDARY' as MinimumEducationEnum,
        DIPLOMA: 'DIPLOMA' as MinimumEducationEnum,
        UNDERGRADUATE: 'UNDER_GRADUATE' as MinimumEducationEnum,
        POSTGRADUATE: 'POST_GRADUATE' as MinimumEducationEnum,
        PHD: 'PHD' as MinimumEducationEnum,
        VOCATIONALTRAINING: 'VOCATIONAL_TRAINING' as MinimumEducationEnum
    }
}
