/**
 * sthaapakx API
 * sthaapakx API documentation
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { FormFieldDTO } from './formFieldDTO';
import { SchemeCriteriaDTO } from './schemeCriteriaDTO';


export interface SchemeFormDTO {
    apiTemplate?: string;
    apiType?: SchemeFormDTO.ApiTypeEnum;
    apiUrl?: string;
    audioLink?: string;
    autoFill?: boolean;
    autoFillSource?: string;
    category?: string;
    content?: string;
    description?: string;
    fields?: Array<FormFieldDTO>;
    icon?: string;
    id?: number;
    linkLink?: string;
    name?: string;
    pdfLink?: string;
    schemeCriteria?: Array<SchemeCriteriaDTO>;
    tags?: string;
    valueType?: string;
    videoLink?: string;
}
export namespace SchemeFormDTO {
    export type ApiTypeEnum = 'POST' | 'GET' | 'PUT';
    export const ApiTypeEnum = {
        POST: 'POST' as ApiTypeEnum,
        GET: 'GET' as ApiTypeEnum,
        PUT: 'PUT' as ApiTypeEnum
    }
}
