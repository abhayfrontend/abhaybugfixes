/**
 * sthaapakx API
 * sthaapakx API documentation
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { LanguageConfigDTO } from './languageConfigDTO';


export interface MenuSectionConfigDTO {
    activityName?: string;
    apiName?: string;
    apiURL?: string;
    color?: string;
    description?: string;
    icon?: string;
    id?: number;
    multiLinguals?: Array<LanguageConfigDTO>;
    name?: string;
    sequence?: number;
}
