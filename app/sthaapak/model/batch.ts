/**
 * sthaapakx API
 * sthaapakx API documentation
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { Course } from './course';


export interface Batch {
    batchEndDate?: Date;
    batchLastDate?: Date;
    batchStartDate?: Date;
    capicity?: number;
    courses?: Array<Course>;
    id?: number;
    mouSigned?: Date;
    mouStatus?: string;
}
