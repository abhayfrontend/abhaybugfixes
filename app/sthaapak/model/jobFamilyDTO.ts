/**
 * sthaapakx API
 * sthaapakx API documentation
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { ContructFilterDTO } from './contructFilterDTO';
import { FileDTO } from './fileDTO';
import { JobRoleDTO } from './jobRoleDTO';
import { LanguageConfigDTO } from './languageConfigDTO';
import { OrganizationDTO } from './organizationDTO';


export interface JobFamilyDTO {
    additionalFiles?: Array<FileDTO>;
    audioLink?: string;
    bigImageLink?: string;
    content?: string;
    description?: string;
    displayOrder?: number;
    filters?: Array<ContructFilterDTO>;
    id?: number;
    image?: string;
    jobRoles?: Array<JobRoleDTO>;
    multiLinguals?: Array<LanguageConfigDTO>;
    name?: string;
    organizations?: Array<OrganizationDTO>;
    pdfLink?: string;
    tags?: string;
    url?: string;
    videoLink?: string;
}
