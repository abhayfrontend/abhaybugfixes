/**
 * sthaapakx API
 * sthaapakx API documentation
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { CareerPathDTO } from './careerPathDTO';
import { ExtraDataDTO } from './extraDataDTO';


export interface OrganizationDTO {
    businessContactId?: number;
    careerPaths?: Array<CareerPathDTO>;
    description?: string;
    extraData?: Array<ExtraDataDTO>;
    id?: number;
    images?: string;
    name?: string;
    saasLicenseId?: number;
    supportContactId?: number;
    tags?: string;
    type?: OrganizationDTO.TypeEnum;
    url?: string;
}
export namespace OrganizationDTO {
    export type TypeEnum = 'CORPORATE' | 'NGO' | 'PSU' | 'SME' | 'APP' | 'TRAINING_PARTNERS' | 'FACILITIES_MANAGEMENT' | 'RETAIL' | 'HOSPITALITY' | 'OEM_SPONSORED_AUTHORISED_SERVICE_PARTNERS_AND_DEALERS' | 'MANUFACTURER' | 'THIRD_PARTY_SERVICE' | 'TRANSPORT_FLEET_OPERATORS';
    export const TypeEnum = {
        CORPORATE: 'CORPORATE' as TypeEnum,
        NGO: 'NGO' as TypeEnum,
        PSU: 'PSU' as TypeEnum,
        SME: 'SME' as TypeEnum,
        APP: 'APP' as TypeEnum,
        TRAININGPARTNERS: 'TRAINING_PARTNERS' as TypeEnum,
        FACILITIESMANAGEMENT: 'FACILITIES_MANAGEMENT' as TypeEnum,
        RETAIL: 'RETAIL' as TypeEnum,
        HOSPITALITY: 'HOSPITALITY' as TypeEnum,
        OEMSPONSOREDAUTHORISEDSERVICEPARTNERSANDDEALERS: 'OEM_SPONSORED_AUTHORISED_SERVICE_PARTNERS_AND_DEALERS' as TypeEnum,
        MANUFACTURER: 'MANUFACTURER' as TypeEnum,
        THIRDPARTYSERVICE: 'THIRD_PARTY_SERVICE' as TypeEnum,
        TRANSPORTFLEETOPERATORS: 'TRANSPORT_FLEET_OPERATORS' as TypeEnum
    }
}
