import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Configuration } from './configuration';

import { AccountResourceService } from './sdk/accountResource.service';
import { AddressResourceService } from './sdk/addressResource.service';
import { AppConfigResourceService } from './sdk/appConfigResource.service';
import { BatRawScoresResourceService } from './sdk/batRawScoresResource.service';
import { BatScoreResourceService } from './sdk/batScoreResource.service';
import { BatTestResourceService } from './sdk/batTestResource.service';
import { BatchResourceService } from './sdk/batchResource.service';
import { BookingResourceService } from './sdk/bookingResource.service';
import { CalenderResourceService } from './sdk/calenderResource.service';
import { CampaignGroupResourceService } from './sdk/campaignGroupResource.service';
import { CampaignResourceService } from './sdk/campaignResource.service';
import { CampaignTargetResourceService } from './sdk/campaignTargetResource.service';
import { CareerPathResourceService } from './sdk/careerPathResource.service';
import { CenterResourceService } from './sdk/centerResource.service';
import { ConnectivityResourceService } from './sdk/connectivityResource.service';
import { ConstructScoreResourceService } from './sdk/constructScoreResource.service';
import { ContactResourceService } from './sdk/contactResource.service';
import { ContentCardResourceService } from './sdk/contentCardResource.service';
import { ContentJobResourceService } from './sdk/contentJobResource.service';
import { ContentLibraryResourceService } from './sdk/contentLibraryResource.service';
import { ContentProposalResourceService } from './sdk/contentProposalResource.service';
import { ContentProposalVersionResourceService } from './sdk/contentProposalVersionResource.service';
import { ContentProviderResourceService } from './sdk/contentProviderResource.service';
import { ContentRequestorResourceService } from './sdk/contentRequestorResource.service';
import { ContentResourceService } from './sdk/contentResource.service';
import { ContentRewardResourceService } from './sdk/contentRewardResource.service';
import { ContentRoleResourceService } from './sdk/contentRoleResource.service';
import { ContentVersionFeedbackResourceService } from './sdk/contentVersionFeedbackResource.service';
import { ContructFilterResourceService } from './sdk/contructFilterResource.service';
import { ContructResourceService } from './sdk/contructResource.service';
import { CounsellorResourceService } from './sdk/counsellorResource.service';
import { CoursePlanResourceService } from './sdk/coursePlanResource.service';
import { CourseResourceService } from './sdk/courseResource.service';
import { DataResourceService } from './sdk/dataResource.service';
import { DeviceResourceService } from './sdk/deviceResource.service';
import { EducationResourceService } from './sdk/educationResource.service';
import { EnrollmentResourceService } from './sdk/enrollmentResource.service';
import { EventResourceService } from './sdk/eventResource.service';
import { ExtractorResourceService } from './sdk/extractorResource.service';
import { FacilityResourceService } from './sdk/facilityResource.service';
import { FacultyResourceService } from './sdk/facultyResource.service';
import { FeatureResourceService } from './sdk/featureResource.service';
import { FileResourceService } from './sdk/fileResource.service';
import { FollowupResourceService } from './sdk/followupResource.service';
import { IdentityResourceService } from './sdk/identityResource.service';
import { IndustryResourceService } from './sdk/industryResource.service';
import { InterestResourceService } from './sdk/interestResource.service';
import { JobFamilyResourceService } from './sdk/jobFamilyResource.service';
import { JobResourceService } from './sdk/jobResource.service';
import { JobRoleResourceService } from './sdk/jobRoleResource.service';
import { LanguageConfigResourceService } from './sdk/languageConfigResource.service';
import { LearnerResourceService } from './sdk/learnerResource.service';
import { LifeCycleResourceService } from './sdk/lifeCycleResource.service';
import { LocationResourceService } from './sdk/locationResource.service';
import { MenuConfigResourceService } from './sdk/menuConfigResource.service';
import { MenuSectionConfigResourceService } from './sdk/menuSectionConfigResource.service';
import { MicroContentResourceService } from './sdk/microContentResource.service';
import { MobilizationResourceService } from './sdk/mobilizationResource.service';
import { MobilizerResourceService } from './sdk/mobilizerResource.service';
import { MultilingualResourceService } from './sdk/multilingualResource.service';
import { NotificationResourceService } from './sdk/notificationResource.service';
import { OptionResourceService } from './sdk/optionResource.service';
import { OrganizationResourceService } from './sdk/organizationResource.service';
import { PocInformationResourceService } from './sdk/pocInformationResource.service';
import { ProfileInfoResourceService } from './sdk/profileInfoResource.service';
import { PublishedContentResourceService } from './sdk/publishedContentResource.service';
import { QuestionResourceService } from './sdk/questionResource.service';
import { RatingResourceService } from './sdk/ratingResource.service';
import { RewardResourceService } from './sdk/rewardResource.service';
import { SaasLicenseResourceService } from './sdk/saasLicenseResource.service';
import { ScoreBoardResourceService } from './sdk/scoreBoardResource.service';
import { ServiceCenterResourceService } from './sdk/serviceCenterResource.service';
import { SiteResourceService } from './sdk/siteResource.service';
import { SkillResourceService } from './sdk/skillResource.service';
import { SlotResourceService } from './sdk/slotResource.service';
import { ThresholdResourceService } from './sdk/thresholdResource.service';
import { TrainingPartnerResourceService } from './sdk/trainingPartnerResource.service';
import { UserJwtControllerService } from './sdk/userJwtController.service';
import { UserResourceService } from './sdk/userResource.service';

@NgModule({
    imports: [CommonModule, HttpClientModule],
    declarations: [],
    exports: [],
    providers: [
        AccountResourceService,
        AddressResourceService,
        AppConfigResourceService,
        BatRawScoresResourceService,
        BatScoreResourceService,
        BatTestResourceService,
        BatchResourceService,
        BookingResourceService,
        CalenderResourceService,
        CampaignGroupResourceService,
        CampaignResourceService,
        CampaignTargetResourceService,
        CareerPathResourceService,
        CenterResourceService,
        ConnectivityResourceService,
        ConstructScoreResourceService,
        ContactResourceService,
        ContentCardResourceService,
        ContentJobResourceService,
        ContentLibraryResourceService,
        ContentProposalResourceService,
        ContentProposalVersionResourceService,
        ContentProviderResourceService,
        ContentRequestorResourceService,
        ContentResourceService,
        ContentRewardResourceService,
        ContentRoleResourceService,
        ContentVersionFeedbackResourceService,
        ContructFilterResourceService,
        ContructResourceService,
        CounsellorResourceService,
        CoursePlanResourceService,
        CourseResourceService,
        DataResourceService,
        DeviceResourceService,
        EducationResourceService,
        EnrollmentResourceService,
        EventResourceService,
        ExtractorResourceService,
        FacilityResourceService,
        FacultyResourceService,
        FeatureResourceService,
        FileResourceService,
        FollowupResourceService,
        IdentityResourceService,
        IndustryResourceService,
        InterestResourceService,
        JobFamilyResourceService,
        JobResourceService,
        JobRoleResourceService,
        LanguageConfigResourceService,
        LearnerResourceService,
        LifeCycleResourceService,
        LocationResourceService,
        MenuConfigResourceService,
        MenuSectionConfigResourceService,
        MicroContentResourceService,
        MobilizationResourceService,
        MobilizerResourceService,
        MultilingualResourceService,
        NotificationResourceService,
        OptionResourceService,
        OrganizationResourceService,
        PocInformationResourceService,
        ProfileInfoResourceService,
        PublishedContentResourceService,
        QuestionResourceService,
        RatingResourceService,
        RewardResourceService,
        SaasLicenseResourceService,
        ScoreBoardResourceService,
        ServiceCenterResourceService,
        SiteResourceService,
        SkillResourceService,
        SlotResourceService,
        ThresholdResourceService,
        TrainingPartnerResourceService,
        UserJwtControllerService,
        UserResourceService]
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders {
        return {
            ngModule: ApiModule,
            providers: [{ provide: Configuration, useFactory: configurationFactory }]
        }
    }

    constructor(@Optional() @SkipSelf() parentModule: ApiModule) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import your base AppModule only.');
        }
    }
}
