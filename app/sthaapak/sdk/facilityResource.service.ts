/**
 * sthaapakx API
 * sthaapakx API documentation
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * http://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs/Observable';

import { FacilityDTO } from '../model/facilityDTO';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class FacilityResourceService {

    protected basePath = 'http://stagingapi.productivise.io';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (let consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * createFacility
     * 
     * @param facilityDTO facilityDTO
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public createFacilityUsingPOST(facilityDTO: FacilityDTO, observe?: 'body', reportProgress?: boolean): Observable<FacilityDTO>;
    public createFacilityUsingPOST(facilityDTO: FacilityDTO, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<FacilityDTO>>;
    public createFacilityUsingPOST(facilityDTO: FacilityDTO, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<FacilityDTO>>;
    public createFacilityUsingPOST(facilityDTO: FacilityDTO, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (facilityDTO === null || facilityDTO === undefined) {
            throw new Error('Required parameter facilityDTO was null or undefined when calling createFacilityUsingPOST.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];
        let httpContentTypeSelected:string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set("Content-Type", httpContentTypeSelected);
        }

        return this.httpClient.post<FacilityDTO>(`${this.basePath}/api/facilities`,
            facilityDTO,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * deleteFacility
     * 
     * @param id id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public deleteFacilityUsingDELETE(id: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public deleteFacilityUsingDELETE(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public deleteFacilityUsingDELETE(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public deleteFacilityUsingDELETE(id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling deleteFacilityUsingDELETE.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.delete<any>(`${this.basePath}/api/facilities/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * getAllFacilities
     * 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getAllFacilitiesUsingGET(observe?: 'body', reportProgress?: boolean): Observable<Array<FacilityDTO>>;
    public getAllFacilitiesUsingGET(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<FacilityDTO>>>;
    public getAllFacilitiesUsingGET(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<FacilityDTO>>>;
    public getAllFacilitiesUsingGET(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<Array<FacilityDTO>>(`${this.basePath}/api/facilities`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * getFacility
     * 
     * @param id id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getFacilityUsingGET(id: number, observe?: 'body', reportProgress?: boolean): Observable<FacilityDTO>;
    public getFacilityUsingGET(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<FacilityDTO>>;
    public getFacilityUsingGET(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<FacilityDTO>>;
    public getFacilityUsingGET(id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling getFacilityUsingGET.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<FacilityDTO>(`${this.basePath}/api/facilities/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * searchFacilities
     * 
     * @param query query
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public searchFacilitiesUsingGET(query: string, observe?: 'body', reportProgress?: boolean): Observable<Array<FacilityDTO>>;
    public searchFacilitiesUsingGET(query: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<FacilityDTO>>>;
    public searchFacilitiesUsingGET(query: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<FacilityDTO>>>;
    public searchFacilitiesUsingGET(query: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (query === null || query === undefined) {
            throw new Error('Required parameter query was null or undefined when calling searchFacilitiesUsingGET.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (query !== undefined) {
            queryParameters = queryParameters.set('query', <any>query);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<Array<FacilityDTO>>(`${this.basePath}/api/_search/facilities`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * updateFacility
     * 
     * @param facilityDTO facilityDTO
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public updateFacilityUsingPUT(facilityDTO: FacilityDTO, observe?: 'body', reportProgress?: boolean): Observable<FacilityDTO>;
    public updateFacilityUsingPUT(facilityDTO: FacilityDTO, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<FacilityDTO>>;
    public updateFacilityUsingPUT(facilityDTO: FacilityDTO, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<FacilityDTO>>;
    public updateFacilityUsingPUT(facilityDTO: FacilityDTO, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (facilityDTO === null || facilityDTO === undefined) {
            throw new Error('Required parameter facilityDTO was null or undefined when calling updateFacilityUsingPUT.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];
        let httpContentTypeSelected:string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set("Content-Type", httpContentTypeSelected);
        }

        return this.httpClient.put<FacilityDTO>(`${this.basePath}/api/facilities`,
            facilityDTO,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
