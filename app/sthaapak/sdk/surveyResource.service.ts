/**
 * sthaapakx API
 * sthaapakx API documentation
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional } from '@angular/core';
import {
    HttpClient, HttpHeaders, HttpParams,
    HttpResponse, HttpEvent
} from '@angular/common/http';
import { CustomHttpUrlEncodingCodec } from '../encoder';

import { Observable } from 'rxjs/Observable';

import { SurveyDTO } from '../model/surveyDTO';

import { BASE_PATH, COLLECTION_FORMATS } from '../variables';
import { Configuration } from '../configuration';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import { base } from '../../shared/configs/util';

@Injectable()
export class SurveyResourceService {

    protected basePath = base;
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional() @Inject(BASE_PATH) basePath: string,
        @Optional() configuration: Configuration, public roleGuardService: RoleGuardService) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    public defaultHeaders = new HttpHeaders(
        {
            'Authorization': 'Bearer ' + this.roleGuardService.token
        }
    );


    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (let consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * createSurvey
     * 
     * @param surveyDTO surveyDTO
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public createSurveyUsingPOST(surveyDTO: SurveyDTO, observe?: 'body', reportProgress?: boolean): Observable<SurveyDTO>;
    public createSurveyUsingPOST(surveyDTO: SurveyDTO, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<SurveyDTO>>;
    public createSurveyUsingPOST(surveyDTO: SurveyDTO, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<SurveyDTO>>;
    public createSurveyUsingPOST(surveyDTO: SurveyDTO, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        if (surveyDTO === null || surveyDTO === undefined) {
            throw new Error('Required parameter surveyDTO was null or undefined when calling createSurveyUsingPOST.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];
        let httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set("Content-Type", httpContentTypeSelected);
        }

        return this.httpClient.post<SurveyDTO>(`${this.basePath}/api/surveys`,
            surveyDTO,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * deleteSurvey
     * 
     * @param id id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public deleteSurveyUsingDELETE(id: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public deleteSurveyUsingDELETE(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public deleteSurveyUsingDELETE(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public deleteSurveyUsingDELETE(id: number, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling deleteSurveyUsingDELETE.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.delete<any>(`${this.basePath}/api/surveys/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * getAllOfflineSurveys
     * 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getAllOfflineSurveysUsingGET(observe?: 'body', reportProgress?: boolean): Observable<string>;
    public getAllOfflineSurveysUsingGET(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<string>>;
    public getAllOfflineSurveysUsingGET(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<string>>;
    public getAllOfflineSurveysUsingGET(observe: any = 'body', reportProgress: boolean = false): Observable<any> {

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<string>(`${this.basePath}/api/surveys/offline`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * getAllSurveys
     * 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getAllSurveysUsingGET(observe?: 'body', reportProgress?: boolean): Observable<Array<SurveyDTO>>;
    public getAllSurveysUsingGET(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<SurveyDTO>>>;
    public getAllSurveysUsingGET(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<SurveyDTO>>>;
    public getAllSurveysUsingGET(observe: any = 'body', reportProgress: boolean = false): Observable<any> {

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<Array<SurveyDTO>>(`${this.basePath}/api/surveys`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * getSurvey
     * 
     * @param id id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getSurveyUsingGET(id: number, observe?: 'body', reportProgress?: boolean): Observable<SurveyDTO>;
    public getSurveyUsingGET(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<SurveyDTO>>;
    public getSurveyUsingGET(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<SurveyDTO>>;
    public getSurveyUsingGET(id: number, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling getSurveyUsingGET.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<SurveyDTO>(`${this.basePath}/api/surveys/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * searchSurveys
     * 
     * @param query query
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public searchSurveysUsingGET(query: string, observe?: 'body', reportProgress?: boolean): Observable<Array<SurveyDTO>>;
    public searchSurveysUsingGET(query: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<SurveyDTO>>>;
    public searchSurveysUsingGET(query: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<SurveyDTO>>>;
    public searchSurveysUsingGET(query: string, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        if (query === null || query === undefined) {
            throw new Error('Required parameter query was null or undefined when calling searchSurveysUsingGET.');
        }

        let queryParameters = new HttpParams({ encoder: new CustomHttpUrlEncodingCodec() });
        if (query !== undefined) {
            queryParameters = queryParameters.set('query', <any>query);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<Array<SurveyDTO>>(`${this.basePath}/api/_search/surveys`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * updateSurvey
     * 
     * @param surveyDTO surveyDTO
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public updateSurveyUsingPUT(surveyDTO: SurveyDTO, observe?: 'body', reportProgress?: boolean): Observable<SurveyDTO>;
    public updateSurveyUsingPUT(surveyDTO: SurveyDTO, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<SurveyDTO>>;
    public updateSurveyUsingPUT(surveyDTO: SurveyDTO, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<SurveyDTO>>;
    public updateSurveyUsingPUT(surveyDTO: SurveyDTO, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        if (surveyDTO === null || surveyDTO === undefined) {
            throw new Error('Required parameter surveyDTO was null or undefined when calling updateSurveyUsingPUT.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];
        let httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set("Content-Type", httpContentTypeSelected);
        }

        return this.httpClient.put<SurveyDTO>(`${this.basePath}/api/surveys`,
            surveyDTO,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
