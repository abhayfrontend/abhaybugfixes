/**
 * sthaapakx API
 * sthaapakx API documentation
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * http://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs/Observable';

import { ContentProviderDTO } from '../model/contentProviderDTO';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class ContentProviderResourceService {

    protected basePath = 'http://stagingapi.productivise.io';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (let consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * createContentProvider
     * 
     * @param contentProviderDTO contentProviderDTO
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public createContentProviderUsingPOST(contentProviderDTO: ContentProviderDTO, observe?: 'body', reportProgress?: boolean): Observable<ContentProviderDTO>;
    public createContentProviderUsingPOST(contentProviderDTO: ContentProviderDTO, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<ContentProviderDTO>>;
    public createContentProviderUsingPOST(contentProviderDTO: ContentProviderDTO, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<ContentProviderDTO>>;
    public createContentProviderUsingPOST(contentProviderDTO: ContentProviderDTO, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (contentProviderDTO === null || contentProviderDTO === undefined) {
            throw new Error('Required parameter contentProviderDTO was null or undefined when calling createContentProviderUsingPOST.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];
        let httpContentTypeSelected:string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set("Content-Type", httpContentTypeSelected);
        }

        return this.httpClient.post<ContentProviderDTO>(`${this.basePath}/api/content-providers`,
            contentProviderDTO,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * deleteContentProvider
     * 
     * @param id id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public deleteContentProviderUsingDELETE(id: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public deleteContentProviderUsingDELETE(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public deleteContentProviderUsingDELETE(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public deleteContentProviderUsingDELETE(id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling deleteContentProviderUsingDELETE.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.delete<any>(`${this.basePath}/api/content-providers/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * getAllContentProviders
     * 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getAllContentProvidersUsingGET(observe?: 'body', reportProgress?: boolean): Observable<Array<ContentProviderDTO>>;
    public getAllContentProvidersUsingGET(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<ContentProviderDTO>>>;
    public getAllContentProvidersUsingGET(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<ContentProviderDTO>>>;
    public getAllContentProvidersUsingGET(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<Array<ContentProviderDTO>>(`${this.basePath}/api/content-providers`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * getContentProvider
     * 
     * @param id id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getContentProviderUsingGET(id: number, observe?: 'body', reportProgress?: boolean): Observable<ContentProviderDTO>;
    public getContentProviderUsingGET(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<ContentProviderDTO>>;
    public getContentProviderUsingGET(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<ContentProviderDTO>>;
    public getContentProviderUsingGET(id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling getContentProviderUsingGET.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<ContentProviderDTO>(`${this.basePath}/api/content-providers/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * searchContentProviders
     * 
     * @param query query
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public searchContentProvidersUsingGET(query: string, observe?: 'body', reportProgress?: boolean): Observable<Array<ContentProviderDTO>>;
    public searchContentProvidersUsingGET(query: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<ContentProviderDTO>>>;
    public searchContentProvidersUsingGET(query: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<ContentProviderDTO>>>;
    public searchContentProvidersUsingGET(query: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (query === null || query === undefined) {
            throw new Error('Required parameter query was null or undefined when calling searchContentProvidersUsingGET.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (query !== undefined) {
            queryParameters = queryParameters.set('query', <any>query);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<Array<ContentProviderDTO>>(`${this.basePath}/api/_search/content-providers`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * updateContentProvider
     * 
     * @param contentProviderDTO contentProviderDTO
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public updateContentProviderUsingPUT(contentProviderDTO: ContentProviderDTO, observe?: 'body', reportProgress?: boolean): Observable<ContentProviderDTO>;
    public updateContentProviderUsingPUT(contentProviderDTO: ContentProviderDTO, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<ContentProviderDTO>>;
    public updateContentProviderUsingPUT(contentProviderDTO: ContentProviderDTO, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<ContentProviderDTO>>;
    public updateContentProviderUsingPUT(contentProviderDTO: ContentProviderDTO, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (contentProviderDTO === null || contentProviderDTO === undefined) {
            throw new Error('Required parameter contentProviderDTO was null or undefined when calling updateContentProviderUsingPUT.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];
        let httpContentTypeSelected:string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set("Content-Type", httpContentTypeSelected);
        }

        return this.httpClient.put<ContentProviderDTO>(`${this.basePath}/api/content-providers`,
            contentProviderDTO,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
