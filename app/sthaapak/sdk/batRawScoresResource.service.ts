/**
 * sthaapakx API
 * sthaapakx API documentation
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * http://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional } from '@angular/core';
import {
    HttpClient, HttpHeaders, HttpParams,
    HttpResponse, HttpEvent
} from '@angular/common/http';
import { CustomHttpUrlEncodingCodec } from '../encoder';

import { Observable } from 'rxjs/Observable';

import { BatRawScoresDTO } from '../model/batRawScoresDTO';

import { BASE_PATH, COLLECTION_FORMATS } from '../variables';
import { Configuration } from '../configuration';
import { base } from '../../shared/configs/util';


@Injectable()
export class BatRawScoresResourceService {

    protected basePath = base + '';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional() @Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (let consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * batchCreate
     * 
     * @param scores scores
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public batchCreateUsingPOST(scores: Array<BatRawScoresDTO>, observe?: 'body', reportProgress?: boolean): Observable<string>;
    public batchCreateUsingPOST(scores: Array<BatRawScoresDTO>, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<string>>;
    public batchCreateUsingPOST(scores: Array<BatRawScoresDTO>, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<string>>;
    public batchCreateUsingPOST(scores: Array<BatRawScoresDTO>, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        if (scores === null || scores === undefined) {
            throw new Error('Required parameter scores was null or undefined when calling batchCreateUsingPOST.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];
        let httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set("Content-Type", httpContentTypeSelected);
        }

        return this.httpClient.post<string>(`${this.basePath}/api/bat-raw-scores/add/bulk`,
            scores,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * createBatRawScores
     * 
     * @param batRawScoresDTO batRawScoresDTO
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public createBatRawScoresUsingPOST(batRawScoresDTO: BatRawScoresDTO, observe?: 'body', reportProgress?: boolean): Observable<BatRawScoresDTO>;
    public createBatRawScoresUsingPOST(batRawScoresDTO: BatRawScoresDTO, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<BatRawScoresDTO>>;
    public createBatRawScoresUsingPOST(batRawScoresDTO: BatRawScoresDTO, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<BatRawScoresDTO>>;
    public createBatRawScoresUsingPOST(batRawScoresDTO: BatRawScoresDTO, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        if (batRawScoresDTO === null || batRawScoresDTO === undefined) {
            throw new Error('Required parameter batRawScoresDTO was null or undefined when calling createBatRawScoresUsingPOST.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];
        let httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set("Content-Type", httpContentTypeSelected);
        }

        return this.httpClient.post<BatRawScoresDTO>(`${this.basePath}/api/bat-raw-scores`,
            batRawScoresDTO,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * deleteBatRawScores
     * 
     * @param id id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public deleteBatRawScoresUsingDELETE(id: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public deleteBatRawScoresUsingDELETE(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public deleteBatRawScoresUsingDELETE(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public deleteBatRawScoresUsingDELETE(id: number, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling deleteBatRawScoresUsingDELETE.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.delete<any>(`${this.basePath}/api/bat-raw-scores/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * getAllBatRawScores
     * 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getAllBatRawScoresUsingGET(observe?: 'body', reportProgress?: boolean): Observable<Array<BatRawScoresDTO>>;
    public getAllBatRawScoresUsingGET(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<BatRawScoresDTO>>>;
    public getAllBatRawScoresUsingGET(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<BatRawScoresDTO>>>;
    public getAllBatRawScoresUsingGET(observe: any = 'body', reportProgress: boolean = false): Observable<any> {

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<Array<BatRawScoresDTO>>(`${this.basePath}/api/bat-raw-scores`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * getAllBatScoresAsCsv
     * 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getAllBatScoresAsCsvUsingGET(observe?: 'body', reportProgress?: boolean): Observable<string>;
    public getAllBatScoresAsCsvUsingGET(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<string>>;
    public getAllBatScoresAsCsvUsingGET(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<string>>;
    public getAllBatScoresAsCsvUsingGET(observe: any = 'body', reportProgress: boolean = false): Observable<any> {

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<string>(`${this.basePath}/api/bat-raw-scores/all/csv`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * getBatRawScores
     * 
     * @param id id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getBatRawScoresUsingGET(id: number, observe?: 'body', reportProgress?: boolean): Observable<BatRawScoresDTO>;
    public getBatRawScoresUsingGET(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<BatRawScoresDTO>>;
    public getBatRawScoresUsingGET(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<BatRawScoresDTO>>;
    public getBatRawScoresUsingGET(id: number, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling getBatRawScoresUsingGET.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<BatRawScoresDTO>(`${this.basePath}/api/bat-raw-scores/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * searchBatRawScores
     * 
     * @param query query
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public searchBatRawScoresUsingGET(query: string, observe?: 'body', reportProgress?: boolean): Observable<Array<BatRawScoresDTO>>;
    public searchBatRawScoresUsingGET(query: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<BatRawScoresDTO>>>;
    public searchBatRawScoresUsingGET(query: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<BatRawScoresDTO>>>;
    public searchBatRawScoresUsingGET(query: string, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        if (query === null || query === undefined) {
            throw new Error('Required parameter query was null or undefined when calling searchBatRawScoresUsingGET.');
        }

        let queryParameters = new HttpParams({ encoder: new CustomHttpUrlEncodingCodec() });
        if (query !== undefined) {
            queryParameters = queryParameters.set('query', <any>query);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        return this.httpClient.get<Array<BatRawScoresDTO>>(`${this.basePath}/api/_search/bat-raw-scores`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * updateBatRawScores
     * 
     * @param batRawScoresDTO batRawScoresDTO
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public updateBatRawScoresUsingPUT(batRawScoresDTO: BatRawScoresDTO, observe?: 'body', reportProgress?: boolean): Observable<BatRawScoresDTO>;
    public updateBatRawScoresUsingPUT(batRawScoresDTO: BatRawScoresDTO, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<BatRawScoresDTO>>;
    public updateBatRawScoresUsingPUT(batRawScoresDTO: BatRawScoresDTO, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<BatRawScoresDTO>>;
    public updateBatRawScoresUsingPUT(batRawScoresDTO: BatRawScoresDTO, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
        if (batRawScoresDTO === null || batRawScoresDTO === undefined) {
            throw new Error('Required parameter batRawScoresDTO was null or undefined when calling updateBatRawScoresUsingPUT.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set("Accept", httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];
        let httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set("Content-Type", httpContentTypeSelected);
        }

        return this.httpClient.put<BatRawScoresDTO>(`${this.basePath}/api/bat-raw-scores`,
            batRawScoresDTO,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
