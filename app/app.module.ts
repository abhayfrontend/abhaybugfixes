
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from "./shared/shared.module";
import { ToastrModule } from 'ngx-toastr';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { ContentLayoutComponent } from "./layouts/content/content-layout.component";
import { FullLayoutComponent } from "./layouts/full/full-layout.component";
import { DragulaService } from 'ng2-dragula';
import { AuthService } from './shared/auth/auth.service';
import { AuthGuardService } from './shared/auth/auth-guard.service';

import * as $ from 'jquery';
import { RoleGuardService } from './shared/auth/role-guard.service';
import { AuthenticationService } from './shared/auth/authentication-service.service';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppConfigResourceService, UserResourceService, NotificationResourceService, CampaignGroupResourceService, DeviceResourceService, ContentResourceService, PublishedContentResourceService, MicroContentResourceService, IndustryResourceService, JobFamilyResourceService, JobRoleResourceService, CoursePlanResourceService, CareerPathResourceService, SkillResourceService } from './sthaapak';
import { PreviewService } from './shared/auth/preview.service';
import { LoginComponent } from './login/login.component';
import { LinkComponent } from './link/link.component';
import { NgxEditorModule } from 'ngx-editor';
import { UserGuardService } from './shared/auth/userguard.service';
import { InviteComponent } from './invite/invite.component';
import { SupportGroupResourceService } from './sthaapak/sdk/supportGroupResource.service';
import { SurveyQuestionResourceService } from './sthaapak/sdk/surveyQuestionResource.service';
import { SurveyResourceService } from './sthaapak/sdk/surveyResource.service';
import { ChatHistoryResourceService } from './sthaapak/sdk/chatHistoryResource.service';
import { TaskGroupResourceService } from './sthaapak/sdk/taskGroupResource.service';
import { TaskResourceService } from './sthaapak/sdk/taskResource.service';
import { TinymceModule } from 'angular2-tinymce';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { DxPivotGridModule, DxCheckBoxModule } from 'devextreme-angular';
import { AnalyticsComponent } from './analytics/analytics.component';

export function tokenGetter() {
    return localStorage.getItem('token');
}

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        LoginComponent,
        AppComponent,
        FullLayoutComponent,
        ContentLayoutComponent,
        LinkComponent,
        InviteComponent,
        ForgotPasswordComponent
    ],
    imports: [
        JwtModule.forRoot({
            config: {
                tokenGetter: tokenGetter,
                whitelistedDomains: ['localhost:4200'],
                blacklistedRoutes: []
            }
        }),
        BrowserAnimationsModule,
        StoreModule.forRoot({}),
        AppRoutingModule,
        SharedModule,
        HttpClientModule,
        ToastrModule.forRoot(),
        NgbModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBr5_picK8YJK7fFR2CPzTVMj6GG1TtRGo'
        }),
        FormsModule,
        ReactiveFormsModule,
        NgxEditorModule,
        TinymceModule.withConfig({})


    ],


    providers: [
        AuthService,
        AuthGuardService,
        DragulaService,
        RoleGuardService,
        AuthenticationService,
        JwtHelperService,
        PreviewService,
        AppConfigResourceService,
        UserResourceService,
        NotificationResourceService,
        CampaignGroupResourceService,
        DeviceResourceService,
        PublishedContentResourceService,
        MicroContentResourceService,
        UserGuardService,
        SupportGroupResourceService,
        SurveyQuestionResourceService,
        SurveyResourceService,
        IndustryResourceService,
        ChatHistoryResourceService,
        JobFamilyResourceService,
        JobRoleResourceService,
        TaskGroupResourceService,
        TaskResourceService,
        IndustryResourceService,
        JobRoleResourceService,
        CoursePlanResourceService,
        CareerPathResourceService,
        SkillResourceService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
