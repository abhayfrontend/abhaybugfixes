import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobFamilyAddComponent } from './jobFamily-add.component';
import { JobFamilyListComponent } from './jobFamily-list.component';
import { HomeComponent } from './app/home/home.component';
import { AfterUploadComponent } from './app/after-upload/after-upload.component';


const routes: Routes = [
  {
    path: '',
    children: [

      {
        path: 'add',
        component: JobFamilyAddComponent,
        data: {
          title: 'Add newJobFamily '
        }
      }, {
        path: 'list',
        component: JobFamilyListComponent,
        data: {
          title: 'ListJobFamily'
        }
      },
      {
        path: 'bulk',
        component: HomeComponent,
        data: {
          title: 'Bulk RegisterJobFamily'
        }
      },
      {

        path: 'afterUpload',
        component: AfterUploadComponent

      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobFamilyRoutingModule { }
