import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PreviewService } from '../../../shared/auth/preview.service';

@Component({
  selector: 'app-create-sms',
  templateUrl: './create-sms.component.html',
  styleUrls: ['./create-sms.component.css']
})
export class CreateSmsComponent implements OnInit {

  @Output() messageReady = new EventEmitter;
  templates = [{
    name: 'Registration - English', message: `Hello, Welcome to Productivise. Please click on the link below to get the App.
   Link: https://login.productivise.io/create/com.sthaapak.productivise/` + this.previewService.organizationId.getValue()
  }];
  templateName;
  nameOfTemplate;
  message;
  constructor(private previewService: PreviewService) { }

  ngOnInit() {


  }
  IsMessageReady() {
    if (this.message) {
      this.messageReady.emit(this.message);
    }
    else {
      this.messageReady.emit('');
    }
  }

  sendMsg() {
    // console.log('tn:', this.nameOfTemplate);
    // console.log('tn:', this.message);
    alert("Msg Sent");
  }

  changeTemplate() {
    // console.log('tn:', this.templateName);
    this.message = this.templateName;
    this.IsMessageReady();
  }


}
