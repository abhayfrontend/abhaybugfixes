import { Component, OnInit, Input } from '@angular/core';
import { PreviewService } from '../../../shared/auth/preview.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../../shared/auth/role-guard.service';
import * as moment from 'moment';
import { base } from '../../../shared/configs/util';
import { IndustryResourceService, JobFamilyResourceService } from '../../../sthaapak';
import swal from 'sweetalert2';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // Variables
  message;
  file: boolean = true;
  created;
  breadCrumb;
  // End Variables
  contacts = [];
  organization;
  sent = 0;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };
  constructor(public previewService: PreviewService, private router: Router, private http: HttpClient,
    private roleGuardService: RoleGuardService, private department: JobFamilyResourceService) {
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnInit() {
    this.contacts = this.previewService.SmsList.getValue();
    this.http.get(base + '/api/organizations/' + this.previewService.organizationId.getValue(), this.httpOptions).subscribe(org => {
      this.organization = <any>org;
    })
  }

  EnableSend(e) {
    this.message = e;
  }


  saveIndustry() {
    this.created = 0;
    swal({
      title: 'Are you sure you want to create these departments?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Yes, create it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then((confirm) => {
      if (confirm.value) {
        for (let i = 0; i < this.contacts.length; i++) {
          this.department.createJobFamilyUsingPOST({
            "name": this.contacts[i][0],
            "tags": null,
            "description": null,
            "url": null,
            "bigImageLink": this.contacts[i][1],
            "videoLink": this.contacts[i][2],
            "pdfLink": this.contacts[i][3],
            "audioLink": this.contacts[i][4],
            "content": this.contacts[i][5],
            "displayOrder": this.contacts[i][6],
            "organizations": [this.organization]
          }).subscribe(ind => {

            // console.log('department:', ind)
            if (ind.id) {
              this.created += 1;
            }
            if (i === this.contacts.length - 1) {
              swal('Successfully Created ' + this.created + ' Departments.', '', 'success');

            }

          })

        }
      }
      else {
        swal('No Departments were Created.', '', 'error');
      }

    });
  }
  enableNext(e) {
    this.file = e.length > 0 ? false : true;
    this.previewService.editSmsList(e);
    // console.log('e:', e);
    this.contacts = this.previewService.SmsList.getValue();
  }



}
