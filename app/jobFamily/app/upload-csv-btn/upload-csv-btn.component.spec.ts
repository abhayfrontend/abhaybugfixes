import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadCsvBtnComponent } from './upload-csv-btn.component';

describe('UploadCsvBtnComponent', () => {
  let component: UploadCsvBtnComponent;
  let fixture: ComponentFixture<UploadCsvBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadCsvBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadCsvBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
