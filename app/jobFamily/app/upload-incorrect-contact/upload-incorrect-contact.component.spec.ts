import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadIncorrectContactComponent } from './upload-incorrect-contact.component';

describe('UploadIncorrectContactComponent', () => {
  let component: UploadIncorrectContactComponent;
  let fixture: ComponentFixture<UploadIncorrectContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadIncorrectContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadIncorrectContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
