import { Component, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { UserResourceService, CampaignGroupResourceService } from '../sthaapak';
import { PreviewService } from '../shared/auth/preview.service';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import swal from 'sweetalert2';
import { base } from '../shared/configs/util';
import { SupportGroupResourceService } from '../sthaapak/sdk/supportGroupResource.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-dt-filter',
    templateUrl: './jobFamily-list.component.html',
    styleUrls: ['./jobFamily-list.component.scss']
})

export class JobFamilyListComponent implements OnInit {
    rows = [];
    filterOption;
    temp = [];
    data = [];
    groups = [];
    inputOption = {};
    createdBy = [];
    sgroup;
    cgroup;
    status = ['Active', 'Inactive'];
    // Table Column Titles
    columns = [
        { prop: 'firstName' },
        { prop: 'lastName' },
        { prop: 'phone' },
        { prop: 'createdDate' }
    ];

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.roleGuardService.token
        })
    };

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private userService: UserResourceService, private previewService: PreviewService, private http: HttpClient, private router: Router,
        private roleGuardService: RoleGuardService, private groupService: CampaignGroupResourceService, private supportGroup: SupportGroupResourceService) {
        if (!localStorage.getItem('token')) {
            this.router.navigateByUrl('/login');
        }
    }
    ngOnInit() {
        this.getUsers();
        this.getGroups();
    }

    changeFilter(e) {
        // console.log('filter:', this.temp);
        this.filterOption = e.target.value;
        this.rows = this.temp;
    }

    reset() {
        this.filterOption = '';
        this.rows = this.temp;
    }

    getUsers() {
        this.temp = [];
        this.rows = [];
        this.data = [];
        this.http.get(base + '/api/industries', this.httpOptions).subscribe(res => {
            let result = <any>res;
            // console.log('date1:', result[0]);
            // console.log('reult:', result)
            // console.log('org1: ', Number(this.previewService.organizationId.getValue()));
            result.forEach(element => {
                element.jobFamiliesList = [];
                let jobRoleList;
                // console.log('org2: ', element.orgId)
                element.organizations.forEach(el => {
                    // console.log('org2: ', el);
                    if (Number(el.id) === Number(this.previewService.organizationId.getValue()) && element.tags === 'newinfo') {
                        element.jobFamilies.forEach(jf => {
                            jobRoleList = jf.jobRoles.map(e => e.name);
                            this.data.push({ name: jf.name, image: jf.image, content: jf.content, url: jf.url, jobRoleList: jobRoleList.join(','), industry: element.name });
                        });

                    }
                });

            });

            // console.log('data:', this.data);
            this.temp = [...this.data];
            this.rows = this.data;
            // console.log('temp:', this.temp);



        })
    }

    filterListByType(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            if (val === 'VIDEO' && d.videoLink) {
                return true;

            }
            if (val === 'BLOG' && d.previewImage) {
                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByCreatedBy(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            return d.createdBy === val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByStatus(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log(d.activated);
            // console.log(val);
            if (val === 'Active') {
                return d.activated === true;
            }
            if (val === 'Inactive') {
                return d.activated === false;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByRegDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log('date:', val)
            if (new Date(d.createdDate).getDate() === new Date(val).getDate() && new Date(d.createdDate).getMonth() === new Date(val).getMonth() && new Date(d.createdDate).getFullYear() === new Date(val).getFullYear()) {


                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByPublDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {

            if (new Date(d.publishedDate).getDate() === new Date(val).getDate() && new Date(d.publishedDate).getMonth() === new Date(val).getMonth() && new Date(d.publishedDate).getFullYear() === new Date(val).getFullYear()) {

                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }


    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.firstName.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    deleteContent(row) {

        swal({
            title: 'Are you sure you want to delete this industry record?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.userService.deleteUserUsingDELETE(row.login).subscribe(res => {
                    let result = <any>res;
                    // console.log('delte', result)
                    this.getUsers();

                })
            }
            else {
                swal(
                    'Cancelled',
                    'The user record was not deleted.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'The user record was not deleted.',
                    'error'
                )
            }
        })


    }

    addToGroup(row) {
        swal.mixin({
            input: 'text',
            confirmButtonText: 'Next &rarr;',
            showCancelButton: true,
            progressSteps: ['1', '2', '3']
        }).queue([{
            title: 'Select a Group',
            input: 'select',
            inputOptions: this.inputOption,
            inputPlaceholder: 'Select a Group',
            showCancelButton: true,
            inputValidator: (value) => {
                return new Promise((resolve) => {
                    if (value) {
                        resolve()
                    } else {
                        resolve('You need to select one Group')
                    }
                })
            }
        }, {
            title: 'Are you sure you want to add this user to the group?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, add it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }]).then(result => {
            let proceed = true;
            // console.log('form', result);
            if (result.value) {
                let campaignGroup = result.value[0];

                this.groups.forEach(e => {
                    if (e.name === campaignGroup) {
                        this.cgroup = <any>e;
                        // console.log('cgr', this.cgroup);
                        this.cgroup.learners.forEach(element => {
                            if (element.id === row.learner.id) {
                                swal({
                                    title: 'Error',
                                    text: 'This user is already a part of this group.',
                                    onClose: () => swal.close()
                                })
                                proceed = false;
                                return;
                            }
                        });
                    }

                });

                this.supportGroup.getAllSupportGroupsUsingGET().subscribe(el => {
                    let sgroups = <any>el;
                    // console.log(campaignGroup);
                    sgroups.forEach(element => {

                        if (campaignGroup === element.name && Number(element.category) === Number(this.previewService.organizationId.getValue())) {
                            this.sgroup = element;
                            // console.log(this.sgroup);
                        }
                    });

                })
                let conf = this.cgroup;
                // console.log(conf);
                conf['learners'].push(row.learner);

                // console.log('2', conf);
                if (proceed) {
                    this.groupService.updateCampaignGroupUsingPUT(conf).subscribe(ans => {
                        this.http.get(base + '/api/members/' + row.langKey, this.httpOptions).subscribe(member => {
                            let smember = <any>member;
                            this.sgroup['members'].push(smember);
                            this.supportGroup.updateSupportGroupUsingPUT(this.sgroup).subscribe(don => {
                                // console.log(don);
                                this.http.post(base + '/chat-histories', {
                                    "attachment": true,
                                    "attachmentLink": "string",
                                    "category": "string",
                                    "description": conf['learner'].name + " has been added to " + this.sgroup.name + " group by " + this.previewService.Username.getValue(),
                                    "from": "string",
                                    "group": this.sgroup.id,
                                    "name": "string",
                                    "orgId": this.previewService.organizationId.getValue(),
                                    "subCategory": "broadcast",
                                    "tags": "string",
                                    "to": "string",
                                    "ts": new Date().getTime()
                                }, this.httpOptions).subscribe(chat => {
                                    // console.log(chat);
                                })
                            })
                        })
                    })
                }




            }
            else {
                swal('User not added in the Group', '', 'error')
            }
        });
    }

    getGroups() {
        this.groupService.getAllCampaignGroupsUsingGET().subscribe(res => {
            let result = <any>res;
            // console.log('groups:', result);
            this.groups = [];
            result.forEach((e, i) => {


                if (Number(e.orgId) === Number(this.previewService.organizationId.getValue())) {
                    this.groups.push(e);
                    this.inputOption[e.name] = e.name;
                    // console.log('g:', this.groups);
                }
            });
            // console.log('groups2:', this.groups);
        })
    }
}
