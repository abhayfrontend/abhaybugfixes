import { Component, OnInit, OnDestroy } from '@angular/core';
import { QuillEditorComponent } from 'ngx-quill/src/quill-editor.component';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import { PreviewService } from '../shared/auth/preview.service';
import * as AWS from '../shared/configs/aws.config';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { base, proxyApi } from '../shared/configs/util';
@Component({
  selector: 'app-config)',
  templateUrl: './config-add.component.html',
  styleUrls: ['./config-add.component.scss']
})


export class ConfigAddComponent implements OnInit, OnDestroy {

  // Variables
  about;
  dashboardIcon;
  dashboardName;
  appVideo;
  appTheme;
  appLogo;
  appName;
  subtabs;
  obj;
  page;
  ConfigImage;
  validFileExtensions = ['.jpg', '.jpeg', '.bmp', '.gif', '.png', '.ico'];
  validFileExtensionsVideo = ['.mp4', '.avi', '.mov', '.mwv', '.flv'];
  dashboard;
  name;
  subCategory;
  interface;
  microBackground = '#0074be';
  items = [];
  orgId;
  numbers = [];
  oldConfigs = [];
  tabs;
  finish = 0;
  subCategory_0;
  subCategory_1;
  subCategory_2;
  subCategory_3;
  subCategory_4;
  subCategory_5;
  rule;
  splashBackground;
  iconOption;
  // End Variables
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };


  constructor(private http: HttpClient, private roleGuardService: RoleGuardService, public previewService: PreviewService, private router: Router) {


    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }


  imageChanged(event) {
    let timerInterval;
    const file = event.target.files[0];
    let _URL = window.URL;
    let error = false;
    if (event.target.files && event.target.files[0]) {

      let sFileName = event.target.files[0].name;
      // console.log('sfilename: ', sFileName);
      let blnValid = false;
      for (let j = 0; j < this.validFileExtensions.length; j++) {
        let sCurExtension = this.validFileExtensions[j];
        if (sFileName.slice(sFileName.length - sCurExtension.length, sFileName.length).toLowerCase() === sCurExtension.toLowerCase()) {
          blnValid = true;
          break;
        }
      }
      if (!blnValid) {
        swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensions.join(', '));
        return false;
      }
      // console.log('size:', event.target.files[0].size);
      if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 2000000)) {
        swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 2 Mb');
        return false;
      }

      let reader = new FileReader();


      //Initiate the FileReader object.
      //Read the contents of Image File.
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e) => {
        //Initiate the JavaScript Image object.
        let image = new Image();
        //Set the Base64 string return from FileReader as source.
        image.src = _URL.createObjectURL(file);
        image.onload = () => {
          //Determine the Height and Width.
          let height = image.height;
          let width = image.width;
          swal({
            title: 'Uploading!',
            onOpen: () => {
              swal.showLoading();
              Promise.resolve(this.uploadFile(file)).then(location => {
                // console.log('location:', location);
                if (this.page === 1) {
                  this.appLogo = <any>location
                }
                else {
                  this.dashboardIcon = <any>location
                }

              });
              timerInterval = setInterval(() => {
                if (this.appLogo || this.dashboardIcon) {
                  swal.close();
                  clearInterval(timerInterval);
                  // console.log('time1')
                }
              }, 100)
            },
            onClose: () => {
              clearInterval(timerInterval)
            }
          }).then(ans => {
            clearInterval(timerInterval);
          });
          return true;
        };
      }


    }
  }

  uploadFile(file) {
    let upload = AWS.s3.upload({ Key: this.previewService.organizationId.getValue() + '/config/images/' + new Date() + file.name, Bucket: AWS.bucketName, Body: file, ACL: 'public-read' });
    let promise = upload.promise();
    return promise.then((data) => {
      // console.log(data);
      return data.Location;
    }, (err) => {
      // console.log(err);
    });

  }

  videoChanged(event) {
    let timerInterval;
    let _URL = window.URL;
    const file = event.target.files[0];
    let error = false;
    let upload = AWS.s3.upload({ Key: this.previewService.organizationId.getValue() + '/config/videos/' + file.name + new Date(), Bucket: AWS.bucketName, Body: file, ACL: 'public-read' });
    let promise = upload.promise();
    if (event.target.files && event.target.files[0]) {

      let sFileName = event.target.files[0].name;
      // console.log('sfilename: ', sFileName);
      let blnValid = false;
      for (let j = 0; j < this.validFileExtensionsVideo.length; j++) {
        let sCurExtension = this.validFileExtensionsVideo[j];
        if (sFileName.slice(sFileName.length - sCurExtension.length, sFileName.length).toLowerCase() === sCurExtension.toLowerCase()) {
          blnValid = true;
          break;
        }
      }
      if (!blnValid) {
        swal('Invalid File', 'Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensionsVideo.join(', '));
        return false;
      }
      // console.log('size:', event.target.files[0].size);
      if ((event.target.files[0].size < 5000) || (event.target.files[0].size > 20000000)) {
        swal('File Size Too Large or Too Small', 'Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 2 Mb');
        return false;
      }
      const file = event.target.files[0];
      swal({
        title: 'Uploading!',
        onOpen: () => {
          swal.showLoading();
          promise.then((data) => {
            // console.log(data);
            this.appVideo = data.Location;

          }, (err) => {
            // console.log(err);
          });
          timerInterval = setInterval(() => {
            if (this.appVideo) {
              swal.close(); clearInterval(timerInterval);
            }
          }, 100)
        },
        onClose: () => {
          clearInterval(timerInterval)
        }
      });
    }
  }

  ngOnInit() {
    this.page = 1;
    this.orgId = this.previewService.organizationId.getValue();
    this.obj = <any>this.previewService.Config.getValue();
    if (this.obj.id) {
      this.dashboard = this.obj.category;
      this.name = this.obj.value;
      this.subCategory = this.obj.subCategory;
      this.interface = this.obj.name;
      this.microBackground = this.dashboard === 'microLearningBackground' ? this.obj.value : null;
    }
  }

  ngOnDestroy() {
    this.previewService.editConfig({});
  }

  changeSubcategories(e) {
    this.numbers = new Array(Number(this.subtabs)).fill(0).map((x, i) => i);
    // console.log('num', this.numbers, this.subtabs);

  }

  setDashboardItems() {
    this.items = Array(Number(this.tabs));
  }

  clearForm() {
    this.appName = '';
    this.appLogo = '';
    this.appTheme = '';
    this.appVideo = '';
    this.about = '';
    this.interface = '';
    this.dashboardName = '';
    this.rule = '';
    this.iconOption = '';
    for (let index = 0; index < this.subtabs; index++) {
      let element = <HTMLInputElement>document.getElementById('subCategory' + index)
      element.value = '';
      element.innerText = '';

    }
    this.subtabs = '';

  }

  saveConfig() {
    if (this.page === 1) {
      if (!this.validateContentForm1()) {
        return;
      }
    }

    if (this.page === 2) {
      if (!this.validateContentForm2()) {
        return;
      }
    }

    if (this.page >= 3) {
      if (!this.validateContentForm3()) {
        return;
      }
    }





    // console.log('page', this.page, this.tabs);
    this.page++;
    if (this.page >= 3) {
      let subCategories = '';
      for (let index = 0; index < this.subtabs - 1; index++) {
        let text = (<HTMLInputElement>document.getElementById('subCategory' + index)).value
        subCategories += text + ',';
      }
      subCategories += (<HTMLInputElement>document.getElementById('subCategory' + String(this.subtabs - 1))).value
      this.items[this.page - 4] = {
        name: this.interface,
        value: this.dashboardName,
        subCategory: subCategories,
        category: 'DASHBOARD',
        organizationId: this.previewService.organizationId.getValue(),
        description: this.dashboardIcon
      };
      subCategories = '';
      this.clearForm();
      // console.log('Items:', this.items);

      // console.log('page:', this.page);
      // console.log('tabs:', this.tabs);
    }
  }
  createConfig() {
    this.saveConfig();
    // console.log('app:', this.items);
    swal({
      title: 'Are you sure you want to delete the Previous Configuration and Add this Configuration?',
      text: "You won't be able to revert this! Deleting this app-config will make all the content in the previous Dashboards Invisible.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then((confirm) => {
      // console.log('con:', confirm);
      if (confirm.value === true) {
        this.getConfigs().subscribe(configs => {
          this.oldConfigs = <any>configs;
          this.oldConfigs.forEach(el => {
            this.http.delete(base + '/api/app-configs/' + el.id, this.httpOptions).subscribe(response => {
              // console.log('del:', response);
            });
          });
          this.createConfigs();

        });
      }
      else {
        swal(
          'Cancelled',
          'Your configuration item was not deleted.',
          'error'
        )
      }
    }, (dismiss) => {
      // console.log('dismis:', dismiss)
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Your configuration item was not deleted.',
          'error'
        )
      }
    })

  }

  getConfigs() {
    return this.http.get(base + '/api/app-configs/org/' + this.orgId);
  }

  createConfigs() {
    // console.log('Items:', this.items);
    // console.log('app:', [{
    //   'name': this.appName,
    //   'description': this.appLogo,
    //   'category': 'APPNAME',
    //   'value': this.appName,
    //   organizationId: this.previewService.organizationId.getValue()
    // }, {
    //   'name': 'primary_color',
    //   'description': null,
    //   'category': 'APPTHEME',
    //   'value': this.appTheme,
    //   organizationId: this.previewService.organizationId.getValue()
    // }]);

    this.http.post(base + '/api/app-configs', {
      'name': this.appName,
      'description': this.appLogo,
      'category': 'APPNAME',
      'value': this.appName,
      organizationId: this.previewService.organizationId.getValue()
    }, this.httpOptions).subscribe(configRes => {
      // console.log('configLast:', configRes);
    });

    this.http.post(base + '/api/app-configs', {
      'name': 'primary_color',
      'description': null,
      'category': 'APPTHEME',
      'value': this.appTheme,
      organizationId: this.previewService.organizationId.getValue()
    }, this.httpOptions).subscribe(configRes => {
      // console.log('configLast:', configRes);
    });
    switch (this.appTheme) {
      case 'Green':
        this.appTheme = "#4caf50";
        break;
      case 'Sky Blue':
        this.appTheme = "#e0eff9";
        break;
      case 'Dark Blue':
        this.appTheme = "#039be5";
        break;
    }

    this.http.post(base + '/api/app-configs', {
      "name": "splashLogoBackground",
      "description": this.appLogo,
      "category": "splashBackground",
      "value": null,
      "subCategory": null,
      "tags": null,
      "type": null,
      "numValue": null,
      "path": null,
      "icon": null,
      "displayOrder": null,
      'organizationId': this.orgId
    }, this.httpOptions).subscribe(configRes => {
      // console.log('config:', configRes)

    });

    this.http.post(base + '/api/app-configs', {
      'name': 'splashBackground',
      'description': this.splashBackground || 'https://s3.ap-south-1.amazonaws.com/app-config-images/splash_bg.jpg',
      'category': 'splashBackground',
      'organizationId': this.orgId
    }, this.httpOptions).subscribe(configRes => {
      // console.log('config:', configRes)

    });
    this.http.post(base + '/api/app-configs', {
      "name": "sideMenuBackground\t\t\t\t\t\t",
      "description": "https://s3.ap-south-1.amazonaws.com/app-config-images/Ativitti/sidebar-bg.png",
      "category": "sideMenuBackground",
      "value": null,
      "subCategory": null,
      "tags": null,
      "type": null,
      "numValue": null,
      "path": null,
      "icon": null,
      "displayOrder": null,
      'organizationId': this.orgId
    }, this.httpOptions).subscribe(configRes => {
      // console.log('config:', configRes)

    });

    this.http.post(base + '/api/app-configs', {
      "name": "statusBarBackground\t",
      "description": null,
      "category": "statusBarBackground",
      "value": "#d8e0e5",
      "subCategory": null,
      "tags": null,
      "type": null,
      "numValue": null,
      "path": null,
      "icon": null,
      "displayOrder": null,
      'organizationId': this.orgId
    }, this.httpOptions).subscribe(configRes => {
      // console.log('config:', configRes)

    });

    this.http.post(base + '/api/app-configs', {
      "name": "microLearningBackground",
      "description": null,
      "category": "microLearningBackground",
      "value": "#6b70b8",
      "subCategory": null,
      "tags": null,
      "type": null,
      "numValue": null,
      "path": null,
      "icon": null,
      "displayOrder": null,
      'organizationId': this.orgId
    }, this.httpOptions).subscribe(configRes => {
      // console.log('config:', configRes)

    });
    this.http.post(base + '/api/app-configs', {
      "name": "dashboardBackground",
      "description": null,
      "category": "dashboardBackground",
      "value": this.appTheme,
      "subCategory": null,
      "tags": null,
      "type": null,
      "numValue": null,
      "path": null,
      "icon": null,
      "displayOrder": null,
      'organizationId': this.orgId
    }, this.httpOptions).subscribe(configRes => {
      // console.log('config:', configRes)

    });
    this.http.post(base + '/api/app-configs', {
      "name": "toolbarBackground\t",
      "description": null,
      "category": "toolbarBackground",
      "value": this.appTheme,
      "subCategory": null,
      "tags": null,
      "type": null,
      "numValue": null,
      "path": null,
      "icon": null,
      "displayOrder": null,
      'organizationId': this.orgId
    }, this.httpOptions).subscribe(configRes => {
      // console.log('config:', configRes)

    });


    if (this.appVideo || this.about) {

      this.http.post(base + '/api/app-configs', {
        'name': 'About Us',
        'description': this.about,
        'category': 'ABOUT',
        'value': 'About Us',
        'subCategory': null,
        'tags': null,
        'type': null,
        'numValue': null,
        'path': this.appVideo,
        'icon': null,
        'displayOrder': this.items.length + 3,
        'extraData': [],
        'multiLinguals': [],
        organizationId: this.previewService.organizationId.getValue()
      }, this.httpOptions).subscribe(configRes => {
        // console.log('configLast:', configRes);
      });

      this.http.post(base + '/api/app-configs', {
        'name': 'About Us',
        'description': null,
        'category': 'SIDEMENU',
        'value': 'About Us',
        'displayOrder': this.items.length + 3,
        organizationId: this.previewService.organizationId.getValue()
      }, this.httpOptions).subscribe(configRes => {
        // console.log('configLast:', configRes);
      });

    }


    for (let index = 0; index < this.items.length - 1; index++) {
      this.http.post(base + '/api/app-configs', this.items[index], this.httpOptions).subscribe(configRes => {
        // console.log('config:', configRes)
      });
    }
    this.http.post(base + '/api/app-configs', this.items[this.items.length - 1], this.httpOptions).subscribe(configRes => {
      // console.log('configLast:', configRes);
      swal(
        'App Config has been created successfully',
        'Your configuration has been saved successfully.',
        'success'
      ).then(a => {
        this.clearForm()
        this.page = 1;
      });
      this.sendUpdate('no value', 'no value', 'Dashboard');
    });
    /*  this.http.post(base+'/api/app-configs', {
        'name': this.interface,
        'category': this.dashboard,
        'value': this.name,
        'subCategory': this.subCategory,
        'tags': null,
        'type': null,
        'numValue': null,
        'path': null,
        'icon': null,
        'organizationId': this.orgId
      }, this.httpOptions).subscribe(configRes => {
        // console.log('config:', configRes)
        this.router.navigate(['/config/list']);
      });
  */
  }

  cancel() {
    swal(
      'Cancelled',
      `Your App Configuration was not completed. `,
      'error'
    ).then(a => {
      this.clearForm();
      this.page = 1;
    });
  }

  sendUpdate(category, subcategory, screen) {

    let message = {
      topic: this.previewService.organizationId.getValue(),
      data: {
        silent: true,
        save: false,
        title: category,
        body: subcategory,
        screenType: screen,
        image: 'jpg',
        specialParameters: 'll',
        tags: 'Silent',
        orgId: this.previewService.organizationId.getValue(),
        name: screen
      }

    }
    // console.log('message:', message);
    this.http.post(proxyApi + '/contentUpdate', message, this.httpOptions).subscribe((response) => {
      // Response is a message ID string.
      // console.log('Successfully sent message:', response);

    }, (error) => {
      // console.log('Error sending message:', error);
    });
  }



  /* updateConfig() {
     this.http.put(base+'/api/app-configs', {
       'id': this.obj.id,
       'name': this.interface,
       'category': this.dashboard,
       'value': this.name,
       'subCategory': this.subCategory,
       'tags': null,
       'type': null,
       'numValue': null,
       'path': null,
       'icon': null,
       'configId': this.orgId,
       'description': this.obj.description,
       'displayOrder': this.obj.displayOrder
     }, this.httpOptions).subscribe(configRes => {
       // console.log('config:', configRes)
       this.router.navigate(['/config/list']);
     });
   }
   */

  validateContentForm1() {
    let isValid = true;
    let contentNameInput, contentTypeInput, contentDescriptionInput, categoryInput, subCategoryInput, imageInput, jobRoleInput;

    if (!this.appName || this.appName.trim() === '') {
      contentTypeInput = document.getElementById('name');
      contentTypeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      contentTypeInput = document.getElementById('name');
      contentTypeInput.classList.remove('error');
      isValid = true;
    }
    if (!this.appLogo || this.appLogo.trim() === '') {
      contentTypeInput = document.getElementById('icon');
      contentTypeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      contentTypeInput = document.getElementById('icon');
      contentTypeInput.classList.remove('error');
      isValid = true;
    }
    if (!this.appTheme || this.appTheme.trim() === '') {
      contentTypeInput = document.getElementById('appTheme');
      contentTypeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      contentTypeInput = document.getElementById('appTheme');
      contentTypeInput.classList.remove('error');
      isValid = true;
    }
    return isValid;
  }

  validateContentForm2() {
    let isValid = true;
    let contentNameInput, contentTypeInput, contentDescriptionInput, categoryInput, subCategoryInput, imageInput, jobRoleInput;
    return isValid;
  }

  validateContentForm3() {
    let isValid = true;
    let contentNameInput, contentTypeInput, contentDescriptionInput, categoryInput, subCategoryInput, imageInput, jobRoleInput;
    if (this.page === 3) {
      if (!this.tabs || this.tabs.trim() === '') {
        contentTypeInput = document.getElementById('tabs');
        contentTypeInput.classList.add('error');
        isValid = false;
        return isValid;
      } else {
        contentTypeInput = document.getElementById('tabs');
        contentTypeInput.classList.remove('error');
        isValid = true;
      }
    }

    if (!this.dashboardName || this.dashboardName.trim() === '') {
      contentTypeInput = document.getElementById('dashboardName');
      contentTypeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      contentTypeInput = document.getElementById('dashboardName');
      contentTypeInput.classList.remove('error');
      isValid = true;
    }

    if (!this.interface || this.interface.trim() === '') {
      contentTypeInput = document.getElementById('interface');
      contentTypeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      contentTypeInput = document.getElementById('interface');
      contentTypeInput.classList.remove('error');
      isValid = true;
    }

    if (!this.interface || this.interface.trim() === '') {
      contentTypeInput = document.getElementById('interface');
      contentTypeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      contentTypeInput = document.getElementById('interface');
      contentTypeInput.classList.remove('error');
      isValid = true;
    }

    if (!this.rule || this.rule.trim() === '') {
      contentTypeInput = document.getElementById('radioButtons');
      contentTypeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      contentTypeInput = document.getElementById('radioButtons');
      contentTypeInput.classList.remove('error');
      isValid = true;
    }

    if (this.rule === 'upload' && (!this.dashboardIcon || this.dashboardIcon.trim() === '')) {
      imageInput = document.getElementById('dashboardIcon');
      imageInput.classList.add('error');
      isValid = false;
      return isValid;
    } else if (this.rule === 'upload' && (this.dashboardIcon || this.dashboardIcon.trim() !== '')) {
      imageInput = document.getElementById('dashboardIcon');
      imageInput.classList.remove('error');
      isValid = true;
    }

    if (this.rule === 'select' && (!this.iconOption || this.iconOption.trim() === '')) {
      contentNameInput = document.getElementById('iconOption');
      contentNameInput.classList.add('error');
      isValid = false;
      return isValid;
    } else if (this.rule === 'select' && (this.iconOption || this.iconOption.trim() !== '')) {
      contentNameInput = document.getElementById('iconOption');
      contentNameInput.classList.remove('error');
      isValid = true;
    }

    if (!this.subtabs || this.subtabs.trim() === '') {
      contentTypeInput = document.getElementById('subtabs');
      contentTypeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      contentTypeInput = document.getElementById('subtabs');
      contentTypeInput.classList.remove('error');
      isValid = true;
    }

    for (let i = 0; i < this.subtabs; i++) {
      let element = <HTMLInputElement>document.getElementById('subCategory' + String(i));
      if (!element.value || element.value.trim() === '') {
        contentTypeInput = document.getElementById('subCategory' + i);
        contentTypeInput.classList.add('error');
        isValid = false;
        return isValid;
      } else {
        contentTypeInput = document.getElementById('subCategory' + i);
        contentTypeInput.classList.remove('error');
        isValid = true;
      }
    }

    return isValid;
  }

}
