import { JobFamilyListComponent } from './../jobFamily/jobFamily-list.component';
import { JobFamily } from './../sthaapak/model/jobFamily';
import { value } from './../shared/data/dropdowns';
import { Course } from './../sthaapak/model/course';
import { Component, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import { PreviewService } from '../shared/auth/preview.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { base, proxyApi } from '../shared/configs/util';
import { resolve } from 'q';
import * as AWS from '../shared/configs/aws.config';
import { CampaignGroupResourceService, DeviceResourceService, UserResourceService, IndustryResourceService } from '../sthaapak';

@Component({
    selector: 'app-dt-filter',
    templateUrl: './config-list.component.html',
    styleUrls: ['./config-list.component.scss']
})

export class ConfigListComponent implements OnInit {
    categoryName : string;
    selectedCourseId : string;
    selectedModuleId : string;
    learningTabs : string = "";
    industryTabs : string = "";
    industryTab : string;
    counter: number = 0;
    JobFamilyId: string;
    length: number;
    selectedLearningTab: string;
    newsId: string;
    newsFlag: boolean = false;
    microTab: string;
    rows = [];
    icon;
    obj;
    temp = [];
    temp2 = [];
    rows2 = [];
    temp3 = [];
    rows3 = [];
    temp4 = [];
    rows4 = [];
    temp5 = [];
    rows5 = [];
    temp6 = [];
    rows6 = [];
    temp7 = [];
    temp8 = [];
    rows8 = [];
    rows9 = [];
    temp9 = [];
    rows10 = [];
    temp10 = [];
    rows11 = [];
    temp11 = [];
    courses;
    news;
    battleCards;
    jobFamilies;
    bannerIcon;
    taskBotIcon;
    taskBotPdf;
    logoImage = '';
    videoUrl;
    sidelocation;
    groups;
    subcategoriesLength = 0;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.roleGuardService.token
        })
    };

    // Table Column Titles
    columns10 = [
        { prop: 'name' },
        { prop: 'value' }
    ]

    columns = [
        { prop: 'name' },
        { prop: 'description' },
        { prop: 'category' },
        { prop: 'value' },
        { prop: 'subCategory' }



    ];

    columns11 = [
        {prop: 'name'},
        {prop: 'icon'},
        {prop: 'value'},
        {prop: 'description'}
    ]
    theme = [];
    columns2 = [
        { prop: 'name' }
    ]

    columns3 = [
        { prop: 'description' },
        { prop: 'path' }
    ]

    column4 = [
        { prop: 'value' },
        { prop: 'name' }
    ]
    @ViewChild(DatatableComponent) table: DatatableComponent;

    ngOnInit() {
        this.logoImage = '';
        this.getConfigs();
        this.getGroups();
    }

    getGroups() {
        this.http.get(base + '/api/campaign-groups/filterby/' + this.previewService.organizationId.getValue(), this.httpOptions).subscribe(cgroup => {
            let groups = <any>cgroup;
            this.groups = {};
            groups.forEach(element => {
                this.groups[element.id] = element.name;
            });
        })
    }

    getConfigs() {
        this.rows = [];

        this.temp = [];
        this.temp2 = [];
        this.rows2 = [];
        this.temp3 = [];
        this.rows3 = [];
        this.temp4 = [];
        this.rows4 = [];
        this.temp7 = [];
        this.rows8 = [];
        this.temp8 = [];
        this.rows9 = [];
        this.temp9 = [];
        this.rows10 = [];
        this.temp10 = [];
        this.rows11 = [];
        this.temp11 = [];

        this.http.get(base + '/api/app-configs/org/' + this.previewService.organizationId.getValue(), this.httpOptions).subscribe(res => {
            let data = <any>res;
            // console.log('data:', data);
            let configs = [];
            let appname = [];
            let theme = [];
            let about = [];
            let logo = [];
            let profile = [];
            let sideconfigs = [];
            let emailscreenshot = [];
            let emailsop = [];
            let banner = [];
            let taskbot = [];
            data.forEach(element => {
                // console.log(JSON.stringify(element))
                if (element.category === 'DASHBOARD') {
                    configs.push(element);
                }
                if(element.category === "TaskBot"){
                    taskbot.push(element);
                }
                if (element.category === 'APPNAME') {
                    appname.push(element);
                }
                if (element.name === 'About Us') {
                    about.push(element);
                }
                if (element.name === 'dashboardBackground' || element.category === 'dashboardBackground' || element.name === 'microLearningBackground' || element.category === 'microLearningBackground') {
                    if (element.name === 'dashboardBackground' || element.category === 'dashboardBackground') {
                        element.name = 'App Theme';
                    }
                    if (element.name === 'microLearningBackground' || element.category === 'microLearningBackground') {
                        element.name = 'Micro-Learning Theme';
                    }
                    switch (element.value) {
                        case '#4caf50':
                            element.value = 'Green';
                            break;
                        case '#e0eff9':
                            element.value = 'Sky Blue';
                            break;
                        case '#039be5':
                            element.value = 'Dark Blue';
                            break;
                        case '#6b70b8':
                            element.value = 'Violet';
                            break;
                        case '#336699':
                            element.value = 'Blue';
                            break;

                    }
                    theme.push(element);
                }
                if (element.category === 'splashBackground' || element.category === 'APPNAME' || element.category === 'splashLogoBackground') {
                    element.type = element.name;
                    logo.push(element);
                    
                }
                if (element.category === 'PROFILE') {
                    profile.push(element);
                }
                if (element.category === 'SIDEMENU') {
                    sideconfigs.push(element);
                }
                if (element.category === 'EmailScreenshot') {
                    emailscreenshot.push(element);
                }
                if (element.category === 'EmailSOP') {
                    emailsop.push(element);
                }

                if (element.category === 'Banner') {
                    banner.push(element);
                }

            });
            this.temp = [...configs];
            this.rows = configs;
            // console.log(configs);
            this.temp2 = [...appname];
            this.rows2 = appname;

            this.temp3 = [...about];
            this.rows3 = about;

            this.temp4 = [...theme];
            this.rows4 = theme;

            this.temp5 = [...logo];
            this.rows5 = logo;

            this.temp6 = [...profile];
            this.rows6 = profile;

            this.temp7 = [...sideconfigs];

            this.temp8 = [...emailscreenshot];
            this.rows8 = emailscreenshot;

            this.temp9 = [...emailsop];
            this.rows9 = emailsop;


            this.temp10 = [...banner];
            this.rows10 = banner;

            this.temp11 = [...taskbot];
            this.rows11 = taskbot;


        });
    }
    constructor(private http: HttpClient, public deviceService: DeviceResourceService, private roleGuardService: RoleGuardService, private previewService: PreviewService,
        private router: Router) {
        if (!localStorage.getItem('token')) {
            this.router.navigateByUrl('/login');
        }
    }



    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.value.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    deleteConfig(row) {
        // console.log('row:', row);
        swal({
            title: 'Are you sure you want to delete this?',
            text: "You won't be able to revert this! Deleting this app-config will make all the content in this Dashboard Invisible.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.http.delete(base + '/api/app-configs/' + row.id, this.httpOptions).subscribe(response => {
                    // console.log(response)
                    this.sendUpdate(row.value, row.path, 'DASHBOARD');
                    this.getConfigs();
                    this.temp7.forEach(e => {
                        if ((e.value === row.value) && (e.category === 'SIDEMENU') && (e.organizationId === this.previewService.organizationId.getValue())) {
                            this.http.delete(base + '/api/app-configs/' + e.id, this.httpOptions).subscribe(res => {
                                // console.log(res)
                                this.sendUpdate(e.value, e.path, 'DASHBOARD');
                                this.getConfigs();
                            });
                        }
                    })

                });

            }
            else {
                swal(
                    'Cancelled',
                    'Your configuration item was not deleted.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your configuration item was not deleted.',
                    'error'
                )
            }
        })


    }


    createDashboardItem() {
        swal({
            title: 'Are you sure you want to create a new Dashboard Item?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {

                swal.mixin({
                    input: 'text',
                    confirmButtonText: 'Next &rarr;',
                    showCancelButton: true,
                    progressSteps: ['1', '2', '3', '4', '5', '6']
                }).queue([
                    {
                        title: 'Enter Name For Your Dashboard Item',
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                if (value && value.trim() !== '') {
                                    resolve();
                                }
                                resolve('Please input a non-empty value.')

                            })
                        }
                    },
                    {
                        title: 'Select Type of Dashboard Item',
                        input: 'select',
                        inputOptions: {
                            'Blogs': 'Blogs',
                            'MicroLearning': 'MicroLearning',
                            'Chat': 'Chat',
                            'Feedback': 'SOP',
                            'Industry': 'Info Cards',
                            'Operations': 'CheckList',
                            'Category': 'Category',
                            'WebView': 'WebView'
                        },
                        inputPlaceholder: 'Select Type of Dashboard Item',
                        showCancelButton: true,
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                this.subcategoriesLength = Number(value);
                                resolve()
                            })
                        }
                    },
                    {
                        title: 'Select Number Of Sub Categories',
                        input: 'select',
                        inputOptions: {
                            '2': '2',
                            '3': '3',
                            '4': '4',
                            '5': '5',
                            '6': '6',
                            '7': '7',
                            '8': '8',
                            '9': '9',
                            '10': '10',
                            '11': '11',
                            '12': '12',
                            '13': '13',
                            '14': '14',
                            '15': '15',
                            '16': '16',
                            '17': '17',
                            '18': '18',
                            '19': '19',
                            '20': '20'
                        },
                        inputPlaceholder: 'Select Number of Sub Categories',
                        showCancelButton: true,
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                if (value) {
                                    this.subcategoriesLength = Number(value);
                                    resolve()
                                }
                                else {
                                    resolve('Select Number of Sub Categories');
                                }
                            })
                        }
                    },
                    {
                        title: 'Enter Names for Sub Categories(comma separated)',
                        inputPlaceholder: 'Enter Names for Sub Categories',
                        showCancelButton: true,
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                // console.log('value:', value)
                                let subs = value.split(',');
                                // console.log('sub', this.subcategoriesLength);
                                if ((subs.length === this.subcategoriesLength)) {
                                    for (let i = 0; i < subs.length; i++) {
                                        if (subs[i].length < 1 || subs[i].length > 30) {
                                            resolve(`Each Sub Category should have at least one character. Sub Category name can't be blank.`);
                                        }
                                    }
                                    resolve()
                                } else {
                                    resolve('You need to Enter ' + this.subcategoriesLength + ' Sub Categories.')
                                }
                            })
                        }
                    },
                    {
                        title: 'Select Dashboard Icon image',
                        input: 'file',
                        inputAttributes: {
                            'accept': 'image/*',
                            'aria-label': 'Upload image for Dashboard Icon'
                        },

                        preConfirm: (value) => {
                            if (value) {
                                this.uploadFile(value).then(location => this.icon = location);
                            }



                        }
                    },
                    {
                        title: 'Select Sidemenu Icon image',
                        input: 'file',
                        inputAttributes: {
                            'accept': 'image/*',
                            'aria-label': 'Upload image for Sidemenu Icon'
                        },
                        preConfirm: (value) => {
                            if (value) {
                                this.uploadFile(value).then(location => this.sidelocation = location);
                            }
                        }
                    }
                ]).then((result) => {

                    // console.log('result1:', result);
                    if (!result.dismiss) {
                        let obj = {
                            "name": "",
                            "description": "",
                            "category": "DASHBOARD",
                            "value": "",
                            "subCategory": "",
                            "tags": null,
                            "type": null,
                            "numValue": null,
                            "path": 'true',
                            "icon": "",
                            "displayOrder": 10,
                            "organizationId": this.previewService.organizationId.getValue(),
                            "extraData": [],
                            "multiLinguals": []
                        };






                        swal({
                            title: 'New Dashboard Item:',
                            html:
                                'Your answers: <pre><code>' +
                                'Dashboard Item Name :' + result.value[0] + '</br>' +
                                'Dashboard Item Type :' + result.value[1] + '</br>' +
                                'Sub Categories are :' + result.value[3] + '</br>' +
                                'Dashboard Icon:' + this.icon +
                                'Sidemenu Icon:' + this.sidelocation +
                                '</code></pre>',
                            confirmButtonText: 'Done',
                            showCancelButton: true,
                        }).then(oa => {
                            // console.log('result2:', oa);

                            if (!oa.dismiss) {
                                obj.name = result.value[1];
                                obj.value = result.value[0];
                                obj.subCategory = result.value[3];
                                obj.description = this.icon || obj.description;
                                this.http.post(base + '/api/app-configs', obj, this.httpOptions).subscribe(responseput => {
                                    // console.log('put', responseput);
                                    this.sendUpdate(obj.value, obj.path, 'DASHBOARD');
                                    this.getConfigs();
                                })
                                obj.category = 'SIDEMENU';
                                obj.description = this.sidelocation;
                                this.http.post(base + '/api/app-configs', obj, this.httpOptions).subscribe(responseput => {
                                    // console.log('put', responseput);
                                    this.sendUpdate(obj.value, obj.path, 'DASHBOARD');
                                    this.getConfigs();
                                })
                            }
                        })



                    }



                })
            }
            else {
                swal(
                    'Cancelled',
                    'Your dashboard item was not created.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your dashboard item was not created.',
                    'error'
                )
            }
        })


    }

    editConfig(obj) {
        let oldName = obj.value;
        oldName = oldName.split(' ').join('%20');
        swal({
            title: 'Are you sure you want to edit this?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {

                swal.mixin({
                    input: 'text',
                    confirmButtonText: 'Next &rarr;',
                    showCancelButton: true,
                    progressSteps: ['1', '2', '3', '4']
                }).queue([
                    {
                        title: 'Enter Name For Your Dashboard Item',
                        inputValue: obj.value,
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                if (value && value.trim() !== '') {
                                    resolve();
                                }
                                resolve('Please input a non-empty value.')

                            })
                        }
                    },
                    {
                        title: 'Select Number Of Sub Categories',
                        input: 'select',
                        inputOptions: {
                            '2': '2',
                            '3': '3',
                            '4': '4',
                            '5': '5',
                            '6': '6',
                            '7': '7',
                            '8': '8',
                            '9': '9',
                            '10': '10',
                            '11': '11',
                            '12': '12',
                            '13': '13',
                            '14': '14',
                            '15': '15',
                            '16': '16',
                            '17': '17',
                            '18': '18',
                            '19': '19',
                            '20': '20'
                        },
                        inputPlaceholder: 'Select Number of Sub Categories',
                        showCancelButton: true,
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                if (value) {
                                    this.subcategoriesLength = Number(value);
                                    resolve()
                                }
                                else {
                                    resolve('Select Number of Sub Categories');
                                }

                            })
                        }
                    },
                    {
                        title: 'Enter Name for Sub Categories(comma separated)',
                        inputPlaceholder: 'Enter Name for Sub Categories',
                        showCancelButton: true,
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                // console.log('value:', value)
                                let subs = value.split(',');
                                // console.log('sub', this.subcategoriesLength);
                                if ((subs.length === this.subcategoriesLength)) {
                                    for (let i = 0; i < subs.length; i++) {
                                        if (subs[i].length < 1 || subs[i].length > 30) {
                                            resolve(`Each Sub Category should have at least one character. Sub Category name can't be blank.`);
                                        }
                                    }
                                    resolve()
                                } else {
                                    resolve('You need to Enter' + this.subcategoriesLength + 'Sub Categories.')
                                }
                            })
                        }
                    },
                    {
                        title: 'Select Dashboard Icon image',
                        input: 'file',
                        inputAttributes: {
                            'accept': 'image/*',
                            'aria-label': 'Upload image for Dashboard Icon'
                        },

                        preConfirm: (value) => {
                            if (value) {
                                this.uploadFile(value).then(location => this.icon = location);
                            }
                            else {
                                this.icon = obj.description
                            }



                        }
                    },
                    {
                        title: 'Select Sidemenu Icon image',
                        input: 'file',
                        inputAttributes: {
                            'accept': 'image/*',
                            'aria-label': 'Upload image for Sidemenu Icon'
                        },
                        preConfirm: (value) => {
                            if (value) {
                                this.uploadFile(value).then(location => this.sidelocation = location);
                            }

                        }
                    }
                ]).then((result) => {


                    swal({
                        title: 'New Dashboard Item:',
                        html:
                            'Your answers: <pre><code>' +
                            'Dashboard Item Name :' + result.value[0] + '</br>' +
                            'Sub Categories are :' + result.value[2] + '</br>' +
                            'Dashboard Icon:' + this.icon +
                            'Sidemenu Icon:' + this.sidelocation +
                            '</code></pre>',
                        confirmButtonText: 'Done',
                        showCancelButton: true,
                    }).then(oa => {
                        // console.log('OBJ::', obj);
                        let prevValue = obj.value;
                        obj.value = result.value[0];
                        obj.subCategory = result.value[2];
                        obj.description = this.icon || obj.description;
                        this.http.put(base + '/api/app-configs/', obj, this.httpOptions).subscribe(responseput => {
                            // console.log('put', responseput);
                            
                        });
                        obj.category = 'SIDEMENU';
                        this.temp7.forEach(e => {
                            if ((e.value === prevValue) && (e.category === 'SIDEMENU') && (e.organizationId === this.previewService.organizationId.getValue())) {
                                e.value = result.value[0];
                                e.subCategory = result.value[2];
                                e.description = this.sidelocation || e.description;
                                
                                        this.http.put(base + '/api/app-configs', e, this.httpOptions).subscribe(res => {
                                            // console.log(res)
                                            switch(obj.name){
                                                case "Industry":
                                                    this.http.get(base + '/api/_search/job-families?query=tags%3A%22'+oldName+'%22', this.httpOptions).subscribe(res => {
                                                        let data:any;
                                                        data = res;
                                                        for(let e of data){
                                                            e.tags = obj.value;
                                                            this.http.put(base + '/api/job-families',e,this.httpOptions).subscribe(response => {
                                                                
                                                            });
                                                        }
                
                                                    })
                                                    this.http.get(base + '/api/_search/industries?query=tags%3A%22'+oldName+'%22', this.httpOptions).subscribe(res => {
                                                        let data:any;
                                                        data = res;
                                                        for(let e of data){
                                                            e.tags = obj.value;
                                                            this.http.put(base + '/api/industries',e,this.httpOptions).subscribe(response => {
                                                                
                                                            });
                                                        }
                                                        
                                                    })
                                                break;
                                                case "MicroLearning":
                                                this.http.get(base + '/api/_search/micro-contents?query=category%3A%22'+oldName+'%22', this.httpOptions).subscribe(res => {
                                                    let data:any;
                                                    data = res;
                                                    for(let e of data){
                                                        e.category = obj.value;
                                                        this.http.put(base + '/api/micro-contents',e,this.httpOptions).subscribe(response => {
                                                            
                                                        });
                                                    }
                                                    // this.sendUpdate(obj.value, obj.path, 'DASHBOARD');
                                                })
                                                break;
                                                case "Blogs":
                                                this.http.get(base + '/api/_search/published-contents?query=category%3A%22'+oldName+'%22', this.httpOptions).subscribe(res => {
                                                    let data:any;
                                                    data = res;
                                                    for(let e of data){
                                                        e.category = obj.value;
                                                        this.http.put(base + '/api/published-contents',e,this.httpOptions).subscribe(response => {
                                                            
                                                        });
                                                    }
                                                    // this.sendUpdate(obj.value, obj.path, 'DASHBOARD');
                                                })
                                                break; 
                                            }
                                            this.sendUpdate(obj.value, obj.path, 'DASHBOARD');
                                            this.getConfigs();
                                });
                            }
                        })


                    })



                })
            }
            else {
                swal(
                    'Cancelled',
                    'Your configuration item was not edited.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your configuration item was not edited.',
                    'error'
                )
            }
        })

    }
    uploadFile(file) {
        let upload = AWS.s3.upload({ Key: this.previewService.organizationId.getValue() + '/config/images/' + new Date() + file.name, Bucket: AWS.bucketName, Body: file, ACL: 'public-read' });
        let promise = upload.promise();
        return promise.then((data) => {
            // console.log(data);
            return data.Location;
        }, (err) => {
            // console.log(err);
        });

    }
    editAppName(row) {
        swal({
            title: 'Are you sure you want to edit your App Name?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm, row)
            if (confirm.value === true) {
                swal({
                    title: 'Edit New App Name',
                    input: 'text',
                    inputPlaceholder: 'New App Name',
                    inputAutoTrim: true,
                    focusConfirm: false,
                    showCancelButton: true,
                    showConfirmButton: true,
                    confirmButtonColor: '#0CC27E',
                    cancelButtonColor: '#FF586B',
                    confirmButtonText: 'Yes, edit it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success btn-raised mr-5',
                    cancelButtonClass: 'btn btn-danger btn-raised',
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            value = value.trim();
                            if (value.length) {
                                resolve()
                            }
                            else
                                resolve('You need to give a name to your App.')

                        })
                    }
                }).then(formValues => {
                    // console.log('form', formValues);
                    if (formValues.value) {
                        let conf = row;
                        conf.value = formValues.value;
                        conf.type = 'SETTING_STR'
                        // console.log(conf);
                        this.http.put(base + '/api/app-configs', conf, this.httpOptions).subscribe(ans => {
                            if (ans) {
                                swal('Successfully Changed App Name', '', 'success');
                                this.sendUpdate(row.value, row.path, 'DASHBOARD');
                                this.getConfigs();

                            }
                            else {
                                swal('App Name was not Changed', '', 'error');
                            }
                        })

                    }
                    else {
                        swal('App Name was not Changed', '', 'error');
                    }

                });

            }
        });
    }

    editAppAbout(obj) {
        let timerInterval1;
        swal({
            title: 'Are you sure you want to edit About Us Section?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {

                swal.mixin({
                    input: 'text',
                    confirmButtonText: 'Next &rarr;',
                    showCancelButton: true,
                    progressSteps: ['1', '2', '3']
                }).queue([
                    {
                        title: 'Enter Description for Your Organization',
                    },
                    {
                        title: 'Select Video',
                        input: 'file',
                        inputAttributes: {
                            'accept': 'video/*',
                            'aria-label': 'Upload your video'
                        }
                    }
                ]).then((result) => {
                    // console.log('result:', result);
                    if (result.value && result.value[1]) {
                        swal({
                            title: 'Uploading!',
                            onOpen: () => {
                                swal.showLoading();
                                this.uploadFile(result.value[1]).then(location => {
                                    this.videoUrl = location;
                                    this.icon = location;
                                    swal({
                                        title: 'About US after Editing',
                                        html:
                                            'Your answers: <pre><code>' +
                                            'Description :' + result.value[0] + '</br>' +
                                            'Video File:' + this.icon +
                                            '</code></pre>',
                                        confirmButtonText: 'Done',
                                        showCancelButton: true,
                                    }).then(oa => {
                                        obj.path = this.icon || obj.path;
                                        obj.description = result.value[0];
                                        this.http.put(base + '/api/app-configs/', obj, this.httpOptions).subscribe(a => {
                                            // console.log(a);
                                        })
                                    })
                                })
                                timerInterval1 = setInterval(() => {
                                    if (this.videoUrl) {
                                        swal.clickConfirm();
                                        clearInterval(timerInterval1);
                                        // console.log('time1')
                                    }
                                }, 100)
                            },
                            onClose: () => {
                                clearInterval(timerInterval1)
                            }
                        }).then(ans => {
                            clearInterval(timerInterval1);
                            setInterval(function () {
                                swal({
                                    title: 'About Us after Editing',
                                    html:
                                        'Your answers: <pre><code>' +
                                        'Description :' + result.value[0] + '</br>' +
                                        'Video File:' + this.icon +
                                        '</code></pre>',
                                    confirmButtonText: 'Done',
                                    showCancelButton: true,
                                }).then(oa => {
                                    obj.path = this.icon || obj.path;
                                    obj.description = result.value[0];
                                    this.http.put(base + '/api/app-configs/', obj, this.httpOptions).subscribe(a => {
                                        // console.log(a);
                                    })
                                })
                            }, 1000);
                        });


                    }
                    else if (result.value) {
                        this.icon = obj.path;
                        swal({
                            title: 'About Us after Editing',
                            html:
                                'Your answers: <pre><code>' +
                                'Description :' + result.value[0] + '</br>' +
                                'Video File:' + this.icon +
                                '</code></pre>',
                            confirmButtonText: 'Done',
                            showCancelButton: true,
                        }).then(oa => {
                            obj.path = this.icon || obj.path;
                            obj.description = result.value[0];
                            this.http.put(base + '/api/app-configs/', obj, this.httpOptions).subscribe(a => {
                                // console.log(a);
                                this.sendUpdate(obj.value, obj.path, 'DASHBOARD');
                            })
                        })


                    }
                    else {
                        swal(
                            'Cancelled',
                            'Your configuration item was not edited.',
                            'error'
                        )
                    }
                })
            }
            else {
                swal(
                    'Cancelled',
                    'Your configuration item was not edited.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your configuration item was not edited.',
                    'error'
                )
            }
        })
    }


    editAppTheme(row) {
        // console.log(row);
        if (row.name === 'microLearningBackground' || row.category === 'microLearningBackground') {
            swal({
                title: 'Are you sure you want to edit your Micro-Learning Theme?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0CC27E',
                cancelButtonColor: '#FF586B',
                confirmButtonText: 'Yes, edit it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success btn-raised mr-5',
                cancelButtonClass: 'btn btn-danger btn-raised',
                buttonsStyling: false
            }).then((confirm) => {
                // console.log('con:', confirm)
                if (confirm.value === true) {
                    swal({
                        title: 'Select Theme',
                        input: 'select',
                        inputOptions: {

                            '#6b70b8': 'Violet',
                            '#336699': 'Blue'
                        },
                        inputPlaceholder: 'Select a Theme',
                        showCancelButton: true,
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                if (value) {
                                    resolve()
                                } else {
                                    resolve('You need to select one Theme')
                                }
                            })
                        }
                    }).then(formValues => {
                        // console.log('form', formValues);
                        if (formValues.value) {
                            let conf = row;
                            conf.value = formValues.value;
                            // console.log(conf.value);
                            this.http.put(base + '/api/app-configs', conf, this.httpOptions).subscribe(ans => {
                                if (ans) {
                                    swal('Successfully Changed Micro-Learning Theme', '', 'success');
                                    this.sendUpdate(row.value, row.path, 'DASHBOARD');
                                    this.getConfigs();

                                }
                                else {
                                    swal('Micro-Learning Theme was not Changed', '', 'error');
                                }
                            })

                        }
                        else {
                            swal('Micro-Learning Theme was not Changed', '', 'error');
                        }

                    });

                }
            });
        }
        if (row.name === 'dashboardBackground' || row.category === 'dashboardBackground') {
            swal({
                title: 'Are you sure you want to edit your Micro-Learning Theme?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0CC27E',
                cancelButtonColor: '#FF586B',
                confirmButtonText: 'Yes, edit it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success btn-raised mr-5',
                cancelButtonClass: 'btn btn-danger btn-raised',
                buttonsStyling: false
            }).then((confirm) => {
                // console.log('con:', confirm)
                if (confirm.value === true) {
                    swal({
                        title: 'Select Theme',
                        input: 'select',
                        inputOptions: {
                            '#4caf50': 'Green',
                            '#039be5': 'Dark Blue',
                            '#e0eff9': 'Sky Blue'

                        },
                        inputPlaceholder: 'Select a Theme',
                        showCancelButton: true,
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                if (value) {
                                    resolve()
                                } else {
                                    resolve('You need to select one Theme')
                                }
                            })
                        }
                    }).then(formValues => {
                        // console.log('form', formValues);
                        if (formValues.value) {
                            let conf = row;
                            conf.value = formValues.value;
                            // console.log(conf.value);
                            this.http.put(base + '/api/app-configs', conf, this.httpOptions).subscribe(ans => {
                                // console.log(ans);
                                this.http.get(base + '/api/app-configs/org/' + this.previewService.organizationId.getValue(), this.httpOptions).subscribe(ans => {
                                    let ans2 = <any>ans;
                                    ans2.forEach(element => {
                                        if (element.name === 'toolbarBackground' || element.category === 'toolbarBackground') {
                                            element.value = formValues.value;
                                            this.http.put(base + '/api/app-configs', element, this.httpOptions).subscribe(ans3 => {

                                            })
                                            this.sendUpdate(row.value, row.path, 'DASHBOARD');
                                        }
                                        if (element.name === 'statusBarBackground' || element.category === 'statusBarBackground') {
                                            element.value = formValues.value;
                                            this.http.put(base + '/api/app-configs', element, this.httpOptions).subscribe(ans3 => {
                                                if (ans3) {
                                                    swal('Successfully changed App Theme', '', 'success').then(changed =>
                                                        this.getConfigs());
                                                    this.sendUpdate(row.value, row.path, 'DASHBOARD');
                                                }

                                                else {
                                                    swal('App Theme was not Changed', '', 'error');
                                                }
                                            })
                                        }
                                    });

                                })
                            })


                        }
                        else {
                            swal('App Theme was not Changed', '', 'error');
                        }

                    });

                }
            });
        }
    }


    editAppLogo(row) {
        this.logoImage = '';
        // console.log(row);
        if (row.name === 'APPNAME' || row.category === 'APPNAME') {
            swal({
                title: 'Are you sure you want to change your APP LOGO?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0CC27E',
                cancelButtonColor: '#FF586B',
                confirmButtonText: 'Yes, edit it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success btn-raised mr-5',
                cancelButtonClass: 'btn btn-danger btn-raised',
                buttonsStyling: false
            }).then((confirm) => {
                // console.log('con:', confirm)
                if (confirm.value === true) {
                    swal({
                        title: 'Select image',
                        input: 'file',
                        inputAttributes: {
                            'accept': 'image/*',
                            'aria-label': 'Upload your profile picture'
                        },
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                if (value) {
                                    this.uploadFile(value).then(location => {
                                        this.logoImage = <any>location;
                                        if (this.logoImage) {
                                            resolve()
                                        }
                                        else {
                                            resolve('Image could not be uploaded. Try Again.')
                                        }
                                    });

                                } else {
                                    resolve('You need to upload one Background Image')
                                }
                            })
                        }
                    }).then(formValues => {
                        // console.log('form', formValues);
                        if (formValues.value) {
                            let conf = row;
                            conf.type = null;
                            conf.description = this.logoImage;
                            // console.log(conf.description);
                            this.http.put(base + '/api/app-configs', conf, this.httpOptions).subscribe(ans => {
                                if (ans) {
                                    swal('Successfully Changed App Logo', '', 'success');
                                    this.sendUpdate(row.value, row.path, 'DASHBOARD');
                                    this.getConfigs();

                                }
                                else {
                                    swal('App Logo was not Changed', '', 'error');
                                }
                            })

                        }
                        else {
                            swal('App Logo was not Changed', '', 'error');
                        }

                    });

                }
            });
        }
        if (row.name === 'splashBackground') {
            swal({
                title: 'Are you sure you want to edit your splash Background?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0CC27E',
                cancelButtonColor: '#FF586B',
                confirmButtonText: 'Yes, edit it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success btn-raised mr-5',
                cancelButtonClass: 'btn btn-danger btn-raised',
                buttonsStyling: false
            }).then((confirm) => {
                // console.log('con:', confirm)
                if (confirm.value === true) {
                    swal({
                        title: 'Select image',
                        input: 'file',
                        inputAttributes: {
                            'accept': 'image/*',
                            'aria-label': 'Upload your profile picture'
                        },
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                if (value) {
                                    this.uploadFile(value).then(location => {
                                        this.logoImage = <any>location;
                                        if (this.logoImage) {
                                            resolve()
                                        }
                                        else {
                                            resolve('Image could not be uploaded. Try Again.')
                                        }
                                    });

                                } else {
                                    resolve('You need to upload one Background Image')
                                }
                            })
                        }
                    }).then(formValues => {
                        // console.log('form', formValues);
                        if (formValues.value) {
                            let conf = row;
                            conf.type = null;

                            conf.description = this.logoImage;
                            // console.log(conf.description);
                            this.http.put(base + '/api/app-configs', conf, this.httpOptions).subscribe(ans => {
                                // console.log(ans);
                                swal('Successfully Changed Splash Background', '', 'success');
                                this.sendUpdate(row.value, row.path, 'DASHBOARD');
                            })
                        }
                        else {
                            swal('Splash Background was not Changed', '', 'error');
                        }

                    });

                }
            });
        }

        if (row.name === 'splashLogoBackground') {
            swal({
                title: 'Are you sure you want to edit your splash Background Logo?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0CC27E',
                cancelButtonColor: '#FF586B',
                confirmButtonText: 'Yes, edit it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success btn-raised mr-5',
                cancelButtonClass: 'btn btn-danger btn-raised',
                buttonsStyling: false
            }).then((confirm) => {
                // console.log('con:', confirm)
                if (confirm.value === true) {
                    swal({
                        title: 'Select image',
                        input: 'file',
                        inputAttributes: {
                            'accept': 'image/*',
                            'aria-label': 'Upload your profile picture'
                        },
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                if (value) {
                                    this.uploadFile(value).then(location => {
                                        this.logoImage = <any>location;
                                        if (this.logoImage) {
                                            resolve()
                                        }
                                        else {
                                            resolve('Image could not be uploaded. Try Again.')
                                        }
                                    });

                                } else {
                                    resolve('You need to upload one Background Image')
                                }
                            })
                        }
                    }).then(formValues => {
                        // console.log('form', formValues);
                        if (formValues.value) {
                            let conf = row;
                            conf.type = null;

                            conf.description = this.logoImage;
                            // console.log(conf.description);
                            this.http.put(base + '/api/app-configs', conf, this.httpOptions).subscribe(ans => {
                                // console.log(ans);
                                swal('Successfully Changed Splash Logo Background', '', 'success');
                                this.sendUpdate(row.value, row.path, 'DASHBOARD');
                            })
                        }
                        else {
                            swal('Splash Logo Background was not Changed', '', 'error');
                        }

                    });

                }
            });
        }
    }
    createUserDefinedFields() {
        swal({
            title: 'Are you sure you want to create a user defined field?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, Proceed!',
            cancelButtonText: 'No, Cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                swal.mixin({
                    input: 'text',
                    confirmButtonText: 'Next &rarr;',
                    showCancelButton: true,
                    progressSteps: ['1', '2', '3']
                }).queue([
                    {
                        title: 'Enter Name For Your Dashboard Item',
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                if (value && value.trim() !== '') {
                                    resolve();
                                }
                                resolve('Please input a non-empty value.')

                            })
                        }
                    },
                    {
                        title: 'Select Type Of Field',
                        input: 'select',
                        inputOptions: {
                            'TEXT': 'TEXT',
                            'DATE': 'DATE',
                            'DROPDOWN': 'DROPDOWN',

                        },
                        inputPlaceholder: 'Select Type of Field',
                        showCancelButton: true,
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                if (!value) {
                                    resolve('Select one Field Type');
                                }
                                else resolve();
                            })
                        },
                        preConfirm: (value) => {
                            if (value === 'DROPDOWN') {
                                swal.insertQueueStep({
                                    title: 'How many Options can this field have?',
                                    input: 'number',
                                    inputAttributes: { 'min': '0', 'max': '20' },
                                    inputPlaceholder: 'Number of Options',
                                    showCancelButton: true,
                                    inputValidator: (value) => {
                                        return new Promise((resolve) => {
                                            if (Number(value) < 1 || Number(value) > 20) {
                                                resolve('Please enter a number between 1 and 20');
                                            }
                                            else resolve();
                                        })
                                    }
                                })
                            }
                        }

                    },
                ]).then((result) => {
                    // console.log('Result:', result)
                    if (result.value[1]) {
                        if (result.value[2]) {
                            let steps = new Array(Number(result.value[2]));
                            let queueSteps = new Array(Number(result.value[2]));

                            steps = steps.map((e, i) => {
                                queueSteps[i] = {
                                    title: 'Enter Option ' + Number(i + 1),
                                    inputValidator: (value) => {
                                        return new Promise((resolve) => {
                                            if (!value || value.trim() === '') {
                                                resolve("Option can't be empty");
                                            }
                                            else resolve();
                                        })
                                    },
                                }
                                return i + 1
                            });
                            // console.log('steps:', steps);
                            // console.log('questeps:', queueSteps);

                            swal.mixin({
                                input: 'text',
                                confirmButtonText: 'Next &rarr;',
                                showCancelButton: true,
                                progressSteps: steps
                            }).queue(queueSteps).then(res => {
                                if (res.value) {
                                    // console.log('values:', res);
                                    this.obj = {
                                        "name": result.value[0],
                                        "description": null,
                                        "category": "PROFILE",
                                        "value": res.value.join(','),
                                        "subCategory": result.value[1],
                                        "tags": null,
                                        "type": null,
                                        "numValue": null,
                                        "path": 'true',
                                        "icon": null,
                                        "displayOrder": this.rows6.length + 1,
                                        "organizationId": this.previewService.organizationId.getValue(),
                                        "extraData": [],
                                        "multiLinguals": []
                                    }
                                    // console.log('obj:', this.obj);
                                    swal({
                                        title: 'Are you sure you want to create this user defined field?',
                                        type: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#0CC27E',
                                        cancelButtonColor: '#FF586B',
                                        confirmButtonText: 'Yes, edit it!',
                                        cancelButtonText: 'No, cancel!',
                                        confirmButtonClass: 'btn btn-success btn-raised mr-5',
                                        cancelButtonClass: 'btn btn-danger btn-raised',
                                        buttonsStyling: false
                                    }).then((confirm) => {
                                        if (confirm.value) {
                                            this.http.post(base + '/api/app-configs', this.obj, this.httpOptions).subscribe(success => {
                                                let successResp = <any>success;
                                                if (successResp.id) {
                                                    swal('Your Field was created Successfully', '', 'success');
                                                    this.getConfigs();
                                                }
                                            }, err => {
                                                swal('Sorry, Your Field could not be created', '', 'error')
                                            });
                                        }
                                        else swal(
                                            'Cancelled',
                                            'Your Field was not created.',
                                            'error'
                                        )

                                    });
                                }
                                else {
                                    swal(
                                        'Cancelled',
                                        'Your Field was not created.',
                                        'error'
                                    )
                                }
                            })
                        }
                        else {
                            this.obj = {
                                "name": result.value[0],
                                "description": null,
                                "category": "PROFILE",
                                "value": null,
                                "subCategory": result.value[1],
                                "tags": null,
                                "type": null,
                                "numValue": null,
                                "path": 'true',
                                "icon": null,
                                "displayOrder": this.rows6.length + 1,
                                "organizationId": this.previewService.organizationId.getValue(),
                                "extraData": [],
                                "multiLinguals": []
                            }
                            // console.log('obj:', this.obj);
                            swal({
                                title: 'Are you sure you want to create this user defined field?',
                                text: "You won't be able to revert this!",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#0CC27E',
                                cancelButtonColor: '#FF586B',
                                confirmButtonText: 'Yes, edit it!',
                                cancelButtonText: 'No, cancel!',
                                confirmButtonClass: 'btn btn-success btn-raised mr-5',
                                cancelButtonClass: 'btn btn-danger btn-raised',
                                buttonsStyling: false
                            }).then((confirm) => {
                                if (confirm.value) {
                                    this.http.post(base + '/api/app-configs', this.obj, this.httpOptions).subscribe(success => {
                                        let successResp = <any>success;
                                        if (successResp.id) {
                                            swal('Your Field was created Successfully', '', 'success');
                                            this.sendUpdate(successResp.id, successResp.value, 'DASHBOARD');
                                            this.getConfigs();
                                        }
                                    }, err => {
                                        swal('Sorry, Your Field could not be created', '', 'error')
                                    });
                                }
                                else swal(
                                    'Cancelled',
                                    'Your Field was not created.',
                                    'error'
                                )

                            });
                        }


                    } else {
                        swal(
                            'Cancelled',
                            'Your Field was not created.',
                            'error'
                        )
                    }

                });
            }
            else {
                swal(
                    'Cancelled',
                    'Your Field was not created.',
                    'error'
                )
            }
        });
    }
    editUserDefinedFields(row) {
        swal({
            title: 'Are you sure you want to edit this user defined field?',
            text: "This will affect the existing users! Proceed with Caution.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                swal.mixin({
                    input: 'text',
                    confirmButtonText: 'Next &rarr;',
                    showCancelButton: true,
                    progressSteps: ['1', '2', '3']
                }).queue([
                    {
                        title: 'Edit Your Dashboard Item Name',
                        inputValue: row.name
                    },
                    {
                        title: 'Select Type Of Field',
                        input: 'select',
                        inputValue: row.category,
                        inputOptions: {
                            'TEXT': 'TEXT',
                            'DATE': 'DATE',
                            'DROPDOWN': 'DROPDOWN',

                        },

                        inputPlaceholder: 'Select Type of Field',
                        showCancelButton: true,
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                if (!value) {
                                    resolve('Select one Field Type');
                                }
                                else resolve();
                            })
                        },
                        preConfirm: (value) => {
                            if (value === 'DROPDOWN') {
                                swal.insertQueueStep({
                                    title: 'How many Options can this field have?',
                                    input: 'number',
                                    inputValue: row.value ? row.value.length : null,
                                    inputPlaceholder: 'Number of Options',
                                    inputAttributes: { 'min': '0', 'max': '20' },
                                    showCancelButton: true,
                                    inputValidator: (value) => {
                                        return new Promise((resolve) => {
                                            if (Number(value) < 1 || Number(value) > 20) {
                                                resolve('Please enter a number between 1 and 20');
                                            }
                                            else resolve();
                                        })
                                    }
                                })
                            }
                        }

                    },
                ]).then((result) => {
                    // console.log('Result:', result)
                    if (result.value[1]) {
                        if (result.value[2]) {
                            let steps = new Array(Number(result.value[2]));
                            let queueSteps = new Array(Number(result.value[2]));

                            steps = steps.map((e, i) => {
                                queueSteps[i] = {
                                    title: 'Enter Option ' + Number(i + 1),
                                    inputValue: row.value ? row.value.split(',')[i] : null,
                                    inputValidator: (value) => {
                                        return new Promise((resolve) => {
                                            if (!value || value.trim() === '') {
                                                resolve(`Option can't be empty.`);
                                            }
                                            else resolve();
                                        })
                                    },
                                }
                                return i + 1
                            });
                            // console.log('steps:', steps);
                            // console.log('questeps:', queueSteps);

                            swal.mixin({
                                titleText: 'Enter Option Value',
                                input: 'text',
                                confirmButtonText: 'Next &rarr;',
                                showCancelButton: true,
                                progressSteps: steps
                            }).queue(queueSteps).then(res => {
                                if (res.value) {
                                    // console.log('values:', res);
                                    this.obj = {
                                        "id": row.id,
                                        "name": result.value[0],
                                        "description": null,
                                        "category": "PROFILE",
                                        "value": res.value.join(','),
                                        "subCategory": result.value[1],
                                        "tags": null,
                                        "type": null,
                                        "numValue": null,
                                        "path": 'true',
                                        "icon": null,
                                        "displayOrder": row.displayOrder,
                                        "organizationId": this.previewService.organizationId.getValue(),
                                        "extraData": [],
                                        "multiLinguals": []
                                    }
                                    // console.log('obj:', this.obj);
                                    swal({
                                        title: 'Are you sure you want to edit this user defined field?',
                                        text: "You won't be able to revert this!",
                                        type: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#0CC27E',
                                        cancelButtonColor: '#FF586B',
                                        confirmButtonText: 'Yes, edit it!',
                                        cancelButtonText: 'No, cancel!',
                                        confirmButtonClass: 'btn btn-success btn-raised mr-5',
                                        cancelButtonClass: 'btn btn-danger btn-raised',
                                        buttonsStyling: false
                                    }).then((confirm) => {
                                        if (confirm.value) {
                                            this.http.put(base + '/api/app-configs', this.obj, this.httpOptions).subscribe(success => {
                                                let successResp = <any>success;
                                                if (successResp.id) {
                                                    swal('Your Field was edited Successfully', '', 'success');
                                                    this.sendUpdate(row.value, row.path, 'DASHBOARD');
                                                    this.getConfigs();
                                                }
                                            }, err => {
                                                swal('Sorry, Your Field could not be edited', '', 'error')
                                            });
                                        }
                                        else swal(
                                            'Cancelled',
                                            'Your Field was not edited.',
                                            'error'
                                        )

                                    });
                                }
                                else {
                                    swal(
                                        'Cancelled',
                                        'Your Field was not edited.',
                                        'error'
                                    )
                                }
                            })
                        }
                        else {
                            this.obj = {
                                "id": row.id,
                                "name": result.value[0],
                                "description": null,
                                "category": "PROFILE",
                                "value": null,
                                "subCategory": result.value[1],
                                "tags": null,
                                "type": null,
                                "numValue": null,
                                "path": 'true',
                                "icon": null,
                                "displayOrder": row.displayOrder,
                                "organizationId": this.previewService.organizationId.getValue(),
                                "extraData": [],
                                "multiLinguals": []
                            }
                            // console.log('obj:', this.obj);
                            swal({
                                title: 'Are you sure you want to create this user defined field?',
                                text: "You won't be able to revert this!",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#0CC27E',
                                cancelButtonColor: '#FF586B',
                                confirmButtonText: 'Yes, edit it!',
                                cancelButtonText: 'No, cancel!',
                                confirmButtonClass: 'btn btn-success btn-raised mr-5',
                                cancelButtonClass: 'btn btn-danger btn-raised',
                                buttonsStyling: false
                            }).then((confirm) => {
                                if (confirm.value) {
                                    this.http.put(base + '/api/app-configs', this.obj, this.httpOptions).subscribe(success => {
                                        let successResp = <any>success;
                                        if (successResp.id) {
                                            swal('Your Field was created Successfully', '', 'success');
                                            this.sendUpdate(row.value, row.path, 'DASHBOARD');
                                            this.getConfigs();
                                        }
                                    }, err => {
                                        swal('Sorry, Your Field could not be created', '', 'error')
                                    });
                                }
                                else swal(
                                    'Cancelled',
                                    'Your Field was not edited.',
                                    'error'
                                )

                            });
                        }


                    } else {
                        swal(
                            'Cancelled',
                            'Your Field was not edited.',
                            'error'
                        )
                    }

                });
            }
            else {
                swal(
                    'Cancelled',
                    'Your Field was not edited.',
                    'error'
                )
            }
        });

    }

    customizeConfig(row) {
        swal({
            title: 'Are you sure you want to make this visible to a particular group only?',
            text: "This will affect the existing users! Proceed with Caution.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                swal({
                    title: 'Select the Group',
                    input: 'select',
                    inputValue: row.icon,
                    inputOptions: this.groups,

                    inputPlaceholder: 'Select a Group',
                    showCancelButton: true,
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if (!value) {
                                resolve('Select one Group');
                            }
                            else resolve();
                        })
                    }
                }).then((result) => {
                    // console.log('resultGroup:', result)
                    row.icon = result.value;
                    this.http.put(base + '/api/app-configs', row, this.httpOptions).subscribe(app => {
                        // console.log('APP:', app);
                        let conf = <any>app;
                        swal('Your Dashboard Item is now visible to members of ' + this.groups[conf.value] + ' only.', '', 'success');
                        this.sendUpdate(row.value, row.path, 'DASHBOARD');
                    }, err => {
                        swal('Error', 'Your Dashboard Item could not be configured', 'error');

                    })
                });
            }

            else {
                swal(
                    'Cancelled',
                    'Your Dashboard Item has not been configured',
                    'error'
                )
            }
        })
    }
    disableContent(row) {
        row.path = 'false';
        this.http.put(base + '/api/app-configs', row, this.httpOptions).toPromise().then(res => {
            // console.log(res);
            this.temp7.forEach(element => {
                if (element.value === row.value) {
                    element.path = 'false';
                    this.http.put(base + '/api/app-configs', element, this.httpOptions).toPromise().then(resp => {
                        // console.log(resp);
                    });
                }
            });
            this.getConfigs();
            this.sendUpdate(row.value, row.path, 'DASHBOARD');
        })
    }
    enableContent(row) {
        row.path = 'true';
        this.http.put(base + '/api/app-configs', row, this.httpOptions).toPromise().then(res => {
            this.temp7.forEach(element => {
                if (element.value === row.value) {
                    element.path = 'true';
                    this.http.put(base + '/api/app-configs', element, this.httpOptions).toPromise().then(resp => {
                        // console.log(resp);
                    });
                }
            });
            this.getConfigs();
            this.sendUpdate(row.value, row.path, 'DASHBOARD');
        })
    }

    sendUpdate(category, subcategory, screen) {

        let message = {
            topic: this.previewService.organizationId.getValue(),
            data: {
                silent: true,
                save: false,
                title: category,
                body: subcategory,
                screenType: screen,
                image: 'jpg',
                specialParameters: 'll',
                tags: 'Silent',
                orgId: this.previewService.organizationId.getValue(),
                name: screen
            }
        }
        // console.log('message:', message);
        this.http.post(proxyApi + '/contentUpdate', message, this.httpOptions).subscribe((response) => {
            // Response is a message ID string.
            // console.log('Successfully sent message:', response);

        }, (error) => {
            // console.log('Error sending message:', error);
        });
    }

    editEmailScreenshot(row) {


        swal({
            title: 'Are you sure you want to edit this field?',
            text: "This will change the recipients of the screenshot events.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                let emailAddresses;
                swal({
                    title: 'Enter the list of email addresses.(comma separated)',
                    input: 'email',
                    confirmButtonText: 'Save and Exit!',
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if (value && value.trim() !== '') {
                                emailAddresses = value.split(',');
                                // console.log('em:', emailAddresses)
                                for (let index = 0; index < emailAddresses.length; index++) {
                                    if ((/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/).test(emailAddresses[index])) {
                                        continue;
                                    }
                                    else {
                                        resolve('Please input atleast one valid email address.');
                                        break;
                                    }
                                }
                                resolve();
                            }

                            resolve('Please input atleast one email address.')

                        })
                    }
                }).then((confirmEmails) => {
                    // console.log('confEma', confirmEmails);
                    if (confirmEmails.value) {
                        // console.log(confirmEmails)
                        row.value = confirmEmails.value;
                        this.http.put(base + '/api/app-configs', row, this.httpOptions).toPromise().then(putres => {
                            // console.log('putres:', putres);
                            swal('You have successfully changed the email addresses.', '', 'success').then(a => { });
                            this.getConfigs();

                        })
                    }
                })
            }

        });
    }


    createEmailScreenshotAddressField() {

        swal({
            title: 'Are you sure you want to record Screenshots on App?',
            text: "This will create the recipients of the screenshot events.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, create it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                let emailAddresses;
                swal({
                    title: 'Enter the list of email addresses.(comma separated)',
                    input: 'email',
                    confirmButtonText: 'Save and Exit!',
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if (value && value.trim() !== '') {
                                emailAddresses = value.split(',');
                                // console.log('em:', emailAddresses)
                                for (let index = 0; index < emailAddresses.length; index++) {
                                    if ((/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/).test(emailAddresses[index])) {
                                        continue;
                                    }
                                    else {
                                        resolve('Please input atleast one valid email address.');
                                        break;
                                    }
                                }
                                resolve();
                            }

                            resolve('Please input atleast one email address.')

                        })
                    }
                }).then((confirmEmails) => {
                    let row = {

                        "name": "EmailScreenshot",
                        "description": "string",
                        "category": "EmailScreenshot",
                        "value": confirmEmails.value,
                        "subCategory": "string",
                        "tags": "string",
                        "type": "SETTING_STR",
                        "numValue": 0,
                        "path": "true",
                        "icon": "string",
                        "displayOrder": 0,
                        "organizationId": this.previewService.organizationId.getValue(),
                        "extraData": [],
                        "multiLinguals": []
                    };
                    // console.log('confEma', confirmEmails);
                    if (confirmEmails.value) {
                        // console.log(confirmEmails)
                        row.value = confirmEmails.value;
                        this.http.post(base + '/api/app-configs', row, this.httpOptions).toPromise().then(putres => {
                            // console.log('putres:', putres);
                            swal('You have successfully changed the email addresses.', '', 'success').then(a => { });
                            this.getConfigs();
                        })
                    }
                })
            }

        });
    }


    editEmailSOP(row) {


        swal({
            title: 'Are you sure you want to edit this field?',
            text: "This will change the recipients of the SOP submit events.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                let emailAddresses;
                swal({
                    title: 'Enter the list of email addresses.(comma separated)',
                    input: 'email',
                    confirmButtonText: 'Save and Exit!',
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if (value && value.trim() !== '') {
                                emailAddresses = value.split(',');
                                // console.log('em:', emailAddresses)
                                for (let index = 0; index < emailAddresses.length; index++) {
                                    if ((/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/).test(emailAddresses[index])) {
                                        continue;
                                    }
                                    else {
                                        resolve('Please input atleast one valid email address.');
                                        break;
                                    }
                                }
                                resolve();
                            }

                            resolve('Please input atleast one email address.')

                        })
                    }
                }).then((confirmEmails) => {
                    // console.log('confEma', confirmEmails);
                    if (confirmEmails.value) {
                        // console.log(confirmEmails)
                        row.value = confirmEmails.value;
                        this.http.put(base + '/api/app-configs', row, this.httpOptions).toPromise().then(putres => {
                            // console.log('putres:', putres);
                            swal('You have successfully changed the email addresses.', '', 'success').then(a => { });
                            this.getConfigs();

                        })
                    }
                })
            }

        });
    }


    createEmailSOP() {

        swal({
            title: 'Are you sure you want to link an email address with SOP submit events?',
            text: "This will create the recipients of the SOP submit events.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, create it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                let emailAddresses;
                swal({
                    title: 'Enter the list of email addresses.(comma separated)',
                    input: 'email',
                    confirmButtonText: 'Save and Exit!',
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if (value && value.trim() !== '') {
                                emailAddresses = value.split(',');
                                // console.log('em:', emailAddresses)
                                for (let index = 0; index < emailAddresses.length; index++) {
                                    if ((/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/).test(emailAddresses[index])) {
                                        continue;
                                    }
                                    else {
                                        resolve('Please input atleast one valid email address.');
                                        break;
                                    }
                                }
                                resolve();
                            }

                            resolve('Please input atleast one email address.')

                        })
                    }
                }).then((confirmEmails) => {
                    let row = {

                        "name": "EmailSOP",
                        "description": "string",
                        "category": "EmailSOP",
                        "value": confirmEmails.value,
                        "subCategory": "string",
                        "tags": "string",
                        "type": "SETTING_STR",
                        "numValue": 0,
                        "path": "true",
                        "icon": "string",
                        "displayOrder": 0,
                        "organizationId": this.previewService.organizationId.getValue(),
                        "extraData": [],
                        "multiLinguals": []
                    };
                    // console.log('confEma', confirmEmails);
                    if (confirmEmails.value) {
                        // console.log(confirmEmails)
                        row.value = confirmEmails.value;
                        this.http.post(base + '/api/app-configs', row, this.httpOptions).toPromise().then(putres => {
                            // console.log('putres:', putres);
                            swal('You have successfully changed the email addresses.', '', 'success').then(a => { });
                            this.getConfigs();
                        })
                    }
                })
            }

        });
    }


    createBanner() {
        this.http.get(base + '/api/micro-contents/org/' + this.previewService.organizationId.getValue(), this.httpOptions).toPromise().then(res => {
            this.courses = <any>res;
        });
        this.http.get(base + '/api/published-contents/org/' + this.previewService.organizationId.getValue(), this.httpOptions).toPromise().then(res => {
            this.news = <any>res;
        })
        this.http.get(base + '/api/industries', this.httpOptions).toPromise().then(res => {
            this.battleCards = <any>res;
        })
      
        
        swal({
            title: 'Are you sure you want to add a banner to the Dashboard?',
            text: "This will add a banner in the dashboard.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, create it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            this.counter = 0;
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.counter++;
                let emailAddresses;
                swal.mixin({
                    input: 'text',
                    confirmButtonText: 'Next &rarr;',
                    showCancelButton: true,
                    progressSteps: ['1', '2', '3', '4', '5', '6']
                }).queue([{
                    title: 'Select Dashboard Banner image',
                    input: 'file',
                    inputAttributes: {
                        'accept': 'image/*, video/*',
                        'aria-label': 'Upload image/video for Dashboard Banner'
                    },
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if (!value) {
                                resolve('Please Select a file.')
                            }
                            else {
                                swal.showLoading();
                                this.uploadFile(value).then(bannerlocation => { this.bannerIcon = bannerlocation; resolve(); });
                            }
                        })
                    }
                },
                {
                    title: 'Select Deeplink Category',
                    input: 'select',
                    inputValue: 'None',
                    inputOptions: this.rows.reduce((inputs = { None: 'None' }, e) => {
                        inputs[e.value] = e.value;
                        inputs['None'] = 'None';
                        return inputs;
                    }, {}),

                    inputPlaceholder: 'Select Dashboard Item',
                    showCancelButton: true,
                    inputValidator: (value) => {
                        
                        return new Promise((resolve) => {
                            if (!value) {
                                resolve('Select one Field Type');
                            }
                            else {
                                this.categoryName = value;
                                this.counter++;
                                resolve();
                            }
                        })
                    },
                    preConfirm: (value) => {
                        // console.log('value:', value)
                       
                        if (value === 'None') {

                        }
                        else {
                            let selectedItem = this.rows.reduce((item = {}, e) => {
                                // console.log('e::', e)
                                if (e.value === value) {
                                    item = e;
                                }
                                return item;
                            }, {});
                            // console.log('selectedItem:', selectedItem)
                        
                            
                            switch (selectedItem.name) {
                                case 'MicroLearning':
                                    this.learningTabs = "";
                                    // console.log("tabs : " + this.learningTabs)
                                    swal.insertQueueStep({
                                        title: 'Select the Tab',
                                        input: 'select',
                                        inputValue: 'None',
                                        inputOptions: selectedItem.subCategory.split(',').reduce((inputs = {}, e) => {
                                            inputs[e] = e;
                                            this.learningTabs += e+",";
                                            return inputs;
                                        }, {}),

                                        inputPlaceholder: 'Select Tab Item',
                                        showCancelButton: true,
                                        inputValidator: (value) => {
                                            this.microTab = value;
                                            return new Promise((resolve) => {
                                                if (!value) {
                                                    resolve('Select one Field Type');
                                                }
                                                else {
                                                    this.counter++;
                                                    resolve();
                                                }
                                            })
                                        },
                                        preConfirm: (valuel) => {
                                            let selectedCourse;
                                            // console.log('learning tabs: '+ this.learningTabs);
                                            
                                            let result = this.courses;
                                            this.selectedLearningTab = valuel;
                                            // console.log("learning tab : " + valuel);
                                            result = result.reduce((courses = {}, el) => {
                                                if (el.subcategory === valuel) {
                                                    courses[el.id] = el.name;
                                                    selectedCourse = el;
                                                    
                                                }
                                                return courses;
                                            }, {})

                                            swal.insertQueueStep({
                                                title: 'Select the Course',
                                                input: 'select',
                                                inputOptions: result,

                                                inputPlaceholder: 'Select the Course',
                                                showCancelButton: true,
                                                inputValidator: (valueCourse) => {
                                                    this.selectedCourseId = valueCourse;
                                                    return new Promise((resolve) => {
                                                        if (!valueCourse) {
                                                            resolve('Select one Field Type');
                                                        }
                                                        else {
                                                            this.counter++;
                                                            resolve();
                                                        }
                                                    })
                                                },
                                                preConfirm: (valueCourse) => {

                                                    let result;
                                                    result = selectedCourse.cards.reduce((modules = {}, el) => {
                                                        modules[el.id] = el.name;
                                                        return modules;
                                                    }, {})

                                                    swal.insertQueueStep({
                                                        title: 'Select the Module',
                                                        input: 'select',
                                                        inputOptions: result,

                                                        inputPlaceholder: 'Select the Module',
                                                        showCancelButton: true,
                                                        inputValidator: (valueModule) => {
                                                            this.selectedModuleId = valueModule;
                                                            return new Promise((resolve) => {
                                                                if (!valueModule) {
                                                                    resolve('Select one Field Type');
                                                                }
                                                                else {
                                                                    this.counter++;
                                                                    this.length = this.counter;
                                                                    this.counter = 0;
                                                                    resolve();
                                                                }
                                                            })
                                                        }

                                                    });

                                                }
                                            });

                                        }


                                    });

                                    break;
                                case 'Blogs':
                                    this.newsFlag = true;
                                    swal.insertQueueStep({
                                        title: 'Select the Tab',
                                        input: 'select',
                                        inputValue: 'None',
                                        inputOptions: selectedItem.subCategory.split(',').reduce((inputs = {}, e) => {
                                            inputs[e] = e;
                                            return inputs;
                                        }, {}),

                                        inputPlaceholder: 'Select Tab Item',
                                        showCancelButton: true,
                                        inputValidator: (valueNewsTab) => {
                                            return new Promise((resolve) => {
                                                if (!valueNewsTab) {
                                                    resolve('Select one Field Type');
                                                }
                                                else {
                                                    this.counter++;
                                                    resolve();
                                                }
                                            })
                                        },
                                        preConfirm: (valuel) => {

                                            
                                            let result = this.news.reduce((news = {}, el) => {
                                                if (el.subCategory === valuel && el.category === selectedItem.value) {
                                                    news[el.id] = el.name;

                                                }
                                                return news;
                                            }, {})



                                            swal.insertQueueStep({
                                                title: 'Select the News',
                                                input: 'select',
                                                inputValue: 'None',
                                                inputOptions: result,

                                                inputPlaceholder: 'Select News',
                                                showCancelButton: true,
                                                inputValidator: (value) => {
                                                    this.newsId = value;
                                                    return new Promise((resolve) => {
                                                        if (!value) {
                                                            resolve('Select one Field Type');
                                                        }
                                                        else {
                                                            this.counter++;
                                                            this.length = this.counter;
                                                            this.counter = 0;
                                                            resolve();
                                                        }
                                                    })
                                                }
                                            });




                                        }
                                    });
                                    break;
                                case 'News':
                                    this.newsFlag = true;
                                    swal.insertQueueStep({
                                        title: 'Select the Tab',
                                        input: 'select',
                                        inputValue: 'None',
                                        inputOptions: selectedItem.subCategory.split(',').reduce((inputs = {}, e) => {
                                            inputs[e] = e;
                                            return inputs;
                                        }, {}),

                                        inputPlaceholder: 'Select Tab Item',
                                        showCancelButton: true,
                                        inputValidator: (valueNewsTab) => {
                                            // this.categoryName = valueNewsTab;
                                            return new Promise((resolve) => {
                                                if (!valueNewsTab) {
                                                    resolve('Select one Field Type');
                                                }
                                                else {
                                                    this.counter++;
                                                    resolve();
                                                }
                                            })
                                        },
                                        preConfirm: (valuel) => {

                                            // console.log("value : " + valuel);
                                            let result = this.news.reduce((news = {}, el) => {
                                                // console.log("el : " + el.subCategory + " "+ el.category);
                                                if (el.subCategory === valuel && el.category === selectedItem.value) {
                                                    news[el.id] = el.name;
                                                    // console.log("inside news");
                                                }
                                                return news;
                                            }, {})



                                            swal.insertQueueStep({
                                                title: 'Select the News',
                                                input: 'select',
                                                inputValue: 'None',
                                                inputOptions: result,

                                                inputPlaceholder: 'Select News',
                                                showCancelButton: true,
                                                inputValidator: (value) => {
                                                    this.newsId = value;
                                                    return new Promise((resolve) => {
                                                        if (!value) {
                                                            resolve('Select one Field Type');
                                                        }
                                                        else {
                                                            this.counter++;
                                                            this.length = this.counter;
                                                            this.counter = 0;
                                                            resolve();
                                                        }
                                                    })
                                                }
                                        
                                            });




                                        }
                                    });
                                    break;
                                case 'Industry':
                                this.industryTabs = "";
                                let map = new Map();
                                    swal.insertQueueStep({
                                        title: 'Select the Tab',
                                        input: 'select',
                                        inputValue: 'None',
                                        inputOptions: this.battleCards.reduce((inputs = {}, e) => {
                                            if (e.tags === selectedItem.value) {
                                                inputs[e.id] = e.name;
                                                map.set(e.id,e);
                                                this.JobFamilyId = e.id;
                                                this.industryTabs += e.name + ",";
                                                
                                            }
                                            // console.log("industry : " + e.name);
                                        
                                            return inputs;
                                        }, {}),

                                        inputPlaceholder: 'Select Tab Item',
                                        showCancelButton: true,
                                        inputValidator: (valueNewsTab) => {
                                            // console.log("news tab : " + valueNewsTab)
                                            // this.JobFamilyId = valueNewsTab;
                                            
                                            this.JobFamilyId = valueNewsTab;
                                            this.industryTab = map.get(+valueNewsTab).name;
                                            // console.log("industry : "+ this.industryTab);
                                            return new Promise((resolve) => {
                                                if (!valueNewsTab) {
                                                    resolve('Select one Field Type');
                                                }
                                                else {
                                                    this.counter++;
                                                    resolve();
                                                }
                                            })
                                        },
                                        preConfirm: (valuel) => {
                                            let result = map.get(+this.JobFamilyId).jobFamilies;
                                            result = result.reduce((inputs = {}, el) => {
                                                if (el.tags === selectedItem.value) {
                                                    inputs[el.id] = el.name;
                                                    
                                                }
                                                return inputs;
                                            }, {})

                                            swal.insertQueueStep({
                                                title: 'Select the card',
                                                input: 'select',
                                                inputOptions: result,

                                                inputPlaceholder: 'Select the card',
                                                showCancelButton: true,
                                                inputValidator: (valueJobFamily) => {
                                                    this.JobFamilyId = valueJobFamily;
                                                    // console.log("job family id : " + this.JobFamilyId);
                                                    return new Promise((resolve) => {
                                                        if (!valueJobFamily) {
                                                            resolve('Select one Field Type');
                                                        }
                                                        else {
                                                            this.counter++;
                                                            this.length = this.counter;
                                                            this.counter = 0;
                                                            resolve();
                                                        }
                                                    })
                                                },

                                    });
                                }
                            });
                                    break;
                                default:
                                    // console.log('SWITCH', selectedItem);
                                    break;
                            }
                        }

                    }

                }
                ]).then((confirmEmails) => {
                    // console.log("confirm emails length: ", this.length);
                    let deeplink = "";
                    
                    if (this.length == 4 && !this.newsFlag) {
                        deeplink = `productiviseapp://productivise/industry?category=${this.categoryName}&tabs=${this.industryTabs.substring(0,this.industryTabs.length-1)}&tabName=${this.industryTab}&jobFamilyId=${this.JobFamilyId}`;
                    }
                    if (this.length == 5) {
                        // console.log("inside deeplink")
                        deeplink = `productiviseapp://productivise/learning?category=${this.categoryName}&tabs=${this.learningTabs.substring(0,this.learningTabs.length-1)}&tabName=${this.selectedLearningTab}&courseId=${this.selectedCourseId}&moduleId=${this.selectedModuleId}`;
                    }
                    if (this.length == 4 && this.newsFlag) {
                        deeplink = `productiviseapp://productivise/published?category=${this.categoryName}&newsId=${this.newsId}`;
                    }
                    this.length = 0;
                    deeplink = deeplink.split(' ').join('%20');
                    // console.log("deeplink : " + deeplink);
                    let row = {

                        "name": "Banner",
                        "description": "string",
                        "category": "Banner",
                        "value": this.bannerIcon,
                        "subCategory": "string",
                        "tags": "string",
                        "type": "SETTING_STR",
                        "numValue": 0,
                        "path": "true",
                        "icon": deeplink,
                        "displayOrder": this.temp10.length + 1,
                        "organizationId": this.previewService.organizationId.getValue(),
                        "extraData": [],
                        "multiLinguals": []
                    };
                    // console.log('confEma', confirmEmails);
                    if (confirmEmails.value) {
                        // console.log(confirmEmails)
                        row.value = this.bannerIcon;
                        this.sendUpdate(row.value, row.path, 'DASHBOARD');
                            this.http.post(proxyApi + '/contentUpdate', {
                                'topic': this.previewService.organizationId.getValue(),
                        
                                'data': {
                                  'silent': false,
                                  'save': true,
                                  'body': 'Please open the app to check it out.',
                                  'campaignId': 82551,
                                  'ctaURL': 'string',
                                  'description':'Info',
                                  'image': null,
                                  'lifeCycleId': 257501,
                                  'microContentId': 1253,
                                  'name': 'New banner added!',
                                  'scheduled': null,
                                  'screenType': null,
                                  'sequence': null,
                                  'sound': new Date().getTime(),
                                  'specialParameters': null,
                                  'tags': 'Notify',
                                  'template': 'string',
                                  'title': 'New banner added',
                                  'orgId': this.previewService.organizationId.getValue(),
                                  'groupId': null,
                                  'userId': null
                                }
                        
                              }, this.httpOptions).subscribe(res => {
                                //   console.log("push notification has been sent");
                            });
                        this.http.post(base + '/api/app-configs', row, this.httpOptions).toPromise().then(putres => {
                            // console.log('putres:', putres);
                            swal('You have successfully added a banner image.', '', 'success').then(a => { });
                            this.getConfigs();
                        })
                    }
                })
            }

        });
    }

    getInputOptions() {
        let obj = {};
        this.temp10.forEach((e, i) => {
            Object.defineProperty(obj, i + 1, i + 1);
        })
        return obj;
    }

    deleteBanner(row) {
        // console.log('row:', row);
        swal({
            title: 'Are you sure you want to delete this banner?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.http.delete(base + '/api/app-configs/' + row.id, this.httpOptions).subscribe(response => {
                    // console.log(response)
                    // console.log("rows : " + row.value + " " + row.path);
                    this.sendUpdate(row.value, row.path, 'DASHBOARD');
                    swal('Success', 'One banner has been deleted.', 'success')
                    this.getConfigs();
                });

            }
            else {
                swal(
                    'Cancelled',
                    'Your banner was not deleted.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your banner was not deleted.',
                    'error'
                )
            }
        })


    }


    editBanner(row) {
        
        swal({
            title: 'Are you sure you want to edit this banner?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            this.bannerIcon = row.value;
            // console.log('con:', confirm)
            if (confirm.value === true) {
                let emailAddresses;
                swal({
                    title: 'Select Dashboard Banner image',
                    input: 'file',
                    inputAttributes: {
                        'accept': 'image/*',
                        'aria-label': 'Upload image for Dashboard Banner'
                    },
                    inputValidator: (value) => {
                        return new Promise((resolve) => {


                            if (!value) {
                                resolve('Please Select a file.')
                            }
                            else {
                                swal.showLoading();
                                this.uploadFile(value).then(bannerlocation => { this.bannerIcon = bannerlocation; resolve(); });
                            }
                        })
                    }
                }
                ).then((confirmEmails) => {
                    // console.log('confEma', confirmEmails);
                    if (confirmEmails.value) {
                        // console.log(confirmEmails)
                        row.value = this.bannerIcon;
                        this.http.put(base + '/api/app-configs', row, this.httpOptions).toPromise().then(putres => {
                            // console.log('putres:', putres);
                            this.sendUpdate(row.value, row.path, 'DASHBOARD');
                            
                            this.http.post(proxyApi + '/contentUpdate', {
                                'topic': this.previewService.organizationId.getValue(),
                        
                                'data': {
                                'silent': false,
                                'save': true,
                                'body': 'Banner has been updated',
                                'campaignId': 82551,
                                'ctaURL': 'string',
                                'description':'Info',
                                'image': null,
                                'lifeCycleId': 257501,
                                'microContentId': 1253,
                                'name': 'A banner updated!',
                                'scheduled': null,
                                'screenType': 'DASHBOARD',
                                'sequence': null,
                                'sound': new Date().getTime(),
                                'specialParameters': '',
                                'tags': 'Notify',
                                'template': '',
                                'title': '',
                                'orgId': this.previewService.organizationId.getValue(),
                                'groupId': null,
                                'userId': null
                                }
                        
                            }, this.httpOptions).subscribe(res => {
                                // console.log("push notification has been sent");
                            });
                            swal('You have successfully changed the banner image.', '', 'success').then(a => { });
                            this.getConfigs();
                        })
                    }
                })
            }

        });
    }

    deleteTaskbot(row){
        swal({
            title: 'Are you sure you want to delete this Taskbot?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.http.delete(base + '/api/app-configs/' + row.id, this.httpOptions).subscribe(response => {
                    // console.log(response)
                    // console.log("rows : " + row.value + " " + row.path);
                    this.sendUpdate(row.value, row.path, 'DASHBOARD');
                    swal('Success', 'One taskbot has been deleted.', 'success')
                    this.getConfigs();
                });

            }
            else {
                swal(
                    'Cancelled',
                    'Your taskbot was not deleted.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your taskbot was not deleted.',
                    'error'
                )
            }
        })
    }

    convertExcelToJson(key){
        
        this.http.get(proxyApi + '/exceltojson/'+key, this.httpOptions).subscribe((response) => {

        }, (error) => {

        });
    }

    createTaskbot(){
        let taskbotName;
        swal({
            title: 'Are you sure you want to add a Taskbot to the Dashboard?',
            text: "This will add a Taskbot in the dashboard.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, create it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            this.counter = 0;
            console.log('con:', confirm)
            if (confirm.value === true) {
                this.counter++;
                let emailAddresses;
                swal.mixin({
                    input: 'text',
                    confirmButtonText: 'Next &rarr;',
                    showCancelButton: true,
                    progressSteps: ['1', '2', '3']
                }).queue([{
                    title: 'Type name for Taskbot',
                    input: 'text',
                    inputAttributes: {
                        'maxlength': '32'
                    },
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if (!value) {
                                resolve('Please type name for Taskbot.')
                            }
                            else {
                                taskbotName = value;
                                console.log(taskbotName);
                                resolve();
                            }
                        })
                    }
                },
                    
                {
                    title: 'Select Taskbot icon',
                    input: 'file',
                    inputAttributes: {
                        'accept': 'image/*',
                        'aria-label': 'Upload icon for Taskbot'
                    },
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if (!value) {
                                resolve('Please Select a file.')
                            }
                            else {
                                swal.showLoading();
                                this.uploadFile(value).then(taskBotlocation => { this.taskBotIcon = taskBotlocation; console.log(this.taskBotIcon);resolve(); });
                            }
                        })
                    }
                },
                {
                    title: 'Select file',
                    input: 'file',
                    inputAttributes: {
                        'accept': 'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'aria-label': 'Upload file for Taskbot'
                    },
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if (!value) {
                                resolve('Please Select a file.')
                            }
                            else {
                                swal.showLoading();
                                this.uploadFile(value).then(taskBotPdflocation => { this.taskBotPdf = taskBotPdflocation; console.log(this.taskBotPdf); resolve(); });
                            }
                        })
                    }
                }
            ]).then((confirmEmails) => {
                let row = {

                    "name": taskbotName,
                    "description": this.taskBotPdf,
                    "category": "TaskBot",
                    "value": "string",
                    "subCategory": "string",
                    "tags": "string",
                    "type": "SETTING_STR",
                    "numValue": 0,
                    "path": "true",
                    "icon": this.taskBotIcon,
                    "displayOrder": this.temp11.length + 1,
                    "organizationId": this.previewService.organizationId.getValue(),
                    "extraData": [],
                    "multiLinguals": []
                };
                row.value = this.taskBotIcon;
                console.log('row', JSON.stringify(row));
                
                    this.http.post(base + '/api/app-configs', row, this.httpOptions).toPromise().then(putres => {
                        // console.log('putres:', putres);
                        swal('You have successfully added a taskbot.', '', 'success').then(a => { });
                        let key:string;
                        key = this.taskBotPdf.substring(this.taskBotPdf.lastIndexOf("/")+1);
                        this.convertExcelToJson(key);
                        this.getConfigs();
                    })
                
            })
            }
        });
    
    }



}


