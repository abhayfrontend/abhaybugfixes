const exec = require('child_process').exec;
const express= require('express');
const app= express();
const path = require('path');
const bodyParser = require('body-parser');
const request = require('request');
const AWS = require('aws-sdk');
const cors = require('cors');
const multer = require('multer');
const axios = require('axios');
const URL = 'https://devapi.productivise.io';
const elasticsearch = require('elasticsearch');
const nodemailer = require('nodemailer');
const client = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace'
});
// set the directory for the uploads to the uploaded to
const download = ( url, image_path ) => axios( { 'url' : url, 'responseType' : 'stream' } ).then( response =>
{
    response.data.pipe( fs.createWriteStream( image_path ) );

    return { 'status' : true, 'error' : '' };

}).catch( error => ( { 'status' : false, 'error' : 'Error: ' + error.message } ) );
var DIR = './uploads/';
//define the type of upload multer would be doing and pass in its destination, in our case, its a single file with the name photo
var storage = multer.diskStorage({ //multers disk storage settings
        destination: function (req, file, cb) {
            cb(null, './uploads/');
        },
        filename: function (req, file, cb) {
            var datetimestamp = Date.now();
            cb(null, file.fieldname + '-' + file.originalname);
        }
    });
var pptToImageStorage = multer.diskStorage({ //multers disk storage settings
        destination: function (req, file, cb) {
            cb(null, './pptuploads/');
        },
        filename: function (req, file, cb) {
            var datetimestamp = Date.now();
            cb(null, file.originalname);
        }
    });
let uploadPPT = multer({storage:pptToImageStorage}).single('ppt');
var upload = multer({storage:storage}).single('zip');
let FCM = require('fcm-push');
let serverKey = 'AAAYHBeiJU:APA91bGjHHhLr7Vb-9NOXwRL9xLWiu4IxQB-TNOGgYhDxOasRT9jZZIW7l780O1VPq4rWX_Ak75FUbjObzh45CBnqfYV8oq_NhC_-EQRgJaoayYqwhZjj6wRAxG_dRHEiZnXJP1OBPpW';
let fcm = new FCM(serverKey);
let admin  = require('firebase-admin');
const CronJob = require('cron').CronJob;
const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const Thumbnail = require('thumbnail');

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/photoslibrary', 'https://www.googleapis.com/auth/photoslibrary.readonly', 'https://www.googleapis.com/auth/photoslibrary.readonly.appcreateddata'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

const sns = new AWS.SNS({ apiVersion: 'latest' });
app.use(bodyParser.json({ extended: true, limit: '50mb' }));
app.use(express.static(__dirname + '/dist'));
app.use(cors());
app.listen(process.env.PORT || 3000)



let appp= admin.initializeApp({
     credential: admin.credential.cert({
       projectId: 'yuva-191013',
       privateKey: '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCUQxMs15y8hPCP\nMMdoT7a657I+HXn/kj1Wq72xVx3Q2DL4xYUUKJrAn4kVMk7tF3TGYMTUm8+nfjCr\nzhFrtfLqlpbhEp1kjPDzGKIW6R4Kg5drRey/oWu+QnToxXgVp3UwwHmsi7LwEF4i\n0AXetyuu+Qq9wV6E0qf6HRv9zBbtxKvHHimPetNJnJS2MFfZW2uLDLS5W72QvNcm\nGX494rtukAXZTJiK2zytR1zG4P2xqE8qYvurhXU8ZzCGNvpIuguXofb0+MLRuV3E\nS/CYldyF8BOGWoB5XOq8vM2GwKFVTPjZsf1TIZL6o2ng82Rl+FnUvZpqRtAZcQp8\nvQXYd/fNAgMBAAECggEAAWkkTvur4zOnn1Er6znrnO/UnMMxNJBAEv72LTBimVNe\npB/VrOlYVEdgVEo4K94v2gMJ3xUEHuL0Pxo/mlpZmfJTXrJg5YKdeKQDciYx3JCV\nJFaUErG27FFGZVWbAie9ACOrX2EtPhwFwp00mm0nQQEKdrTsO5Aj5t0X4FcXYRfy\nLW/XuQxoFGACfHE8xIAQ4WpHHNyyxqJjIOEH6lhu00fGIbldCmvEQ8Qu9simcdCn\n0R8bzZVnaU3fuD/beXar5TFq51BTFpWwpSjepm85UcGpxAecEzk5GVe8gpWjQuv5\nzz4NmdMeClTcQUKnK/J8iTpLzh2GVUJyx0KqPbKucQKBgQDLotA/cmMa+u2qfO4b\n6EOSZfHIqiPeylDkSg/T7vYZf/8BiLRBuO2wcrcYEpPOCUA071NQo/k5uW1eKDW2\nXO+/QZX0Jh0ucp9Uq4oX1/ts9tzdRAhXiXM7GiNlOLlh3+2DiSkV4vznPb1Hhs+j\nc7gxlmpUAtmFOpHntrjC6WY8/QKBgQC6YwnwuhFFGzpHVMZxAgcH/MYi15D3lAfO\n5bAvgf66eeadRcCQqBCA5DPN8AVi64WA/TRN/+b2N7KM+JtCWjtAhUQuBibH9usA\n+2Xuhb5sjm6mA+csdf/tdh/QYSAutHaGKM3M+YwTy07rvHceilJLvB6CI2rINIXU\ncpC7Ef8HEQKBgGUG944ir+x4xWSp6m/KAYwmK5hEYNttQEiiPcwFZ+8qw1b9fYCF\nwNajcxbsUIwNpt8cRRdi4oiy6AGorU02OnOGlGC2cA/VKhsVp8NAkipzJwxXblQP\niy2tQBY92Csor2tspweGv7XxuXA8/9K13JItb78SUtx4PF7bFjsNFH35AoGBALFM\n4PnTpfX9DpUoOZTqftHETJ58PPSmJj9UEyM+H5g3uX5GCgYROZHCgt0Vw6nzrQ2k\nz4qbMZNqLDkZjGez+cVIgDDfvShe4/INM0uQgjI101+tOsz6erfitCS/H6QOGulI\nC20+vVdfAPYSU6YDooFUtBjLpBpKjDwpty+c0OJhAoGBAIHLTLkSiKuRnkeRMGZA\n1lKfy2YKb1KBo8LjtvH2PLW3y2NYPr/D1LYSbxTvFdmQDH0DzSQD7cGpotKWdq5S\nZrDalQrE7KIlcGuMCkmTse2REbKsvrvDK0PRTW81xPXR2G3pM28mxXaQDGlkoDBb\nIexhR2tS6uVq6/eehPoFgV1g\n-----END PRIVATE KEY-----\n',
       clientEmail: 'firebase-adminsdk-x635x@yuva-191013.iam.gserviceaccount.com'
     })
   });
const schedule = require('node-schedule');

var rule = new schedule.RecurrenceRule();
rule.dayOfWeek = [0, new schedule.Range(0, 6)];
rule.hour = 18;
rule.minute = 40;
schedule.scheduleJob('*/30 * * * *', function () {

console.log('Works');
let date = new Date().getTime();
	request({method: 'POST', json: true, uri: URL +'/api/authenticate', body: {'username':'admin','password':'admin'}, headers: {
        'Content-Type': 'application/json'
      }
    }, (err, r, response) => {
if(err){
//res.status(500).send({'error:':err})
}
else{
console.log(response);
        token = response.id_token;
 

request({method: 'GET', json: true, uri: URL +'/api/organizations', headers: {
        'Content-Type': 'application/json',
	'Authorization':'Bearer ' + token
      }
    }, (err, r, response) => {
if(err){
//res.status(500).send({'error:':err})
}
else{
console.log('org:', response);
response = JSON.parse(JSON.stringify(response));
console.log('organization', typeof(response));
Array.prototype.forEach.call(response, (e) => {
organizationId = e.id;
console.log('ORGID:', organizationId);
activeUsers = [];
totalUsersRegistered = [];
latestActiveUsers = [];
request({method: 'GET', json: true, uri: URL + '/api/_search/events?query=orgID%3D '+organizationId , headers: {
        'Content-Type': 'application/json',
        'Authorization':'Bearer ' + token

      }
    }, (err, r, response) => {
if(err){
//res.status(500).send({'error:':err})
}
else{
response = JSON.parse(JSON.stringify(response));
console.log('response:', response);
Array.prototype.forEach.call(response, (element) => {

if((Number(element.orgID)==Number(organizationId)) && (Number(element.unixDate) < Number(date)) && (Number(element.unixDate) > (Number(date) -(24 * 60 * 60 * 1000))) && (latestActiveUsers.indexOf(element.userID) < 0) ){
latestActiveUsers.push(element.userID);
}
if((Number(element.orgID)==Number(organizationId)) && (activeUsers.indexOf(element.userID) < 0) ){
activeUsers.push(element.userID);
}
if(totalUsersRegistered.indexOf(element.userID) < 0 ){
totalUsersRegistered.push(element.userID);
}



            });
request({method: 'GET', json: true, uri: URL+'/api/users/filterby/'+ organizationId , headers: {
        'Content-Type': 'application/json',
        'Authorization':'Bearer ' + token

      }
    }, (err, r, response) => {
if(err){
//res.status(500).send({'error:':err})
}
else{
response = JSON.parse(JSON.stringify(response));
let totalUsers =response.length;
console.log('length:', response);
request({method: 'POST', json: true, uri: URL + '/api/bookings', body: {
  "name": "",
  "type": "",
  "category": e.name,
  "email": e.name + " organization",
  "address": "",
  "agenda": "Total Users registered - "+totalUsers,
  "mobileNumber": "",
  "skills": "",
  "tags": "sysui",
  "qualifications":new Date().getTime(),
  "experince": e.id,
  "multipleLearners": true,
  "calenderId": null,
  "slotId": null
}, headers: {
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+ token
  }
    }, (err, r, response) => {
if(err){
//res.status(500).send({'error:':err})
}
else{
request({method: 'POST', json: true, uri: URL +'/api/bookings', body: {
   "name": "",
  "type": "",
  "category": e.name,
  "email": e.name + " organization",
  "address": "",
  "agenda": "Total Active Users - "+activeUsers.length,
  "mobileNumber": "",
  "skills": "",
  "tags": "sysui",
  "qualifications":new Date().getTime(),
  "experince": e.id,
  "multipleLearners": true,
  "calenderId": null,
  "slotId": null
}, headers: {
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+ token
  }
    }, (err, r, response) => {
if(err){
//res.status(500).send({'error:':err})
}
else{
console.log('saved notification2:', response);

request({method: 'POST', json: true, uri:URL + '/api/bookings', body: {
  "name": "",
  "type": "",
  "category": e.name,
  "email":e.name + " organization",
  "address": "",
  "agenda": "Total active users in last 24 hours - "+latestActiveUsers.length,
  "mobileNumber": "",
  "skills": "",
  "tags": "sysui",
  "qualifications":new Date().getTime(),
  "experince": e.id,
  "multipleLearners": true,
  "calenderId": null,
  "slotId": null

}, headers: {
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+ token
  }
    }, (err, r, response) => {
if(err){
//res.status(500).send({'error:':err})
}
else{
console.log('saved notification:', response);
}
})



}
})



console.log('saved notification:', response);
}
})

}

});  // Get total users

}

}); // Get active Users




})

}
});      

}
});

});

AWS.config.update({
	'accessKeyId': 'AKIAI32BGT3YUJK2YE6A',
	'secretAccessKey': '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',
	'region': 'us-east-1'
});
// ================================================================================================ ES ============================================================================================== //

app.post('/esearch', async (req,res)=>{
let {index, type, body, aggs} = req;
/*
const response = await client.search({
  index: index,
  type: type,
  body:body
})




 
for (const tweet of response.hits.hits) {
  console.log('tweet:', tweet);
}

*/
console.log('BODY:',body)

try {
  const response = await client.search({
index:'event',
type:'event',
body:body

 });
  console.log(response);
res.status(200).send({data:response})
} catch (error) {
  console.trace(error.message);
res.status(304).send({err:'There was some unexpected error. Please try again later.'})
}

})

// =============================================================================================== End ES =========================================================================================== //




// ======================================================================================= Node Mailer ============================================================================================== //

app.post('/sendMail', (req,res)=>{
let {email, subject, message} = req.body;
main(email,subject,message).catch(error => console.log(error));
res.status(200).send({data:'mail sent'});
})


async function main(email,subject,message){

  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let account = await nodemailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport
/* const transporter = nodemailer.createTransport({
    name:'ethereal.email',
	host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'thaddeus.hyatt70@ethereal.email',
        pass: 'SKcCPAQcnTSYyHGGTC'
    }
});
*/
console.log('Account:', account);

let transporter = nodemailer.createTransport({
    sendmail: true,
    newline: 'unix',
    path: '/usr/sbin/sendmail'
});
transporter.sendMail({
    from: 'support@productivise.io',
    to: email,
    subject: subject,
    text: message
}, (err, info) => {
console.log('e:',err);
    console.log(info.envelope);
    console.log(info.messageId);
});

/*
 let transporter = nodemailer.createTransport({
        name:'https://ethereal.email',
        host: account.smtp.host,
        port: account.smtp.port,
        secure: account.smtp.secure,
        auth: {
            user: account.user,
            pass: account.pass
        }
    });


  // setup email data with unicode symbols
  let mailOptions = {
    from: account.user, // sender address
    to:[email], // list of receivers
    subject:subject, // Subject line
    text: message, // plain text body
    html: `<b> $message <b>` // html body
  };
console.log('mailOptions: ',mailOptions)
transporter.verify(function(error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log('Server is ready to take our messages');
  }
});

  // send mail with defined transport object
  let info = await transporter.sendMail(mailOptions)

  console.log("Message sent: %s", info.messageId);
  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
*/
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

app.get('/sendemail/address=:address/subject=:subject/template=:template', function(request, response) {
  var address = request.params.address;
  var template = request.params.template;
  var subject = request.params.subject;
 
  let transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
          user: 'productivise@gmail.com',
          pass: 'Qwerty@123#'
      }
  });
  let mailOptions = {
      from: '"Productivise Support"', // sender address
      to: address, // list of receivers
      subject: subject, // Subject line
      text: template, // plain text body
      html: '' // html body
  };

  transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
          return console.log(error);
      }
      console.log('Message %s sent: %s', info.messageId, info.response);
          response.render('index');
      });
});


// ======================================================================================End Node Mailer ============================================================================================ //

app.post('/auth/google', (req,res)=>{
console.log(req.body);

res.status(200).send('Done');
})
app.post('/files', (req,res) => {
let authtoken='';
fs.readFile('photoscredentials.json', (err, content) => {
  if (err) return console.log('Error loading client secret file:', err);
  // Authorize a client with credentials, then call the Google Drive API.
  authorize(JSON.parse(content), listFiles);
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  const {client_secret, client_id, redirect_uris} = credentials.web;
  const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getAccessToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getAccessToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error retrieving access token', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) console.error(err);
        console.log('Token stored to', TOKEN_PATH);
authtoken = token;
      });
      callback(oAuth2Client);
    });
  });
}

function getRefreshToken(oAuth2Client,callback){

}

/**
 * Lists the names and IDs of up to 10 files.
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function listFiles(auth) {
 /* const drive = google.drive({version: 'v3', auth});
  drive.files.list({
    pageSize: 10,
    fields: 'nextPageToken, files(id, name)',
  }, (err, res) => {
    if (err) return console.log('The API returned an error: ' + err);
    const files = res.data.files;
    if (files.length) {
      console.log('Files:');
      files.map((file) => {
console.log('File:', JSON.stringify(file));
        console.log(`${file.name} (${file.id})`);
      });
    } else {
      console.log('No files found.');
    }
  });*/
let responseToken;
console.log('AUTH:', auth.credentials.access_token);
request({method: 'POST', json: true, uri: URL + '/api/authenticate', body: {'username':'admin','password':'admin'}, headers: {
        'Content-Type': 'application/json'
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log(response);
        token = response.id_token;
do{
request({method: 'GET', json: true, uri:'https://photoslibrary.googleapis.com/v1/mediaItems?pageSize=100', headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + auth.credentials.access_token //  ya29.GlunBgOJJ9OEErPqxiBCrEzu7EhggKqEZXQj_VP481ULR77_gk74d5ViTuMTqyqK8mdkrSakKn6BCTwa93xrv9MQkaid9mRjPxDobp1ZRs_G-MkaYjVmWdj2miqU'
      }
    }, (err, r, response) => {
if(err || (response.error && response.error.code === 401)){
console.log('Auth Error:', err);
request({method: 'POST', json: true, uri:'https://authorization-server.com/oauth/token',body:auth, headers: {
        'Content-Type': 'application/json'
         }
    }, (err, r, response) => {
if(err){
console.log('RefeshErr:', err);
res.status(500).send({'error:':err})
}
else{
console.log('Refresh',response)
res.status(500).send({'error:':err})
}
});
}
else{

console.log('ALBUM:',response);
responseToken = response.nextPageToken;
let photos = response.mediaItems;
for(let i=0;i<photos.length; i++){
	request({method: 'POST', json: true, uri:URL + '/api/files', body: {
    "name": photos[i]['filename'],
    "value": photos[i].id,
    "filePath": photos[i]['productUrl'],
    "cdnPath": photos[i]['baseUrl']+'=w100-h100',
    "template": null,
    "publishedDate": photos[i].mediaMetadata.creationTime,
    "language": photos[i].mediaMetadata.photo.cameraModel,
    "author": null,
    "description": photos[i].mimeType,
    "summary": null,
    "tags": null,
    "source": null,
    "type": null
  
}, headers: {
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+ token
  }
    }, (err, r, response) => {
if(err){
refs.status(500).send({'error:':err})
}
else{
console.log('saved notification:', response);
}
})

}
}
});
} while(responseToken);
}
})
}
}) 
//==================================================================================================== PPT TO IMAGE =======================================================================================

app.post('/pptToImage',(req,resppt)  =>{

var path = '';
    // uploadPPT(req, res, function (err) {
      
//  if (err) {
          // An error occurred when uploading
//          console.log(err);
//          return res.status(422).send("an Error occured")
//        } 
       // No error occured.
       download(req.body.file,'/home/sthaapakx/portal/pptuploads/Presentations-Tips.ppt').then(responseAxios =>{
path = 'Presentations-Tips.ppt';
process.env.time = new Date().getTime();

console.log("PATH:", responseAxios);
        let argument1 = String(path);
        var options = {
    env : process.env
}
process.env.fileName = argument1;
process.env.folderName = 'pptuploads';
process.env.pptName = process.env.fileName;
process.env.pdfName = 'Presentations-Tips.pdf' ;
//process.env.pdfName = process.env.pdfName.splice(0,process.env.pptName.split('.').length-1);
process.env.imageName = 'Presentations-Tips';
//process.env.pdfName = process.pdfName.join()+'.pdf';
console.log("ENV:",process.env.time);

        const testscript = exec('sh convertanddeploytos3script.sh', options);
       testscript.stdout.on('data', function(data){
console.log('OUTDATA:', data);
//return resppt.status(200).send({data:'https://s3.ap-south-1.amazonaws.com/sify-content/pptUploads/', env:process.env.time}); 
   // sendBackInfo();
// return resppt.status(200).send({data: data, env:process.env});
});
testscript.stdout.on('close', function(data){
console.log('OUTDATA:', testscript.stdout);
});

testscript.stderr.on('data', function(data){
    console.log('ErrData:', data);
    // triggerErrorStuff(); 
 console.log({error:data });
});
return req.setTimeout(60000,()=>{
 return resppt.status(200).send({data:'https://s3.ap-south-1.amazonaws.com/sify-content/pptUploads/', env:process.env.time});
});
});
/*path = 'Presentations-Tips.ppt';
console.log("PATH:", path);
        let argument1 = String(path);
        var options = {
    env : process.env
}
process.env.fileName = argument1;
process.env.folderName = 'pptuploads';
process.env.pptName = process.env.fileName;
process.env.pdfName = 'Presentations-Tips.pdf' ;
//process.env.pdfName = process.env.pdfName.splice(0,process.env.pptName.split('.').length-1);
process.env.imageName = 'Presentations-Tips';
//process.env.pdfName = process.pdfName.join()+'.pdf';
console.log("ENV:", process.env);

        const testscript = exec('sh convertanddeploytos3script.sh', options);
*/
/*        testscript.stdout.on('data', function(data){
    console.log('OutData:', data);
    // sendBackInfo();
// return resppt.status(200).send({data: data, env:process.env});
});
testscript.stdout.on('close', function(data){
    console.log('OutData Close:', data);
return resppt.status(200).send({data: data, env:process.env});
});

testscript.stderr.on('data', function(data){
    console.log('ErrData:', data);
    // triggerErrorStuff(); 
 console.log({error:data, env:process.env });
});
*/
//resppt.status(200).send({data: 'data', env:process.env});
       
// });
});


//===================================================================================================End PPT TO IMAGE =====================================================================================

// ==================================================================================================== HTML UPLOAD =========================================================================================
app.post('/upload',(req,res)=>{

var path = '';
     upload(req, res, function (err) {
        if (err) {
          // An error occurred when uploading
          console.log(err);
          return res.status(422).send("an Error occured")
        }  
       // No error occured.
        path = req.file.path;
	let argument1 = String(path);
	var options = {
    env : process.env
}
process.env.fileName = argument1.split('/')[1];
process.env.folderName = 'website';
	const testscript = exec('sh deploytos3script.sh', options);

	testscript.stdout.on('data', function(data){
    console.log('OutData:', data); 
    // sendBackInfo();
});

testscript.stderr.on('data', function(data){
    console.log('ErrData:', data);
    // triggerErrorStuff(); 
});

        return res.send('http://sify.productivise.io.s3-website-us-east-1.amazonaws.com/'); 
});
});

//====================================================================================================End HTML UPLOAD ======================================================================================

app.post('/subscribe',(req,res)=>{
console.log('reqBody:',req.body);
admin.messaging().subscribeToTopic(req.body.registrationTokens,'/topics/'+req.body.topic)
  .then(function(response) {
    // See the MessagingTopicManagementResponse reference documentation
    // for the contents of response.
    console.log('Successfully subscribed to topic:', JSON.parse(JSON.stringify(response)));
res.status(200).send(response); 
 })
  .catch(function(error) {
    console.log('Error subscribing to topic:',JSON.parse(JSON.stringify(error)));
res.status(500).send(error); 
 });



});

//==================================================================================================== Notification ======================================================================================


app.post('/contentUpdate',(req,res)=>{



   // Request methods you wish to allow


    // Request headers you wish to allow



    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)






console.log('body:', req.body);
let message={};

if(req.body.token){
if(!req.body['data']['silent']){



 message = {
				token: String(req.body.token), // String(req.body.token || req.body.topic),//'eK90wpvBlaQ:APA91bEUzQtEpN590XRch191VH8VaYsnIYPdWdPHn7ParLR29mFBEqfNJZZnE9_oPwKCKNJrhRFRiLfcU_S_Qw-hiw3o1KoZa174ybrhDArwYPg5GL8_NDJufBVmiwFjN3JCuZfwXraK', // required fill with device token or topics
				notification: {
					title: req.body.data.name || ' ',
					body: req.body.data.body || ' '
                                        
				

			
				},

	data: {
                                        title: req.body.data.title || ' ',
                                        body: req.body.data.body || ' ',
                                        screenType: req.body.data.screenType || ' ',
                                        image: req.body.data.image || ' ', //'https://www.jagranimages.com/images/14_03_2018-court.jpg',
                                        specialParameters: req.body.data.specialParameters || ' ',
tags:'Notify',
orgId: String(req.body.data.orgId) || ' ',
name : req.body.data.name || ' '
                                },
android:{
priority:"high"
          
}
	};
} 
if(req.body['data']['silent']){



 message = {
				token: String(req.body.token), // String(req.body.token || req.body.topic),//'eK90wpvBlaQ:APA91bEUzQtEpN590XRch191VH8VaYsnIYPdWdPHn7ParLR29mFBEqfNJZZnE9_oPwKCKNJrhRFRiLfcU_S_Qw-hiw3o1KoZa174ybrhDArwYPg5GL8_NDJufBVmiwFjN3JCuZfwXraK', // required fill with device token or topics

//contentAvailable : true,
data: {
                                        title: req.body.data.title || ' ',
                                        body: req.body.data.body || ' ',
                                        screenType: req.body.data.screenType || ' ',
                                        image: req.body.data.image || ' ', //'https://www.jagranimages.com/images/14_03_2018-court.jpg',
                                        specialParameters: req.body.data.specialParameters || ' ',
tags:'Silent',
orgId: String(req.body.data.orgId) || ' ',
name : req.body.data.name || ' ',
template : req.body.data.template || ' '
                                },
android:{
priority:"high"
}



			};
}

}
if(req.body.topic){

if(!req.body['data']['silent']){
 message = {
                                topic: String(req.body.topic), // String(req.body.token || req.body.topic),//'eK90wpvBlaQ:APA91bEUzQtEpN590XRch191VH8VaYsnIYPdWdPHn7ParLR29mFBEqfNJZZnE9_oPwKCKNJrhRFRiLfcU_S_Qw-hiw3o1KoZa174ybrhDArwYPg5GL8_NDJufBVmiwFjN3JCuZfwXraK', // required fill with device token or topics
                                notification: {
                                        title: req.body.data.name || ' ',
                                        body: req.body.data.body || ' '
                                        

                                },
                               data: {
                                        title: req.body.data.title || ' ',
                                        body: req.body.data.body || ' ',
                                        screenType: req.body.data.screenType || ' ',
                                        image: req.body.data.image || ' ', //'https://www.jagranimages.com/images/14_03_2018-court.jpg',
                                        specialParameters: req.body.data.specialParameters || ' ',
tags:'Notify',
orgId: String(req.body.data.orgId) || ' ',
name : req.body.data.name || ' '
                                },
android:{
priority:"high"
 
},
apns:{
payload:{

title:"Very good Notification",
aps:{
sound:"default"
}
}
}





                        };

}
if(req.body['data']['silent']){
 message = {
                               topic: String(req.body.topic), // String(req.body.token || req.body.topic),//'eK90wpvBlaQ:APA91bEUzQtEpN590XRch191VH8VaYsnIYPdWdPHn7ParLR29mFBEqfNJZZnE9_oPwKCKNJrhRFRiLfcU_S_Qw-hiw3o1KoZa174ybrhDArwYPg5GL8_NDJufBVmiwFjN3JCuZfwXraK', // required fill with device token or topics

//contentAvailable : true,

                                data: {
                                        title: req.body.data.title || ' ',
                                        body: req.body.data.body || ' ',
                                        screenType: req.body.data.screenType || ' ',
                                        image: req.body.data.image || ' ', //'https://www.jagranimages.com/images/14_03_2018-court.jpg',
                                        specialParameters: req.body.data.specialParameters || ' ',
tags:'Silent',
orgId: String(req.body.data.orgId) ||  ' ' ,
name : req.body.data.name || ' '
                                },
android:{
priority:"high"
}

                        };

}

}
console.log('MESSAGE:', message);
let options = {
  priority: "high"
 
};
if(!req.body.data.scheduled){

admin.messaging().send(message)
    .then((response) => {
      // Response is a message ID string.
      console.log('Successfully sent message:', response);
res.status(200).send({'Response Sent':response});
if(req.body.data.save){

request({method: 'POST', json: true, uri: URL + '/api/authenticate', body: {'username':'admin','password':'admin'}, headers: {
        'Content-Type': 'application/json',
        'Authorization':'Bearer ' + token
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log(response);
        token = response.id_token;


request({method: 'POST', json: true, uri:URL + '/api/notifications', body: {
    "name": req.body.data.name,
    "template": "string",
    "tags": req.body.data.save ? 'Notify' : "update",
    "description": "Info",
    "image": "Info",
    "ctaURL": "string",
    "sequence": null,
    "scheduled": null,
    "sound": new Date().getTime(),
    "title": req.body.data.title,
    "body": req.body.data.body,
    "screenType": req.body.data.screenType,
    "specialParameters": null,
    "orgId": req.body.data.orgId,
    "groupId": null,
    "userId":null,
    "campaignId": 82551,
    "lifeCycleId": 257501,
    "microContentId": null
}, headers: {
        'Content-Type': 'application/json',
	'Authorization':'Bearer '+ token    
  }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
console.log('saved notification:', response);
}
})
}
});
}
    })
    .catch((error) => {
res.status(500).send(error);
      console.log('Error sending message:', error);
    });
}
if(req.body.data.scheduled){
console.log('Schedule:', req.body.data.scheduled);
try{
 new CronJob(String(req.body.data.scheduled), function() {
 admin.messaging().send(message)
    .then((response) => {
      // Response is a message ID string.
 
    console.log('Successfully sent message:', response);

if(req.body.data.save){

request({method: 'POST', json: true, uri: URL + '/api/authenticate', body: {'username':'admin','password':'admin'}, headers: {
        'Content-Type': 'application/json',
         'Authorization':'Bearer ' + token
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log(response);
        token = response.id_token;


request({method: 'POST', json: true, uri:URL + '/api/notifications', body: {
    "name": req.body.data.name,
    "template": "string",
    "tags": req.body.data.save ? 'Notify' : "update",
    "description": "Info",
    "image": "Info",
    "ctaURL": "string",
    "sequence": null,
    "scheduled": req.body.data.scheduled,
    "sound": new Date().getTime(),
    "title": req.body.data.title,
    "body": req.body.data.body,
    "screenType": req.body.data.screenType,
    "specialParameters": null,
    "orgId": req.body.data.orgId,
    "groupId": null,
    "userId":null,
    "campaignId": 82551,
    "lifeCycleId": 257501,
    "microContentId": null
}, headers: {
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+ token
  }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
console.log('saved notification:', response);
}
})
}
});
}
    })

}, null, true, 'Asia/Kolkata');
res.status(200).send({'Response Sent':{}});

}
catch(e){
console.log('error:', e);
}
//res.status(200).send({'Response Sent':{}});

}
/*
fcm.send(message).then(response => {
console.log('message:', message);
					console.log("Successfully sent with response: ", response);
					//res.json({ status: "success", data: response })
res.status(200).send(response);
				},err=>{
console.log('message:', message);
console.log("Error sent with response: ", err);
res.status(500).send(err);
}
)
*/

});

//==================================================================================================== End Notifications =====================================================================================


app.post('/addToGroup',(req,res)=>{
request({method: 'POST', json: true, uri: URL+'/api/authenticate', body: {'username':'admin','password':'admin'}, headers: {
        'Content-Type': 'application/json'
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log(response);
	token = response.id_token;

request({method: 'PUT', json: true, uri:URL + '/api/learners', body: req.body, headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log(response);
	request({method: 'GET', json: true, uri: URL + '/api/members/'+req.body.description, headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log(response);
response.gehash1 = req.body.profileImage;
	request({method: 'PUT', json: true, uri: URL +'/api/members', body: response, headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log(response);
newmember=response;
	request({method: 'GET', json: true, uri: URL +'/api/campaign-groups/filterby/'+(req.body['orgId'] || req.body['organizationId']), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    }, (err, r, response) => {
    if(err){
res.status(500).send({'error:':err})
}
else{
      console.log('Cgroups',response[0]);
      
	response.forEach(e=>{
	  console.log('Name of group',e.name);
	    console.log('Cgroups',req.body.jobRole + '\n');

  e.name =  e.name.trim(); 
if(req.body.jobRole!='Others' && e.name == req.body.jobRole){
      console.log('Cgroups 1',e);
e.learners.push(req.body);
request({method: 'PUT', json: true, uri: URL +'/api/campaign-groups', body: e, headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log('putCgroup',response);
request({method: 'GET', json: true, uri: URL +'/api/support-groups', headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      
response.forEach(ele=>{
ele.name = ele.name.trim();
if(ele.name == req.body.jobRole){
console.log('sgroup:', ele);

ele.members.push(newmember)
console.log('sgroupmembers:', ele.members);
request({method: 'PUT', json: true, uri: URL + '/api/support-groups', body: ele, headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log(response);

}
})

}
})

}
})

}
})

}
if(req.body.jobRole=='Others' && e.name == 'Sky is the limit'){
      console.log('Cgroups 1',e);
e.learners.push(req.body);
request({method: 'PUT', json: true, uri: URL +'/api/campaign-groups', body: e, headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log('putCgroup',response);
request({method: 'GET', json: true, uri: URL +'/api/support-groups', headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      
response.forEach(ele=>{
if(ele.name == 'Sky is the limit'){
console.log('sgroup:', ele);

ele.members.push(newmember)
console.log('sgroupmembers:', ele.members);
request({method: 'PUT', json: true, uri:URL+ '/api/support-groups', body: ele, headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log(response);

}
})

}
})

}
})

}
})

}
})

}
})

	
}
})

}
})
}
});
}
});
});


/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/*reset password */
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

app.post('/resetPassword', (req,res)=>{

request({method: 'POST', json: true, uri: URL +'/api/authenticate', body: {'username':'admin','password':'admin'}, headers: {
        'Content-Type': 'application/json'
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log(response);
	token = response.id_token;


let newUser = req.body;
 request({method: 'PUT', json: true, uri: URL +'/api/users/saas/reset', body: newUser, headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    }, (err, r, response) => {
console.log(err, response);
if(err){
res.status(500).send({'error:':err})
}
else{
res.status(200).send(response);

}
});
}
});
});
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/*reset password end */
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

app.post('/inviteuser',(req,res) => {
console.log('body:', req.body);
let phone = req.body.phone;
let name = req.body.name;
let invitedBy = req.body.invitedBy;
let password = req.body.password;
let org = req.body.org;
let token = req.body.token;
let role = req.body.roleId;
let newUser = {
    'login': phone,
    'firstName': name.split(' ')[0],
    'lastName': name.split(' ')[1] || null,
    'email': name.split(' ')[0] + phone + '@gmail.com',
    'imageUrl': null,
    'activated': true,
    'langKey': null,
    'createdBy': invitedBy || 'admin',
    'createdDate': new Date().toISOString(),
    'lastModifiedBy': invitedBy || 'admin',
    'lastModifiedDate': new Date().toISOString(),
    'authorities': [
      'ROLE_USER'
    ],
    'phone': phone,
    'userId': null,
    'userType': 0,
    'orgId': org,
    'deviceID': role || '',
    'geoHash': null,
    'location': null,
    'aadharNumber': null,
    'userStatus': null,
    'userPassword': password || 'qwerty'
  };
  console.log('newUser:', newUser);
request({method: 'POST', json: true, uri:URL + '/api/authenticate', body: {'username':'admin','password':'admin'}, headers: {
        'Content-Type': 'application/json'
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log(response);
        token = response.id_token;


  request({method: 'POST',
    json: true,uri:URL+'/api/users/register',body: newUser,headers:{
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + token
  } },(err,r ,resp) => {
if(!resp.firstName){
console.log()
res.status(400).send({'error': resp})
}
else

 if(err){
res.status(500).send({'error ': err})
}
else{
    console.log('user:', resp);

    let result = resp;
    console.log('memberData:', result);

    let memberData = {
      'name': result['firstName'] +' '+ (result['lastName'] || ''),
      'userId': result['id'],
      'learnerId': result['learner']['id'],
      'category': org,
      'subCategory': null,
      'locationName': result['location'],
      'gehash1': result['geoHash'],
      'memberType': 'APPLICANT',
      'aadhaar': result['phone'] || null,
      'mobileNo': result['phone'] || phone,
      'cardId': null
    };
    console.log('memberData:', memberData);
    request({method: 'POST', json: true, uri:URL+ '/api/members', body: memberData, headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    }, (err, r, response) => {
if(err){
res.status(500).send({'error:':err})
}
else{
      console.log(response);
let memberResponse = response;
      console.log('member:', memberResponse);
      newUser.id = result.id;
      newUser.learnerId = result['learner']['id'];
      newUser.langKey = memberResponse.id;
      newUser.userPassword = password || 'qwerty';

      request({method: 'PUT',
    json: true, uri: URL +'/api/users', body: newUser, headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      }, (err, r, respo) => {
if(err){
res.status(500).send({'error:':err})
}
else{

let message = 'Hello ' + name +'.'+ invitedBy + ' invited you to Productivise. Link: '+URL+'/create/com.sthaapak.productivise/' + this.org + '/' + this.id;
    
     if (invitedBy) {
console.log('Message Sent!!', this.message);
    sns.publish({
      Message: message, PhoneNumber: '+91' + phone, MessageAttributes: {
        'AWS.SNS.SMS.SenderID': {
          DataType: 'String',
          StringValue: 'PRDCTVSE'
        },
        'AWS.SNS.SMS.MaxPrice': {
          DataType: 'Number',
          StringValue: '00.01'
        },
        'AWS.SNS.SMS.SMSType': {
          DataType: 'String',
          StringValue: 'Transactional'
        }
      }
    }).promise().then(res => {
      console.log(res);
    });
}
        console.log('user:', respo);
res.status(200).send(respo);
}
      });
}
    });
}
  });
}
});
});
app.get('/*',function(req,res){

res.sendFile(path.join(__dirname+'/dist/index.html'));
});

/*
app.get('*', function (req, res) {
  const index = path.join(__dirname,'../dist/index.html');
  res.sendFile(index);
});*/

console.log("server running");
