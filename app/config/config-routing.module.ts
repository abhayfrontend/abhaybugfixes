import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigAddComponent } from './config-add.component';
import { ConfigListComponent } from './config-list.component';


const routes: Routes = [
  {
    path: '',
    children: [

      {
        path: 'add',
        component: ConfigAddComponent,
        data: {
          title: 'App Config'
        }
      }, {
        path: 'list',
        component: ConfigListComponent,
        data: {
          title: 'List App Config'
        }
      },
      {
        path: 'edit',
        component: ConfigAddComponent,
        data: {
          title: 'Edit App Config'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigRoutingModule { }
