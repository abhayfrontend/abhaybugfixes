import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CustomFormsModule } from 'ng2-validation';
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { ConfigAddComponent } from './config-add.component';
import { ConfigListComponent } from './config-list.component';
import { ConfigRoutingModule } from "./config-routing.module";



@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        ConfigRoutingModule,
        CustomFormsModule,
        MatchHeightModule,
        NgbModule,
        NgxDatatableModule
    ],
    declarations: [
        ConfigAddComponent,
        ConfigListComponent
    ]


})
export class ConfigModule { }
