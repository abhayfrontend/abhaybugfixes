import { Component, OnInit, Input } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { base } from '../shared/configs/util';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import { PagerService } from '../pager.service';
import { PreviewService } from '../shared/auth/preview.service';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pager',
  templateUrl: 'pager.component.html'
})

export class PagerComponent implements OnInit {
  private allItems: any[];

  // pager object
  pager: any = {};
  organizationId;
  // paged items
  pagedItems: any[];
  @Input() getValues: any;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  }
  constructor(private http: HttpClient, private previewService: PreviewService, private pagerService: PagerService,
    private roleGuardService: RoleGuardService, private router: Router) {

  }

  // array of all items to be paged


  showStats(e) {
    // console.log('usere:', e);
    this.previewService.editStatsOf(e);
    this.router.navigate(['/dashboard/dashboard2']);

  }




  ngOnInit() {
    this.allItems = this.getValues;
    // console.log('Values:', this.getValues);
    this.organizationId = this.previewService.organizationId.getValue();
    this.setPage(1);
    // get dummy data
    /*
    this.http.get(base + '/api/learners', this.httpOptions).subscribe(res => {

      let result = <any>res;
      // console.log('resilPAGER:', result);
      result = result.filter(element => {
        if (element.organizationId === this.organizationId || element.orgId === this.organizationId) {
          if (!element['name']) {
            // console.log('ERROR NAME:', element);
          }
          if (element['name'].split(' ')[1] === 'null') {
            element.name = element.name.split(' ')[0];
          }
          return true;
        }
        return false;
      });
      this.allItems = result;

      // initialize to page 1

    });*/
    /*
    this.setPage(1);
    this.temp = [...result];
    this.rows = this.temp;
    //this.temp = [...this.temp, ...this.temp2];
    // console.log('temp:', this.temp);
      .map((response: Response) => response.json())
      .subscribe(data => {
        // set items to json response
        this.allItems = data;

        // initialize to page 1
        this.setPage(1);
      });
      */
  }

  setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.allItems.length, page);

    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}

