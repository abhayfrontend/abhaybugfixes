import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CustomFormsModule } from 'ng2-validation';
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { MessagingAddComponent } from './messaging-add.component';
import { MessagingListComponent } from './messaging-list.component';
import { MessagingRoutingModule } from "./messaging-routing.module";



@NgModule({
    imports: [
        CommonModule,
       
        ReactiveFormsModule,
        FormsModule,
        MessagingRoutingModule,
       
        CustomFormsModule,
        MatchHeightModule,
        NgbModule,
        NgxDatatableModule
    ],
    declarations: [
       
        MessagingAddComponent,
         MessagingListComponent
    ]

    
})
export class  MessagingModule { }
