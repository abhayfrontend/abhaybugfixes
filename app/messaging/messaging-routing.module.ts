import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessagingAddComponent } from './messaging-add.component';
import { MessagingListComponent } from './messaging-list.component';


const routes: Routes = [
  {
    path: '',
    children: [

      {
        path: 'add',
        component: MessagingAddComponent,
        data: {
          title: 'Send personal message  with a group '
        }
      },{
               path: 'list',
               component: MessagingListComponent,
               data: {
                 title: 'List Internal Messaging '
               }
             }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MessagingRoutingModule { }
