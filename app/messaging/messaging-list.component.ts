import { Component, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

declare var require: any;
const data: any = require('../shared/data/company.json');

@Component({
    selector: 'app-dt-filter',
    templateUrl: './messaging-list.component.html',
    styleUrls: ['./messaging-list.component.scss']
})

export class MessagingListComponent {
    rows = [];

    temp = [];

    // Table Column Titles
    columns = [
       { prop: 'template' },
       { prop: 'to' },
       { prop: 'sender' },
       { prop: 'subject' },
       { prop: 'content' },
       { prop: 'Start time' },
       { prop: 'Stop time' },



    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor() {
        this.temp = [...data];
        this.rows = data;
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }
}