import { Component, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { UserResourceService, CampaignGroupResourceService, DeviceResourceService } from '../sthaapak';
import { PreviewService } from '../shared/auth/preview.service';
import swal from 'sweetalert2';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';

import { base } from '../shared/configs/util';
import { SupportGroupResourceService } from '../sthaapak/sdk/supportGroupResource.service';
import { ChatHistoryResourceService } from '../sthaapak/sdk/chatHistoryResource.service';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import { Router } from '@angular/router';
@Component({
    selector: 'app-dt-filter',
    templateUrl: './poweruser-list.component.html',
    styleUrls: ['./poweruser-list.component.scss']
})

export class PoweruserListComponent implements OnInit {
    rows = [];
    filterOption;
    createdBy = [''];
    status = ['', 'Active', 'Inactive'];
    temp = [];
    inputOption = {};
    groups = [];
    data = [];
    cgroup;
    sgroup;
    // Table Column Titles
    columns = [
        { prop: 'firstName' },
        { prop: 'lastName' },
        { prop: 'phone' },
        { prop: 'createdDate' },
        { prop: 'userType' }
    ];

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.roleGuardService.token
        })
    };

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private userService: UserResourceService, private previewService: PreviewService, private http: HttpClient, private chathistory: ChatHistoryResourceService,
        private roleGuardService: RoleGuardService, private groupService: CampaignGroupResourceService, private supportGroup: SupportGroupResourceService,
        private device: DeviceResourceService, private router: Router) {
        if (!localStorage.getItem('token')) {
            this.router.navigateByUrl('/login');
        }
    }
    ngOnInit() {
        this.getUsers();
        let result = <any>this.previewService.campaignGroups.getValue();
        result.forEach((e, i) => {
            this.inputOption[e.name] = e.name;
            // console.log('g:', this.inputOption);

        });
        // console.log('groups2:', this.groups);
        if (this.groups.length < 1) {
            this.getGroups();
        }

    }
    getGroups() {
        this.groupService.getAllCampaignGroupsUsingGET().subscribe(res => {
            let result = <any>res;
            // console.log('groups:', result);
            this.groups = [];
            result.forEach((e, i) => {


                if (Number(e.orgId) === Number(this.previewService.organizationId.getValue())) {
                    this.groups.push(e);
                    this.inputOption[e.name] = e.name;
                    // console.log('g:', this.groups);
                }
            });
            // console.log('groups2:', this.groups);
        })


    }
    reset() {
        this.filterOption = '';
        this.rows = this.temp;
    }

    getUsers() {
        this.rows = [];

        this.temp = [];
        this.data = [];
        this.userService.getAllUserForOrgidUsingGET(Number(this.previewService.organizationId.getValue())).subscribe(res => {
            let result = <any>res;
            // console.log('reult:', result)
            // console.log('org1: ', Number(this.previewService.organizationId.getValue()));
            result.forEach(element => {
                // console.log('org2: ', element.orgId)
                if (element.orgId && element.orgId === Number(this.previewService.organizationId.getValue()) && element.userType !== 0 && element.userType !== 7) {
                    element.createdDate = new Date(element.createdDate).toDateString();
                    switch (element.userType) {
                        case 1:
                            element.userType = 'Content Manager';
                            break;
                        case 2:
                            element.userType = 'Administrator';
                            break;
                        case 3:
                            element.userType = 'HR';
                            break;
                        case 4:
                            element.userType = 'Campaign Manager';
                            break;
                        case 6:
                            element.userType = 'Super User';
                            break;
                    }
                    this.data.push(element);
                    if (element.createdDate) {

                        element.createdDate = new Date(element.createdDate).toDateString();
                        // console.log('createdBy', element.createdBy);

                    }
                    if (this.createdBy.indexOf(element.createdBy) < 0) {
                        this.createdBy.push(element.createdBy);
                    }

                }


            });
            // console.log('data:', this.data);
            this.temp = [...this.data];
            this.rows = this.data;
            // console.log('temp:', this.temp);


        })
    }


    addToGroup(row) {
        swal.mixin({
            confirmButtonText: 'Next &rarr;',
            showCancelButton: true,
            progressSteps: ['1', '2', '3']
        }).queue([{
            title: 'Select a Group',
            input: 'select',
            inputOptions: this.inputOption,
            inputPlaceholder: 'Select a Group',
            showCancelButton: true,
            inputValidator: (value) => {
                return new Promise((resolve) => {
                    if (value) {
                        resolve()
                    } else {
                        resolve('You need to select one Group')
                    }
                })
            }
        }, {
            title: 'Are you sure you want to add this user to the group?',
            showCancelButton: true,
            showConfirmButton: true,

        }]).then(result => {
            let proceed = true;
            // console.log('form', result);
            if (result.value) {
                let campaignGroup = result.value[0];

                this.groups.forEach(e => {
                    if (e.name === campaignGroup) {
                        this.cgroup = <any>e;
                        // console.log('cgr', this.cgroup);
                        this.cgroup.learners.forEach(element => {
                            if (element.id === row.learner.id) {
                                swal({
                                    title: 'Error',
                                    text: 'This user is already a part of this group.',
                                    onClose: () => swal.close()
                                })
                                proceed = false;
                                return;
                            }
                        });
                    }

                });

                this.supportGroup.getAllSupportGroupsUsingGET().subscribe(el => {
                    let sgroups = <any>el;
                    // console.log(campaignGroup);
                    sgroups.forEach(element => {

                        if (campaignGroup === element.name && Number(element.category) === Number(this.previewService.organizationId.getValue())) {
                            this.sgroup = element;
                            // console.log(this.sgroup);
                        }
                    });

                })
                let conf = this.cgroup;
                // console.log(conf);
                conf['learners'].push(row.learner);

                // console.log('2', conf);
                if (proceed) {
                    this.groupService.updateCampaignGroupUsingPUT(conf).subscribe(ans => {
                        this.http.get(base + '/api/members/' + row.langKey, this.httpOptions).subscribe(member => {
                            let smember = <any>member;
                            // console.log('member:', smember);

                            // console.log('members in group:', this.sgroup['members']);
                            let newsgroup = this.sgroup;
                            newsgroup['members'].push(smember);
                            // console.log('members in group:', newsgroup['members']);

                            this.supportGroup.updateSupportGroupUsingPUT(newsgroup).subscribe(don => {
                                // console.log(don);
                                this.chathistory.createChatHistoryUsingPOST({
                                    "attachment": true,
                                    "attachmentLink": "string",
                                    "category": "string",
                                    "description": row['learner'].name + " has been added to " + this.sgroup.name + " group by " + this.previewService.Username.getValue(),
                                    "from": "string",
                                    "group": this.sgroup.id,
                                    "name": "string",
                                    "orgId": this.previewService.organizationId.getValue(),
                                    "subCategory": "broadcast",
                                    "tags": "string",
                                    "to": "string",
                                    "ts": new Date().getTime()
                                }).subscribe(chat => {
                                    this.http.get(base + '/api/campaign-groups', this.httpOptions).subscribe(cgroups => {
                                        let campaignGroups = <any>cgroups;
                                        campaignGroups = campaignGroups.filter(e => e.orgId === this.previewService.organizationId.getValue())
                                        this.previewService.editCampaignGroup([]);
                                    })
                                    this.http.get(base + '/api/support-groups', this.httpOptions).subscribe(sgroups => {
                                        let supportGroups = <any>sgroups;
                                        supportGroups = supportGroups.filter(e => e.category === this.previewService.organizationId.getValue())
                                        this.previewService.editSupportGroup([]);
                                    })
                                    // console.log(chat);
                                    swal({
                                        title: 'User Added To Group',
                                        text: 'This user has been added to the group.',
                                        type: 'success',
                                        onClose: () => swal.close()
                                    })
                                })
                            })
                        })
                    })
                }




            }
            else {
                swal('User not added in the Group', '', 'error')
            }
        });
    }


    changeFilter(e) {
        // console.log('filter:', this.temp);
        this.filterOption = e.target.value;
        this.rows = this.temp;
    }


    filterListByCreatedBy(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            return (d.createdBy === val) || !val;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByStatus(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log(d.activated);
            // console.log(val);
            if (val === 'Active') {
                return d.activated === true || !val;
            }
            if (val === 'Inactive') {
                return d.activated === false || !val;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    filterListByRegDate(event) {
        // console.log('temptt:', this.temp);
        this.rows = this.temp;
        // console.log('temptr:', this.rows);

        const val = event.target.value;
        // filter our data
        const temp3 = this.temp.filter(function (d) {
            // console.log('date:', d.createdDate)
            if (!val) {
                return true;
            }
            if (new Date(d.createdDate).getDate() === new Date(val).getDate() && new Date(d.createdDate).getMonth() === new Date(val).getMonth() && new Date(d.createdDate).getFullYear() === new Date(val).getFullYear()) {


                return true;
            }
            return false;
        });

        // update the rows
        this.rows = temp3;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.firstName.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    deleteContent(row) {

        swal({
            title: 'Are you sure you want to delete this user record?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success btn-raised mr-5',
            cancelButtonClass: 'btn btn-danger btn-raised',
            buttonsStyling: false
        }).then((confirm) => {
            // console.log('con:', confirm)
            if (confirm.value === true) {
                this.userService.deleteUserUsingDELETE(row.login).subscribe(res => {
                    let result = <any>res;
                    // console.log('delte', result)
                    this.getUsers();

                })
            }
            else {
                swal(
                    'Cancelled',
                    'The user record was not deleted.',
                    'error'
                )
            }
        }, (dismiss) => {
            // console.log('dismis:', dismiss)
            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'The user record was not deleted.',
                    'error'
                )
            }
        })

    }

    openDetails(row) {
        // console.log('row:', row);
        swal({
            html: `<table class="customers">
  <tr>
    <th>Property</th>
    <th>Value</th>
  </tr>
  <tr>
    <td>First Name</td>
    <td>` + row.firstName + `<td>
  </tr>
  <tr>
    <td>Last Name</td>
    <td>` + row.lastName + `</td>
  </tr>
  <tr>
    <td>Phone Number</td>
    <td>` + row.phone + `</td>
  </tr>
  <tr>
    <td>Role</td>
    <td>` + row.userType + `</td>
  </tr>
  <tr>
    <td>Created Date</td>
    <td>` + row.createdDate + `</td>
  </tr>
  </table>`,
            customClass: 'table'
        })
    }

    editContent(row) {
        this.previewService.editUser(row);
        this.router.navigate(['/poweruser/edit'])
    }
}
