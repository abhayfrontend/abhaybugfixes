import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { QuillEditorComponent } from 'ngx-quill/src/quill-editor.component';
import { PreviewService } from '../shared/auth/preview.service';
import { UserResourceService } from '../sthaapak';
import { FormGroup, FormControl, ReactiveFormsModule, FormsModule, Validators } from '@angular/forms';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import swal from 'sweetalert2';
import * as AWS from '../shared/configs/aws.config';
import { base, proxyApi } from '../shared/configs/util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-poweruser)',
  templateUrl: './poweruser-add.component.html',
  styleUrls: ['./poweruser-add.component.scss']
})

export class PoweruserAddComponent implements OnInit, AfterViewInit, OnDestroy {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };
  emailMessage;
  myform: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  email: FormControl;
  phone: FormControl;
  group: FormControl;
  userType: Number;
  location: FormControl;
  indusEl;
  password;
  message = `Hello, Welcome to Productivise. Click on the link below to get the App.` +
    proxyApi + `/create/com.sthaapak.productivise/` + this.previewService.organizationId.getValue()
  constructor(private previewService: PreviewService, private userService: UserResourceService, private http: HttpClient,
    private roleGuardService: RoleGuardService, private router: Router) {
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }


  ngAfterViewInit() {
    let usertoEdit = <any>this.previewService.usertoEdit.getValue();
    // console.log('userEdit', usertoEdit)
    if (usertoEdit.id) {

      this.indusEl = <HTMLInputElement>document.getElementById('name');
      /*
      // console.log('userEditName', this.indusEl)
      this.indusEl.value = usertoEdit.firstName;
      // console.log('userEditName', this.indusEl.value)

      this.indusEl.innerText = usertoEdit.firstName;
      */
      this.myform.controls['firstName'].setValue(usertoEdit.firstName);
      this.myform.controls['lastName'].setValue(usertoEdit.lastName);
      this.myform.controls['email'].setValue(usertoEdit.email);
      this.myform.controls['phone'].setValue(usertoEdit.phone);
      this.myform.controls['location'].setValue(usertoEdit.location);
    }
  }

  createFormControls() {
    this.firstName = new FormControl('', Validators.required);
    this.lastName = new FormControl('', Validators.required);
    this.email = new FormControl('', [Validators.required, Validators.email]);
    this.location = new FormControl('', Validators.required);
    this.phone = new FormControl('', Validators.required);
    this.group = new FormControl('', Validators.required);

  }

  createForm() {
    this.myform = new FormGroup({
      firstName: this.firstName,
      phone: this.phone,
      group: this.group,
      lastName: this.lastName,
      email: this.email,
      location: this.location
    });
  }


  ngOnInit() {
    this.password = Math.random().toString(36).slice(2, 8);
    this.emailMessage = `Hello, Welcome to Productivise. Click on the link below to get the App.` + proxyApi
      + `/create/com.sthaapak.productivise/` + this.previewService.organizationId.getValue() + `\n Your password is: ` + this.password;

    this.createFormControls();
    this.createForm();
  }

  onSubmit() {

    if (this.myform.controls['firstName'].value && this.myform.controls['firstName'].value.trim() === "") {
      this.myform.controls['firstName'].setValue('');
    }
    if (this.myform.controls['lastName'].value && this.myform.controls['lastName'].value.trim() === "") {
      this.myform.controls['lastName'].setValue('');
    }
    if (!this.myform.valid) {
      // console.log('Form', this.myform.status);
      this.myform.controls['firstName'].markAsTouched();
      this.myform.controls['lastName'].markAsTouched();
      this.myform.controls['email'].markAsTouched();
      this.myform.controls['phone'].markAsTouched();
      this.myform.controls['location'].markAsTouched();
      this.myform.controls['group'].markAsTouched();
      // this.myform.controls['indus'].markAsTouched();
      // this.myform.controls['department'].markAsTouched();
      // this.myform.controls['jobRole'].markAsTouched();
      return;
    }
    let DTO = Object.assign({}, this.myform.value, {
      activated: true,
      orgId: this.previewService.organizationId.getValue(),
      group: null, userType: this.group.value, login: this.email.value, userPassword: this.password, langKey: null,
      imageUrl: null
    })
    DTO = JSON.parse(JSON.stringify(DTO));
    // console.log('DTO:', DTO);
    let element;
    switch (this.group.value) {
      case 1:
        element = 'Content Manager';
        break;
      case 2:
        element = 'Administrator';
        break;
      case 3:
        element = 'HR';
        break;
      case 4:
        element = 'Campaign Manager';
        break;
      case 6:
        element = 'Super User';
        break;
    }
    swal({
      title: 'Are you sure you want to creat this Power-User?',
      html: `<div><span><b>First Name:</b> ` + this.firstName.value + `</span><br/>` + `<span><b>Last Name: </b> ` + this.lastName.value + `</span > <br/>`
        + `<span><b>Email: </b> ` + this.email.value + `</span > <br/>` +
        `<span><b>Phone Number:</b>` + this.phone.value + `</span><br/>` + `<span><b>Role:</b>` + element + `</span><br/>` +
        `<span><b>Location:</b>` + this.location.value + `</span></div>`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Yes, creat it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then((confirm) => {
      // console.log('con:', confirm)
      if (confirm.value === true) {
        this.http.post(base + '/api/users/register', DTO, this.httpOptions).subscribe(res => {
          // console.log('response to post:', res);
          let result = <any>res;
          // console.log('memberData:', result);
          let memberData = {
            'name': result['firstName'] + (result['lastName'] || ''),
            'userId': result['id'],
            'learnerId': result['learner']['id'],
            'category': this.previewService.organizationId.getValue(),
            'subCategory': null,
            'locationName': result['location'],
            'gehash1': result['geoHash'],
            'memberType': 'APPLICANT',
            'aadhaar': result['phone'] || null,
            'mobileNo': result['phone'] || this.phone.value,
            'cardId': null
          };
          // console.log('memberData:', memberData);
          this.http.post(base + '/api/members', memberData, this.httpOptions).subscribe(response => {
            // console.log(response);
            let memberResponse = <any>response;
            // console.log('member:', memberResponse);
            result.learnerId = result['learner']['id'];
            result.langKey = memberResponse.id;
            result.userPassword = this.password
            result.id = result.id;
            result.authorities = ['ROLE_ADMIN', 'ROLE_USER'];
            let learnerObj = Object.assign({}, result.learner);
            learnerObj.tags = Number(this.previewService.organizationId.getValue()) === 261351 ? 261701 : 267451;
            learnerObj.city = result['location'];
            this.http.put(base + '/api/users', result, this.httpOptions).subscribe(respo => {
              // console.log('user:', respo);
              this.http.put(base + '/api/learners', learnerObj, this.httpOptions).subscribe(learnerresp => {
                // console.log('learner:', learnerresp);
                this.sendEmail('Welcome To Productivise', this.emailMessage, result['email']);
                this.myform.reset();
                swal('User Created Successfully.', '', 'success');
              })
              this.http.get(base + '/api/campaign-groups', this.httpOptions).subscribe(groupResp => {
                let groups = <any>groupResp;
                groups.forEach(element => {
                  if (element.name === 'Content-Managers' && element.orgId === this.previewService.organizationId.getValue() && this.group.value === 1) {
                    element.learners.push(result.learner);
                    this.http.put(base + '/api/campaign-groups', element, this.httpOptions).subscribe(putGroup => {
                      // console.log('putResp:', putGroup);
                    })
                  }
                  if (element.name === 'Administrators' && element.orgId === this.previewService.organizationId.getValue() && this.group.value === 2) {
                    element.learners.push(result.learner);
                    this.http.put(base + '/api/campaign-groups', element, this.httpOptions).subscribe(putGroup => {
                      // console.log('putResp:', putGroup);
                    })
                  }
                  if (element.name === 'HR Managers' && element.orgId === this.previewService.organizationId.getValue() && this.group.value === 3) {
                    element.learners.push(result.learner);
                    this.http.put(base + '/api/campaign-groups', element, this.httpOptions).subscribe(putGroup => {
                      // console.log('putResp:', putGroup);
                    })
                  }

                  if (element.name === 'Campaign-Managers' && element.orgId === this.previewService.organizationId.getValue() && this.group.value === 4) {
                    element.learners.push(result.learner);
                    this.http.put(base + '/api/campaign-groups', element, this.httpOptions).subscribe(putGroup => {
                      // console.log('putResp:', putGroup);
                    })
                  }

                  if (element.name === 'Super-Users' && element.orgId === this.previewService.organizationId.getValue() && this.group.value === 6) {
                    element.learners.push(result.learner);
                    this.http.put(base + '/api/campaign-groups', element, this.httpOptions).subscribe(putGroup => {
                      // console.log('putResp:', putGroup);
                    })
                  }
                });
              });
            });
            this.http.get(base + '/api/support-groups', this.httpOptions).subscribe(groupResp => {
              let groups = <any>groupResp;
              groups.forEach(element => {
                if (element.name === 'Content-Managers' && element.orgId === this.previewService.organizationId.getValue() && this.group.value === 1) {
                  element.members.push(memberResponse);
                  this.http.put(base + '/api/support-groups', element, this.httpOptions).subscribe(putGroup => {
                    // console.log('putResp:', putGroup);
                  })
                }
                if (element.name === 'Administrators' && element.orgId === this.previewService.organizationId.getValue() && this.group.value === 2) {
                  element.members.push(memberResponse);
                  this.http.put(base + '/api/support-groups', element, this.httpOptions).subscribe(putGroup => {
                    // console.log('putResp:', putGroup);
                  })
                }
                if (element.name === 'HR Managers' && element.orgId === this.previewService.organizationId.getValue() && this.group.value === 3) {
                  element.members.push(memberResponse);
                  this.http.put(base + '/api/support-groups', element, this.httpOptions).subscribe(putGroup => {
                    // console.log('putResp:', putGroup);
                  })
                }
                if (element.name === 'Campaign-Managers' && element.orgId === this.previewService.organizationId.getValue() && this.group.value === 4) {
                  element.members.push(memberResponse);
                  this.http.put(base + '/api/support-groups', element, this.httpOptions).subscribe(putGroup => {
                    // console.log('putResp:', putGroup);
                  })
                }
                if (element.name === 'Super-Users' && element.orgId === this.previewService.organizationId.getValue() && this.group.value === 6) {
                  element.members.push(memberResponse);
                  this.http.put(base + '/api/support-groups', element, this.httpOptions).subscribe(putGroup => {
                    // console.log('putResp:', putGroup);
                  })
                }
              });
            });

          }, err => {
            swal(
              'User Exists',
              'Your User was not created.',
              'error'
            )
          });
          // console.log('Form Submitted!');
          // console.log(DTO);



        }, err => {
          swal(
            'User Exists',
            'Your User was not created.',
            'error'
          )
        });
      }
      else {
        swal(
          'Cancelled',
          'Your Power-User was not created.',
          'error'
        )
      }
    }, (dismiss) => {
      // console.log('dismis:', dismiss)
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Your Power-User was not created.',
          'error'
        )
      }
    })



  }

  sendSMS() {

    // console.log('Message Sent!!')
    // console.log('contacts:', this.phone.value);
    const sns = AWS.sns;
    sns.publish({
      Message: this.message, PhoneNumber: '+91' + this.phone.value, MessageAttributes: {
        'AWS.SNS.SMS.SenderID': {
          DataType: 'String',
          StringValue: 'Sthaapak'
        },
        'AWS.SNS.SMS.MaxPrice': {
          DataType: 'Number',
          StringValue: '00.01'
        },
        'AWS.SNS.SMS.SMSType': {
          DataType: 'String',
          StringValue: 'Transactional'
        }
      }
    }).promise().then(res => {
      // console.log(res);

    });

  }

  ngOnDestroy() {
    this.previewService.editUser({});

  }

  sendEmail(subject, message, email) {
    // console.log('message:', message)
    return this.http.post(proxyApi + '/sendMail', {
      message: message,
      email: email,
      subject: subject
    }).toPromise().then(mailRes => {
      let result = <any>mailRes;
      // this.sendSMS(this.message, user.phone)
    })
  }

}
