import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PoweruserAddComponent } from './poweruser-add.component';
import { PoweruserListComponent } from './poweruser-list.component';


const routes: Routes = [
  {
    path: '',
    children: [

      {
        path: 'add',
        component: PoweruserAddComponent,
        data: {
          title: 'Add new Power User '
        }
      },
      {
        path: 'list',
        component: PoweruserListComponent,
        data: {
          title: 'List Power User'
        }
      },
      {
        path: 'edit',
        component: PoweruserAddComponent,
        data: {
          title: 'Edit a Power User'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PoweruserRoutingModule { }
