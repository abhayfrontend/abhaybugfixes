import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CustomFormsModule } from 'ng2-validation';
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { PoweruserAddComponent } from './poweruser-add.component';
import { PoweruserListComponent } from './poweruser-list.component';
import { PoweruserRoutingModule } from "./poweruser-routing.module";
import { UserGuardService } from '../shared/auth/userguard.service';



@NgModule({
    imports: [
        CommonModule,

        ReactiveFormsModule,
        FormsModule,
        PoweruserRoutingModule,

        CustomFormsModule,
        MatchHeightModule,
        NgbModule,
        NgxDatatableModule
    ],
    declarations: [

        PoweruserAddComponent,
        PoweruserListComponent],
    providers: [UserGuardService]


})
export class PoweruserModule { }
