import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CustomFormsModule } from 'ng2-validation';
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { AnalyticsComponent } from './analytics.component';
import { AnalyticsRoutingModule } from './analytics-routing.module';
import { DxCheckBoxModule, DxPivotGridModule } from 'devextreme-angular';



@NgModule({
	imports: [
		CommonModule,
		ReactiveFormsModule,
		FormsModule,
		AnalyticsRoutingModule,
		CustomFormsModule,
		MatchHeightModule,
		NgbModule,
		NgxDatatableModule,
		DxPivotGridModule,
		DxCheckBoxModule
	],
	declarations: [
		AnalyticsComponent
	]


})
export class AnalyticsModule { }
