import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnalyticsComponent } from './analytics.component';

const routes: Routes = [
	{
		path: '',
		children: [

			{
				path: 'time',
				component: AnalyticsComponent,
				data: {
					title: 'Time Spent By Users'
				}
			}, {
				path: 'events',
				component: AnalyticsComponent,
				data: {
					title: 'Events Completed by Users'
				}
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class AnalyticsRoutingModule { }
