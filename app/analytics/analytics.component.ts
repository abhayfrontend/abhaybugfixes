import { Component, OnInit, Inject } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { DxPivotGridModule, DxCheckBoxModule } from 'devextreme-angular';
import { Service, Sale } from '../shared/auth/app.service';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../shared/auth/role-guard.service';
import { PreviewService } from '../shared/auth/preview.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { base, proxyApi } from '../shared/configs/util';
import PivotGridDataSource from 'devextreme/ui/pivot_grid/data_source';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss', 'dx.common.css', 'dx.light.css'],
  providers: [
    { provide: 'Window', useValue: window }, Service
  ],
  preserveWhitespaces: true
})
export class AnalyticsComponent implements OnInit {
  result;
  pivotGridDataSource: any;
  showDataFields: boolean = true;
  showRowFields: boolean = true;
  showColumnFields: boolean = true;
  showFilterFields: boolean = true;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };
  ngOnInit() {

  }


  constructor(service: Service, private http: HttpClient, private roleGuardService: RoleGuardService, private previewService: PreviewService,
    private router: Router, @Inject('Window') private window: Window) {
    this.getAppTimeSpentEvents();
  }

  getAppTimeSpentEvents() {
    this.http.post(proxyApi + '/esearch', {
      "query": {
        "bool": {
          "must": [
            {
              "match": {
                "orgID": this.previewService.organizationId.getValue()
              }
            },
            {
              "match": {
                "name": "AppTimeSpent"
              }
            },
            {
              "range": {
                "date": {
                  "gte": "now-1w/w",
                  "lte": "now"
                }
              }
            }

          ]
        }
      },
      "aggs": {

        "group_by_state": {
          "terms": {
            "field": "userID",
            "size": 500

          },
          "aggs": {
            "TIMESPENT": {
              "date_histogram": {
                "field": "date",
                "interval": "1d",
                "format": "yyyy-MM-dd"
              },
              "aggs": {
                "time": {
                  "scripted_metric": {
                    "init_script": "_agg['transactions'] = [];_agg['userData']=[]",
                    "map_script": "if(doc['strValue'].value != \"\" && doc['category'].value != \"\" && doc['category'].value != 0){_agg.transactions.add(Long.valueOf(doc['strValue'].value) - Long.valueOf(doc['category'].value)); _agg['userData'].add(doc['category'].value)}",
                    "combine_script": "profit = 0; for (t in _agg.transactions) { profit = Long.valueOf(profit) + Long.valueOf(t) }; return profit",
                    "reduce_script": "profit = [];sum=0; for (a in _aggs) {if(a != null) profit.add(a) };for(int i:profit) {sum += i;}; return sum/60000;"

                  }
                }
              }

            },
            "hits": {
              "top_hits": {
                "_source": {
                  "includes": ["date", "name4", "name5", "category", "strValue"]
                },
                "size": 1
              }
            }

          }

        }

      }
    }
      , this.httpOptions).toPromise().then(res => {
        let result = <any>res;


        this.result = result.data.aggregations['group_by_state'].buckets;

        // console.log('result:', result);
        let ans = [];
        this.result = this.result.forEach(e => {

          e.TIMESPENT.buckets.forEach(element => {
            ans.push({
              user: e.key, time: Math.floor(element.time.value), date: element.key_as_string, yesterday: (new Date(element.key_as_string).getDate() ===
                new Date().getDate() - 1 ? Math.floor(element.time.value) : 0), today: (new Date(element.key_as_string).getDate() ===
                  new Date().getDate() ? Math.floor(element.time.value) : 0),
              role: e.hits.hits.hits[0]._source.name4, location: e.hits.hits.hits[0]._source.name5
            });
          });

        })
        // console.log(ans);
        this.pivotGridDataSource = new PivotGridDataSource({
          fields: [{
            caption: "User",
            width: 120,
            dataField: "user",
            area: "row",
            selector: function (data) {
              return data.user;
            }
          }, {
            caption: "Location",
            dataField: "location",
            width: 150,
            area: "row"
          }, {
            caption: "Role",
            dataField: "role",
            width: 150,
            area: "row"
          },

          // {
          //   dataField: "date",
          //   dataType: "date",
          //   area: "column"
          // },
          {
            dataField: "time",
            dataType: "number",
            format: function (value) {
              // ...
              return value + ' minutes';
            },
            summaryType: "sum",
            area: "data"
          }, {

            dataField: "today",
            width: 150, format: function (value) {
              // ...
              return value + ' minutes';
            },
            summaryType: "sum",
            area: "data"
          }
            ,
          {

            dataField: "yesterday", format: function (value) {
              // ...
              return value + ' minutes';
            },
            summaryType: "sum",
            width: 150,
            area: "data"
          }],
          store: ans
        });

      })
  }


  contextMenuPreparing(e) {
    var dataSource = e.component.getDataSource(),
      sourceField = e.field;

    if (sourceField) {
      if (!sourceField.groupName || sourceField.groupIndex === 0) {
        e.items.push({
          text: "Hide field",
          onItemClick: function () {
            var fieldIndex;
            if (sourceField.groupName) {
              fieldIndex = dataSource.getAreaFields(sourceField.area, true)[sourceField.areaIndex].index;
            } else {
              fieldIndex = sourceField.index;
            }

            dataSource.field(fieldIndex, {
              area: null
            });
            dataSource.load();
          }
        });
      }

      if (sourceField.dataType === "number") {
        var setSummaryType = function (args) {
          dataSource.field(sourceField.index, {
            summaryType: args.itemData.value
          });

          dataSource.load();
        },
          menuItems = [];

        e.items.push({ text: "Summary Type", items: menuItems });

        for (let summaryType of ["Sum", "Avg", "Min", "Max"]) {
          var summaryTypeValue = summaryType.toLowerCase();

          menuItems.push({
            text: summaryType,
            value: summaryType.toLowerCase(),
            onItemClick: setSummaryType,
            selected: e.field.summaryType === summaryTypeValue
          });
        };
      }
    }
  }



  generatePdf() {
    // console.log()
    const div = document.getElementById("content");
    // console.log(div.clientHeight, div.clientWidth);
    const options = { background: "white", height: div.clientHeight, width: div.clientWidth };

    html2canvas(div, options).then((canvas) => {
      //Initialize JSPDF
      let doc = new jsPDF("l", "px", "a4");
      //Converting canvas to Image
      let imgData = canvas.toDataURL("image/PNG");
      //Add image Canvas to PDF
      doc.addImage(imgData, 'PNG', 30, 0);

      let pdfOutput = doc.output();
      // using ArrayBuffer will allow you to put image inside PDF
      let buffer = new ArrayBuffer(pdfOutput.length);
      let array = new Uint8Array(buffer);
      for (let i = 0; i < pdfOutput.length; i++) {
        array[i] = pdfOutput.charCodeAt(i);
      }

      //Name of pdf
      const fileName = "example.pdf";

      // Make file
      doc.save(fileName);

    });
  }

}
