import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.css']
})
export class UploadFileComponent implements OnInit {

  @Output() enableNextButton = new EventEmitter;

  constructor() { }

  ngOnInit() {
  }

  enableNext(e) {
    // console.log('e:', e);
    this.enableNextButton.emit(e);
  }

}
