import { Component, OnInit } from '@angular/core';
import * as AWS from '../../../shared/configs/aws.config';
import { PreviewService } from '../../../shared/auth/preview.service';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../../shared/auth/role-guard.service';
import * as moment from 'moment';
import { base } from '../../../shared/configs/util';
import { IndustryResourceService } from '../../../sthaapak';
@Component({
  selector: 'app-after-upload',
  templateUrl: './after-upload.component.html',
  styleUrls: ['./after-upload.component.css']
})
export class AfterUploadComponent implements OnInit {
  message;
  contacts = [];
  sent = 0;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };
  constructor(public previewService: PreviewService, private router: Router, private http: HttpClient,
    private roleGuardService: RoleGuardService, private industry: IndustryResourceService) {
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnInit() {
    this.contacts = this.previewService.SmsList.getValue();
  }

  EnableSend(e) {
    this.message = e;
  }


  saveIndustry() {
    for (let i = 0; i < this.contacts.length; i++) {
      this.industry.createIndustryUsingPOST({
        "name": this.contacts[i][0],
        "tags": null,
        "description": null,
        "url": null,
        "images": null,
        "imageLink": this.contacts[i][1],
        "videoLink": this.contacts[i][2],
        "pdfLink": this.contacts[i][3],
        "audioLink": this.contacts[i][4],
        "content": this.contacts[i][5],
        "displayOrder": this.contacts[i][6],
      }).subscribe(ind => {
        // console.log(ind)
      })

    }
  }
}

