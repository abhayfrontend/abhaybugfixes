import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadIncorrectFileComponent } from './download-incorrect-file.component';

describe('DownloadIncorrectFileComponent', () => {
  let component: DownloadIncorrectFileComponent;
  let fixture: ComponentFixture<DownloadIncorrectFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadIncorrectFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadIncorrectFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
