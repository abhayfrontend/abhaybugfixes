import { Component, OnInit, Output } from '@angular/core';
import * as AWS from '../../../shared/configs/aws.config';
import { EventEmitter } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../../shared/auth/role-guard.service';
import { Router } from '@angular/router';
import { PreviewService } from '../../../shared/auth/preview.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-upload-csv-btn',
  templateUrl: './upload-csv-btn.component.html',
  styleUrls: ['./upload-csv-btn.component.css']
})
export class UploadCsvBtnComponent implements OnInit {

  // Variables
  csv;
  incorrectData = [];
  list;
  contacts;
  emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  validFileExtensions = ['csv'];
  userListFile = '';
  @Output() fileUpload = new EventEmitter;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };
  // End Variables
  constructor(public http: HttpClient, private roleGuardService: RoleGuardService,
    private previewService: PreviewService, private router: Router) { }

  ngOnInit() {
  }
  changeName() {

  }
  fileChanged(event) {
    let timerInterval;
    let _URL = window.URL;

    if (event.target.files && event.target.files[0]) {

      let sFileName = event.target.files[0].name;
      // console.log('sfilename: ', sFileName);
      let blnValid = false;
      for (let j = 0; j < this.validFileExtensions.length; j++) {
        let sCurExtension = this.validFileExtensions[j];
        if (sFileName.slice(sFileName.length - sCurExtension.length, sFileName.length).toLowerCase() === sCurExtension.toLowerCase()) {
          blnValid = true;
          break;
        }
      }
      if (!blnValid) {
        alert('Sorry, ' + sFileName + ' is invalid, allowed extensions are: ' + this.validFileExtensions.join(','));
        return false;
      }
      // console.log('size:', event.target.files[0].size);
      if ((event.target.files[0].size < 100) || (event.target.files[0].size > 2000000)) {
        alert('Sorry, ' + sFileName + ' is invalid, allowed file size is 5Kb to 2 Mb');
        return false;
      }



      let file = event.target.files[0];


      let upload = AWS.s3.upload({ Key: this.previewService.organizationId.getValue() + '/industry/csv/' + new Date() + file.name, Bucket: AWS.bucketName, Body: file, ACL: 'public-read' });
      let promise = upload.promise();
      swal({
        title: 'Uploading!',
        onOpen: () => {
          swal.showLoading();
          promise.then(data => {
            // console.log(data);
            this.userListFile = data.Location
          });
          timerInterval = setInterval(() => {
            if (this.userListFile) {
              this.http.get(this.userListFile, {
                responseType: 'text'
              }).subscribe(csvData => {
                this.csv = csvData;
                this.list = this.CSVToArray(this.csv, ',');
                // console.log('csvData:', this.list);
                this.contacts = this.list.slice(1, this.list.length - 1);
                this.contacts.forEach((element, i) => {
                  if (!element[0]) {
                    this.incorrectData.push(element);
                    this.contacts.splice(i, 1);
                    // console.log('incorrect:', element)
                  }

                });
                if (this.contacts.length < this.list.length - 2) {

                  swal({
                    title: 'Invalid Records',
                    text: 'The CSV file has incorrect records. Only the Correct records will be uploaded. Do you want to proceed ?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#0CC27E',
                    cancelButtonColor: '#FF586B',
                    confirmButtonText: 'Yes, Proceed!',
                    cancelButtonText: 'No, Quit!',
                    confirmButtonClass: 'btn btn-success btn-raised mr-5',
                    cancelButtonClass: 'btn btn-danger btn-raised',
                    buttonsStyling: false
                  }).then((confirm) => {
                    // console.log('con:', confirm)
                    if (confirm.value === true) {
                      // console.log('finalConta:', this.contacts)
                      this.fileUpload.emit(this.contacts);
                      swal.close();
                      clearInterval(timerInterval);
                      event.target.files = null;
                      file = null;
                    }
                    else {
                      swal(
                        'Cancelled',
                        'The user records were not uploaded.',
                        'error'
                      )
                      event.target.files = null;
                      file = null;

                    }
                  }, (dismiss) => {
                    // console.log('dismis:', dismiss)
                    event.target.files = null;
                    file = null;

                    // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                    if (dismiss === 'cancel') {
                      swal(
                        'Cancelled',
                        'The user records were not uploaded.',
                        'error'
                      )
                      file = null;
                    }
                  })
                }
                else {
                  this.fileUpload.emit(this.contacts);
                  swal.close();
                  clearInterval(timerInterval);
                  event.target.files = null;
                  file = null;

                }
              });
              clearInterval(timerInterval);

            }
          }, 500)
        },
        onClose: () => {
          clearInterval(timerInterval)
        }
      });

    }

  }



  CSVToArray(strData, strDelimiter) {
    // Check to see if the delimiter is defined. If not,
    // then default to comma.
    strDelimiter = (strDelimiter || ',');
    // Create a regular expression to parse the CSV values.
    var objPattern = new RegExp((
      // Delimiters.
      '(\\' + strDelimiter + '|\\r?\\n|\\r|^)' +
      // Quoted fields.
      '(?:\'([^\']*(?:\'\'[^\']*)*)\'|' +
      // Standard fields.
      '([^\'\\' + strDelimiter + '\\r\\n]*))'), 'gi');
    // Create an array to hold our data. Give the array
    // a default empty first row.
    var arrData = [[]];
    // Create an array to hold our individual pattern
    // matching groups.
    var arrMatches = null;
    // Keep looping over the regular expression matches
    // until we can no longer find a match.
    while (arrMatches = objPattern.exec(strData)) {
      // Get the delimiter that was found.
      var strMatchedDelimiter = arrMatches[1];
      // Check to see if the given delimiter has a length
      // (is not the start of string) and if it matches
      // field delimiter. If id does not, then we know
      // that this delimiter is a row delimiter.
      if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {
        // Since we have reached a new row of data,
        // add an empty row to our data array.
        arrData.push([]);
      }
      // Now that we have our delimiter out of the way,
      // let's check to see which kind of value we
      // captured (quoted or unquoted).
      if (arrMatches[2]) {
        // We found a quoted value. When we capture
        // this value, unescape any double quotes.
        var strMatchedValue = arrMatches[2].replace(
          new RegExp('\'\'', 'g'), '\'');
      } else {
        // We found a non-quoted value.
        var strMatchedValue = arrMatches[3];
      }
      // Now that we have our value string, let's add
      // it to the data array.
      arrData[arrData.length - 1].push(strMatchedValue);
    }
    // Return the parsed data.
    return (arrData);
  }

}
