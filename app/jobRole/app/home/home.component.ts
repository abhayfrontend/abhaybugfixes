import { Component, OnInit, Input } from '@angular/core';
import { PreviewService } from '../../../shared/auth/preview.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { RoleGuardService } from '../../../shared/auth/role-guard.service';
import * as moment from 'moment';
import { base } from '../../../shared/configs/util';
import { IndustryResourceService, JobFamilyResourceService, JobRoleResourceService } from '../../../sthaapak';
import swal from 'sweetalert2';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // Variables
  message;
  file: boolean = true;
  created;
  breadCrumb;
  // End Variables
  contacts = [];
  organization;
  sent = 0;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };
  constructor(public previewService: PreviewService, private router: Router, private http: HttpClient,
    private roleGuardService: RoleGuardService, private roles: JobRoleResourceService) {
    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnInit() {
    this.contacts = this.previewService.SmsList.getValue();
    this.http.get(base + '/api/organizations/' + this.previewService.organizationId.getValue(), this.httpOptions).subscribe(org => {
      this.organization = <any>org;
    })
  }

  EnableSend(e) {
    this.message = e;
  }


  saveIndustry() {
    let error = [];
    let not = [];
    this.created = 0;
    swal({
      title: 'Are you sure you want to create these roles?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Yes, create it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then((confirm) => {
      if (confirm.value) {
        for (let i = 0; i < this.contacts.length; i++) {
          this.roles.createJobRoleUsingPOST({
            "name": this.contacts[i][0],
            "tags": this.contacts[i][6],
            "description": this.contacts[i][1],
            "url": null,
            "image": this.contacts[i][2],
            "minimumExperince": this.contacts[i][3],
            "maximumExperince": this.contacts[i][4],
            "level": this.contacts[i][5],
            "minimumEducation": this.contacts[i][6],
            "videoLink": this.contacts[i][7],
            "pdfLink": this.contacts[i][8],
            "audioLink": this.contacts[i][9],
            "content": this.contacts[i][10],
            "trainingDuration": this.contacts[i][11],
            "displayOrder": this.contacts[i][12],
            "extraData": [],
            "careerPaths": [],
            "skills": [],
            "additionalFiles": [],
            "multiLinguals": [],
            "organizations": [this.organization]
          }).subscribe(ind => {

            // console.log('department:', ind);
            if (ind.id) {
              this.created += 1;
            }
            if (i === this.contacts.length - 1 && !error.length) {
              swal('Successfully Created ' + this.created + ' Job Roles.', '', 'success');
            }

            if (i === this.contacts.length - 1 && error.length > 1) {
              swal('Successfully Created ' + this.created + ' Job Roles.', 'Some Job Roles Could Not be Created. Failure on rows:' + error, 'success');
            }
            if (i === this.contacts.length - 1 && error.length === 1) {
              swal('Successfully Created ' + this.created + ' Job Roles.', 'One Job Role Could Not be Created. Failure on row:' + error, 'success');
            }

          }, err => {
            // console.log(err);
            error.push(i + 2);
            if (i === this.contacts.length - 1 && error.length > 1) {
              swal('Successfully Created ' + this.created + ' Job Roles.', ' Some Job Roles Could Not be Created. Failure on rows:' + error, 'error');
            }
            if (i === this.contacts.length - 1 && error.length === 1) {
              swal('Successfully Created ' + this.created + ' Job Roles.', ' One Job Role Could Not be Created. Failure on row:' + error, 'error');
            }

          })

        }
      }
      else {
        swal('No Job Roles were Created.', '', 'error');
      }

    });
  }
  enableNext(e) {
    this.file = e.length > 0 ? false : true;
    this.previewService.editSmsList(e);
    // console.log('e:', e);
    this.contacts = this.previewService.SmsList.getValue();
  }



}
