import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManuallyAddBtnComponent } from './manually-add-btn.component';

describe('ManuallyAddBtnComponent', () => {
  let component: ManuallyAddBtnComponent;
  let fixture: ComponentFixture<ManuallyAddBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManuallyAddBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManuallyAddBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
