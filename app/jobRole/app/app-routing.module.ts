import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AfterUploadComponent } from './after-upload/after-upload.component';


const routes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'upload',
				component: HomeComponent
			},
			{
				path: 'afterUpload',
				component: AfterUploadComponent
			}
		]
	}
];


@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class OrganizationRoutingModule { }
