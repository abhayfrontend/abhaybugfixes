import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobRoleAddComponent } from './jobRole-add.component';
import { JobRoleListComponent } from './jobRole-list.component';
import { HomeComponent } from './app/home/home.component';
import { AfterUploadComponent } from './app/after-upload/after-upload.component';


const routes: Routes = [
  {
    path: '',
    children: [

      {
        path: 'add',
        component: JobRoleAddComponent,
        data: {
          title: 'Add new JobRole '
        }
      }, {
        path: 'list',
        component: JobRoleListComponent,
        data: {
          title: 'List JobRole'
        }
      },
      {
        path: 'bulk',
        component: HomeComponent,
        data: {
          title: 'Bulk Register JobRole'
        }
      },
      {

        path: 'afterUpload',
        component: AfterUploadComponent

      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobRoleRoutingModule { }
