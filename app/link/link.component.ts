import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { base } from '../shared/configs/util';

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss']
})
export class LinkComponent implements OnInit {

  id;
  referrer;
  constructor(private route: ActivatedRoute, private externalRoute: Router) {
    this.route.params.subscribe(params => {
      // console.log(params);
      this.id = params.id;
      this.referrer = params.referrer;
      if (params.lid) {
        this.referrer = params.referrer + '&lid=' + params.lid;
      }
    });

  }


  ngOnInit() {


    this.route.params.subscribe(params => {
      // console.log(params);
      this.id = params.id;
      this.referrer = params.referrer;
    });
    //this.externalRoute.navigateByUrl('https://play.google.com/store/apps/details?id=com.sthaapak.yuva&referrer=' + this.id);
    window.location.href = 'https://play.google.com/store/apps/details?id=' + this.id + '&referrer=' + this.referrer;

  }



}
