import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import { SchedulerComponent } from './scheduler/scheduler.component';
import { SchedulerRoutingModule } from './scheduler-routing.module';
import { CronEditorModule } from 'cron-editor/cron-editor';
@NgModule({
  imports: [
    CommonModule,
    SchedulerRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    CustomFormsModule,
    MatchHeightModule,
    NgbModule,
    NgxDatatableModule,
    CronEditorModule
  ],
  declarations: [SchedulerComponent]
})
export class SchedulerModule { }
