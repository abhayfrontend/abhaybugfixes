import { Component, OnInit, OnDestroy } from '@angular/core';
import { RoleGuardService } from '../../shared/auth/role-guard.service';
import { PublishedContentResourceService, CampaignGroupResourceService, DeviceResourceService } from '../../sthaapak';
import { PreviewService } from '../../shared/auth/preview.service';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { CronOptions } from 'cron-editor/cron-editor';
import swal from 'sweetalert2';
import { base } from '../../shared/configs/util';
import { proxyApi } from '../../shared/configs/util';

import { Router } from '@angular/router';

@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.scss']
})
export class SchedulerComponent implements OnInit, OnDestroy {


  scheduleDate: any = "";
  public cronOptions: CronOptions = {
    formInputClass: 'form-control cron-editor-input',
    formSelectClass: 'form-control cron-editor-select',
    formRadioClass: 'cron-editor-radio',
    formCheckboxClass: 'cron-editor-checkbox',

    defaultTime: "10:00:00",

    hideMinutesTab: false,
    hideHourlyTab: false,
    hideDailyTab: false,
    hideWeeklyTab: false,
    hideMonthlyTab: false,
    hideYearlyTab: false,
    hideAdvancedTab: true,
    use24HourTime: true,
    hideSeconds: false
  };

  /* variables */
  obj;
  learnerIds = [];
  screen;
  groups = [];
  url;
  imageName;
  title;
  messageContent;
  thumbnailImage = null;
  groupsSelected = [];
  learners = [];
  tokens = [];
  cronExpression;
  groupIds = [];
  notificationCronExpression;
  Expiries = ['12 Hours', '1 Day', '3 Days', '1 Week', '2 Weeks', '1 Month', '3 Months', '6 Months', '1 Year', 'Never']
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.roleGuardService.token
    })
  };
  /* end variables */


  groupSelection: any = {
    that: this,
    everyGroup: true,
    particularGroup: false,
    everyGroupClicked: function () {
      this.that.learners = [];
      this.everyGroup = true;
      this.particularGroup = false;
      this.that.http.get(base + '/api/users?size=10000', this.that.httpOptions).subscribe(res => {
        let result = <any>res;
        // console.log('userResou:', result);
        result.forEach((element, i) => {
          if (element.orgId === this.that.previewService.organizationId.getValue()) {
            this.that.http.get(base + '/api/learners/' + element.learnerId, this.that.httpOptions).subscribe(response => {
              let learnerObject = <any>response;
              // console.log('lobj:', learnerObject);
              this.that.learners.push(learnerObject);
            });
          }
          if (i === result.length - 1) {
            this.that.getTokens(this.that.learners);
          }
        });


      })
    },
    particularGroupClicked: function () {
      this.that.learners = [];
      this.particularGroup = true;
      this.everyGroup = false;
    },
    searchVisible: false,
    selectedGroups: {
    },
    toggleListSelection: function (string) {
      this.searchVisible = !this.searchVisible;
      this.that.learners = [];
      this.that.groupsSelected = [];
      this.selectedGroups[string] = !this.selectedGroups[string];
      let groups = Object.keys(this.selectedGroups);
      // console.log('groupskeys:', groups);
      // console.log('groupsksele:', this.selectedGroups);
      groups.forEach(e => {
        // console.log('g1:', this.selectedGroups[e]);
        if (this.selectedGroups[e] === true) {
          this.that.groupsSelected.push(e)
          // console.log('groupselectd:', this.that.groupsSelected);
          this.that.groups.forEach((element, i) => {
            if (element.name === e && (this.that.groups.indexOf(element) === i)) {
              this.that.learners.push(...element.learners)
              // console.log('learners:', this.that.learners);
              this.that.groupIds.push(element.id);
              let interval = setInterval(() => {
                if (i === this.that.groups.length - 1) {
                  // console.log('tokens For:', this.that.learners);

                  clearInterval(interval);
                }
              }, 500);
            }
          });
        }
      });

      this.that.getTokens(this.that.learners);
    }
  }




  notificationSelection: any = {
    yes: true,
    no: false,
    yesClicked: function () {
      this.yes = true;
      this.no = false;
      this.step3Visible = true;
    },
    noClicked: function () {
      this.no = true;
      this.yes = false;
      this.step3Visible = false;
    },
    step3Visible: true,
  }

  scheduleSelection: any = {
    input: HTMLInputElement,
    min: new Date().toISOString().slice(0, 16).toString(),
    max: new Date(new Date().getTime() + 86400000 * 90).toISOString().slice(0, 16).toString(),
    now: true,
    later: false,
    once: true,
    repeat: false,
    laterScheduleVisible: false,
    nowClicked: function () {
      this.later = false;
      this.now = true;
      this.laterScheduleVisible = false;
    },
    laterClicked: function () {
      this.later = true;
      this.now = false;
      this.laterScheduleVisible = true;
      // console.log("ELEMRNT:", document.getElementById("date-field"), "2018-05-05T16:15");
      this.input = <HTMLInputElement>document.getElementById('date-field');
      this.input.min = this.min;
      this.input.max = this.max;
      // console.log('min:', this.min);
      // console.log('max:', this.max);
      // document.getElementById("date-field").max = this.max;
      // console.log("ELEMRNT:", document.getElementById("date-field"), "2018-05-05T16:15");
    },
    onceClicked: function () {
      this.once = true;
      this.repeat = false;
      this.repeatScheduleVisible = false;
    },
    repeatClicked: function () {
      this.once = false;
      this.repeat = true;
      this.repeatScheduleVisible = true;
    },
    repeatScheduleVisible: false,
    ExpiryPopupVisible: false,
    selectedExpiry: null,
    selectedExpiryTime: null
  }

  selectExpiry(type) {
    this.scheduleSelection.selectedExpiry = type;

    switch (type) {
      case '12 Hours':
        this.scheduleSelection.selectedExpiryTime = 12 * 3600 * 1000;
        break;
      case '1 Day':
        this.scheduleSelection.selectedExpiryTime = 24 * 3600 * 1000;
        break;
      case '3 Days':
        this.scheduleSelection.selectedExpiryTime = 3 * 24 * 3600 * 1000;
        break;
      case '1 Week':
        this.scheduleSelection.selectedExpiryTime = 7 * 24 * 3600 * 1000;
        break;
      case '2 Weeks':
        this.scheduleSelection.selectedExpiryTime = 14 * 24 * 3600 * 1000;
        break;
      case '1 Month':
        this.scheduleSelection.selectedExpiryTime = 30 * 24 * 3600 * 1000;
        break;
      case '3 Months':
        this.scheduleSelection.selectedExpiryTime = 90 * 24 * 3600 * 1000;
        break;
      case '6 Months':
        this.scheduleSelection.selectedExpiryTime = 180 * 24 * 3600 * 1000;
        break;
      case '1 Year':
        this.scheduleSelection.selectedExpiryTime = 365 * 24 * 3600 * 1000;
        break;

      default:
        this.scheduleSelection.selectedExpiryTime = 0;
        break;
    }
    // this.scheduleSelection.ExpiryPopupVisible = false;

  }


  constructor(private previewService: PreviewService, private groupService: CampaignGroupResourceService,
    private deviceService: DeviceResourceService, private http: HttpClient, private router: Router,
    private roleGuardService: RoleGuardService, private contentService: PublishedContentResourceService) {

    if (!localStorage.getItem('token')) {
      this.router.navigateByUrl('/login');
    }
  }
  ngOnInit() {
    this.obj = this.previewService.contentToSchedule.getValue();
    if (!this.obj.id) {
      this.router.navigate(['/content/list']);
    }
    this.http.get(base + '/api/app-configs/org/' + this.previewService.organizationId.getValue(), this.httpOptions).subscribe(res => {
      let data = <any>res;
      data.forEach(element => {
        if (element.category = 'DASHBOARD' && element.value === this.obj.category) {
          this.screen = element.name;
        }
      });

    });
    this.groupService.getAllCampaignGroupsUsingGET().subscribe(res => {
      let result = <any>res;
      result = result.filter(e => {
        if (Number(e.orgId) === Number(this.previewService.organizationId.getValue())) {
          return true;
        }
      })
      // console.log('groups:', result);
      this.groups = [...result]
    })
  }

  ngOnDestroy() {
    this.previewService.editContentToSchedule({});
  }

  closeDropdown() {
    if (this.groupSelection.searchVisible) {

      this.groupSelection.searchVisible = false;
    }
  }


  validationOnTitle() {
    let elm = <HTMLInputElement>document.getElementById('title-field');
    if (elm.value.length < 1 || elm.value.length > 100) {
      swal('', 'Please enter atleast 1 character and atmost 100 characters.');
      // now focus it

    }
  }

  validationOnMessage() {
    let elm = <HTMLInputElement>document.getElementById('message-field');
    if (elm.value.length < 1 || elm.value.length > 150) {
      swal('', 'Please enter atleast 1 character and atmost 150 characters.');
      // now focus it
    }
  }

  getTokens(learners) {
    this.deviceService.getAllDevicesUsingGET().subscribe(res => {
      let result: any = res;
      // console.log('devices:', result);
      for (let i = 0; i < learners.length; i++) {
        for (let j = 0; j < result.length; j++) {
          if (learners[i]['registeredNumber'] === result[j]['phoneNumber']) {
            this.tokens.push(result[j]['fcmToken']);
            this.learnerIds.push(learners[i].id);
            // console.log('token+$i: ', result[j]['phoneNumber'], result[j]['fcmToken']);
          }
        }
      }
      return this.tokens;
    });
  }


  sendNotifications() {


    if (this.scheduleSelection.later) {
      let dateObject = new Date(this.scheduleDate);
      // console.log('date:', dateObject);
      this.notificationCronExpression = "0" + " " + dateObject.getMinutes() + " " + dateObject.getHours() + " " + dateObject.getDate() + " " + Number(dateObject.getMonth() + 1) + " * " + dateObject.getFullYear();
      // console.log('dateExpress:', this.notificationCronExpression);
    }
    if (this.scheduleSelection.now) {
      this.notificationCronExpression = null;
    }
    if (this.groupSelection.everyGroup) {

      this.http.post(proxyApi + '/contentUpdate', {
        'topic': this.previewService.organizationId.getValue(),

        'data': {
          'silent': false,
          'save': true,
          'body': this.messageContent,
          'campaignId': 0,
          'ctaURL': 'string',
          'description': 'Info',
          'image': null,
          'lifeCycleId': 257501,
          'microContentId': 1253,
          'name': this.title,
          'scheduled': this.scheduleSelection.repeat ? this.cronExpression : this.notificationCronExpression,
          'screenType': this.screen,
          'sequence': null,
          'sound': new Date().getTime(),
          'specialParameters': this.groupsSelected.length > 0 ? this.groupIds.join() : null,
          'tags': 'Notify',
          'template': 'string',
          'title': this.obj.category,
          'orgId': this.previewService.organizationId.getValue(),
          'groupId': null,
          'userId': null

        }

      }, this.httpOptions).subscribe(res => {
        // console.log('sent notification to all', res);
        swal('Successfully Sent Notification.', '', 'success');
        this.router.navigate(['/notification/list'])

      })
    } else {

      this.sendNotification({
        'notificationName': this.title, 'cronExpression': this.scheduleSelection.repeat ? this.cronExpression : this.notificationCronExpression,
        'notificationText': this.messageContent, 'thumbnailImage': this.thumbnailImage, 'tags': 'Notify', 'tokens': this.tokens,
        specialParameters: this.groupIds.join() || null, title: this.obj.category, screenType: this.screen, orgId: this.previewService.organizationId.getValue(),
        cgroupId: this.groupIds.join() || null, userId: null, sgroupId: null
      });
    }
  };

  sendNotification({ notificationName, cronExpression, notificationText, thumbnailImage, tags, tokens, specialParameters, title, screenType, orgId, sgroupId, cgroupId, userId }) {

    let url = '';


    // console.log('notification:', JSON.parse(JSON.stringify({

    //   'notificationDTO': {
    //     'body': notificationText,
    //     'campaignId': 0,
    //     'ctaURL': 'string',
    //     'description': 'Info',
    //     'image': thumbnailImage,
    //     'lifeCycleId': 0,
    //     'microContentId': 0,
    //     'name': notificationName,
    //     'scheduled': cronExpression,
    //     'screenType': screenType,
    //     'sequence': this.previewService.organizationId.getValue(),
    //     'sound': new Date().getTime(),
    //     'specialParameters': specialParameters,
    //     'tags': tags,
    //     'template': null,
    //     'title': title,
    //   }, 'deviceID': tokens.join()
    // })));

    if (this.scheduleSelection.repeat && this.cronExpression) {
      url = base + '/api/notifications/schedule/send?deviceID='

    }
    else url = base + '/api/notifications/send?deviceID=';


    do {
      let tok = this.tokens.splice(0, 10);
      this.http.post(url + String(tok.join()), JSON.parse(JSON.stringify({


        'body': notificationText,
        'campaignId': 0,
        'ctaURL': 'string',
        'description': 'Info',
        'image': thumbnailImage,
        'lifeCycleId': 257501,
        'microContentId': 1253,
        'name': notificationName,
        'scheduled': cronExpression,
        'screenType': screenType,
        'sequence': sgroupId,
        'sound': new Date().getTime(),
        'specialParameters': specialParameters,
        'tags': tags,
        'template': null,
        'title': notificationName,
        'orgId': orgId,
        'groupId': cgroupId,
        'userId': userId

      })), this.httpOptions).subscribe(response => {
        // console.log('response subscribe:', response);
      });
    } while ((this.tokens.length / 10) > 0 || this.tokens.length);


    this.http.post(base + '/api/notifications', {
      'body': notificationText,
      'campaignId': 82551,
      'ctaURL': 'string',
      'description': 'Info',
      'image': thumbnailImage,
      'lifeCycleId': 257501,
      'microContentId': 1253,
      'name': notificationName,
      'scheduled': cronExpression,
      'screenType': screenType,
      'sequence': sgroupId,
      'sound': new Date().getTime(),
      'specialParameters': specialParameters,
      'tags': tags,
      'template': null,
      'title': title,
      'orgId': orgId,
      'groupId': cgroupId,
      'userId': userId
    }, this.httpOptions)
      .subscribe(res => {
        // console.log('notification:', res)
        swal('Successfully Sent Notification.', '', 'success');

      });

    //this.notificationSuccessfullySent(this.pushNotificationName || this.popNotificationText, result.name ? true : false);
    //this.resetFields();



  }

  publishContent() {

    if (!this.validateContentForm()) {
      return;
    }
    // console.log('Cron:', this.obj);

    this.obj.publishedDate = this.scheduleDate ? new Date(this.scheduleDate) : new Date();
    this.obj.state = 'PUBLISHED';
    this.obj.postDate = new Date(this.obj.postDate);
    this.obj.scheduled = this.cronExpression;
    this.obj.expiry = this.scheduleSelection.selectedExpiryTime === 'Never' ? null : new Date(new Date(this.obj.publishedDate).getTime() + this.scheduleSelection.selectedExpiryTime);
    this.obj.expiryDate = this.scheduleSelection.selectedExpiryTime === 'Never' ? null : new Date(new Date(this.obj.publishedDate).getTime() + this.scheduleSelection.selectedExpiryTime);

    swal({
      title: 'Are you sure you want to schedule this content?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Yes, schedule it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success btn-raised mr-5',
      cancelButtonClass: 'btn btn-danger btn-raised',
      buttonsStyling: false
    }).then((confirm) => {
      if (confirm.value) {
        if (this.obj.contentType) {
          this.contentService.updatePublishedContentUsingPUT(this.obj).subscribe(res => {
            let result = <any>res;
            swal(
              'Your Content was Scheduled Successfully.',
              '',
              'success'
            )
            // console.log('scored it', result);
            if (!result.name) {
              // console.log('err', result);
            }
            else {
              if (this.notificationSelection.yes) {
                this.sendNotifications();
              }

            }
          }, error => {
            swal(
              'Error',
              'Your Content could not be scheduled.',
              'error'
            )
          })
        }
        if (!this.obj.type && !this.obj.contentType) {
          this.http.put(base + '/api/micro-contents', this.obj, this.httpOptions).subscribe(res => {
            let result = <any>res;
            // console.log('scored it', result);
            swal(
              'Your Content was Scheduled Successfully.',
              '',
              'success'
            )
            if (!result.name) {
              // console.log('err', result);
            }
            else {
              if (this.notificationSelection.yes) {
                this.sendNotifications();
              }
            }
          }, error => {
            swal(
              'Error',
              'Your Content could not be scheduled.',
              'error'
            )
          })
        }
      }
      else {
        swal(
          'Cancelled',
          'Your Content was not scheduled.',
          'error'
        )
      }

    });
  }


  validateContentForm() {
    // console.log('Enter')
    let isValid = true;
    let contentNameInput, contentTypeInput, contentDescriptionInput, categoryInput, subCategoryInput, imageInput, typeInput, negativeInput;

    if (this.groupSelection.particularGroup && !this.groupsSelected.length) {
      // console.log('Group', this.groupSelection.particularGroup);
      contentDescriptionInput = document.getElementById('groupList');
      contentDescriptionInput.classList.add('error');
      isValid = false;
      return isValid;
    } else if (this.groupSelection.particularGroup && this.groupsSelected.length) {
      // console.log('Group', this.groupSelection.particularGroup);
      contentDescriptionInput = document.getElementById('groupList');
      contentDescriptionInput.classList.remove('error');
      isValid = true;
    }
    if (this.notificationSelection.step3Visible) {
      if (!this.title || this.title.trim() === "") {
        // console.log('Title', this.title);
        typeInput = document.getElementById('title');
        typeInput.classList.add('error');
        isValid = false;
        return isValid;
      } else {
        // console.log('Title', this.title);
        typeInput = document.getElementById('title');
        typeInput.classList.remove('error');
        isValid = true;
      }

      if (!this.messageContent || this.messageContent.trim() === "") {
        contentNameInput = document.getElementById('messageContent');
        contentNameInput.classList.add('error');
        isValid = false;
        return isValid;
      } else {
        contentNameInput = document.getElementById('messageContent');
        contentNameInput.classList.remove('error');
        isValid = true;
      }

    }



    if (this.scheduleSelection.later && !this.scheduleDate) {
      negativeInput = document.getElementById('date-field');
      negativeInput.classList.add('error');
      isValid = false;
      return isValid;
    } else if (this.scheduleSelection.later && this.scheduleDate) {
      negativeInput = document.getElementById('date-field');
      negativeInput.classList.remove('error');
      isValid = true;
    }

    if (this.scheduleSelection.repeat && !this.cronExpression) {
      categoryInput = document.getElementById('cron');
      categoryInput.classList.add('error-text');
      isValid = false;
      return isValid;
    } else if (this.scheduleSelection.repeat && this.cronExpression) {
      categoryInput = document.getElementById('cron');
      categoryInput.classList.remove('error-text');
      isValid = true;
    }

    if (!this.scheduleSelection.selectedExpiry) {
      subCategoryInput = document.getElementById('expiry');
      subCategoryInput.classList.add('error');
      isValid = false;
      return isValid;
    } else {
      subCategoryInput = document.getElementById('expiry');
      subCategoryInput.classList.remove('error');
      isValid = true;
    }

    return isValid;
  }


}
